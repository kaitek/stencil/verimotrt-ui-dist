'use strict';

const index = require('./index-597ed9a1.js');
const appGlobals = require('./app-globals-fd59c03d.js');

index.patchBrowser().then(options => {
  appGlobals.globalScripts();
  return index.bootstrapLazy([["app-root_2.cjs",[[1,"app-root",{"splashDestroy":[64],"splashShow":[64]}],[1,"screen-splash",{"destroy":[64]}]]]], options);
});
