'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-597ed9a1.js');
const appGlobals = require('./app-globals-fd59c03d.js');

const defineCustomElements = (win, options) => {
  if (typeof window !== 'undefined') {
    return index.patchEsm().then(() => {
      appGlobals.globalScripts();
      index.bootstrapLazy([["app-root_2.cjs",[[1,"app-root",{"splashDestroy":[64],"splashShow":[64]}],[1,"screen-splash",{"destroy":[64]}]]]], options);
    });
  }
  return Promise.resolve();
};

exports.defineCustomElements = defineCustomElements;
