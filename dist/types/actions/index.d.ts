import { AppSetNameAction } from './app';
export interface NullAction {
    type: TypeKeys.NULL;
}
export declare type ActionTypes = NullAction | AppSetNameAction;
export declare enum TypeKeys {
    NULL = "NULL",
    ERROR = "ERROR",
    APP_SET_NAME = "APP_SET_NAME"
}
