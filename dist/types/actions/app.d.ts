import { TypeKeys } from '../actions/index';
export interface AppSetNameAction {
    type: TypeKeys.APP_SET_NAME;
    name: string;
}
export declare const appSetName: (name: string) => (dispatch: any, _getState: any) => Promise<any>;
