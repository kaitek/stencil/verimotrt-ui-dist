/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */
import { HTMLStencilElement, JSXBase } from "./stencil-public-runtime";
import { ScreenSplash, } from "./components/screen-splash/screen-splash";
export namespace Components {
    interface AppRoot {
        "splashDestroy": () => Promise<void>;
        "splashShow": () => Promise<void>;
    }
    interface ScreenSplash {
        "destroy": () => Promise<void>;
    }
}
declare global {
    interface HTMLAppRootElement extends Components.AppRoot, HTMLStencilElement {
    }
    var HTMLAppRootElement: {
        prototype: HTMLAppRootElement;
        new (): HTMLAppRootElement;
    };
    interface HTMLScreenSplashElement extends Components.ScreenSplash, HTMLStencilElement {
    }
    var HTMLScreenSplashElement: {
        prototype: HTMLScreenSplashElement;
        new (): HTMLScreenSplashElement;
    };
    interface HTMLElementTagNameMap {
        "app-root": HTMLAppRootElement;
        "screen-splash": HTMLScreenSplashElement;
    }
}
declare namespace LocalJSX {
    interface AppRoot {
    }
    interface ScreenSplash {
    }
    interface IntrinsicElements {
        "app-root": AppRoot;
        "screen-splash": ScreenSplash;
    }
}
export { LocalJSX as JSX };
declare module "@stencil/core" {
    export namespace JSX {
        interface IntrinsicElements {
            "app-root": LocalJSX.AppRoot & JSXBase.HTMLAttributes<HTMLAppRootElement>;
            "screen-splash": LocalJSX.ScreenSplash & JSXBase.HTMLAttributes<HTMLScreenSplashElement>;
        }
    }
}
