export declare class ScreenSplash {
    myElement: HTMLElement;
    destroy(): Promise<void>;
    render(): any;
}
