import "@stencil/redux";
import { Store } from "@stencil/redux";
export declare class AppRoot {
    myElement: HTMLElement;
    store: Store;
    private screenSplash;
    componentWillLoad(): void;
    componentDidLoad(): void;
    splashDestroy(): Promise<void>;
    splashShow(): Promise<void>;
    render(): any;
}
