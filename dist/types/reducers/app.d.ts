import { ActionTypes } from '../actions/index';
interface AppState {
    name: string;
}
declare const app: (state: AppState, action: ActionTypes) => AppState;
export default app;
