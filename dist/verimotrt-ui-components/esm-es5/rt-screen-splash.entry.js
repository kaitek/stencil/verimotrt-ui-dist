import { r as registerInstance, h, g as getElement } from './index-2b115e74.js';
var RtScreenSplash = /** @class */ (function () {
    function RtScreenSplash(hostRef) {
        registerInstance(this, hostRef);
    }
    RtScreenSplash.prototype.render = function () {
        var _this = this;
        return (h("div", { class: "flex flex--col flex--center flex--stretch" }, h("div", null, h("px-logo", { cls: "v1 m" })), h("div", null, h("px-steps", { ref: function (el) { return _this.stepper = el; }, items: [
                { "id": "db_start", "label": "DB Başlatma" },
                { "id": "2", "label": "Select Services" },
                { "id": "3", "label": "Billing" },
                { "id": "4", "label": "Review" },
                { "id": "5", "label": "Deploy" }
            ], currentStep: 1 }))));
    };
    Object.defineProperty(RtScreenSplash.prototype, "myElement", {
        get: function () { return getElement(this); },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RtScreenSplash, "style", {
        get: function () { return ""; },
        enumerable: true,
        configurable: true
    });
    return RtScreenSplash;
}());
export { RtScreenSplash as rt_screen_splash };
