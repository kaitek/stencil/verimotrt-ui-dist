import { r as registerInstance, h, g as getElement } from './index-2b115e74.js';

const RtScreenSplash = class {
    constructor(hostRef) {
        registerInstance(this, hostRef);
    }
    render() {
        return (h("div", { class: "flex flex--col flex--center flex--stretch" }, h("div", null, h("px-logo", { cls: "v1 m" })), h("div", null, h("px-steps", { ref: (el) => this.stepper = el, items: [
                { "id": "db_start", "label": "DB Başlatma" },
                { "id": "2", "label": "Select Services" },
                { "id": "3", "label": "Billing" },
                { "id": "4", "label": "Review" },
                { "id": "5", "label": "Deploy" }
            ], currentStep: 1 }))));
    }
    get myElement() { return getElement(this); }
    static get style() { return ""; }
};

export { RtScreenSplash as rt_screen_splash };
