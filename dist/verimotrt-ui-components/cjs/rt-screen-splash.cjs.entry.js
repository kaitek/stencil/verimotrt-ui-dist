'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const index = require('./index-168459e6.js');

const RtScreenSplash = class {
    constructor(hostRef) {
        index.registerInstance(this, hostRef);
    }
    render() {
        return (index.h("div", { class: "flex flex--col flex--center flex--stretch" }, index.h("div", null, index.h("px-logo", { cls: "v1 m" })), index.h("div", null, index.h("px-steps", { ref: (el) => this.stepper = el, items: [
                { "id": "db_start", "label": "DB Başlatma" },
                { "id": "2", "label": "Select Services" },
                { "id": "3", "label": "Billing" },
                { "id": "4", "label": "Review" },
                { "id": "5", "label": "Deploy" }
            ], currentStep: 1 }))));
    }
    get myElement() { return index.getElement(this); }
    static get style() { return ""; }
};

exports.rt_screen_splash = RtScreenSplash;
