export declare class AppGrid {
    elContainer: HTMLElement;
    usersSmall: object[];
    componentDidLoad(): void;
    handleTableAction(e: any): void;
    render(): any;
}
