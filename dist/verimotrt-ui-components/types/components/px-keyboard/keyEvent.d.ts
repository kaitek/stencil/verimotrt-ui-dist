declare class keyEvent {
    keyCode: number;
    charCode: number;
    modifiers: string[];
    altKey: boolean;
    ctrlKey: boolean;
    metaKey: boolean;
    shiftKey: boolean;
    type: string;
    constructor(data: any, type: string);
    toNative(): any;
    fire(element: any): void;
    static simulate(charCode: any, keyCode: any, modifiers: any, element: any, repeat?: any): void;
}
export { keyEvent };
