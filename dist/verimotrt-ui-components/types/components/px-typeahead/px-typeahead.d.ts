import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxTypeahead {
    elContainer: HTMLDivElement;
    elSearchBox: HTMLInputElement;
    elSearchResults: HTMLUListElement;
    disabled: boolean;
    placeholder: string;
    localCandidates: string[];
    _suggestions: string[];
    maxSuggestions: number;
    value: string;
    inputValue: string;
    _isFocused: boolean;
    required: boolean;
    pxTypeaheadSearchInputChange: EventEmitter;
    pxTypeaheadItemSelected: EventEmitter;
    pxTypeaheadItemDeselected: EventEmitter;
    toggleClass(name: string, bool: boolean, node: Element): void;
    _computeHasInputAndNotDisabled(): boolean;
    _setSearchResultSize(): void;
    _isSuggestionsEmpty(): number;
    _keydown(e: KeyboardEvent): void;
    _onhover(e: any): void;
    _onfocus(): void;
    _onblur(): void;
    _onSearchInputChange(e: any): void;
    _select(e: any): void;
    _downArrowPress(): void;
    _upArrowPress(): void;
    _enterPress(): boolean;
    _backspacePress(e: any): void;
    _checkIfSingleResult(): void;
    _localCandidatesSearch(term: any): any[];
    /**
     * Highlights matching letters in bold.
     */
    _boldResults(items: any, term: any): any;
    _search(term: any): void;
    _clear(): void;
    _disabledClass(disabled: any): "" | "disableIcon";
    _requiredChanged(newValue: any): void;
    getTemplateSearchIcon(): any;
    getTemplateCloseIcon(): any;
    getTemplateResults(): any;
    getTemplateSuggestionsEmpty(): any;
    render(): any[];
}
