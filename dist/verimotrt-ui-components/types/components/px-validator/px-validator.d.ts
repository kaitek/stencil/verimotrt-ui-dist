import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxValidator {
    validators: any[];
    multiStepValidation: string[];
    validationMethod: string;
    arguments: any[];
    pxValidatorLoaded: EventEmitter;
    _validationMethodChanged(): void;
    errorObject(): {
        passedValidation: boolean;
        failedValidationFunctionName: string;
        failedValidationErrorMessage: string;
    };
    isNumber(value: any): {
        passedValidation: boolean;
        failedValidationFunctionName: string;
        failedValidationErrorMessage: string;
    };
    alwaysFails(): {
        passedValidation: boolean;
        failedValidationFunctionName: string;
        failedValidationErrorMessage: string;
    };
    isBetween(value: number, min: number, max: number): {
        passedValidation: boolean;
        failedValidationFunctionName: string;
        failedValidationErrorMessage: string;
    };
    componentDidLoad(): void;
    render(): any;
}
