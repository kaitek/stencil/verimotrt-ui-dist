import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxValidation {
    elements: NodeListOf<HTMLPxValidatorElement>;
    private element;
    val: string;
    pxValidatonStateChange: EventEmitter;
    validate(value: any): {
        passedValidation: boolean;
    };
    componentDidLoad(): void;
    render(): any;
}
