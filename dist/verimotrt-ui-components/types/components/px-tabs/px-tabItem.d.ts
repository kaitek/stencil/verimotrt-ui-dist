import { ITabItem } from "./px-tabItem-interface";
export declare class TabItem {
    id: string;
    title: string;
    constructor(obj: ITabItem);
}
