export interface ITabItem {
    id: string;
    title: string;
}
