import { EventEmitter } from '../../stencil-public-runtime';
import { IronPagesElement } from '@polymer/iron-pages';
import { TabItem } from './px-tabItem';
export declare class PxTabs {
    myElement: HTMLElement;
    elDivContainer: HTMLElement;
    elPages: IronPagesElement;
    selected: string;
    items: TabItem[];
    pxTabChanged: EventEmitter;
    _selectedWatcher(): void;
    componentDidLoad(): void;
    _click(key: number): void;
    getTemplateTabItems(): any[];
    render(): any;
}
