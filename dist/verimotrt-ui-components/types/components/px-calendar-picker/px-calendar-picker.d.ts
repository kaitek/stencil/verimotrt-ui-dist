import { EventEmitter } from '../../stencil-public-runtime';
import 'moment/dist/locale/tr';
import { Moment } from 'moment-timezone';
export declare class PxCalendarPicker {
    baseDate: Moment;
    timeZone: string;
    displayMode: 'time' | 'day' | 'month' | 'year';
    _currentDisplayMode: 'time' | 'day' | 'month' | 'year';
    _oldBaseDate: Moment;
    preventRangeSelection: boolean;
    hidePreviousButton: boolean;
    hideNextButton: boolean;
    shrink: boolean;
    isAtSelectionLevel: boolean;
    _valuesDisplayed: number[];
    waitToFire: boolean;
    _title: string;
    fromMoment: Moment;
    toMoment: Moment;
    blockFutureDates: boolean;
    blockPastDates: boolean;
    maxDate: string;
    minDate: string;
    daysOfTheWeek: string[];
    pxDateSelected: EventEmitter;
    pxCalendarRangeSelected: EventEmitter;
    pxCalendarSelected: EventEmitter;
    componentWillLoad(): void;
    componentDidLoad(): void;
    selectedListener(e: any): void;
    getToday(): Promise<void>;
    _getTableClass(shrink: any): string;
    _cellClicked(e: any): void;
    _displayModeChanged(): void;
    _currentDisplayModeChanged(): void;
    _isDayMode(_currentDisplayMode: any): boolean;
    /**
     * Change the display mode after the user clicked the title
     */
    _onNextDisplayMode(): void;
    _getCurrentTitle(_currentDisplayMode: any, baseDate: any): void;
    _getDaysOfTheWeek(): any[];
    _renderCurrent(): void;
    /**
    * This event is fired when the user selects a date range in the calendar & 'preventRangeSelection' is false
    *
    * @event px-calendar-range-selected
    * @param {object} range - {from, to} : the ISO 8601 Strings representing the date/time range selected by the user
    * @param {object} momentObjs - {from, to} : the moment.js objects representing the date/time range selected by the user
    */
    /**
    * This event is fired when the user selects a date in the calendar & 'preventRangeSelection' is true
    *
    * @event px-calendar-selected
    * @param {object} momentObj : the moment.js object representing the date/time selected by the user
    */
    _selectDate(newSelectedDate: any): void;
    _constructYear(): any[];
    _constructYearRange(): any[];
    _constructMonth(): any[];
    /**
     * Called when clicking the 'next' arrow
     */
    _onNext(): void;
    /**
     * Called when clicking the 'previous' arrow
     */
    _onPrevious(): void;
    _isAtSelectionLevel(currentDisplayMode: any, displayMode: any): boolean;
    _baseDateChanged(newDate: any): void;
    /**
     * Copies the time in toPreserve to dest and returns dest.
     * used in `px-calendar-picker`, `px-datetime-picker`, and `px-datetime-range-panel`
     *
     * @param {} toPreserve
     * @param {} dest
     */
    _preserveTime(toPreserve: Moment, dest: Moment): any;
    getTemplateDayNames(): any[];
    getTemplateWeekRow(): any[];
    getTemplateDayCell(week: any): any;
    render(): any;
}
