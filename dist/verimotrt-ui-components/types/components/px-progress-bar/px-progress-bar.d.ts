export declare class PxProgressBar {
    myElement: HTMLElement;
    value: number;
    infinite: boolean;
    min: number;
    max: number;
    _ratio: number;
    getValue(): Promise<number>;
    setValue(value: number): Promise<void>;
    componentDidLoad(): void;
    _setMinAriaLabel(newMin: any, oldMin: any): void;
    _setMaxAriaLabel(newMax: any, oldMax: any): void;
    _changeHandler(): void;
    __calculate(): void;
    /**
     * Function to transform the filled portion of the progress bar.
     */
    _updateProgress(newRatio: any, oldRatio: any): void;
    /**
     * Function to compute the ratio based on the value.
     * Also clips values that are out of range (0 to 100).
     */
    _computeRatio(value: any, min: any, max: any): number;
    render(): any;
}
