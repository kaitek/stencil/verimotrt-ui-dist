export declare class PxAlertLabel {
    type: string;
    label: string;
    badge: boolean;
    _getPoints(type: string): "16.5,3 32,30 1,30" | "16,0.5 32.5,16 16,32.5, 0.5,16" | "6.6,32.5 26.4,32.5 32.5,13 16,0.5 0.5,13" | "3,3 3,30 30,30 30,3";
    _isCircle(type: string): boolean;
    getTemplate(): any;
    render(): any;
}
