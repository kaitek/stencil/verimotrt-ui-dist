export declare class PxCard {
    headerText: string;
    icon: string;
    fullBleed: boolean;
    hideHeader: boolean;
    getTamplateIcon(): any;
    getTamplateHeader(): any;
    render(): any;
}
