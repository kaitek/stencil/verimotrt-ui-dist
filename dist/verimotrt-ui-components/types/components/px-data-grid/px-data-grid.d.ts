import { EventEmitter } from '../../stencil-public-runtime';
import { GridElement } from '@vaadin/vaadin-grid';
import '@vaadin/vaadin-grid/all-imports.js';
export declare class PxDataGrid {
    myElement: HTMLElement;
    pxTableSelect: EventEmitter;
    pxTableDeselect: EventEmitter;
    _lastColumnsReceived: object[];
    _selectedActionItems: object[];
    columns: object[];
    expandedItems: object[];
    itemActions: object[];
    selectedItems: object[];
    tableData: object[];
    _groupByColumn: any;
    activeItem: object;
    defaultColumnFlexGrow: number;
    loadingSpinnerDebounce: number;
    pageSize: number;
    size: number;
    _expandableRows: boolean;
    _hasLocalDataProvider: boolean;
    _loading: boolean;
    _spinnerHidden: boolean;
    allowSortBySelection: boolean;
    editable: boolean;
    flexToSize: boolean;
    hideActionMenu: boolean;
    hideColumnFilter: boolean;
    hideSelectionColumn: boolean;
    instantSortWhenSelection: boolean;
    multiSort: boolean;
    resizable: boolean;
    selectable: boolean;
    sortable: boolean;
    striped: boolean;
    useKeyIfMissing: boolean;
    _autoFilterValue: string;
    _columnSelectorLabel: string;
    defaultColumnWidth: string;
    gridHeight: string;
    itemIdPath: string;
    selectionMode: 'none' | 'single' | 'multi';
    _columnIdSetting: boolean;
    _filters: any;
    _previousSorters: any;
    _spinnerHiddenTimeout: any;
    _vaadinGrid: GridElement;
    item: any;
    componentWillLoad(): void;
    componentDidLoad(): void;
    disconnectedCallback(): void;
    _checkColumnResizable(gridResizable: any, columnResizable: any): any;
    _computeColumnSelectorLabel(selectedItems: any): string;
    _doInstantSortAfterSelect(): boolean;
    _getColumnByPath(columnPath: any): object;
    _getColumnElementById(columnId: any): Element;
    _getColumnFlexGrow(column: any): any;
    _getColumns(): Element[];
    _getColumnWidth(column: any): any;
    _getColumnsWithName(name: any): Element[];
    /**
     * Returns a value that identifies the item. Uses `itemIdPath` if available.
     * Can be customized by overriding.
     */
    getItemId(item: any): any;
    _getItemIndexInArray(item: any, array: any): number;
    _getValue(column: any, item: any): any;
    _gridGetItemId(item: any): any;
    _groupByColumnAllowed(hasLocalDataProvider: any, expandableRows: any): boolean;
    _handledeSelectItem(item: any): void;
    _handleSelectAllItems(e: any): void;
    _handleSelectItem(item: any): void;
    _isAutoHeight(gridHeight: any): boolean;
    _isMultiSelect(): boolean;
    _isSelectable(): boolean;
    _isStriped(striped: any, groupByColumn: any): boolean;
    _itemsEqual(item1: any, item2: any): boolean;
    _offerActionColumn(editable: any, itemActions: any): any;
    _offerEditColumn(editable: any): any;
    _resolveColumnPath(column: any): any;
    _selectedItemsChanged(selectedItems: any): void;
    _undefinedToFalse(value: any): any;
    get(path: any, item: any): any;
    /**
     * Returns currently visible data if called with `true`.
     * Returns all cached data if called with `undefined`.
     *
     * @param {?boolean} visibleOnly
     * @return Array
     */
    getData(visibleOnly: any): {}[];
    getVisibleColumns(): Element[];
    resolveColumnHeader(column: any): any;
    getTemplateSelectable(): any;
    getTemplateColumnHeader(column: any): any;
    getTemplateColumns(): any[];
    getTemplateOfferEditColumn(): void;
    getTemplateOfferActionColumn(): void;
    render(): any[];
}
