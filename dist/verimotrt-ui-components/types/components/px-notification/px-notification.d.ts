import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxNotification {
    pxNotificationActionTapped: EventEmitter;
    statusIcon: string;
    content: string;
    actionIcon: string;
    type: 'important' | 'warning' | 'error' | 'info' | 'healthy' | 'unknown';
    small: boolean;
    opened: boolean;
    _hideIfEmpty(icon: any): string;
    _handleTap(): void;
    render(): any[];
}
