import { IThresholdConfig } from "./px-thresholdConfig-interface";
export declare class ThresholdConfig {
    min: number;
    max: number;
    color: string;
    constructor(obj: IThresholdConfig);
}
