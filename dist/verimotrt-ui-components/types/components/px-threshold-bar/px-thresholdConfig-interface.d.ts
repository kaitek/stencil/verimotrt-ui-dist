export interface IThresholdConfig {
    min: number;
    max: number;
    color: string;
}
