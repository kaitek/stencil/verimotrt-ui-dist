import { ThresholdConfig } from './px-thresholdConfig';
export declare class PxThresholdBar {
    myElement: HTMLElement;
    min: number;
    max: number;
    hideValue: boolean;
    hideScale: boolean;
    config: ThresholdConfig[];
    value: number;
    uom: string;
    _scaleFactor: number;
    elDivContainer: HTMLElement;
    _refreshStyle: boolean;
    getValue(): Promise<number>;
    setValue(value: number): Promise<void>;
    _configDataChanged(config: ThresholdConfig[]): void;
    _scaleDataChanged(): void;
    componentDidLoad(): void;
    _calculateLeftOffset(): number;
    _calculatePaddingWidth(): number;
    _checkValuesSet(includeValue: any): boolean;
    _getMarkerLineStyle(): {
        left: string;
        display?: undefined;
    } | {
        display: string;
        left?: undefined;
    };
    _getMarkerStyle(): {
        left: string;
        display?: undefined;
    } | {
        display: string;
        left?: undefined;
    };
    _getStyles(config: ThresholdConfig): {
        width: string;
        'background-color': string;
    };
    _getValueStyle(): {
        left: string;
        display?: undefined;
    } | {
        display: string;
        left?: undefined;
    };
    getPreItem(i: number, tConfig: ThresholdConfig): any;
    getTemplateConfigItems(): any[][];
    render(): any;
}
