export declare class PxSpinner {
    finished: boolean;
    size: number;
    render(): any;
}
