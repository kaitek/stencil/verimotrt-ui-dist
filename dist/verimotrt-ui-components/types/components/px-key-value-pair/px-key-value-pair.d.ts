export declare class PxKeyValuePair {
    label: string;
    value: string;
    uom: string;
    size: 'alpha' | 'beta' | 'gamma' | 'delta' | 'epsilon' | 'regular';
    _getAdjustedSize(size: any): "value" | "epsilon" | "zeta" | "delta";
    getTemplateUom(): any;
    render(): any[];
}
