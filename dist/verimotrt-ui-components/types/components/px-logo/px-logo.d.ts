export declare class PxLogo {
    myElement: HTMLElement;
    type: string;
    cls: string;
    componentWillLoad(): void;
    componentDidLoad(): void;
    getTemplateLogo(_logo: string): any;
    render(): any;
}
