import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxButton {
    text: string;
    size: 'small' | 'large' | 'huge' | 'full';
    type: 'primary' | 'tertiary' | 'call-to-action' | 'bare' | 'bare-primary';
    disabled: boolean;
    pxButtonClick: EventEmitter;
    handleClick(event: MouseEvent): void;
    render(): any;
}
