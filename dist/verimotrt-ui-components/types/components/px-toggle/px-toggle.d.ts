import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxToggle {
    size: 'regular' | 'small' | 'large' | 'huge';
    disabled: boolean;
    checked: boolean;
    pxToggleCheckedChanged: EventEmitter;
    clickHandler(): void;
    render(): any[];
}
