import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxIcon {
    icon: string;
    classPrefix: string;
    size: 'small' | 'middle' | 'large' | 'huge';
    pxstyle: {
        [key: string]: string;
    };
    disabled: boolean;
    pxIconTapped: EventEmitter;
    _handleTapped(evt: Event): void;
    render(): any;
}
