import { EventEmitter } from '../../stencil-public-runtime';
import { IronPagesElement } from '@polymer/iron-pages';
export declare class PxViewHeader {
    myElement: HTMLElement;
    mainTitle: string;
    subTitle: string;
    isPrevVisible: boolean;
    isNextVisible: boolean;
    selected: string;
    items: {
        [key: string]: string;
    }[];
    elPages: IronPagesElement;
    elDivContainer: HTMLElement;
    pxViewHeaderChanged: EventEmitter;
    buttonStateChange(btn: 'prev' | 'next', state: 'enable' | 'disable'): Promise<void>;
    _selectedWatcher(): void;
    componentWillLoad(): void;
    componentDidLoad(): void;
    btnBackClick(evt: any): void;
    btnNextClick(evt: any): void;
    getTemplateSubTitle(): any;
    getTemplateDeckSelector(): any[];
    render(): any;
}
