export declare class PxOverlayContainer {
    elDivOverlayHost: HTMLElement;
    elParent: Document;
    _attachedOverlays: any;
    _attachmentRequestBound: any;
    containerType: string;
    componentDidLoad(): void;
    componentDidUnload(): void;
    _attachmentRequest(evt: any): void;
    appendNewContent(target: any, content: any): void;
    detachContent(target: any): void;
    _getHostRoot(): Document;
    _refireEvent(target: any, evt: Event): void;
    removeElem(overlay: any, elem: HTMLElement): void;
    render(): any;
}
