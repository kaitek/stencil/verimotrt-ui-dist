import { EventEmitter } from '../../stencil-public-runtime';
import { PxOverlayContainer } from './px-overlay-container';
export declare class PxOverlayContent {
    elContainer: PxOverlayContainer;
    elSlot: HTMLElement;
    pxOverlayAttachmentRequest: EventEmitter;
    containerType: string;
    _content: Element[];
    eventNames: string[];
    _isAttached: boolean;
    hoist: boolean;
    _onSlotChangedBound: any;
    componentWillLoad(): void;
    componentDidLoad(): void;
    componentDidUnload(): void;
    hoistOverlay(): void;
    _hoistPropChanged(hoist: any): void;
    _onSlotChange(): void;
    _getSlotContent(): any[];
    removeChild(elem: any): void;
    render(): any;
}
