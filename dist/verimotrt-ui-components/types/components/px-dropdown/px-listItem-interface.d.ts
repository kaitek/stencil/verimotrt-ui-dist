export interface IListItem {
    key: string | number;
    val: string;
    disabled?: boolean;
    disableSelect?: boolean;
    selected?: boolean;
    icon?: string;
    color?: string;
}
