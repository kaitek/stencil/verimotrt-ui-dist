import { ListItem } from './px-listItem';
import "@polymer/iron-media-query/iron-media-query";
import { PxDropdownContent } from './px-dropdown-content';
import { PxDropdownTrigger } from './px-dropdown-trigger';
export declare class PxDropdown {
    elTarget: HTMLDivElement;
    content: PxDropdownContent;
    elContentHTML: HTMLElement;
    trigger: PxDropdownTrigger;
    elTriggerHTML: HTMLElement;
    elTriggerSlot: HTMLElement;
    _focusedOption: HTMLElement;
    _initOpenTrigger: any;
    containerType: string;
    opened: boolean;
    hideChevron: boolean;
    boundTarget: string;
    preventCloseOnOutsideClick: boolean;
    displayValue: string;
    displayValueCount: number;
    disableClear: boolean;
    readOnly: boolean;
    disableSelect: boolean;
    disableTruncate: boolean;
    _displayValueSelected: string;
    items: ListItem[];
    multi: boolean;
    selectBy: string;
    selected: string;
    selectedValues: string[];
    selectedItems: ListItem[];
    allowOutsideScroll: boolean;
    buttonStyle: 'default' | 'bare' | 'bare--primary' | 'tertiary' | 'icon';
    disabled: boolean;
    disabledViewable: boolean;
    searchMode: boolean;
    searchTerm: string;
    sortMode: string;
    hideSelected: boolean;
    icon: string;
    clearSelectionsOnChange: boolean;
    triggerHeight: number;
    showCaret: boolean;
    disableDynamicAlign: boolean;
    verticalAlign: string;
    horizontalAlign: string;
    mobile: boolean;
    mobileAt: number;
    hideMobileButtons: boolean;
    useKeyIfMissing: boolean;
    _keyboardBeingUsed: boolean;
    hoist: boolean;
    _resetMinWidthListener(): void;
    _pxDropdownSelectionChangedListener(event: any): void;
    pxDropdownClearTappedListener(): void;
    keydownHandler(event: KeyboardEvent): void;
    _resetMinWidthWatcher(): void;
    _resetMinWidthWatcher2(): void;
    componentWillLoad(): void;
    componentDidLoad(): void;
    componentDidUnload(): void;
    /**
     * Toggles visibility of the dropdown: opens dropdown if it is closed, and
     * closes dropdown if it is open.
     */
    toggle(): void;
    /**
     * Finds the element in the the trigger slot. If the developer passed an
     * element into the `slot="trigger"` slot, returns that element. Otherwise
     * returns the default trigger in the Shadow DOM. Works for Polymer 1.x and
     * Polymer 2.x, which have different APIs for accomplishing this.
     *
     * @return {HTMLElement|null} - Reference to the trigger element, or null if none found
     */
    _getSlottedOpenTrigger(): HTMLElement;
    /**
     * Resizes the dropdown when the search term is changed.
     */
    _notifyResize(): void;
    render(): any[];
}
