import { EventEmitter } from '../../stencil-public-runtime';
import { IronDropdownElement } from '@polymer/iron-dropdown/iron-dropdown';
import { IronSelectorElement } from '@polymer/iron-selector/iron-selector';
import '@polymer/iron-a11y-keys-behavior/iron-a11y-keys-behavior';
import '@polymer/iron-dropdown/iron-dropdown';
import '@polymer/iron-selector/iron-selector';
import { ListItem } from './px-listItem';
import { PxDropdownTrigger } from './px-dropdown-trigger';
export declare class PxDropdownContent {
    elDivFocusedOption: HTMLDivElement;
    elDropdown: IronDropdownElement;
    elSelector: IronSelectorElement;
    elDivDropdownContent: HTMLDivElement;
    elSearchHtml: HTMLInputElement;
    pxDropdownClick: EventEmitter;
    pxDropdownSelectionChanged: EventEmitter;
    keyEvent: KeyboardEvent;
    openTrigger: PxDropdownTrigger;
    opened: boolean;
    hover: boolean;
    boundTarget: string;
    preventCloseOnOutsideClick: boolean;
    disableClear: boolean;
    disableSelect: boolean;
    readOnly: boolean;
    disabledViewable: boolean;
    disableTruncate: boolean;
    displayValueSelected: string;
    /**
       * An array that contains the list of items which show up in the dropdown.
       * Each item can either be a simple string, or an object consisting of:
       * * 'key' - a unique identifier (number or string)
       * * 'val' - the actual text that is displayed
       * * 'disabled' - whether the item should be disabled completely - can't be selected nor used as a menu option (optional)
       * * 'disableSelect` - whether the item should be disabled for selection, but can still be used as a menu option (optional)
       * * 'selected' - whether the item should be selected at instantiation (optional)
       * * 'icon' - an icon name from the px-icon-set to display next to the item (optional)
       * * 'color' - the color to use for the icon - if not specified, the default text colors will be used (optional)
       *
       * Note: if you specify more than one item as `selected`, but `multi` is not enabled,
       * only the *first* selected item will be chosen. See also `clearSelectionsOnChange`.
       */
    items: ListItem[];
    multi: boolean;
    selectBy: string;
    selected: string;
    selectedValues: string[];
    selectedItems: ListItem[];
    allowOutsideScroll: boolean;
    disabled: boolean;
    searchMode: boolean;
    searchTerm: string;
    sortMode: string;
    triggerHeight: number;
    displayValueCount: number;
    showCaret: boolean;
    disableDynamicAlign: boolean;
    clearSelectionsOnChange: boolean;
    hideSelected: boolean;
    displayValue: string;
    verticalAlign: string;
    horizontalAlign: string;
    mobile: boolean;
    mobileAt: number;
    hideMobileButtons: boolean;
    language: string;
    useKeyIfMissing: boolean;
    _keyboardBeingUsed: boolean;
    _computedItems: ListItem[];
    _handleOpenTriggerTappedBound: any;
    _handleClearTappedBound: any;
    _closeBound: any;
    _setReadOnly(): void;
    _setDisabledViewableClass(): void;
    _updateSelection(): void;
    /**
     * Any time that `items` changes, this method will convert any
     * simple strings in the array to an object with key/val.
     */
    _itemsChanged(items: ListItem[]): void;
    /**
       * Bind to open trigger, open/close on trigger tap.
       */
    triggerAssignedHandler(): void;
    openChangeHandler(): void;
    _handleKeyPress(event: KeyboardEvent): void;
    /**
       * When iron-activate is fired, this method checks whether the item is disabled
       * or if selection is disabled in the dropdown overall (for use in menus).
       * If so, it cancels the event so that iron-select is not called.
       */
    /**
    * Event fired when any non-disabled element is clicked in the dropdown.
    * If disableSelect is set on the entire dropdown or individual item,
    * the event is still fired and the dropdown is closed, but the item
    * is not selected. Useful for menu dropdowns, as opposed to selection dropdowns.
    *
    * @event pxDropdownClick
    */
    _handleActivate(evt: any): void;
    /**
       * Handles the selection event from iron-selector to update
       * the label displayed inside the dropdown. Use the displayValueCount property to define how many values
       * are displayed in the trigger value of multiple select lists
       */
    _handleSelection(evt: any): void;
    /**
     * Handles the de-selection event from iron-selector to update
     * the label displayed inside the dropdown.
     */
    _handleDeselection(evt: any): void;
    _handleCancel(e: any): void;
    _handleClose(e: any): void;
    _handleOpen(e: any): void;
    toggleClass(name: string, bool: boolean, node: HTMLElement): void;
    componentWillLoad(): void;
    componentDidLoad(): void;
    componentDidUnload(): void;
    toggle(): void;
    close(): void;
    open(): void;
    _openTriggerChanged(newTrigger: any, oldTrigger: any): void;
    _handleClearTapped(): void;
    /**
     * Searches the DOM for the `boundTarget` element.
     */
    _getFitElement(target: any): any;
    /**
     * Determines the position target element based on `mobile`.
     */
    _getPositionElement(mobile: any, openTrigger: any): any;
    /**
     * Resizes the dropdown when the search term is changed.
     */
    _notifyResize(): void;
    _computeFilter(): void;
    /**
   * Determines whether to display the clear button inside the dropdown in mobile mode.
   */
    _showMobileClearButton(mobile: any, selected: any, selectedValues: any): "" | "btn--disabled";
    /**
     * Initializes the sort function for the dom-repeat inside of iron-selector.
     */
    /**
     * Event handler for mouse move event. We enable mouse events when user moves the mouse.
     * Mouse events are disabled when user uses the keyboard to interact with the dropdown.
     * @private
     */
    _bindMouse(): void;
    /**
     * The sort function used by the dom-repeat inside of iron-selector to
     * sort the items by either 'key' or 'val' based on `sortMode`.
     */
    _computeSort(a: any, b: any): number;
    _setFocusedOption(newOption: any, oldOption: any): void;
    /**
       * Event handler when the mouse hovers over a dropdown list item.
       */
    _hoverOn(event: MouseEvent): void;
    /**
       * Event handler when the mouse hovers out of a dropdown list item.
       */
    _hoverOff(event: MouseEvent): void;
    /**
    * Calculates classes for the caret based on the horizontal and vertical alignment.
    */
    _getCaretClass(showCaret: any, mobile: any, verticalAlign: any, horizontalAlign: any): string;
    /**
    * Calculates class for the dropdown items based on whether truncation is disabled.
    */
    _getTruncateClass(disableTruncate: boolean): "" | "truncate";
    /**
    * Calculates offset for the dropdown based on whether caret is shown.
    */
    _getOffset(showCaret: any, triggerHeight: any, mobile: any): any;
    _getVertical(verticalAlign: any, mobile: any): any;
    _getHorizontal(horizontalAlign: any, mobile: any): any;
    _showCheckmark(multi: any, mobile: any): boolean;
    _showButtons(mobile: any, hideMobileButtons: any): boolean;
    getTemplateSearchBox(): any;
    getTemplateDropdownItems(): any[];
    getTamplateMobileButtons(): any;
    render(): any[];
}
