import { IListItem } from "./px-listItem-interface";
export declare class ListItem {
    key: string | number;
    val: string;
    disabled?: boolean;
    disableSelect?: boolean;
    selected?: boolean;
    icon?: string;
    color?: string;
    constructor(obj: IListItem);
}
