export declare class PxGauge {
    elSVGchartSVG: SVGElement;
    elSVGchartG: SVGGElement;
    elDivchart: HTMLElement;
    svg: any;
    _chart: any;
    myElement: HTMLElement;
    value: number;
    min: number;
    max: number;
    error: number[][];
    anomaly: number[][];
    abnormal: number[][];
    normal: number[][];
    _states: string[];
    unit: string;
    _valueOfProcess: number;
    barWidth: number;
    _actualBarWidth: number;
    _calculatedValue: number;
    marginTop: number;
    marginBottom: number;
    marginRight: number;
    marginLeft: number;
    preventResize: boolean;
    _filledBarArc: any;
    _emptyBarArc: any;
    _targetPerc: number;
    width: number;
    height: number;
    _currentPerc: number;
    getValue(): Promise<number>;
    setValue(value: number): Promise<void>;
    /**
     * Change position of needle and update gauge bars follow _calculatedValue
     * @private
     * @method _valueChanged
     */
    _valueChanged(): void;
    /**
     * Re-render gauge bars with new bar width
     * @private
     * @method _barWidthChanged
     */
    _barWidthChanged(): void;
    componentWillLoad(): void;
    componentDidLoad(): void;
    /**
     * Calculate percentage of input value base on min and max value
     * @private
     * @method _computeValue
     * @return {Number} - The number will be used to calculate needle position
     */
    _computeValue(): number;
    _computeActualBarWidth(): number;
    /**
     * Select elements and convert them to D3 element object
     * @private
     * @method _d3Select
     * @param {String} selector - a valid CSS selector
     * @return {Object} - D3 element object
     */
    _d3Select(selector: any): any;
    /**
     * Convert from percentage to degree
     * @private
     * @method _percToDeg
     * @param {Number} perc - percentage
     * @return {Number} - The number which is converted from percentage to degree
     */
    _percToDeg(perc: any): number;
    /**
     * Convert from percentage to radian
     * @private
     * @method _percToRad
     * @param {Number} perc - percentage
     * @return {Number} - The number which is converted from percentage to radian
     */
    _percToRad(perc: any): number;
    /**
     * Convert from degree to radian
     * @private
     * @method _degToRad
     * @param {Number} deg - degree
     * @return {Number} - The number which is converted from degree to radian
     */
    _degToRad(deg: any): number;
    /**
     * @method _drawChart
     * draws _chart. Debounced.
     */
    _drawChart(): void;
    /**
     * Calculate element attributes and append require elements to main element
     * @private
     * @method _renderChart
     */
    _drawChartDebounced(): void;
    /**
     * Calculate gauge bars radius and size, then append them to main element
     * @private
     * @method _calculateGaugeBarsArc
     */
    _calculateGaugeBarsArc(): void;
    /**
     * Generate needle paths
     * @private
     * @method _generateNeedlePath
     */
    _generateNeedlePath(): void;
    /**
     * Adjust font-size of number and unit label base on bar width
     * @private
     * @method _adjustFontSize
     */
    _adjustFontSize(): void;
    /**
     * Calculate start and end radian of gauge bars.
     * @private
     * @method _repaintGauge
     */
    _repaintGauge(): void;
    /**
     * Check which state can be applied to filled gauge bar by current value
     * @private
     * @method _changeFilledGaugeBarColor
     */
    _changeFilledGaugeBarState(): void;
    /**
     * Change position of needle base on _calculatedValue
     * @private
     * @method _moveTo
     */
    _moveTo(targetPerc: any): void;
    render(): any[];
}
