export declare class PxHeading {
    text: string;
    class: '' | 'heading--page' | 'heading--section' | 'heading--subsection' | 'label' | 'value' | 'alpha' | 'beta' | 'gamma' | 'delta' | 'epsilon' | 'zeta';
    render(): any;
}
