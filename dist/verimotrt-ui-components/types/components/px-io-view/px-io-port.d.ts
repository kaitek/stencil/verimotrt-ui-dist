import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxIOPort {
    myElement: HTMLElement;
    portName: string;
    type: "input" | 'output';
    elDivContainer: HTMLElement;
    lastChangeTime: number;
    value: number;
    pxIOPortTriggered: EventEmitter;
    setValue(state: 0 | 1): Promise<void>;
    _action(): void;
    render(): any;
}
