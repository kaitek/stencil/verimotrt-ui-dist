import { PxIOPort } from './px-io-port';
export declare class PxIOView {
    myElement: HTMLElement;
    inputs: number;
    outputs: number;
    maxIOCount: number;
    inputPorts: PxIOPort[];
    outputPorts: PxIOPort[];
    setPort(group: "input" | 'output', index: number, state: 0 | 1): Promise<void>;
    componentDidLoad(): void;
    selectedListener(e: any): void;
    getTemplatePorts(type: "input" | 'output', count: number): any[];
    render(): any;
}
