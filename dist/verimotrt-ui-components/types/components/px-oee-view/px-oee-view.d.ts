export declare class PxOeeView {
    usability: number;
    performance: number;
    quality: number;
    oee: number;
    render(): any;
}
