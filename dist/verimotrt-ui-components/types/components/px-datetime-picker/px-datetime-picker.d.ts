import { EventEmitter } from '../../stencil-public-runtime';
import { Moment } from 'moment-timezone';
import { PxDatetimeField } from '../px-datetime-field/px-datetime-field';
import { PxDatePickerContent } from './px-date-picker-content';
import { PxTimePickerContent } from './px-time-picker-content';
export declare class PxDatetimePicker {
    field: PxDatetimeField;
    contentDatePicker: PxDatePickerContent;
    contentTimePicker: PxTimePickerContent;
    showTimeZone: 'none' | 'dropdown' | 'extendedDropdown' | 'text' | 'abbreviatedText';
    hideTime: boolean;
    dateFormat: string;
    timeFormat: string;
    required: boolean;
    opened: boolean;
    fieldIsValid: boolean;
    fillContainer: boolean;
    _fillContainer: boolean;
    fitIntoElement: Object;
    _fitIntoElement: Object;
    collapseAt: number;
    collapsed: boolean;
    _belowCollapseSize: boolean;
    useKeyIfMissing: boolean;
    hoist: boolean;
    maxDate: string;
    minDate: string;
    blockFutureDates: boolean;
    blockPastDates: boolean;
    showButtons: boolean;
    timeZone: string;
    momentObj: Moment;
    _tempMomentObj: Moment;
    containerType: string;
    pxDateTimePickerChanged: EventEmitter;
    componentDidLoad(): void;
    pickerValueChange(e: any): void;
    selectedDatePickerListener(e: any): void;
    selectedTimePickerListener(e: any): void;
    /**
     * Rolls back temp moment to the momentObj.
     * Closes panel
     */
    /**
     * Runs validation
     */
    /**
     * If the calendar is open and the dropdown is selected, close the calendar
     */
    _handleTap(evt: any): void;
    _applyTempMoment(): void;
    /**
     * Ran when the momentObjs for field or content have been updated.
     * If it is from the field, then apply the momentObj.
     * If it is from the content and we have no buttons, then apply the momentObj.
     * Else, wait for button events.
     */
    /**
     * Apply temp moment if the dropdown is opened and
     * the field is valid
     */
    /**
     * submit: validate values
     * cancel: rolls back temp moment to the momentObj &closes panel
     */
    /**
     * Opens content
     */
    _iconClicked(evt: any): void;
    /**
     * If opened is false and showButtons true
     * then cancel selection
     */
    _processClose(newVal: any, oldVal: any): void;
    /**
     * Return the collapseAt value with px after it
     */
    _getCollapseQuery(collapseAt: any): any;
    /**
     * Double checks that the collapseAt value is valid
     */
    _collapseQueryIsValid(query: any): boolean;
    /**
     * If the window is smaller than the `collapseAt` value
     * set panel to fill the whole window
     *
     * If the window is larger than the `collapseAt` value
     * reset `_fillContainer` & `_fitIntoElement` to their
     * original values
     */
    _handleBelowCollapseSizeChanged(newVal: any): void;
    /**
     * Sync external fillConatiner with internal _fillConatiner
     */
    _syncFillContainer(newVal: any): void;
    /**
     * Sync external fitIntoElement with internal _fitIntoElement.
     * Make sure `_fitIntoElement` isn't over written if
     * `_belowCollapseSize` is true
     */
    _syncFitIntoElement(newVal: any): void;
    /**
     * Return field node
     */
    _returnField(): PxDatetimeField;
    _rollbackTempMoment(): void;
    getTemplateMediaQuery(): any;
    render(): any[];
}
