import { EventEmitter } from '../../stencil-public-runtime';
import { IronDropdownElement } from '@polymer/iron-dropdown/iron-dropdown';
import '@polymer/iron-dropdown/iron-dropdown';
import { PxTimePicker } from '../px-time-picker/px-time-picker';
import { PxDatetimeField } from '../px-datetime-field/px-datetime-field';
export declare class PxTimePickerContent {
    fieldElem: HTMLElement;
    dropdown: IronDropdownElement;
    timePicker: PxTimePicker;
    openTrigger: PxDatetimeField;
    opened: boolean;
    fieldIsValid: boolean;
    fillContainer: boolean;
    fitIntoElement: Object;
    selectedDate: string;
    pxTimePickerSelected: EventEmitter;
    selectedListener(e: any): void;
    _okClicked(): void;
    triggerAssignedHandler(): void;
    /**
     * Opens the calendar.
     * Called in datetime-picker
     */
    _open(): Promise<void>;
    /**
     * Closes the calendar.
     * Called in datetime-picker
     */
    _close(): Promise<void>;
    _getVerticalOffset(fillContainer: any): 0 | 20;
    _getPositionTarget(fillContainer: any, fitIntoElement: any): any;
    render(): any;
}
