import { EventEmitter } from '../../stencil-public-runtime';
import { Moment } from 'moment-timezone';
import { IronDropdownElement } from '@polymer/iron-dropdown/iron-dropdown';
import '@polymer/iron-dropdown/iron-dropdown';
import { PxCalendarPicker } from '../px-calendar-picker/px-calendar-picker';
import { PxDatetimeField } from '../px-datetime-field/px-datetime-field';
export declare class PxDatePickerContent {
    fieldElem: HTMLElement;
    dropdown: IronDropdownElement;
    calendar: PxCalendarPicker;
    openTrigger: PxDatetimeField;
    hideTime: boolean;
    showButtons: boolean;
    timeFormat: string;
    opened: boolean;
    fieldIsValid: boolean;
    fillContainer: boolean;
    fitIntoElement: Object;
    selectedDate: string;
    baseDate: Moment;
    tempMomentObj: Moment;
    timeZone: string;
    maxDate: string;
    minDate: string;
    blockFutureDates: boolean;
    blockPastDates: boolean;
    containerType: string;
    pxDatePickerSelected: EventEmitter;
    selectedListener(e: any): void;
    _todayClicked(): Promise<void>;
    triggerAssignedHandler(): void;
    /**
     * Opens the calendar.
     * Called in datetime-picker
     */
    _open(): Promise<void>;
    /**
     * Closes the calendar.
     * Called in datetime-picker
     */
    _close(): Promise<void>;
    /**
     * Sync `selcetedDate` with calendar's moment obj
     */
    _updateSelectedDate(moment: any): void;
    /**
     * If fillContainter is true show buttons
     */
    _handleShowButtons(showButtons: any, fillContainer: any): any;
    _getVerticalOffset(fillContainer: any): 0 | 20;
    _getPositionTarget(fillContainer: any, fitIntoElement: any): any;
    render(): any;
}
