/// <reference types="node" />
import { EventEmitter } from "events";
export declare class PxDatetimeValidate extends EventEmitter {
    isValid: boolean;
    validationErrorMessage: string;
    language: string;
    useKeyIfMissing: boolean;
    resources: object;
    private _root;
    private _moment;
    private timeZone;
    private hideDate;
    private hideTime;
    private momentObj;
    private blockFutureDates;
    private blockPastDates;
    private maxDate;
    private minDate;
    private dateFormat;
    private timeFormat;
    private required;
    constructor(config: any);
    toggleClass(name: string, bool: boolean, node: Element): void;
    /**
     * Validation function for 'px-datetime-entry'
     *
     * @event px-moment-valid
     * @param {element} this
  
      * @event px-moment-invalid
      * @param {element} this
      */
    _validateInput(format: any, tz: any): void;
    /**
     * Validate a complete field
     * called in px-datetime-field and px-datetime-range-field
     *
     * @param {String} funcOrigin - The function that called validation. Used for `_handleIncompleteEntries`
     * @return validation result:
     * - MomentObj - The validated new moment obj
     * - False - If the new moment is invalid
     * - FieldSame - If the new moment is the same as the old moment
     * - FieldBlank - If the visible field(s) are blank
     */
    _validateField(funcOrigin: any): any;
    /**
     * Validate a moment object
     * called in from _validateField and px-datetime-field
     *
     * @param {Object} momentObj - Moment object getting validated
     * @return validation result:
     * - MomentObj - The validated new moment obj
     * - False - If the new moment is invalid
     * - FieldSame - If the new moment is the same as the old moment
     */
    _validateMomentObj(validatingMomentObj: any): any;
    /**
     * Reset field to valid
     *
     * @param {Object} submittedMoment - valid momentObj
     */
    _validField(obj: any): void;
    /**
     * Set field to invalid
     * Set validation Error Message, turn on validation error, set isValid to false, and fire event.
     *
     * @param {Object} submittedMoment - Invalid momentObj
     * @param {Object} invalidObj - Stores the different invalid
     */
    _invalidField(submittedMoment: any, invalidObj: any): void;
    /**
     * loop through array of the entry cells and return a string of the value
     *
     * @param {Object} cells - Date or time cell nodes
     */
    _entryInputString(cells: any): string;
    /**
     * Returns a meaningful validation message
     *
     * @param {Object} submittedMoment - Invalid momentObj
     * @param {Object} invalidObj - Stores the different invalid
     */
    _determineValidationMessage(submittedMoment: any, invalidObj: any): string;
    /**
     * Fires when 'validationErrorMessage' is changed
     *
     * @event px-validation-message
     * @param {string} validationErrorMessage - Values validationErrorMessage
     */
    _updateValidationEvent(validationErrorMessage: any): void;
    /**
     * Sets the button state based on if validation has been passed or not. This in turn enables or disables the Submit button.
     *
     * @param {Boolean} state - Where the validation has passed or failed
     */
    _submitButtonState(state: any): void;
    /**
     * If required any incomplete momentObj is invalid
     * If not required determine if the field is blank
     *
     * @param {String} funcOrigin - The function that called validation
     * @param {Element} entry - px-datetime-entry date
     * @param {Object} fieldNodes - Object of date and time cell nodes
     */
    _handleIncompleteEntries(funcOrigin: any, entry: any, fieldNodes: any): false | "FieldBlank";
    /**
     * Checking to see if all cells are empty
     * If a cell has a value return false
     *
     * @param {Object} cells - Date or time cell nodes
     */
    _loopOverCellValues(cells: any): boolean;
}
