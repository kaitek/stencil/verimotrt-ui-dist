import { EventEmitter } from '../../stencil-public-runtime';
import 'moment/dist/locale/tr';
export declare class PxDatetimeEntry {
    elwrapper: HTMLElement;
    pxDropdownFocused: EventEmitter;
    pxDropdownBlured: EventEmitter;
    dateOrTime: string;
    momentFormat: string;
    hideIcon: boolean;
    hideTimeZone: boolean;
    _cellFormatArray: string[];
    _symbolCharArray: string[];
    showTimeZone: string;
    isSelected: boolean;
    hoist: boolean;
    timeZone: string;
    momentObj: object;
    componentWillLoad(): void;
    componentDidLoad(): void;
    toggleClass(name: string, bool: boolean, node: Element): Promise<void>;
    clear(): Promise<void>;
    setValueFromMoment(): Promise<void>;
    _formatChanged(newMomentFormat: any): void;
    _sizeSymbols(): void;
    _splitFormat(momentFormat: any): {
        _cellFormatArray: any;
        _symbolCharArray: any;
    };
    _isSymbol(i: any, _symbolCharArray: any): boolean;
    _returnSymbol(i: any, _symbolCharArray: any): any;
    _setIcon(): "fa-calendar-alt" | "fa-clock";
    _handleDropdownFocus(): void;
    _handleDropdownBlur(): void;
    _showTimeZoneText(showTimeZone: any): boolean;
    _showTimeZoneDropdown(showTimeZone: any): boolean;
    _getTimeZoneList(showTimeZone: any): any;
    _updateTimeZone(e: any): void;
    _handleCopy(e: any): void;
    _handlePaste(e: any): void;
    _getIconClass(hideIcon: any): "" | "u-mr--";
    _getTimeZoneText(timeZone: any, showTimeZone: any): any;
    _getTimeZoneTextClass(showTimeZone: any): string;
    getTemplateEntrySymbol(idx: any, _symbolCharArray: any): any;
    getTamplateEntryCells(): any[][];
    render(): any;
}
