import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxDatetimeButtons {
    pxDatetimeButtonClicked: EventEmitter;
    isSubmitButtonValid: boolean;
    hideSubmit: boolean;
    hideCancel: boolean;
    submitIcon: string;
    spaceBetween: boolean;
    useKeyIfMissing: boolean;
    _fireCancelEvent(): void;
    _fireSubmitEvent(): void;
    submitBtnClasses(base: any, submitButtonValid: any): any;
    _isEqual(source: any, target: any): boolean;
    render(): any;
}
