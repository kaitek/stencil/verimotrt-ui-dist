import { Moment } from 'moment-timezone';
export declare class PxDatetimeEntryCell {
    el: HTMLInputElement;
    moment: Moment;
    momentFormat: string;
    symbol: string;
    order: number;
    isSelected: boolean;
    _max: "" | 12 | 31 | 23 | 24 | 59 | 9 | 99 | 999 | 9999;
    _min: "" | 0 | 1;
    _isNumber: boolean;
    _cellLength: number;
    timeZone: string;
    momentObj: Moment;
    componentWillLoad(): void;
    componentDidLoad(): void;
    clear(): void;
    setValueFromMoment(): void;
    _computeIsNumber(momentFormat: any): boolean;
    _getPattern(): string;
    /**
     * Set the correct type of input field based on the momentFormat
     */
    _getType(): "text" | "tel";
    /**
     * Disables the input field for time zone entry cells.
     */
    _getDisabled(): boolean;
    _formatChanged(): void;
    /**
     * Size the input fields based on the value or placeholder text
     */
    _sizeInputs(): void;
    /**
     * Sets up regular key bindings
     */
    /**
     * Sets up separator keybindings
     */
    /**
     * If `momentObj` or `timeZone` update, reflect this in the cell
     */
    _updateInputValue(): void;
    /**
     * When resources is changed, update placeholder text and resize input if need.
     */
    _onResourcesUpdated(): void;
    /**
     * Fires when 'left' key is hit
     *
     * @event px-entry-cell-move
     * @param {number} dir - Values -1
     */
    /**
     * Fires when 'right' key is hit
     *
     * @event px-entry-cell-move
     * @param {number} dir - Values 1
     */
    /**
     * Propagate the focus event up to entry to apply inline edit styles if applicable.
     */
    /**
     * Allow the user to loop through valid cell values
     * ex If the momentFormat is MM the cell value can go from 12 to 1 by hitting the up arrow
     */
    /**
     * Checks that the value has the correct number of digits for our format
     * Autocomplete function
     */
    _checkValue(): void;
    /**
     * Fires on blur
     *
     * @event px-cell-blurred
     */
    /**
     * Toggles AM & PM when up/down or A/P keys are pressed.
     */
    /**
     * Sets the AM/PM value.
     */
    /**
     * Prevent the delete button from navigating back
     */
    _preventDeleteDefault(evt: any): void;
    /**
     * If the momentFormat format is not 'Z', 'ZZ', 'X', 'x', 'A' or 'a' then return the momentFormat
     * Note: placeholder text supports i18n
     */
    _placeholderText(momentFormat: any): any;
    /**
     * Handles before copy event
     *
     * @event px-request-datetime-entry-copy
     * @param {string} dir - Values clipboardData
     */
    /**
     * Handles before copy event, used for IE instead of the copy event
     */
    /**
     * Handles before paste event, used for IE instead of the paste event
     */
    /**
     * Handles paste event
     */
    /**
     * Handles copy event
     */
    /**
     * Format strings and setting/modification strings are different (?!?!)
     * This method converts one to the other
     */
    _convertMomentFormat(): any;
    /**
     * Set the max value a token can be
     */
    _getMax(momentFormat: any): "" | 23 | 12 | 9 | 99 | 31 | 24 | 59 | 999 | 9999;
    /**
     * Set the min value a token can be
     */
    _getMin(momentFormat: any): "" | 1 | 0;
    render(): any;
}
