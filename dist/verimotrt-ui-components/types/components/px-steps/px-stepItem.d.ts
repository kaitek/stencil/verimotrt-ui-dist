import { IStepItem } from "./px-stepItem-interface";
export declare class StepItem {
    id: string;
    label: string;
    constructor(obj: IStepItem);
}
