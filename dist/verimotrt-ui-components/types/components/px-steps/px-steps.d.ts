import { EventEmitter } from '../../stencil-public-runtime';
import { StepItem } from './px-stepItem';
export declare class PxSteps {
    pxStepsTapped: EventEmitter;
    items: StepItem[];
    completed: String[];
    errored: String[];
    currentStep: number;
    /**
     * Sets the current step to complete and advances to the next step.
     */
    complete(): Promise<void>;
    /**
     * Sets the step at the passed index to error state.
     */
    error(index: any): Promise<void>;
    /**
     * Go forward one step.
     */
    next(): Promise<void>;
    /**
     * Go back one step.
     */
    previous(): Promise<void>;
    /**
     * Jump to an arbitrary step by index.
     */
    jumpToStep(index: any): void;
    /**
     * Calculates which icon to display for a given node.
     */
    _getIcon(item: any): "fa-times-circle" | "fa-check-circle" | "fa-circle";
    /**
     * Calculates which class to assign to a given node.
     */
    _getStepState(item: any, index: any, currentStep: any, completed: any, errored: any): string;
    /**
     * Fires an event when a node is tapped upon.
     */
    _handleTap(event: any): void;
    getTemplateStepItems(): any[];
    render(): any;
}
