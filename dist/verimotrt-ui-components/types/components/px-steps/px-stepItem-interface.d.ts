export interface IStepItem {
    id: string;
    label: string;
}
