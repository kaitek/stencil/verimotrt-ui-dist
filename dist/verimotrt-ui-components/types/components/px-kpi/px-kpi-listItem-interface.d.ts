export interface IKpiListItem {
    label: string;
    value: number;
    uom: string;
}
