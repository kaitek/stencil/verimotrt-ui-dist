import { IKpiListItem } from "./px-kpi-listItem-interface";
export declare class KpiListItem {
    label: string;
    value: number;
    uom: string;
    constructor(obj: IKpiListItem);
}
