import { KpiListItem } from "./px-kpi-listItem";
export declare class PxKpiList {
    label: string;
    values: KpiListItem[];
    statusColor: string;
    statusIcon: string;
    statusLabel: string;
    footer: string;
    getTemplateItems(): any[];
    render(): any[];
}
