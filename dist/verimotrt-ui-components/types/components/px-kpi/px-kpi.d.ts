export declare class PxKpi {
    label: string;
    value: string;
    uom: string;
    statusColor: string;
    statusIcon: string;
    statusLabel: string;
    getTemplateSparkData(): void;
    render(): any[];
}
