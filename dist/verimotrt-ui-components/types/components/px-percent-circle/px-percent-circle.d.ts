export declare class PxPercentCircle {
    elSVGContainer: SVGElement;
    val: number;
    max: number;
    thickness: number;
    _size: number;
    _percent: number;
    _p_tf: string;
    _p_tx: number;
    _p_ty: number;
    _p_pf: string;
    _p_px: number;
    _p_py: number;
    _cx: number;
    _cy: number;
    _r: number;
    _arcPath: string;
    getValue(): Promise<number>;
    setValue(value: number): Promise<void>;
    componentWillLoad(): void;
    componentDidLoad(): void;
    /**
     * Calculated spacing and font sizes based on the size of the component
     * @method _computedPlacement
     * @param size
     */
    _computedPlacement(size: any): void;
    /**
     * Cleanses invalid inputs by clipping value between 0 and 100.
     * @method _computedVal
     * @param val
     */
    _computedVal(val: any): any;
    /**
     * Cleanses invalid values for the maximum
     * @method _computedMax
     * @param max
     */
    _computedMax(max: any): any;
    /**
     * Calculates the percent to display, based on the sanitized value and maximum value.
     * @method _computedPercent
     * @param val
     * @param max
     */
    _computedPercent(val: any, max: any): number;
    /**
     * Sets the view box attribute on the SVG element.
     * @method _setViewBoxAttribute
     * @param size
     */
    _setViewBoxAttribute(size: any): void;
    /**
     * Finds the center of the height/width attributes.
     * @method _computedCoordinate
     * @param val
     */
    _computedCoordinate(val: any): number;
    /**
     * Calculates the inner radius to "hollow out" on the donut (50%, minus the thickness).
     * @method _computedRadius
     * @param val
     * @param stroke
     */
    _computedRadius(val: any, stroke: any): number;
    /**
     * Sanitizes the size property so that it's never smaller than 100
     * @method _computedSize
     * @param size
     */
    _computedSize(rect: any): number;
    /**
     * Sanitizes the thickness property so that it's never too large or too small
     * @method _computedThickness
     * @param size
     * @param thickness
     */
    _computedThickness(size: any, thickness: any): any;
    /**
     * Calculates path for the filled in portion of the donut.
     * @method _computedArcPath
     * @param x
     * @param y
     * @param r
     * @param val
     * @param max
     *
     * Code for this attributed here:
     * http://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
     */
    _computedArcPath(x: any, y: any, r: any, percent: any): string;
    /**
     * Converts a polar value to Cartesian coordinates.
     * @method _polarToCartesian
     * @param centerX
     * @param centerY
     * @param radius
     * @param angleInDegrees
     */
    _polarToCartesian(centerX: any, centerY: any, radius: any, angleInDegrees: any): {
        x: any;
        y: any;
    };
    render(): any;
}
