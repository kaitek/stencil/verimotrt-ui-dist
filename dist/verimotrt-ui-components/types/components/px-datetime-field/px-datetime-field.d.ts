import { EventEmitter } from '../../stencil-public-runtime';
import { Moment } from 'moment-timezone';
import { PxDatetimeEntry } from '../px-datetime-common/px-datetime-entry';
export declare class PxDatetimeField {
    elfieldWrapper: HTMLElement;
    elDate: PxDatetimeEntry;
    elTime: PxDatetimeEntry;
    moment: Moment;
    dateIsValid: boolean;
    timeIsValid: boolean;
    fieldIsValid: boolean;
    isSelected: boolean;
    preventCellFocus: boolean;
    _dropdownFocused: boolean;
    dateFormat: string;
    timeFormat: string;
    showTimeZone: 'none' | 'dropdown' | 'extendedDropdown' | 'text' | 'abbreviatedText';
    hideTime: boolean;
    hideDate: boolean;
    hideIcon: boolean;
    allowWrap: boolean;
    required: boolean;
    showButtons: boolean;
    blockFutureDates: boolean;
    blockPastDates: boolean;
    isValid: boolean;
    hideValidationMessage: boolean;
    _validationErrorMessage: string;
    _forcedFailedClass: boolean;
    preventApply: boolean;
    _dropdownActive: boolean;
    _dropdownHoverFix: boolean;
    hoist: boolean;
    containerType: string;
    maxDate: string;
    minDate: string;
    timeZone: string;
    momentObj: Moment;
    pxDatetimeEntryIconClicked: EventEmitter;
    pxFieldValid: EventEmitter;
    pxMomentChanged: EventEmitter;
    componentDidLoad(): void;
    setDate(momentObj: any): Promise<void>;
    setTime(momentObj: any): Promise<void>;
    _fieldClicked(field: any): void;
    _showTimeZoneInDate(hideTime: any, showTimeZone: any): any;
    /**
     * Toggles classes causing the buttons to wrap under the field
     */
    _getWrapClass(allowWrap: any): string;
    /**
     * Adds a margin right if the buttons are showing
     */
    _getButtonMarginClass(showButtons: any, allowWrap: any): string;
    /**
     * event that watches when the dropdown has focus
     */
    _handleDropdownFocus(e: any): void;
    _handleDropdownBlur(e: any): void;
    /**
     * when dt-field is detached set `_dropdownFocused` to false.
     * prevents the dropdown from being stuck in a focused state
     */
    detached(): void;
    /**
     * This is a quick fix.
     * In order to make dt-field look like an input field we have to overwrite some css on #fieldWrapper.
     *
     * There is a hover state on #fieldWrapper and px-dropdown is a child of #fieldWrapper;
     * therefore, when dropdown is opened the hover to #fieldWrapper is extended.
     * This causes a flash of gray when the user selects a time zone with the mouse.
     * In order to prevent this flash we need to overwrite the hover state by added
     * `.dt-text-field--hover-fix` to #fieldWrapper.
     *
     * The true issue is that we can't listen to px-dropdown.opened because there is a timing issue
     * when px-dropdown.opened is set to false vs when the dropdown is not longer visible. This causes the flash.
     * We couldn't find any other events that don't have a timing issue.
     */
    _handleHoverFix(): void;
    /**
     * isValid & _forcedFailedClass - determines whether to show the validation error classes
     * isSelected - sets the styles when an entryCell is selected
     * _dropdownActive - overwrites the styles when the dropdown is focused
     * _dropdownHoverFix - overwrites the styles when the user selects an item in the dropdown
     */
    _getWrapperClass(isValid: any, _forcedFailedClass: any, isSelected: any, _dropdownActive: any, _dropdownHoverFix: any): string;
    /**
     * Adds a right margin if both entries are showing
     */
    _getEntryDateClass(hideDate: any, hideTime: any, hideIcon: any): string;
    /**
     * Run validation on a moment object that has been set from the outside
     * This will set or unset validation errors
     */
    _validateSetMomentObj(momentObj: any): void;
    _getIsValid(fieldIsValid: any, dateIsValid: any, timeIsValid: any): any;
    _handleValidationMessage(e: any): void;
    /**
     * Helper function for a dom-if
     * Determines whether to display the validation error
     */
    _showValidation(isValid: any, hideValidationMessage: any): boolean;
    /**
     * Handles validation coming from `_validateInput()`
     * Set `dateIsValid` and `timeIsValid` to true as needed
     * Runs validation if buttons are hidden and field is valid
     */
    /**
     * Sets `isValid`, `dateIsValid`, and `timeIsValid` to false as needed.
     */
    _handleInvalidMoment(evt: any): void;
    /**
     * Determines whether to apply or cancel the changes
     * based on the event
     */
    /**
     * Sets the momentObj if the field is valid, preventApply is not true,
     * and the new and old momentObjs are not the same.
     */
    /**
     * Clear field or reset to previous moment objs
     */
    _cancelChanges(): void;
    render(): any;
}
