import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxModal {
    myElement: HTMLElement;
    elDivModal: HTMLDivElement;
    elDivBox: HTMLDivElement;
    elAcceptTriggerContainer: HTMLDivElement;
    elRejectTriggerContainer: HTMLDivElement;
    elRejectButton: HTMLButtonElement;
    elAcceptButton: HTMLButtonElement;
    _firstFocusableElement: HTMLElement;
    _lastFocusableElement: HTMLElement;
    _elementFocusedBeforeOpened: HTMLElement;
    lastFocusedElement: HTMLElement;
    fitInto: object;
    pxModalDismissed: EventEmitter;
    pxModalRejected: EventEmitter;
    pxModalAccepted: EventEmitter;
    headerText: string;
    bodyText: string;
    acceptText: string;
    rejectText: string;
    hideAcceptButton: boolean;
    hideRejectButton: boolean;
    opened: boolean;
    disableCloseOnEscape: boolean;
    disableAutoFocus: boolean;
    fillContainer: boolean;
    autoFitOnAttach: boolean;
    _focusableElements: HTMLElement[];
    style: any;
    getVisible(): Promise<boolean>;
    setVisible(isVisible: boolean): Promise<void>;
    keydownHandler(evt: KeyboardEvent): void;
    _handleFillContainerChanged(): void;
    componentWillLoad(): void;
    componentDidLoad(): void;
    _handleTransitionEnd(evt: TransitionEvent): void;
    findFocusableElements(): void;
    _handleRejectTriggerTapped(e: any): void;
    _handleAcceptTriggerTapped(e: any): void;
    _refit(): void;
    /**
     * IronFitBehavior start
     */
    sizingTarget: HTMLElement;
    horizontalAlign: string;
    verticalAlign: string;
    positionTarget: Element;
    __shouldPosition: boolean;
    _fitInfo: any;
    _localeHorizontalAlign: string;
    get_localeHorizontalAlign(): string;
    _discoverInfo(): void;
    refit(): void;
    resetFit(): void;
    fit(): void;
    constrain(): void;
    __getNormalizedRect(target: Element | Window): DOMRect | {
        top: number;
        left: number;
        width: number;
        height: number;
        right: number;
        bottom: number;
    };
    __sizeDimension(rect: any, positionedBy: any, start: any, end: any, extent: any): void;
    center(): void;
    /**
     * IronFitBehavior finish
     */
    render(): any;
}
