import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxAlertMessage {
    language: string;
    useKeyIfMissing: boolean;
    resources: object;
    type: 'important' | 'warning' | 'error' | 'information' | 'unknown' | 'custom';
    messageTitle: string;
    message: string;
    label: string;
    autoDismiss: number;
    action: 'dismiss' | 'acknowledge' | '';
    hideBadge: boolean;
    hideSeverity: boolean;
    visible: boolean;
    elDivAlert: HTMLDivElement;
    getVisible(): Promise<boolean>;
    setVisible(isVisible: boolean): Promise<void>;
    pxAlertMessageActionTriggered: EventEmitter;
    pxAlertMessageAlertHiddenTriggered: EventEmitter;
    _action(): void;
    _handleAnimationEnd(event: AnimationEvent): void;
    getTemplateActionDetail(): any;
    getTemplateAction(): any;
    render(): any;
}
