import { EventEmitter } from '../../stencil-public-runtime';
export declare class PxTimePicker {
    shrink: boolean;
    displayedValue: string;
    hour: string;
    minute: string;
    _currentDisplayMode: 'time' | 'hour' | 'minute';
    pxTimeSelected: EventEmitter;
    valueChange(): void;
    componentWillLoad(): void;
    _getTableClass(shrink: any): string;
    _onPreviousHour(): void;
    _onPreviousMinute(): void;
    _onNextHour(): void;
    _onNextMinute(): void;
    _selectHour(): void;
    _selectMinute(): void;
    isDisplayModeTime(): boolean;
    isDisplayModeHour(): boolean;
    isDisplayModeMinute(): boolean;
    _onSelectHour(hour: any): void;
    _onSelectMinute(minute: any): void;
    render(): any;
}
