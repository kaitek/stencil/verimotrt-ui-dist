import { h } from "@stencil/core";
export class PxSpinner {
    constructor() {
        this.finished = false;
        this.size = 80;
    }
    render() {
        if (!this.finished) {
            return (h("svg", { width: this.size, height: this.size, viewBox: "0 0 100 100" },
                h("circle", { class: "circle1", cx: "50", cy: "50", r: "45" }),
                h("circle", { class: "circle2", cx: "50", cy: "50", r: "45", transform: "rotate(-45,50,50)" })));
        }
    }
    static get is() { return "px-spinner"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-spinner.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-spinner.css"]
    }; }
    static get properties() { return {
        "finished": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "finished",
            "reflect": false,
            "defaultValue": "false"
        },
        "size": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "size",
            "reflect": false,
            "defaultValue": "80"
        }
    }; }
}
