import { h } from "@stencil/core";
export class PxPercentCircle {
    constructor() {
        this.val = 0;
        this.max = 100;
        this.thickness = 30;
        this._size = 300;
    }
    async getValue() {
        return this.val;
    }
    async setValue(value) {
        this.val = this._computedVal(value);
        this.max = this._computedMax(this.max);
        this.thickness = this._computedThickness(this._size, this.thickness);
        this._computedPlacement(this._size);
        this._cx = this._computedCoordinate(this._size);
        this._cy = this._computedCoordinate(this._size);
        this._r = this._computedRadius(this._size, this.thickness);
        this._percent = this._computedPercent(this.val, this.max);
        this._arcPath = this._computedArcPath(this._cx, this._cy, this._r, this._percent);
        this.elSVGContainer.querySelector('#arc').setAttribute('d', this._arcPath);
        this.elSVGContainer.querySelector('#label').innerHTML = this._percent.toString();
    }
    componentWillLoad() {
        this.val = this._computedVal(this.val);
        this.max = this._computedMax(this.max);
        this.thickness = this._computedThickness(this._size, this.thickness);
        this._computedPlacement(this._size);
        this._cx = this._computedCoordinate(this._size);
        this._cy = this._computedCoordinate(this._size);
        this._r = this._computedRadius(this._size, this.thickness);
        this._percent = this._computedPercent(this.val, this.max);
        this._arcPath = this._computedArcPath(this._cx, this._cy, this._r, this._percent);
    }
    componentDidLoad() {
        this._setViewBoxAttribute(this._size);
    }
    /**
     * Calculated spacing and font sizes based on the size of the component
     * @method _computedPlacement
     * @param size
     */
    _computedPlacement(size) {
        this._p_tf = size * 0.3 + 'px';
        this._p_tx = size * 0.01;
        this._p_ty = size * 0.09;
        this._p_pf = size * 0.2 + 'px';
        this._p_px = size * (-0.05);
        this._p_py = 0;
        return;
        //return {
        //  "tf": size*0.3+'px',
        //  "tx": size*0.01,
        //  "ty": size*0.09,
        //  "pf": size*0.2+'px',
        //  "px": size*(-0.05),
        //  "py": 0}
    }
    /**
     * Cleanses invalid inputs by clipping value between 0 and 100.
     * @method _computedVal
     * @param val
     */
    _computedVal(val) {
        val = parseInt(val);
        if ((!val || val < 0) && val !== 0) {
            console.log('Px Percent Circle: invalid value converted to 0%');
            return 0;
        }
        return val;
    }
    /**
     * Cleanses invalid values for the maximum
     * @method _computedMax
     * @param max
     */
    _computedMax(max) {
        max = parseInt(max);
        if (!max || max <= 0) {
            console.log('Px Percent Circle: invalid max, assumed to be 100');
            return 100;
        }
        return max;
    }
    /**
     * Calculates the percent to display, based on the sanitized value and maximum value.
     * @method _computedPercent
     * @param val
     * @param max
     */
    _computedPercent(val, max) {
        if (val > max) {
            console.log('Px Percent Circle: invalid value converted to 100%');
            this.max = 100;
            this.val = 100;
            return 100;
        }
        return Math.round((val / max) * 100);
    }
    /**
     * Sets the view box attribute on the SVG element.
     * @method _setViewBoxAttribute
     * @param size
     */
    _setViewBoxAttribute(size) {
        this.elSVGContainer.setAttribute('viewBox', ['0', '0', size, size].join(' '));
    }
    /**
     * Finds the center of the height/width attributes.
     * @method _computedCoordinate
     * @param val
     */
    _computedCoordinate(val) {
        return val / 2;
    }
    /**
     * Calculates the inner radius to "hollow out" on the donut (50%, minus the thickness).
     * @method _computedRadius
     * @param val
     * @param stroke
     */
    _computedRadius(val, stroke) {
        return (val / 2) - stroke;
    }
    /**
     * Sanitizes the size property so that it's never smaller than 100
     * @method _computedSize
     * @param size
     */
    _computedSize(rect) {
        return Math.min(rect.height, rect.width);
    }
    /**
     * Sanitizes the thickness property so that it's never too large or too small
     * @method _computedThickness
     * @param size
     * @param thickness
     */
    _computedThickness(size, thickness) {
        thickness = parseInt(thickness);
        return (!thickness || thickness < 1 || thickness / size >= .1) ? size * .1 : thickness;
    }
    /**
     * Calculates path for the filled in portion of the donut.
     * @method _computedArcPath
     * @param x
     * @param y
     * @param r
     * @param val
     * @param max
     *
     * Code for this attributed here:
     * http://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle
     */
    _computedArcPath(x, y, r, percent) {
        var delta = (percent / 100), startAngle = 0, endAngle = 360 * delta;
        endAngle = endAngle >= 360 ? 359.99 : endAngle;
        var start = this._polarToCartesian(x, y, r, endAngle), end = this._polarToCartesian(x, y, r, startAngle), arcSweep = endAngle - startAngle <= 180 ? "0" : "1", d = [
            'M', start.x, start.y,
            'A', r, r, 0, arcSweep, 0, end.x, end.y
        ].join(' ');
        return d;
    }
    /**
     * Converts a polar value to Cartesian coordinates.
     * @method _polarToCartesian
     * @param centerX
     * @param centerY
     * @param radius
     * @param angleInDegrees
     */
    _polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;
        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }
    render() {
        return (h("svg", { id: "container", height: "100%", width: "100%", ref: (el) => this.elSVGContainer = el },
            h("circle", { id: "background", cx: this._cx, cy: this._cy, r: this._r, "stroke-width": this.thickness }),
            h("path", { id: "arc", "stroke-width": this.thickness, d: this._arcPath }),
            h("text", { id: "text", x: this._cx, y: this._cy, "font-size": this._p_tf, dx: this._p_tx, dy: this._p_ty },
                h("tspan", { id: "label" }, this._percent),
                h("tspan", { id: "percent", "font-size": this._p_pf, dx: this._p_px, dy: this._p_py }, " %"))));
    }
    static get is() { return "px-percent-circle"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-percent-circle.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-percent-circle.css"]
    }; }
    static get properties() { return {
        "max": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max",
            "reflect": false,
            "defaultValue": "100"
        },
        "thickness": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "thickness",
            "reflect": false,
            "defaultValue": "30"
        }
    }; }
    static get methods() { return {
        "getValue": {
            "complexType": {
                "signature": "() => Promise<number>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<number>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValue": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
}
