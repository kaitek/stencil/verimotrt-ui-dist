import { h } from "@stencil/core";
export class PxTimePicker {
    constructor() {
        this.shrink = false;
        this.displayedValue = '00:00';
        this.hour = '';
        this.minute = '';
        this._currentDisplayMode = 'time';
    }
    valueChange() {
        this.pxTimeSelected.emit({ hour: this.hour, minute: this.minute, time: this.hour + ':' + this.minute });
    }
    componentWillLoad() {
        let _tmp = this.displayedValue.split(':');
        this.hour = _tmp[0];
        this.minute = _tmp[1];
    }
    _getTableClass(shrink) {
        var base = "table table--no-cells text--center ";
        if (!shrink) {
            base += "constantSize ";
        }
        return base;
    }
    _onPreviousHour() {
        let _h = parseInt(this.hour);
        if (_h == 0) {
            _h = 24;
        }
        this.hour = ("0" + --_h).slice(-2);
    }
    _onPreviousMinute() {
        let _m = parseInt(this.minute);
        if (_m == 0) {
            _m = 60;
        }
        this.minute = ("0" + --_m).slice(-2);
    }
    _onNextHour() {
        let _h = parseInt(this.hour);
        if (_h == 23) {
            _h = -1;
        }
        this.hour = ("0" + ++_h).slice(-2);
    }
    _onNextMinute() {
        let _m = parseInt(this.minute);
        if (_m == 59) {
            _m = -1;
        }
        this.minute = ("0" + ++_m).slice(-2);
    }
    _selectHour() {
        this._currentDisplayMode = 'hour';
    }
    _selectMinute() {
        this._currentDisplayMode = 'minute';
    }
    isDisplayModeTime() {
        return this._currentDisplayMode === 'time';
    }
    isDisplayModeHour() {
        return this._currentDisplayMode === 'hour';
    }
    isDisplayModeMinute() {
        return this._currentDisplayMode === 'minute';
    }
    _onSelectHour(hour) {
        this.hour = hour;
        this._currentDisplayMode = 'time';
    }
    _onSelectMinute(minute) {
        this.minute = minute;
        this._currentDisplayMode = 'time';
    }
    render() {
        return (h("div", { class: this._getTableClass(this.shrink), id: "timepicker" },
            this.isDisplayModeTime() ?
                [
                    h("div", { class: "tr" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onNextHour(), class: "btn--calendar-cell btn btn--huge btn--bare" },
                                h("px-icon", { icon: "fa-arrow-up", class: "arrow", size: "large" }))),
                        h("div", { class: "td" }),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onNextMinute(), class: "btn--calendar-cell btn btn--huge btn--bare" },
                                h("px-icon", { icon: "fa-arrow-up", class: "arrow", size: "large" })))),
                    h("div", { class: "tr" },
                        h("div", { class: "th" },
                            h("div", { class: "calendar-cell" },
                                h("button", { class: "btn btn--huge btn--bare btn--calendar-cell", type: "button", value: this.hour, "on-tap": () => this._selectHour() }, this.hour))),
                        h("div", { class: "th" },
                            h("div", { class: "calendar-cell actionable--huge" }, ":")),
                        h("div", { class: "th" },
                            h("div", { class: "calendar-cell" },
                                h("button", { class: "btn btn--huge btn--bare btn--calendar-cell", type: "button", value: this.minute, "on-tap": () => this._selectMinute() }, this.minute)))),
                    h("div", { class: "tr" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onPreviousHour(), class: "btn--calendar-cell btn btn--huge btn--bare" },
                                h("px-icon", { icon: "fa-arrow-down", class: "arrow", size: "large" }))),
                        h("div", { class: "td" }),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onPreviousMinute(), class: "btn--calendar-cell btn btn--huge btn--bare" },
                                h("px-icon", { icon: "fa-arrow-down", class: "arrow", size: "large" }))))
                ]
                : '',
            this.isDisplayModeHour() ?
                [
                    h("div", { class: "trhour" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('00'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "00")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('01'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "01")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('02'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "02")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('03'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "03"))),
                    h("div", { class: "trhour" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('04'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "04")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('05'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "05")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('06'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "06")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('07'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "07"))),
                    h("div", { class: "trhour" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('08'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "08")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('09'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "09")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('10'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "10")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('11'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "11"))),
                    h("div", { class: "trhour" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('12'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "12")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('13'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "13")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('14'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "14")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('15'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "15"))),
                    h("div", { class: "trhour" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('16'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "16")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('17'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "17")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('18'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "18")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('19'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "19"))),
                    h("div", { class: "trhour" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('20'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "20")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('21'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "21")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('22'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "22")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectHour('23'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "23")))
                ]
                : '',
            this.isDisplayModeMinute() ?
                [
                    h("div", { class: "tr" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('00'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "00")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('05'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "05")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('10'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "10")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('15'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "15"))),
                    h("div", { class: "tr" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('20'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "20")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('25'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "25")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('30'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "30")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('35'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "35"))),
                    h("div", { class: "tr" },
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('40'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "40")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('45'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "45")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('50'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "50")),
                        h("div", { class: "td" },
                            h("button", { type: "button", "on-tap": () => this._onSelectMinute('55'), class: "btn--calendar-cell btn btn--huge btn--bare" }, "55")))
                ]
                : ''));
    }
    static get is() { return "px-time-picker"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-time-picker.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-time-picker.css"]
    }; }
    static get properties() { return {
        "shrink": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "shrink",
            "reflect": false,
            "defaultValue": "false"
        },
        "displayedValue": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "displayed-value",
            "reflect": false,
            "defaultValue": "'00:00'"
        }
    }; }
    static get states() { return {
        "hour": {},
        "minute": {},
        "_currentDisplayMode": {}
    }; }
    static get events() { return [{
            "method": "pxTimeSelected",
            "name": "pxTimeSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "hour",
            "methodName": "valueChange"
        }, {
            "propName": "minute",
            "methodName": "valueChange"
        }]; }
}
