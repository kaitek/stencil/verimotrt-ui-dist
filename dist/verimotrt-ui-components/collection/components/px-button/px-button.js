import { h } from "@stencil/core";
export class PxButton {
    constructor() {
        this.text = '';
        this.disabled = false;
    }
    handleClick(event) {
        //alert('Received the button click!');
        this.pxButtonClick.emit(event);
    }
    render() {
        let _size = '';
        switch (this.size) {
            case "small":
                _size = 'btn--small';
                break;
            case "large":
                _size = 'btn--large';
                break;
            case "huge":
                _size = 'btn--huge';
                break;
            case "full":
                _size = 'btn--full';
                break;
            default:
                break;
        }
        let _type = '';
        switch (this.type) {
            case "primary":
                _type = 'btn--primary';
                break;
            case "tertiary":
                _type = 'btn--tertiary';
                break;
            case "call-to-action":
                _type = 'btn--call-to-action';
                break;
            case "bare":
                _type = 'btn--bare';
                break;
            case "bare-primary":
                _type = 'btn--bare--primary';
                break;
            default:
                break;
        }
        let _disabled = '';
        if (this.disabled) {
            _disabled = 'btn--disabled';
        }
        let _class = "btn" + ' ' + _size + ' ' + _type + ' ' + _disabled;
        return (h("button", { onClick: (event) => this.handleClick(event), class: _class }, this.text));
    }
    static get is() { return "px-button"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-button.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-button.css"]
    }; }
    static get properties() { return {
        "text": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "text",
            "reflect": false,
            "defaultValue": "''"
        },
        "size": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'small' | 'large' | 'huge' | 'full'",
                "resolved": "\"full\" | \"huge\" | \"large\" | \"small\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "size",
            "reflect": false
        },
        "type": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'primary' | 'tertiary' | 'call-to-action' | 'bare' | 'bare-primary'",
                "resolved": "\"bare\" | \"bare-primary\" | \"call-to-action\" | \"primary\" | \"tertiary\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "type",
            "reflect": false
        },
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
    static get events() { return [{
            "method": "pxButtonClick",
            "name": "pxButtonClick",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
}
