import { h } from "@stencil/core";
export class PxAlertLabel {
    constructor() {
        this.badge = false;
    }
    _getPoints(type) {
        if (type === 'important') {
            return '16.5,3 32,30 1,30';
        }
        else if (type === 'warning') {
            return '16,0.5 32.5,16 16,32.5, 0.5,16';
        }
        else if (type === 'info' || type === 'information') {
            return '6.6,32.5 26.4,32.5 32.5,13 16,0.5 0.5,13';
        }
        else {
            return '3,3 3,30 30,30 30,3';
        }
    }
    _isCircle(type) {
        return type === 'unknown';
    }
    getTemplate() {
        if (this.badge) {
            if (this._isCircle(this.type)) {
                return (h("svg", { class: "svg-canvas", viewBox: "0 0 33 33" },
                    h("circle", { id: "circle", cx: "16", cy: "16", r: "15" })));
            }
            else {
                return (h("svg", { class: "svg-canvas", viewBox: "0 0 33 33" },
                    h("polygon", { id: "polygon", points: this._getPoints(this.type) })));
            }
        }
    }
    render() {
        let class_span = 'alertlabel ' + this.type + ' ' + this.badge;
        return (h("span", { class: class_span },
            this.getTemplate(),
            h("div", { class: "label" },
                h("span", { class: "label__text" }, this.label))));
    }
    static get is() { return "px-alert-label"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-alert-label.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-alert-label.css"]
    }; }
    static get properties() { return {
        "type": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "type",
            "reflect": false
        },
        "label": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "label",
            "reflect": false
        },
        "badge": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "badge",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
}
