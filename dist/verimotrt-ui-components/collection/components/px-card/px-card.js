import { h } from "@stencil/core";
export class PxCard {
    constructor() {
        this.fullBleed = false;
        this.hideHeader = false;
    }
    getTamplateIcon() {
        if (this.icon) {
            return (h("px-icon", { id: "headericon", icon: this.icon }));
        }
    }
    getTamplateHeader() {
        if (!this.hideHeader) {
            return ([h("header", { class: "flex flex--middle flex--justify" },
                    h("div", { class: "epsilon caps", id: "headerText" },
                        this.getTamplateIcon(),
                        this.headerText),
                    h("div", { id: "actions" },
                        h("slot", { name: "actions" }))),
                h("div", { class: "contents" },
                    h("slot", null))
            ]);
        }
        else {
            return (h("div", { class: "contents" },
                h("slot", null)));
        }
    }
    render() {
        return this.getTamplateHeader();
    }
    static get is() { return "px-card"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-card.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-card.css"]
    }; }
    static get properties() { return {
        "headerText": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "header-text",
            "reflect": false
        },
        "icon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "icon",
            "reflect": false
        },
        "fullBleed": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "full-bleed",
            "reflect": true,
            "defaultValue": "false"
        },
        "hideHeader": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-header",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
}
