import { h } from "@stencil/core";
export class PxIOPort {
    constructor() {
        this.value = 0;
    }
    async setValue(state) {
        if (state === 1) {
            if (!this.elDivContainer.classList.contains('active')) {
                this.elDivContainer.classList.add('active');
                this.value = 1;
                if (this.type === 'input') {
                    this.lastChangeTime = new Date().getTime();
                }
            }
        }
        else {
            if (this.type === 'input') {
                let _x = new Date().getTime();
                if (_x - this.lastChangeTime > 2000) {
                    if (this.elDivContainer.classList.contains('active')) {
                        this.elDivContainer.classList.remove('active');
                        this.value = 0;
                    }
                }
                else {
                    setTimeout(() => {
                        if (this.elDivContainer.classList.contains('active')) {
                            this.elDivContainer.classList.remove('active');
                            this.value = 1;
                        }
                    }, 2000 - (_x - this.lastChangeTime));
                }
            }
            else {
                if (this.elDivContainer.classList.contains('active')) {
                    this.elDivContainer.classList.remove('active');
                    this.value = 0;
                }
            }
        }
    }
    _action() {
        if (this.portName !== '') {
            this.value = (this.elDivContainer.classList.contains('active') ? 1 : 0);
            this.pxIOPortTriggered.emit(this);
        }
    }
    render() {
        const _id = this.type + '-' + this.portName;
        const _cls = "port btn--bare " + this.type;
        return (h("div", { ref: (el) => this.elDivContainer = el, class: _cls, id: _id, onClick: () => this._action() }, this.portName));
    }
    static get is() { return "px-io-port"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["px-io-port.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-io-port.css"]
    }; }
    static get properties() { return {
        "portName": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "port-name",
            "reflect": false
        },
        "type": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "\"input\"|'output'",
                "resolved": "\"input\" | \"output\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "type",
            "reflect": false
        }
    }; }
    static get events() { return [{
            "method": "pxIOPortTriggered",
            "name": "pxIOPortTriggered",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "setValue": {
            "complexType": {
                "signature": "(state: 0 | 1) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
}
