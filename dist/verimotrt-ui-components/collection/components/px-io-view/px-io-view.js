import { h } from "@stencil/core";
export class PxIOView {
    constructor() {
        this.inputs = 8;
        this.outputs = 8;
        this.maxIOCount = 8;
        this.inputPorts = new Array();
        this.outputPorts = new Array();
    }
    async setPort(group, index, state) {
        const _port = this.myElement.shadowRoot.querySelectorAll('.' + group + '-' + index)[0];
        _port.setValue(state);
    }
    componentDidLoad() {
    }
    selectedListener(e) {
        //@@todo: sunucuya event olarak gönderim yapılacak
        const _port = this.myElement.shadowRoot.querySelectorAll('.' + e.detail.elDivContainer.id)[0];
        // console.log(e.detail.elDivContainer.id,_port);
        _port.setValue(e.detail.value === 0 ? 1 : 0);
    }
    getTemplatePorts(type, count) {
        let _arr = new Array();
        for (let i = 0; i < count; i++) {
            _arr.push((i + 1).toString());
        }
        for (let i = 0; i < (this.maxIOCount - count); i++) {
            _arr.push('');
        }
        return (_arr.map((idx) => {
            const _cls = type + '-' + idx;
            return (h("px-io-port", { class: _cls, "port-name": idx, type: type }));
        }));
    }
    render() {
        return (h("div", { id: "ioview_container", class: "flex flex--col flex--left flex--top" },
            h("div", { id: "input_container", class: "io-view-row" },
                h("div", { id: "inputs", class: "flex flex--row flex--justify flex--top" },
                    h("div", { class: "label-view input active" }, "I"),
                    this.getTemplatePorts('input', this.inputs))),
            h("div", { id: "output_container", class: "io-view-row" },
                h("div", { id: "outputs", class: "flex flex--row flex--justify flex--top" },
                    h("div", { class: "label-view output active" }, "O"),
                    this.getTemplatePorts('output', this.outputs)))));
    }
    static get is() { return "px-io-view"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["px-io-view.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-io-view.css"]
    }; }
    static get properties() { return {
        "inputs": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "inputs",
            "reflect": false,
            "defaultValue": "8"
        },
        "outputs": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "outputs",
            "reflect": false,
            "defaultValue": "8"
        }
    }; }
    static get methods() { return {
        "setPort": {
            "complexType": {
                "signature": "(group: \"input\" | \"output\", index: number, state: 0 | 1) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "PxIOPort": {
                        "location": "import",
                        "path": "./px-io-port"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
    static get listeners() { return [{
            "name": "pxIOPortTriggered",
            "method": "selectedListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
