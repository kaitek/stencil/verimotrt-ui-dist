import { h } from "@stencil/core";
import moment from 'moment-timezone';
import 'moment/dist/locale/tr';
export class PxCalendarCell {
    constructor() {
        this.displayDate = {};
        this.displayMode = 'day';
        this.blockDatesAfter = {};
        this.blockDatesBefore = {};
        this.fromMoment = null;
        this.toMoment = null;
        this.blockFutureDates = false;
        this.blockPastDates = false;
        this.timeZone = 'Europe/Istanbul';
        this.isDisabled = null;
        this.isValidDate = null;
    }
    ;
    componentWillLoad() {
        moment.locale('tr');
        moment.tz(moment(), this.timeZone);
    }
    componentDidLoad() {
        this._updateDate();
    }
    _updateNeeded() {
        if (this.displayDate === undefined || this.displayDate === null
            || this.fromMoment === undefined || this.fromMoment === null
            || this.toMoment === undefined || this.toMoment === null
            || this.displayMode === undefined || this.displayMode === null
            || this.blockDatesAfter === undefined || this.blockDatesAfter === null
            || this.blockDatesBefore === undefined || this.blockDatesBefore === null
            || this.blockFutureDates === undefined || this.blockFutureDates === null
            || this.blockPastDates === undefined || this.blockPastDates === null
            || this.displayDate === undefined || this.displayDate === null)
            return;
        this._updateDate();
    }
    _updateDate() {
        if (this.displayDate === undefined || this.fromMoment === undefined || this.toMoment === undefined || this.displayMode === undefined
            || this.blockDatesAfter === undefined || this.blockDatesBefore === undefined || this.blockFutureDates === undefined
            || this.blockPastDates === undefined || this.displayDate === undefined)
            return;
        var _this = this;
        setTimeout(() => {
            //this.debounce('updateDate', function() {
            var cellStyles = 'calendar-cell calendar-cell-' + _this.displayMode, btnStyles = 'btn btn--huge btn--bare', isDisabled = false;
            _this.isValidDate = _this.displayDate.isValid();
            if (!_this.isValidDate) {
                // if the cell is a blank cell of calendar (before/after start of month)
                _this.displayedValue = '';
            }
            else {
                switch (_this.displayMode) {
                    case 'day':
                        _this.displayedValue = _this.displayDate.format('D');
                        break;
                    case 'month':
                        _this.displayedValue = _this.displayDate.format('MMM');
                        break;
                    case 'year':
                        _this.displayedValue = _this.displayDate.format('YYYY');
                        break;
                }
                if ((_this.blockFutureDates && _this._isPastTodaysDate()) ||
                    (_this.blockPastDates && _this._isBeforeTodaysDate()) ||
                    (Object.keys(_this.blockDatesAfter).length > 0 && _this.displayDate.isAfter(_this.blockDatesAfter, 'day')) ||
                    (Object.keys(_this.blockDatesBefore).length > 0 && _this.displayDate.isBefore(_this.blockDatesBefore, 'day'))) {
                    btnStyles += ' btn--disabled';
                    isDisabled = true;
                }
                else {
                    if (_this._isStartOfRange() || _this._isEndOfRange()) {
                        cellStyles += ' is-selected';
                    }
                    if (_this._isSingleSelected()) {
                        cellStyles += ' is-selected-only';
                    }
                    if (_this._isStartOfWeekOrMonth() || _this._isStartOfRange()) {
                        cellStyles += ' is-start';
                    }
                    if (_this._isEndOfWeekOrMonth() || _this._isEndOfRange()) {
                        cellStyles += ' is-end';
                    }
                    if (_this._isWithinRange()) {
                        cellStyles += ' is-between';
                    }
                    if (_this._isTodaysDate()) {
                        cellStyles += ' is-today';
                    }
                }
            }
            _this.btnStyles = btnStyles;
            _this.isDisabled = isDisabled;
            _this.cellStyles = cellStyles;
        }, 1);
    }
    _selectDate() {
        this.pxCellDateSelected.emit(this.displayDate);
    }
    _isStartOfRange() {
        return this.fromMoment && this.displayDate.isSame(this._getStartDate(), this.displayMode);
    }
    _isEndOfRange() {
        return this.toMoment && this.displayDate.isSame(this._getEndDate(), this.displayMode);
    }
    _isWithinRange() {
        return this.fromMoment && this.toMoment &&
            this.displayDate.isBetween(this._getStartDate(), this._getEndDate(), this.displayMode);
    }
    _getStartDate() {
        var startDate = this.fromMoment;
        if (this.toMoment && this.fromMoment.isAfter(this.toMoment, this.displayMode)) {
            startDate = this.toMoment;
        }
        return startDate;
    }
    _getEndDate() {
        var endDate = this.toMoment;
        if (this.fromMoment && this.fromMoment.isAfter(this.toMoment, this.displayMode)) {
            endDate = this.fromMoment;
        }
        return endDate;
    }
    _isSingleSelected() {
        var singleFrom = this.fromMoment && (!this.toMoment || !this.toMoment.isValid())
            && this.displayDate.isSame(this.fromMoment, this.displayMode), singleTo = this.toMoment && (!this.fromMoment || !this.fromMoment.isValid())
            && this.displayDate.isSame(this.toMoment, this.displayMode);
        return singleFrom || singleTo;
    }
    _isStartOfWeekOrMonth() {
        switch (this.displayMode) {
            case 'day':
                return this.displayDate.date() === 1 || this.displayDate.weekday() === 0;
            case 'month':
                return this.displayDate.month() === 0 || this.displayDate.month() === 4 || this.displayDate.month() === 8;
            case 'year':
                return this.displayDate.year() % 10 === 1 || this.displayDate.year() % 10 === 6;
        }
    }
    _isEndOfWeekOrMonth() {
        switch (this.displayMode) {
            case 'day':
                return this.displayDate.date() === this.displayDate.daysInMonth() || this.displayDate.weekday() === 6;
            case 'month':
                return this.displayDate.month() === 3 || this.displayDate.month() === 7 || this.displayDate.month() === 11;
            case 'year':
                return this.displayDate.year() % 10 === 5 || this.displayDate.year() % 10 === 0;
        }
    }
    _isPastTodaysDate() {
        var todaysDate = moment().tz(this.timeZone);
        return this.displayDate.isAfter(todaysDate, 'day');
    }
    _isBeforeTodaysDate() {
        var todaysDate = moment().tz(this.timeZone);
        return this.displayDate.isBefore(todaysDate, 'day');
    }
    _isTodaysDate() {
        var todaysDate = moment().tz(this.timeZone);
        return this.displayDate.isSame(todaysDate, 'day');
    }
    render() {
        let empty = '0';
        let _btn_cls = this.btnStyles + ' btn--calendar-cell btn--calendar-cell-' + this.displayMode;
        return (h("div", { class: this.cellStyles },
            h("button", { class: _btn_cls, type: "button", value: this.displayedValue, "on-tap": () => this._selectDate(), disabled: this.isDisabled, hidden: !this.isValidDate }, this.displayedValue !== '' ? this.displayedValue : empty)));
    }
    static get is() { return "px-calendar-cell"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-calendar-picker.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-calendar-picker.css"]
    }; }
    static get properties() { return {
        "displayDate": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-date",
            "reflect": false,
            "defaultValue": "{}"
        },
        "displayMode": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'time'|'day'|'month'|'year'",
                "resolved": "\"day\" | \"month\" | \"time\" | \"year\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-mode",
            "reflect": false,
            "defaultValue": "'day'"
        },
        "blockDatesAfter": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Object",
                "resolved": "Object",
                "references": {
                    "Object": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "{}"
        },
        "blockDatesBefore": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Object",
                "resolved": "Object",
                "references": {
                    "Object": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "{}"
        },
        "fromMoment": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "from-moment",
            "reflect": false,
            "defaultValue": "null"
        },
        "toMoment": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "to-moment",
            "reflect": false,
            "defaultValue": "null"
        },
        "blockFutureDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-future-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "blockPastDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-past-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        }
    }; }
    static get states() { return {
        "btnStyles": {},
        "cellStyles": {},
        "displayedValue": {},
        "isDisabled": {},
        "isValidDate": {}
    }; }
    static get events() { return [{
            "method": "pxCellDateSelected",
            "name": "pxCellDateSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "displayDate",
            "methodName": "_updateNeeded"
        }, {
            "propName": "fromMoment",
            "methodName": "_updateNeeded"
        }, {
            "propName": "toMoment",
            "methodName": "_updateNeeded"
        }, {
            "propName": "displayMode",
            "methodName": "_updateNeeded"
        }, {
            "propName": "blockDatesAfter",
            "methodName": "_updateNeeded"
        }, {
            "propName": "blockDatesBefore",
            "methodName": "_updateNeeded"
        }, {
            "propName": "blockFutureDates",
            "methodName": "_updateNeeded"
        }, {
            "propName": "blockPastDates",
            "methodName": "_updateNeeded"
        }, {
            "propName": "displayDate",
            "methodName": "_updateDate"
        }]; }
}
