import { h } from "@stencil/core";
import moment from 'moment-timezone';
import 'moment/dist/locale/tr';
export class PxCalendarPicker {
    constructor() {
        this.timeZone = 'Europe/Istanbul';
        this.displayMode = 'day';
        this._currentDisplayMode = null;
        this.preventRangeSelection = false;
        this.hidePreviousButton = false;
        this.hideNextButton = false;
        this.shrink = false;
        this.isAtSelectionLevel = false; //computed: '_isAtSelectionLevel(_currentDisplayMode, displayMode)'
        this._valuesDisplayed = [];
        this.waitToFire = false;
        this._title = '';
        this.fromMoment = null;
        this.toMoment = null;
        this.blockFutureDates = false;
        this.blockPastDates = false;
    }
    componentWillLoad() {
        moment.locale('tr');
        this.baseDate = moment.tz(moment(), this.timeZone);
        this._currentDisplayMode = this.displayMode;
        this.daysOfTheWeek = this._getDaysOfTheWeek();
        this.baseDate = this.baseDate.locale(moment.locale());
        this._renderCurrent();
    }
    componentDidLoad() {
        //this.addEventListener('px-cell-date-selected', handler);
        this.toMoment = null;
        this.fromMoment = null;
    }
    selectedListener(e) {
        this._cellClicked(e);
    }
    async getToday() {
        //return this._preserveTime(this.tempMomentObj, this.moment.tz(Px.moment(), this.timeZone));
        let _x = moment.tz(moment(), this.timeZone);
        this._selectDate(_x);
        this.pxDateSelected.emit(_x);
        return;
    }
    //_localeChanged() {
    //  if (moment && this.baseDate && this._currentDisplayMode) {
    //    this.daysOfTheWeek = this._getDaysOfTheWeek();
    //    //reset base date so it picks up new moment locale
    //    //get moment locale and update the base date
    //    this.baseDate=this.baseDate.locale(moment.locale());
    //    
    //    this._renderCurrent();
    //  }
    //}
    //_timeZoneChanged() {
    //  if(this.baseDate) {
    //    this.baseDate=moment.tz(this.baseDate, this.timeZone);
    //  }
    //}
    _getTableClass(shrink) {
        var base = "table table--no-cells text--center ";
        if (!shrink) {
            base += "constantSize ";
        }
        return base;
    }
    _cellClicked(e) {
        var momentDate = e.detail, copy = e.detail.clone();
        if (this._currentDisplayMode === 'year') {
            if (this.displayMode === 'year') {
                //we want to select this year.
                this._selectDate(momentDate);
                this.pxDateSelected.emit(copy);
            }
            else {
                //go to 'month' mode
                this.baseDate = momentDate;
                this._currentDisplayMode = 'month';
                e.stopPropagation();
            }
        }
        else if (this._currentDisplayMode === 'month') {
            if (this.displayMode === 'month') {
                //we want to select this month.
                this._selectDate(momentDate);
                this.pxDateSelected.emit(copy);
            }
            else {
                //go to 'day' mode
                this.baseDate = momentDate;
                this._currentDisplayMode = 'day';
                e.stopPropagation();
            }
        }
        else {
            this._selectDate(momentDate);
            this.pxDateSelected.emit(copy);
        }
    }
    _displayModeChanged() {
        if (this.displayMode !== undefined) {
            this._currentDisplayMode = this.displayMode;
        }
    }
    _currentDisplayModeChanged() {
        if (this._currentDisplayMode === 'year') {
            //make sure we display the beginning of the decade
            var year = this.baseDate.year();
            for (var i = 0; i < 10; i++) {
                if (((year - i) % 10) === 1) {
                    break;
                }
            }
            this.baseDate = this.baseDate.clone().subtract(i, 'years');
        }
        else {
            this._renderCurrent();
        }
    }
    _isDayMode(_currentDisplayMode) {
        return _currentDisplayMode === 'day';
    }
    /**
     * Change the display mode after the user clicked the title
     */
    _onNextDisplayMode() {
        if (this._currentDisplayMode === 'day') {
            this._currentDisplayMode = 'month';
        }
        else if (this._currentDisplayMode === 'month') {
            this._currentDisplayMode = 'year';
        }
        //else year already, do nothing
    }
    _getCurrentTitle(_currentDisplayMode, baseDate) {
        if (_currentDisplayMode === undefined || baseDate === undefined || baseDate === null) {
            return;
        }
        else if (_currentDisplayMode === 'day') {
            this._title = baseDate.format('MMMM') + ' ' + baseDate.format('YYYY');
        }
        else if (_currentDisplayMode === 'month') {
            this._title = baseDate.format('YYYY');
        }
        else {
            this._title = baseDate.format('YYYY') + ' - ' + baseDate.clone().add(9, 'years').format('YYYY');
        }
    }
    _getDaysOfTheWeek() {
        var day = moment().startOf('week');
        var daysOfWeek = [];
        for (var i = 0; i < 7; i++) {
            daysOfWeek.push(day.format('dd'));
            day.add(1, 'days');
        }
        return daysOfWeek;
    }
    _renderCurrent() {
        var newValues;
        if (this._currentDisplayMode === 'day') {
            newValues = this._constructMonth();
        }
        else if (this._currentDisplayMode === 'month') {
            newValues = this._constructYear();
        }
        else {
            newValues = this._constructYearRange();
        }
        this._valuesDisplayed = newValues;
        this._getCurrentTitle(this._currentDisplayMode, this.baseDate);
    }
    /**
    * This event is fired when the user selects a date range in the calendar & 'preventRangeSelection' is false
    *
    * @event px-calendar-range-selected
    * @param {object} range - {from, to} : the ISO 8601 Strings representing the date/time range selected by the user
    * @param {object} momentObjs - {from, to} : the moment.js objects representing the date/time range selected by the user
    */
    /**
    * This event is fired when the user selects a date in the calendar & 'preventRangeSelection' is true
    *
    * @event px-calendar-selected
    * @param {object} momentObj : the moment.js object representing the date/time selected by the user
    */
    _selectDate(newSelectedDate) {
        if (!this.preventRangeSelection && this.fromMoment && !this.toMoment) {
            // treat the click as the second date - the range is selected!
            if (newSelectedDate.isBefore(this.fromMoment)) {
                //swap
                this.toMoment = this.fromMoment;
                this.fromMoment = this._preserveTime(this.fromMoment, newSelectedDate);
            }
            else {
                this.toMoment = this._preserveTime(this.toMoment, newSelectedDate);
            }
            this.pxCalendarRangeSelected.emit({ momentObjs: { from: this.fromMoment, to: this.toMoment } });
        }
        else {
            this.toMoment = null;
            this.fromMoment = this._preserveTime(this.fromMoment, newSelectedDate);
            if (this.preventRangeSelection) {
                this.pxCalendarSelected.emit({ momentObj: this.fromMoment });
            }
        }
        this._renderCurrent();
    }
    _constructYear() {
        //3 rows of 4 months
        var current = this.baseDate.clone().month(0), months = [];
        for (var i = 0; i < 3; i++) {
            months[i] = [];
            for (var j = 0; j < 4; j++) {
                months[i].push(current);
                current = current.clone().add(1, 'months');
            }
        }
        return months;
    }
    _constructYearRange() {
        //2 rows of 5 years
        var current = this.baseDate.clone(), years = [];
        for (var i = 0; i < 2; i++) {
            years[i] = [];
            for (var j = 0; j < 5; j++) {
                years[i].push(current);
                current = current.clone().add(1, 'years');
            }
        }
        return years;
    }
    _constructMonth() {
        if (this.baseDate) {
            var numberOfDaysInMonth = this.baseDate.daysInMonth(), dayBeginningOfMonth = this.baseDate.clone().startOf('month').day(), numberOfWeeks = 6, // will always have 6 weeks to make calendar not change height
            daysBeforeMonthStartsCount = moment.localeData().firstDayOfWeek(), emptyCellsCount = dayBeginningOfMonth - daysBeforeMonthStartsCount, date = 1, month = [];
            //make sure we have enough space to display the first day
            if (emptyCellsCount < 0) {
                emptyCellsCount += 7;
            }
            var week = [];
            for (var j = 0; j < emptyCellsCount; j++) {
                week.push(moment(null)); // push empty cells on before the 1st of the month
            }
            for (var weekCount = 0; weekCount < numberOfWeeks; weekCount++) {
                while (week.length < 7) {
                    if (date <= numberOfDaysInMonth) {
                        week.push(this.baseDate.clone().date(date));
                        date++;
                    }
                    else {
                        week.push(moment(null)); // push empty cells on after the last day of the month (to fill the week, extra weeks)
                    }
                }
                month.push(week);
                week = [];
            }
            return month;
        }
    }
    /**
     * Called when clicking the 'next' arrow
     */
    _onNext() {
        //create a new moment object so Polymer knows the property has changed
        var newMoment;
        if (this._currentDisplayMode === 'day') {
            newMoment = moment.tz(this.baseDate, this.timeZone).add(1, 'months');
        }
        else if (this._currentDisplayMode === 'month') {
            newMoment = moment.tz(this.baseDate, this.timeZone).add(1, 'years');
        }
        else {
            newMoment = moment.tz(this.baseDate, this.timeZone).add(10, 'years');
        }
        this.baseDate = newMoment;
        this._renderCurrent();
    }
    /**
     * Called when clicking the 'previous' arrow
     */
    _onPrevious() {
        // create a new moment object so Polymer knows the property has changed
        var newMoment;
        if (this._currentDisplayMode === 'day') {
            newMoment = moment.tz(this.baseDate, this.timeZone).subtract(1, 'months');
        }
        else if (this._currentDisplayMode === 'month') {
            newMoment = moment.tz(this.baseDate, this.timeZone).subtract(1, 'years');
        }
        else {
            newMoment = moment.tz(this.baseDate, this.timeZone).subtract(10, 'years');
        }
        this.baseDate = newMoment;
        this._renderCurrent();
    }
    _isAtSelectionLevel(currentDisplayMode, displayMode) {
        return currentDisplayMode === displayMode;
    }
    _baseDateChanged(newDate) {
        if (newDate === undefined || newDate === null) {
            //make sure baseDate is always defined
            if (this._oldBaseDate) {
                this.baseDate = this._oldBaseDate;
            }
            return;
        }
        else if (!this._oldBaseDate) {
            this._renderCurrent();
        }
        //rerender if we're in day mode and changed month or if we changed year
        else if (this._currentDisplayMode === 'day' &&
            (newDate.isAfter(this._oldBaseDate, 'month') || newDate.isBefore(this._oldBaseDate, 'month'))) {
            this._renderCurrent();
        }
        else if (newDate.isAfter(this._oldBaseDate, 'year') || newDate.isBefore(this._oldBaseDate, 'year')) {
            this._renderCurrent();
        }
        else if (this._oldBaseDate && newDate && newDate.tz() !== this._oldBaseDate.tz()) {
            this._renderCurrent();
        }
        this._oldBaseDate = this.baseDate;
    }
    /**
     * Copies the time in toPreserve to dest and returns dest.
     * used in `px-calendar-picker`, `px-datetime-picker`, and `px-datetime-range-panel`
     *
     * @param {} toPreserve
     * @param {} dest
     */
    _preserveTime(toPreserve, dest) {
        if (toPreserve && dest) {
            dest.hours(toPreserve.hours());
            dest.minutes(toPreserve.minutes());
            dest.seconds(toPreserve.seconds());
            dest.milliseconds(toPreserve.milliseconds());
        }
        return dest;
    }
    getTemplateDayNames() {
        return (this.daysOfTheWeek.map((dayOfTheWeek) => {
            return (h("div", { class: "th caps" }, dayOfTheWeek));
        }));
    }
    getTemplateWeekRow() {
        return (this._valuesDisplayed.map((week) => {
            return (h("div", { class: "tr" }, this.getTemplateDayCell(week)));
        }));
    }
    getTemplateDayCell(week) {
        return (week.map((date) => {
            return (h("div", { class: "td" },
                h("px-calendar-cell", { displayDate: date, fromMoment: this.fromMoment, toMoment: this.toMoment, blockFutureDates: this.blockFutureDates, blockPastDates: this.blockPastDates, displayMode: this._currentDisplayMode, blockDatesAfter: this.maxDate, blockDatesBefore: this.minDate })));
        }));
    }
    render() {
        return (h("div", { class: this._getTableClass(this.shrink), id: "calendar" },
            h("div", { class: "caption" },
                h("button", { type: "button", "on-tap": () => this._onPrevious(), class: "actionable actionable--huge actionable--action title-color nextLastBtn" }, !this.hidePreviousButton ?
                    h("px-icon", { icon: "fa-arrow-left", class: "arrow", size: "large" })
                    : ''),
                h("button", { "on-tap": () => this._onNextDisplayMode(), class: "actionable actionable--huge actionable--action title-color" }, this._title),
                h("button", { id: "btnNextMonth", type: "button", "on-tap": () => this._onNext(), class: "actionable actionable--huge actionable--action title-color nextLastBtn" }, !this.hideNextButton ?
                    h("px-icon", { icon: "fa-arrow-right", class: "arrow", size: "large" })
                    : '')),
            this._isDayMode(this._currentDisplayMode) ?
                h("div", { class: "tr" }, this.getTemplateDayNames())
                : '',
            this.getTemplateWeekRow()));
    }
    static get is() { return "px-calendar-picker"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-calendar-picker.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-calendar-picker.css"]
    }; }
    static get properties() { return {
        "baseDate": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "base-date",
            "reflect": false
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        },
        "displayMode": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'time'|'day'|'month'|'year'",
                "resolved": "\"day\" | \"month\" | \"time\" | \"year\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-mode",
            "reflect": false,
            "defaultValue": "'day'"
        },
        "_currentDisplayMode": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'time'|'day'|'month'|'year'",
                "resolved": "\"day\" | \"month\" | \"time\" | \"year\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_current-display-mode",
            "reflect": false,
            "defaultValue": "null"
        },
        "_oldBaseDate": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_old-base-date",
            "reflect": false
        },
        "preventRangeSelection": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "prevent-range-selection",
            "reflect": false,
            "defaultValue": "false"
        },
        "hidePreviousButton": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-previous-button",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideNextButton": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-next-button",
            "reflect": false,
            "defaultValue": "false"
        },
        "shrink": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "shrink",
            "reflect": false,
            "defaultValue": "false"
        },
        "isAtSelectionLevel": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "is-at-selection-level",
            "reflect": false,
            "defaultValue": "false"
        },
        "_valuesDisplayed": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "number[]",
                "resolved": "number[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "waitToFire": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "wait-to-fire",
            "reflect": false,
            "defaultValue": "false"
        },
        "_title": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_title",
            "reflect": false,
            "defaultValue": "''"
        },
        "fromMoment": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "from-moment",
            "reflect": false,
            "defaultValue": "null"
        },
        "toMoment": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "to-moment",
            "reflect": false,
            "defaultValue": "null"
        },
        "blockFutureDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-future-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "blockPastDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-past-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "maxDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max-date",
            "reflect": false
        },
        "minDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min-date",
            "reflect": false
        }
    }; }
    static get events() { return [{
            "method": "pxDateSelected",
            "name": "pxDateSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxCalendarRangeSelected",
            "name": "pxCalendarRangeSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxCalendarSelected",
            "name": "pxCalendarSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "getToday": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get watchers() { return [{
            "propName": "displayMode",
            "methodName": "_displayModeChanged"
        }, {
            "propName": "_currentDisplayMode",
            "methodName": "_currentDisplayModeChanged"
        }, {
            "propName": "baseDate",
            "methodName": "_baseDateChanged"
        }]; }
    static get listeners() { return [{
            "name": "pxCellDateSelected",
            "method": "selectedListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
