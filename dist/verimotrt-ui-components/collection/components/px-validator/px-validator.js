import { h } from "@stencil/core";
export class PxValidator {
    constructor() {
        this.validators = [];
        this.validationMethod = '';
        this.arguments = [];
    }
    _validationMethodChanged() {
        if (this.validationMethod === undefined && this.multiStepValidation === undefined)
            return;
        if (this.validationMethod && this[this.validationMethod]) {
            this.validators[0] = this[this.validationMethod];
        }
        else if (this.multiStepValidation) {
            this.multiStepValidation.forEach(function (method) {
                this.validators.push(this[method]);
            }.bind(this));
        }
        this.pxValidatorLoaded.emit();
    }
    errorObject() {
        return { passedValidation: false,
            failedValidationFunctionName: '',
            failedValidationErrorMessage: '' };
    }
    isNumber(value) {
        var errObj = { passedValidation: false,
            failedValidationFunctionName: '',
            failedValidationErrorMessage: '' };
        errObj.failedValidationFunctionName = 'isNumber';
        errObj.failedValidationErrorMessage = 'Enter a numeric value';
        errObj.passedValidation = ((value || value === 0) && value.toString() !== "true") ? (+value === +value) : false;
        return errObj;
    }
    alwaysFails() {
        var errObj = { passedValidation: false,
            failedValidationFunctionName: '',
            failedValidationErrorMessage: '' };
        errObj.failedValidationFunctionName = 'alwaysFails';
        errObj.failedValidationErrorMessage = 'Nope';
        errObj.passedValidation = false;
        return errObj;
    }
    isBetween(value, min, max) {
        var errObj = { passedValidation: false,
            failedValidationFunctionName: '',
            failedValidationErrorMessage: '' };
        errObj.failedValidationFunctionName = 'isNumber';
        errObj.failedValidationErrorMessage = 'Enter a number between ' + min + ' and ' + max;
        errObj.passedValidation = (value > min && value < max);
        return errObj;
    }
    componentDidLoad() {
        this._validationMethodChanged();
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "px-validator"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "validators": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "any[]",
                "resolved": "any[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "multiStepValidation": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "validationMethod": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "validation-method",
            "reflect": false,
            "defaultValue": "''"
        },
        "arguments": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "any[]",
                "resolved": "any[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        }
    }; }
    static get events() { return [{
            "method": "pxValidatorLoaded",
            "name": "pxValidatorLoaded",
            "bubbles": true,
            "cancelable": false,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "validationMethod",
            "methodName": "_validationMethodChanged"
        }]; }
}
