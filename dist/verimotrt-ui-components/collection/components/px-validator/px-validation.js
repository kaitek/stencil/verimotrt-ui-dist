import { h } from "@stencil/core";
export class PxValidation {
    validate(value) {
        let t = this;
        var result = { passedValidation: true };
        var validationResult;
        //var args = Array.from(arguments);
        var args = [value];
        this.elements.forEach(function (validatorEl) {
            args = args.concat(validatorEl.arguments);
            validatorEl.validators.every(function (validatorMethod) {
                validationResult = validatorMethod.apply(null, args);
                if (!validationResult.passedValidation) {
                    result = validationResult;
                    //t.pxValidatonStateChange.emit(false);
                    return false;
                }
                //t.pxValidatonStateChange.emit(true);
                return true;
            });
        });
        t.pxValidatonStateChange.emit(result);
        return result;
    }
    componentDidLoad() {
        this.elements = this.element.querySelectorAll('px-validator');
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "px-validation"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "val": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "val",
            "reflect": false
        }
    }; }
    static get events() { return [{
            "method": "pxValidatonStateChange",
            "name": "pxValidatonStateChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get elementRef() { return "element"; }
    static get watchers() { return [{
            "propName": "val",
            "methodName": "validate"
        }]; }
}
