import { h } from "@stencil/core";
export class PxOverlayContent {
    constructor() {
        this._isAttached = false;
        this.hoist = true;
    }
    componentWillLoad() {
        this._content = this._getSlotContent();
        this._isAttached = true;
    }
    componentDidLoad() {
        this._onSlotChangedBound = this._onSlotChange.bind(this);
        //this.elSlot.addEventListener('slotchange', this._onSlotChangedBound);
        this._hoistPropChanged(this.hoist);
    }
    componentDidUnload() {
        this._isAttached = false;
        if (this.elContainer) {
            this.elContainer.detachContent(this);
        }
    }
    hoistOverlay() {
        this.pxOverlayAttachmentRequest.emit();
        // If no container responded, create one
        //if (!event.defaultPrevented) {
        var elem = document.createElement('px-overlay-container');
        if (this.containerType) {
            elem.setAttribute('container-type', this.containerType);
        }
        document.body.appendChild(elem);
        //Polymer.RenderStatus.afterNextRender(elem, function() {
        //  this.dispatchEvent(event);
        //}.bind(this));
        //}
    }
    _hoistPropChanged(hoist) {
        if (!this._isAttached) {
            return;
        }
        if (hoist && !this.elContainer) {
            this.hoistOverlay();
        }
        if (!hoist && this.elContainer) {
            this.elContainer.detachContent(this);
            this.elContainer = undefined;
            this._content.forEach(function (elem) {
                this.appendChild(elem);
            }.bind(this));
        }
    }
    _onSlotChange() {
        if (!this.hoist || !this.elContainer) {
            return;
        }
        var newContent = this._getSlotContent();
        if (newContent.length) {
            for (var i = 0; i < newContent.length; i++) {
                this._content.push(newContent[i]);
            }
            // Array.prototype.push.apply(this._content,newContent);
            this.elContainer.appendNewContent(this, newContent);
        }
    }
    _getSlotContent() {
        var content = []; /*Polymer.FlattenedNodesObserver ?
            Polymer.FlattenedNodesObserver.getFlattenedNodes(this) :
            Polymer.dom(this).getEffectiveChildNodes();*/
        return content.filter(function (n) {
            return n.nodeType === Node.ELEMENT_NODE;
        });
    }
    removeChild(elem) {
        // This is fine...
        // Overwrite the native method so as to not confuse people.
        // There is nothing that can possibly go wrong with this
        if (!elem) {
            return;
        }
        // if not hoisted, rely on normal removeChild to remove it locally
        if (!this.hoist) {
            var f = Element.prototype.removeChild;
            f.apply(this, arguments);
            return;
        }
        var i = this._content.indexOf(elem);
        // if hoisted and something went wrong :-/
        if (i === -1) {
            console.warn('This child should have been hoisted but was not... Trying to remove locally.');
            var f = Element.prototype.removeChild;
            f.apply(this, arguments);
            return;
        }
        this._content.splice(i, 1);
        this.elContainer.removeElem(this, elem);
    }
    render() {
        return (h("slot", null));
    }
    static get is() { return "px-overlay-content"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-overlay-content.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-overlay-content.css"]
    }; }
    static get properties() { return {
        "containerType": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "container-type",
            "reflect": false
        },
        "_content": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Element[]",
                "resolved": "Element[]",
                "references": {
                    "Element": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "eventNames": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "_isAttached": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_is-attached",
            "reflect": false,
            "defaultValue": "false"
        },
        "hoist": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hoist",
            "reflect": false,
            "defaultValue": "true"
        }
    }; }
    static get events() { return [{
            "method": "pxOverlayAttachmentRequest",
            "name": "pxOverlayAttachmentRequest",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
}
