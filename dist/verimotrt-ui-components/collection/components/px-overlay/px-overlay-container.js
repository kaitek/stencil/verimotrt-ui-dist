import { h } from "@stencil/core";
export class PxOverlayContainer {
    componentDidLoad() {
        this._attachmentRequestBound = this._attachmentRequest.bind(this);
        this._attachedOverlays = new Map();
        this.elParent = this._getHostRoot();
        this.elParent.addEventListener('pxOverlayAttachmentRequest', this._attachmentRequestBound);
    }
    componentDidUnload() {
        this.elParent.removeEventListener('pxOverlayAttachmentRequest', this._attachmentRequestBound);
        this.elParent = null;
        this._attachedOverlays.forEach(function (val, key) {
            val.content.forEach(function (child) {
                val.localContainer.removeChild(child);
            });
            key.eventNames.forEach(function (eventName) {
                val.localContainer.removeEventListener(eventName, val.fn);
            });
            key._container = undefined;
            key.hoistOverlay();
        });
        this._attachedOverlays.clear();
    }
    _attachmentRequest(evt) {
        var ne = evt;
        var target = ne.rootTarget;
        if (target.containerType !== this.containerType) {
            return;
        }
        // Create a container for our content
        // We need this to attach listeners to so they dont get
        // trampled by other events with the same name
        var elem = document.createElement('div');
        // set up listeners to refire events
        var refireCB = this._refireEvent.bind(this, target);
        target.eventNames.forEach(function (name) {
            elem.addEventListener(name, refireCB);
        });
        // Store references to our overlay target, content, and local container
        // This way we can handle detachment more easily
        var targetData = {
            content: [],
            localContainer: elem,
            fn: refireCB
        };
        this._attachedOverlays.set(target, targetData);
        // store and attach content
        target._content.forEach(function (child) {
            this._attachedOverlays.get(target).content.push(child);
            elem.appendChild(child);
        }.bind(this));
        // tell the source overlay who we are for future detachment
        target._container = this;
        this.elDivOverlayHost.appendChild(elem);
        evt.preventDefault();
        evt.stopPropagation();
    }
    appendNewContent(target, content) {
        if (!this._attachedOverlays.has(target)) {
            console.warn('Overlay container and container have become desynced. Cant add new content.');
            return;
        }
        var data = this._attachedOverlays.get(target);
        content.forEach(function (child) {
            data.content.push(child);
            data.localContainer.appendChild(child);
        });
    }
    detachContent(target) {
        if (!this._attachedOverlays.has(target)) {
            console.warn('something has gone terribly wrong... overlay and container have become desynced.');
            return;
        }
        var info = this._attachedOverlays.get(target);
        info.content.forEach(function (child) {
            info.localContainer.removeChild(child);
        });
        target.eventNames.forEach(function (eventName) {
            info.localContainer.removeEventListener(eventName, info.fn);
        });
        this.elDivOverlayHost.removeChild(info.localContainer);
        this._attachedOverlays.delete(target);
    }
    _getHostRoot() {
        //var n = this;
        //while (n) {
        //  if (n.nodeType === Node.DOCUMENT_FRAGMENT_NODE && n.host) {
        //    return n;
        //  }
        //  n = n.parentNode;
        //}
        return document;
    }
    _refireEvent(target, evt) {
        evt.preventDefault();
        evt.stopPropagation();
        var newEvent = new CustomEvent(evt.type, evt);
        // save previous event data in case it is needed
        //newEvent.sourceEvent = evt;
        target.dispatchEvent(newEvent);
    }
    removeElem(overlay, elem) {
        var content = this._attachedOverlays.get(overlay);
        var i = content.content.indexOf(elem);
        if (i === -1) {
            console.warn('Container could not locate content to remove...');
            return;
        }
        content.content.splice(i, 1);
        content.localContainer.removeChild(elem);
    }
    render() {
        return (h("div", { id: "overlayHost", ref: (el) => this.elDivOverlayHost = el }));
    }
    static get is() { return "px-overlay-container"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-overlay-container.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-overlay-container.css"]
    }; }
    static get properties() { return {
        "containerType": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "container-type",
            "reflect": false
        }
    }; }
}
