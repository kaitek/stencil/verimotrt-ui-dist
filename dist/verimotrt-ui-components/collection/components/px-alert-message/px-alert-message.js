import { h } from "@stencil/core";
export class PxAlertMessage {
    constructor() {
        this.language = 'en';
        this.useKeyIfMissing = true;
        this.resources = {
            value: function () {
                return {
                    'en': {
                        "Show More": "Show More",
                        "OK": "OK",
                        "Open": "Open"
                    }
                };
            }
        };
        this.type = 'information';
        this.autoDismiss = 0;
        this.action = '';
        this.hideBadge = true;
        this.hideSeverity = false;
        this.visible = false;
    }
    //@Watch('visible')
    //_handleVisibleChanged(isVisible: boolean) {
    //    let alert = this.elDivAlert;
    //    if (alert === null) {
    //        return;
    //    }
    //    if (isVisible && !alert.classList.contains('alert-message--visible')) {
    //        alert.classList.add('alert-message--visible');
    //        alert.classList.add('fade-in');
    //        //this.setAutoDismiss(this.autoDismiss || null);
    //    }else if (!isVisible && alert.classList.contains('alert-message--visible')) {
    //        //this.expanded = false;
    //        alert.classList.add('fade-out');
    //    }
    //}
    async getVisible() {
        return this.visible;
    }
    async setVisible(isVisible) {
        let alert = this.elDivAlert;
        if (alert === null) {
            return;
        }
        if (isVisible && !alert.classList.contains('alert-message--visible')) {
            alert.classList.add('alert-message--visible');
            alert.classList.add('fade-in');
            this.visible = true;
            //this.setAutoDismiss(this.autoDismiss || null);
        }
        else if (!isVisible && alert.classList.contains('alert-message--visible')) {
            //this.expanded = false;
            alert.classList.add('fade-out');
            this.visible = false;
        }
    }
    _action() {
        if (this.action.indexOf('http') != -1) {
            window.open(this.action);
        }
        else if (this.action === 'dismiss' || this.action === 'acknowledge') {
            this.setVisible(false);
        }
        this.pxAlertMessageActionTriggered.emit({ action: this.action });
    }
    //setAutoDismiss(dismissAfter) {
    //    if (this._pendingDismissTask) {
    //      // Clear the current timer
    //      this.cancelAsync(this._pendingDismissTask);
    //    }
    //
    //    if (typeof dismissAfter === 'number' && dismissAfter > 0 && this.visible) {
    //      this._pendingDismissTask = this.async(function() {
    //        this._pendingDismissTask = null;
    //        this.visible = false;
    //        this.fire('px-alert-message-action-auto-dismissed');
    //      }, dismissAfter);
    //    }
    //}
    _handleAnimationEnd(event) {
        var target = this.elDivAlert;
        if (event.target && event.target === target) {
            if (target.classList.contains('fade-out')) {
                target.classList.remove('alert-message--visible');
                target.classList.remove('fade-out');
                this.pxAlertMessageAlertHiddenTriggered.emit();
            }
            if (target.classList.contains('fade-in')) {
                target.classList.remove('fade-in');
            }
        }
    }
    getTemplateActionDetail() {
        if (this.action === 'acknowledge') {
            return (h("button", { id: "actionButton", class: "btn btn--tertiary", onClick: () => this._action() }, "Tamam"));
        }
        if (this.action === 'dismiss') {
            return (h("button", { id: "dismissButton", class: "btn btn--bare dismiss", onClick: () => this._action() },
                h("px-icon", { icon: "fa-times" })));
        }
    }
    getTemplateAction() {
        if (this.action !== '') {
            return (h("div", { class: "action" }, this.getTemplateActionDetail()));
        }
    }
    render() {
        return (h("div", { id: "alert", class: 'alert-message shadow-notification flex flex--left flex--stretch', onAnimationEnd: (event) => this._handleAnimationEnd(event), ref: (el) => this.elDivAlert = el },
            h("div", { class: 'severity flex flex--center ' + this.type },
                this.type !== 'custom' && !this.hideBadge ? h("px-alert-label", { id: "icon", type: this.type, label: this.label, badge: true }) : '',
                h("slot", null)),
            h("div", { class: "message-column flex flex--middle" },
                h("div", { class: "message-container" },
                    h("slot", { name: "content" },
                        h("div", { class: 'message ' + this.type, id: "message" },
                            h("span", { class: "title" }, this.messageTitle),
                            h("span", null, this.message))))),
            this.getTemplateAction()));
    }
    static get is() { return "px-alert-message"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-alert-message.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-alert-message.css"]
    }; }
    static get properties() { return {
        "language": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "language",
            "reflect": false,
            "defaultValue": "'en'"
        },
        "useKeyIfMissing": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "use-key-if-missing",
            "reflect": false,
            "defaultValue": "true"
        },
        "resources": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object",
                "resolved": "object",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "{\r\n        value: function() {\r\n            return {\r\n              'en': {\r\n                \"Show More\": \"Show More\",\r\n                \"OK\": \"OK\",\r\n                \"Open\": \"Open\"\r\n              }\r\n            }\r\n          }\r\n    }"
        },
        "type": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'important' | 'warning' | 'error' | 'information' | 'unknown' | 'custom'",
                "resolved": "\"custom\" | \"error\" | \"important\" | \"information\" | \"unknown\" | \"warning\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "type",
            "reflect": true,
            "defaultValue": "'information'"
        },
        "messageTitle": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "message-title",
            "reflect": false
        },
        "message": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "message",
            "reflect": false
        },
        "label": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "label",
            "reflect": false
        },
        "autoDismiss": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "auto-dismiss",
            "reflect": false,
            "defaultValue": "0"
        },
        "action": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'dismiss' | 'acknowledge' | ''",
                "resolved": "\"\" | \"acknowledge\" | \"dismiss\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "action",
            "reflect": true,
            "defaultValue": "''"
        },
        "hideBadge": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-badge",
            "reflect": false,
            "defaultValue": "true"
        },
        "hideSeverity": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-severity",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
    static get events() { return [{
            "method": "pxAlertMessageActionTriggered",
            "name": "pxAlertMessageActionTriggered",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxAlertMessageAlertHiddenTriggered",
            "name": "pxAlertMessageAlertHiddenTriggered",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "getVisible": {
            "complexType": {
                "signature": "() => Promise<boolean>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<boolean>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setVisible": {
            "complexType": {
                "signature": "(isVisible: boolean) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
}
