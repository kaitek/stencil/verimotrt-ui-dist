export class KpiListItem {
    constructor(obj) {
        this.label = obj.label;
        this.value = obj.value;
        this.uom = obj.uom;
    }
}
