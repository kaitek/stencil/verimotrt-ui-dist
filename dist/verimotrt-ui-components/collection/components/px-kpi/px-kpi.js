import { h } from "@stencil/core";
export class PxKpi {
    // @Prop() sparkType:string;
    // @Prop() sparkData:string[];
    getTemplateSparkData() {
        //if(this.sparkData.length>0){
        //    return(<px-vis-spark type={this.sparkType} width="300" height="50" prevent-resize="true" data={this.sparkData}></px-vis-spark>);
        //}
    }
    render() {
        let iconStyle = {
            color: this.statusColor
        };
        return ([
            h("span", { class: "caps secondary epsilon u-mt0 u-mb" }, this.label),
            h("div", { class: "flex u-mb u--mt-" },
                h("span", { class: "alpha float--left u-mv0 u-mr" }, this.value),
                h("div", { class: "flex flex--col u-pt u-pb-" },
                    h("div", { class: "flex__item" },
                        h("px-icon", { id: "statusIcon", icon: this.statusIcon, class: "u-mb-- flex__item", pxstyle: iconStyle }),
                        h("span", { class: "epsilon" }, this.statusLabel)),
                    h("span", { class: "flex--bottom secondary epsilon" }, this.uom))),
            h("div", null, this.getTemplateSparkData())
        ]);
    }
    static get is() { return "px-kpi"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-kpi.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-kpi.css"]
    }; }
    static get properties() { return {
        "label": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "label",
            "reflect": false
        },
        "value": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "value",
            "reflect": false
        },
        "uom": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "uom",
            "reflect": false
        },
        "statusColor": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-color",
            "reflect": false
        },
        "statusIcon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-icon",
            "reflect": false
        },
        "statusLabel": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-label",
            "reflect": false
        }
    }; }
}
