import { h } from "@stencil/core";
export class PxKpiList {
    getTemplateItems() {
        return (this.values.map((item) => {
            return (h("li", { class: "list-ui__item flex flex--row flex--middle u-ph0 u-pv-" },
                h("div", { class: "flex flex--left flex__item" },
                    h("span", { id: "listLabel" }, item.label)),
                h("div", { class: "flex flex--right flex--bottom flex__item" },
                    h("span", { class: "delta list-item" }, item.value),
                    h("span", { class: "caps u-mb-- u-ml-- list-item-unit" }, item.uom))));
        }));
    }
    render() {
        let iconStyle = {
            color: this.statusColor
        };
        return ([
            h("div", { class: "flex flex--row flex--middle u-mv--" },
                h("div", { class: "flex flex--left flex__item" },
                    h("span", { class: "caps epsilon secondary u-mv0" }, this.label)),
                h("div", { class: "flex flex--right flex__item" },
                    h("px-icon", { id: "statusIcon", icon: this.statusIcon, class: "u-mr--", pxstyle: iconStyle }),
                    h("span", { class: "epsilon" }, this.statusLabel))),
            h("ul", { id: "kpiList", class: "list-ui u-mb--" }, this.getTemplateItems()),
            h("span", { id: "listFooter", class: "zeta" }, this.footer)
        ]);
    }
    static get is() { return "px-kpi-list"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-kpi.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-kpi.css"]
    }; }
    static get properties() { return {
        "label": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "label",
            "reflect": false
        },
        "values": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "KpiListItem[]",
                "resolved": "KpiListItem[]",
                "references": {
                    "KpiListItem": {
                        "location": "import",
                        "path": "./px-kpi-listItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "statusColor": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-color",
            "reflect": false
        },
        "statusIcon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-icon",
            "reflect": false
        },
        "statusLabel": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-label",
            "reflect": false
        },
        "footer": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "footer",
            "reflect": false
        }
    }; }
}
