import { h } from "@stencil/core";
import '@polymer/iron-dropdown/iron-dropdown';
export class PxTimePickerContent {
    constructor() {
        this.openTrigger = null;
        this.selectedDate = 'Saat Seçiniz';
    }
    selectedListener(e) {
        this.pxTimePickerSelected.emit(e.detail);
        //this._close();
    }
    _okClicked() {
        this._close();
    }
    triggerAssignedHandler() {
        this.dropdown.positionTarget = this.openTrigger;
    }
    /**
     * Opens the calendar.
     * Called in datetime-picker
     */
    async _open() {
        this.dropdown.open();
    }
    /**
     * Closes the calendar.
     * Called in datetime-picker
     */
    async _close() {
        this.dropdown.close();
    }
    // ********************************************************************** //
    // Calculate iron-dropdown properties
    // ********************************************************************** //
    _getVerticalOffset(fillContainer) {
        return !fillContainer ? 20 : 0;
    }
    _getPositionTarget(fillContainer, fitIntoElement) {
        return fillContainer ? fitIntoElement : this.fieldElem;
    }
    render() {
        return (h("iron-dropdown", { id: "dropdown", ref: (el) => this.dropdown = el, class: "dt-iron-dropdown", opened: this.opened, "vertical-align": "auto", "horizontal-align": "left", noOverlap: !this.fillContainer, dynamicAlign: !this.fillContainer, verticalOffset: this._getVerticalOffset(this.fillContainer), positionTarget: this._getPositionTarget(this.fillContainer, this.fitIntoElement), fitInto: this.fitIntoElement },
            h("div", { slot: "dropdown-content", class: "dt-container__box shadow-temporary" },
                this.fillContainer ?
                    h("div", { class: "dt-p__header shadow-navigation" },
                        h("p", { class: "dt-p__selected-date" }, this.selectedDate))
                    : '',
                h("div", { class: "dt-p-calendar__wrapper" },
                    h("px-time-picker", { id: "time", ref: (el) => this.timePicker = el, displayedValue: "00:00" }),
                    h("button", { class: "actionable caps dt-p-today", "on-tap": () => this._okClicked() }, "Tamam")))));
    }
    static get is() { return "px-time-picker-content"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-time-picker-content.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-time-picker-content.css"]
    }; }
    static get properties() { return {
        "openTrigger": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "PxDatetimeField",
                "resolved": "PxDatetimeField",
                "references": {
                    "PxDatetimeField": {
                        "location": "import",
                        "path": "../px-datetime-field/px-datetime-field"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "null"
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": true
        },
        "fieldIsValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "field-is-valid",
            "reflect": false
        },
        "fillContainer": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "fill-container",
            "reflect": true
        },
        "fitIntoElement": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Object",
                "resolved": "Object",
                "references": {
                    "Object": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "selectedDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected-date",
            "reflect": false,
            "defaultValue": "'Saat Se\u00E7iniz'"
        }
    }; }
    static get events() { return [{
            "method": "pxTimePickerSelected",
            "name": "pxTimePickerSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "_open": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Opens the calendar.\r\nCalled in datetime-picker",
                "tags": []
            }
        },
        "_close": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Closes the calendar.\r\nCalled in datetime-picker",
                "tags": []
            }
        }
    }; }
    static get watchers() { return [{
            "propName": "openTrigger",
            "methodName": "triggerAssignedHandler"
        }]; }
    static get listeners() { return [{
            "name": "pxTimeSelected",
            "method": "selectedListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
