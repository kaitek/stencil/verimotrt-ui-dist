import { h } from "@stencil/core";
import '@polymer/iron-dropdown/iron-dropdown';
export class PxDatePickerContent {
    constructor() {
        this.openTrigger = null;
        this.selectedDate = 'Tarih Seçiniz';
        this.timeZone = 'Europe/Istanbul';
        this.blockFutureDates = false;
        this.blockPastDates = false;
    }
    selectedListener(e) {
        //debugger
        this.pxDatePickerSelected.emit(e.detail);
        this._close();
    }
    async _todayClicked() {
        this.calendar.getToday();
        //this.tempMomentObj= today;
    }
    triggerAssignedHandler() {
        this.dropdown.positionTarget = this.openTrigger;
    }
    /**
     * Opens the calendar.
     * Called in datetime-picker
     */
    async _open() {
        this.dropdown.open();
    }
    /**
     * Closes the calendar.
     * Called in datetime-picker
     */
    async _close() {
        this.dropdown.close();
    }
    /**
     * Sync `selcetedDate` with calendar's moment obj
     */
    _updateSelectedDate(moment) {
        if (this.fillContainer) {
            moment === null ? this.selectedDate = "Select Date" : this.selectedDate = moment.format('YYYY-MM-DD');
        }
    }
    /**
     * If fillContainter is true show buttons
     */
    _handleShowButtons(showButtons, fillContainer) {
        return fillContainer ? true : showButtons;
    }
    // ********************************************************************** //
    // Calculate iron-dropdown properties
    // ********************************************************************** //
    _getVerticalOffset(fillContainer) {
        return !fillContainer ? 20 : 0;
    }
    _getPositionTarget(fillContainer, fitIntoElement) {
        return fillContainer ? fitIntoElement : this.fieldElem;
    }
    render() {
        return (h("iron-dropdown", { id: "dropdown", ref: (el) => this.dropdown = el, class: "dt-iron-dropdown", opened: this.opened, "vertical-align": "auto", "horizontal-align": "left", noOverlap: !this.fillContainer, dynamicAlign: !this.fillContainer, verticalOffset: this._getVerticalOffset(this.fillContainer), positionTarget: this._getPositionTarget(this.fillContainer, this.fitIntoElement), fitInto: this.fitIntoElement },
            h("div", { slot: "dropdown-content", class: "dt-container__box shadow-temporary" },
                this.fillContainer ?
                    h("div", { class: "dt-p__header shadow-navigation" },
                        h("p", { class: "dt-p__selected-date" }, this.selectedDate))
                    : '',
                h("div", { class: "dt-p-calendar__wrapper" },
                    h("px-calendar-picker", { id: "calendar", ref: (el) => this.calendar = el, class: "dt-p-calendar", fromMoment: this.tempMomentObj, baseDate: this.tempMomentObj, 
                        //dayWeekStartIndex="[[dayWeekStartIndex]]"
                        timeZone: this.timeZone, blockFutureDates: this.blockFutureDates, blockPastDates: this.blockPastDates, preventRangeSelection: true, minDate: this.minDate, maxDate: this.maxDate }),
                    h("button", { class: "actionable caps dt-p-today", "on-tap": () => this._todayClicked() }, "Bug\u00FCn")),
                h("div", { class: "dt-p-time-buttons--wrapper" }, this._handleShowButtons(this.showButtons, this.fillContainer) ?
                    h("px-datetime-buttons", { class: "dt-p-buttons", isSubmitButtonValid: true, spaceBetween: this.fillContainer })
                    : ''))));
    }
    static get is() { return "px-date-picker-content"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-date-picker-content.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-date-picker-content.css"]
    }; }
    static get properties() { return {
        "openTrigger": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "PxDatetimeField",
                "resolved": "PxDatetimeField",
                "references": {
                    "PxDatetimeField": {
                        "location": "import",
                        "path": "../px-datetime-field/px-datetime-field"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "null"
        },
        "hideTime": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-time",
            "reflect": false
        },
        "showButtons": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-buttons",
            "reflect": false
        },
        "timeFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-format",
            "reflect": false
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": true
        },
        "fieldIsValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "field-is-valid",
            "reflect": false
        },
        "fillContainer": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "fill-container",
            "reflect": true
        },
        "fitIntoElement": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Object",
                "resolved": "Object",
                "references": {
                    "Object": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "selectedDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected-date",
            "reflect": false,
            "defaultValue": "'Tarih Se\u00E7iniz'"
        },
        "baseDate": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "base-date",
            "reflect": false
        },
        "tempMomentObj": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "temp-moment-obj",
            "reflect": false
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        },
        "maxDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max-date",
            "reflect": false
        },
        "minDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min-date",
            "reflect": false
        },
        "blockFutureDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-future-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "blockPastDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-past-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "containerType": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "container-type",
            "reflect": false
        }
    }; }
    static get events() { return [{
            "method": "pxDatePickerSelected",
            "name": "pxDatePickerSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "_open": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Opens the calendar.\r\nCalled in datetime-picker",
                "tags": []
            }
        },
        "_close": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Closes the calendar.\r\nCalled in datetime-picker",
                "tags": []
            }
        }
    }; }
    static get watchers() { return [{
            "propName": "openTrigger",
            "methodName": "triggerAssignedHandler"
        }, {
            "propName": "tempMomentObj",
            "methodName": "_updateSelectedDate"
        }]; }
    static get listeners() { return [{
            "name": "pxDateSelected",
            "method": "selectedListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
