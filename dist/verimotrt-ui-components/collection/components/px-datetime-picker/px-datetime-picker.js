import { h } from "@stencil/core";
export class PxDatetimePicker {
    constructor() {
        this.showTimeZone = 'none';
        this.hideTime = false;
        this.dateFormat = 'YYYY-MM-DD';
        this.timeFormat = 'HH:mm';
        this.required = false;
        this.opened = false;
        this.fillContainer = false;
        this.fitIntoElement = window;
        this.collapsed = false; //notify: true,readOnly: true
        this.useKeyIfMissing = true;
        this.hoist = false;
        this.blockFutureDates = false;
        this.blockPastDates = false;
        this.timeZone = 'Europe/Istanbul';
        this.momentObj = null;
    }
    componentDidLoad() {
        //this.$.field.addEventListener('moment-obj-changed', this._tempMomentChanged.bind(this, 'field'));
        //this.$.contentDatePicker.addEventListener('temp-moment-obj-changed', this._tempMomentChanged.bind(this, 'contentDatePicker'));
        this.contentDatePicker.openTrigger = this.field;
        this.contentTimePicker.openTrigger = this.field;
    }
    pickerValueChange(e) {
        //debugger
        this.momentObj = e.detail;
        this.pxDateTimePickerChanged.emit(e.detail);
        //console.log('pickerValueChange',e.detail.format('YYYY-MM-DD HH:mm:ss'));
    }
    selectedDatePickerListener(e) {
        //debugger
        this.field.setDate(e.detail);
    }
    selectedTimePickerListener(e) {
        this.field.setTime(e.detail);
    }
    //focus() {
    //  this.field.focus();
    //}
    /**
     * Rolls back temp moment to the momentObj.
     * Closes panel
     */
    //_onEsc() {
    //  this._rollbackTempMoment();
    //  this.contentDatePicker._close();
    //}
    /**
     * Runs validation
     */
    //_onEnter() {
    //  this._validateCalendarMoment();
    //}
    /**
     * If the calendar is open and the dropdown is selected, close the calendar
     */
    _handleTap(evt) {
        if (this.opened === true) {
            var path = evt.path;
            for (var i = 0; i < path.length; i++) {
                if (path[i].nodeName === 'PX-DROPDOWN') {
                    this.contentDatePicker._close();
                    return;
                }
            }
        }
    }
    _applyTempMoment() {
        if (this.momentObj === null || this._tempMomentObj === null || this.momentObj.toISOString() !== this._tempMomentObj.toISOString()) {
            this.momentObj = this._tempMomentObj;
        }
    }
    /**
     * Ran when the momentObjs for field or content have been updated.
     * If it is from the field, then apply the momentObj.
     * If it is from the content and we have no buttons, then apply the momentObj.
     * Else, wait for button events.
     */
    //_tempMomentChanged(origin) {
    // this debounce is critical for Polymer 1. We are just putting this
    // call on top of the stack because of how the binding flows:
    // change in calendar => binding updates field moment => field
    // moment fires change event => binding done => calendar fires
    // change event.
    // we are therefore interested in the latest update, hence the
    // debounce. no need for an actual timing since this will all be
    // sequential.
    // In Polymer 2 in this scenario only calendar will fire a change
    // event so no problems
    //this.debounce('_tempMomentChanged', function() {
    //    if(origin === 'field') {
    //      this._applyTempMoment();
    //    } else if (origin === 'contentDatePicker' && !this.showButtons) {
    //      this._applyTempMoment();
    //      this.contentDatePicker._close();
    //    }
    //} 0);
    //}
    /**
     * Apply temp moment if the dropdown is opened and
     * the field is valid
     */
    //_validateCalendarMoment() {
    //  if(this.opened) {
    //    if(this.field.isValid) {
    //      this._applyTempMoment();
    //      this.contentDatePicker._close();
    //    } else {
    //      //don't close, can't apply if not valid
    //    }
    //  }
    //}
    /**
     * submit: validate values
     * cancel: rolls back temp moment to the momentObj &closes panel
     */
    //@Listen('pxDatetimeButtonClicked')
    //_buttonClicked(evt) {
    //  if(evt.detail.action) {
    //   this._validateCalendarMoment();
    //  } else {
    //    this._rollbackTempMoment();
    //    this.contentDatePicker._close();
    //  }
    //}
    /**
     * Opens content
     */
    _iconClicked(evt) {
        if (evt.detail.dateOrTime === "Date") {
            this.contentDatePicker._open();
        }
        if (evt.detail.dateOrTime === "Time" && this.momentObj !== null) {
            this.contentTimePicker._open();
        }
    }
    /**
     * If opened is false and showButtons true
     * then cancel selection
     */
    _processClose(newVal, oldVal) {
        if (oldVal === undefined || newVal) {
            return;
        }
        if (newVal === false && this.showButtons) {
            this._rollbackTempMoment();
        }
    }
    // ********************************************************************** //
    // Collapse functions
    // ********************************************************************** //
    /**
     * Return the collapseAt value with px after it
     */
    _getCollapseQuery(collapseAt) {
        if (typeof collapseAt === 'number') {
            return collapseAt + 'px';
        }
        if (typeof collapseAt === 'string' && collapseAt.slice(-2) === 'px' || !isNaN(parseInt(collapseAt))) {
            return collapseAt;
        }
    }
    /**
     * Double checks that the collapseAt value is valid
     */
    _collapseQueryIsValid(query) {
        if (typeof query === 'number') {
            return true;
        }
        if (typeof query === 'string' && query.slice(-2) === 'px' && !isNaN(parseInt(query))) {
            return true;
        }
        if (query === null) {
            this.collapseAt = 0;
            return true;
        }
        return false;
    }
    /**
     * If the window is smaller than the `collapseAt` value
     * set panel to fill the whole window
     *
     * If the window is larger than the `collapseAt` value
     * reset `_fillContainer` & `_fitIntoElement` to their
     * original values
     */
    _handleBelowCollapseSizeChanged(newVal /*, oldVal*/) {
        if (newVal) {
            this._fillContainer = true;
            this._fitIntoElement = window;
            //this._setCollapsed(true);
        }
        if (!newVal) {
            this._fillContainer = this.fillContainer;
            this._fitIntoElement = this.fitIntoElement;
            //this._setCollapsed(false);
        }
    }
    // ********************************************************************** //
    // iron-dropdown functions
    // ********************************************************************** //
    /**
     * Sync external fillConatiner with internal _fillConatiner
     */
    _syncFillContainer(newVal /*, oldVal*/) {
        newVal ? this._fillContainer = true : this._fillContainer = false;
    }
    /**
     * Sync external fitIntoElement with internal _fitIntoElement.
     * Make sure `_fitIntoElement` isn't over written if
     * `_belowCollapseSize` is true
     */
    _syncFitIntoElement(newVal /*, oldVal*/) {
        if (!this._belowCollapseSize) {
            this._fitIntoElement = newVal;
        }
    }
    /**
     * Return field node
     */
    _returnField() {
        return this.field;
    }
    _rollbackTempMoment() {
        this._tempMomentObj = this.momentObj;
    }
    getTemplateMediaQuery() {
        if (this._collapseQueryIsValid(this.collapseAt)) {
            let _queryStyle = "(max-width: " + this._getCollapseQuery(this.collapseAt) + ")";
            return (h("iron-media-query", { query: _queryStyle, "query-matches": this._belowCollapseSize }));
        }
    }
    render() {
        return ([
            this.getTemplateMediaQuery(),
            h("px-datetime-field", { id: "field", ref: (el) => this.field = el, slot: "dropdown-trigger", momentObj: this._tempMomentObj, dateFormat: this.dateFormat, timeFormat: this.timeFormat, hideTime: this.hideTime, timeZone: this.timeZone, showTimeZone: this.showTimeZone, "is-selected": this.opened, blockFutureDates: this.blockFutureDates, blockPastDates: this.blockPastDates, minDate: this.minDate, maxDate: this.maxDate, required: this.required, isValid: this.fieldIsValid, "prevent-notification-on-change": true, "prevent-cell-focus": this._fillContainer, 
                //resources="[[resources]]"
                //language="[[language]]"
                //formats="[[formats]]"
                hoist: this.hoist, containerType: this.containerType }),
            h("px-overlay-content", { hoist: this.hoist, containerType: this.containerType },
                h("px-date-picker-content", { id: "contentDatePicker", ref: (el) => this.contentDatePicker = el, opened: this.opened, tempMomentObj: this._tempMomentObj, timeFormat: this.timeFormat, hideTime: this.hideTime, timeZone: this.timeZone, showButtons: this.showButtons, baseDate: this._tempMomentObj, 
                    //day-week-start-index="[[dayWeekStartIndex]]"
                    blockFutureDates: this.blockFutureDates, blockPastDates: this.blockPastDates, minDate: this.minDate, maxDate: this.maxDate, fieldIsValid: this.fieldIsValid, fillContainer: this._fillContainer, fitIntoElement: this._fitIntoElement })),
            h("px-overlay-content", { hoist: this.hoist, containerType: this.containerType },
                h("px-time-picker-content", { id: "contentTimePicker", ref: (el) => this.contentTimePicker = el, opened: this.opened, fieldIsValid: this.fieldIsValid, fillContainer: this._fillContainer, fitIntoElement: this._fitIntoElement }))
        ]);
    }
    static get is() { return "px-datetime-picker"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-datetime-picker.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-datetime-picker.css"]
    }; }
    static get properties() { return {
        "showTimeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'none'|'dropdown'|'extendedDropdown'|'text'|'abbreviatedText'",
                "resolved": "\"abbreviatedText\" | \"dropdown\" | \"extendedDropdown\" | \"none\" | \"text\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-time-zone",
            "reflect": false,
            "defaultValue": "'none'"
        },
        "hideTime": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-time",
            "reflect": false,
            "defaultValue": "false"
        },
        "dateFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "date-format",
            "reflect": false,
            "defaultValue": "'YYYY-MM-DD'"
        },
        "timeFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-format",
            "reflect": false,
            "defaultValue": "'HH:mm'"
        },
        "required": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "required",
            "reflect": false,
            "defaultValue": "false"
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": true,
            "defaultValue": "false"
        },
        "fieldIsValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "field-is-valid",
            "reflect": false
        },
        "fillContainer": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "fill-container",
            "reflect": false,
            "defaultValue": "false"
        },
        "_fillContainer": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_fill-container",
            "reflect": true
        },
        "fitIntoElement": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Object",
                "resolved": "Object",
                "references": {
                    "Object": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "window"
        },
        "_fitIntoElement": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "Object",
                "resolved": "Object",
                "references": {
                    "Object": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "collapseAt": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "collapse-at",
            "reflect": false
        },
        "collapsed": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "collapsed",
            "reflect": false,
            "defaultValue": "false"
        },
        "_belowCollapseSize": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_below-collapse-size",
            "reflect": false
        },
        "useKeyIfMissing": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "use-key-if-missing",
            "reflect": false,
            "defaultValue": "true"
        },
        "hoist": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hoist",
            "reflect": false,
            "defaultValue": "false"
        },
        "maxDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max-date",
            "reflect": false
        },
        "minDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min-date",
            "reflect": false
        },
        "blockFutureDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-future-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "blockPastDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-past-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "showButtons": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-buttons",
            "reflect": false
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        },
        "momentObj": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "moment-obj",
            "reflect": false,
            "defaultValue": "null"
        },
        "_tempMomentObj": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_temp-moment-obj",
            "reflect": false
        },
        "containerType": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "container-type",
            "reflect": false
        }
    }; }
    static get events() { return [{
            "method": "pxDateTimePickerChanged",
            "name": "pxDateTimePickerChanged",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "opened",
            "methodName": "_processClose"
        }, {
            "propName": "_belowCollapseSize",
            "methodName": "_handleBelowCollapseSizeChanged"
        }, {
            "propName": "fillContainer",
            "methodName": "_syncFillContainer"
        }, {
            "propName": "fitIntoElement",
            "methodName": "_syncFitIntoElement"
        }]; }
    static get listeners() { return [{
            "name": "pxMomentChanged",
            "method": "pickerValueChange",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDatePickerSelected",
            "method": "selectedDatePickerListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxTimePickerSelected",
            "method": "selectedTimePickerListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "tap",
            "method": "_handleTap",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDatetimeEntryIconClicked",
            "method": "_iconClicked",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
