import { h } from "@stencil/core";
export class PxProgressBar {
    constructor() {
        this.value = 0;
        this.infinite = false;
        this.min = 0;
        this.max = 100;
    }
    async getValue() {
        return this.value;
    }
    async setValue(value) {
        this.value = value;
        this.__calculate();
    }
    componentDidLoad() {
        this.myElement.setAttribute('role', 'progressbar');
        this._setMinAriaLabel(this.min, null);
        this._setMaxAriaLabel(this.max, null);
        setTimeout(() => {
            this.__calculate();
        }, 100);
    }
    _setMinAriaLabel(newMin, oldMin) {
        if (typeof newMin !== 'undefined' && newMin !== oldMin) {
            this.myElement.setAttribute('aria-valuemin', newMin);
        }
    }
    _setMaxAriaLabel(newMax, oldMax) {
        if (typeof newMax !== 'undefined' && newMax !== oldMax) {
            this.myElement.setAttribute('aria-valuemax', newMax);
        }
    }
    _changeHandler() {
        this.__calculate();
    }
    __calculate() {
        let _r = this._computeRatio(this.value, this.min, this.max);
        this._updateProgress(_r, this._ratio);
        this._ratio = _r;
    }
    /**
     * Function to transform the filled portion of the progress bar.
     */
    _updateProgress(newRatio, oldRatio) {
        if (typeof newRatio !== 'undefined' && newRatio !== oldRatio) {
            let fillEl = this.myElement.shadowRoot.querySelector('#fill');
            if (fillEl) {
                fillEl.setAttribute('style', 'transform:scaleX(' + newRatio + ')');
            }
            this.myElement.setAttribute('aria-valuenow', this.value.toString());
        }
    }
    /**
     * Function to compute the ratio based on the value.
     * Also clips values that are out of range (0 to 100).
     */
    _computeRatio(value, min, max) {
        if (typeof value === 'undefined' || typeof min === 'undefined' || typeof max === 'undefined')
            return;
        if (isNaN(value) || isNaN(min) || isNaN(max) || value < min) {
            return 0;
        }
        if (value > max) {
            return 1;
        }
        return (value - min) / (max - min);
    }
    render() {
        return (h("div", { id: "background", class: "background" },
            h("div", { id: "fill", class: "fill" })));
    }
    static get is() { return "px-progress-bar"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-progress-bar.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-progress-bar.css"]
    }; }
    static get properties() { return {
        "infinite": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "infinite",
            "reflect": true,
            "defaultValue": "false"
        },
        "min": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min",
            "reflect": false,
            "defaultValue": "0"
        },
        "max": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max",
            "reflect": false,
            "defaultValue": "100"
        },
        "_ratio": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_ratio",
            "reflect": false
        }
    }; }
    static get methods() { return {
        "getValue": {
            "complexType": {
                "signature": "() => Promise<number>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<number>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValue": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "min",
            "methodName": "_setMinAriaLabel"
        }, {
            "propName": "max",
            "methodName": "_setMaxAriaLabel"
        }, {
            "propName": "min",
            "methodName": "_changeHandler"
        }, {
            "propName": "max",
            "methodName": "_changeHandler"
        }]; }
}
