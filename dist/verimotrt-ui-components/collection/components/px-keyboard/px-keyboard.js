import { h } from "@stencil/core";
import { keyEvent } from './keyEvent';
export class PxKeyboard {
    constructor() {
        this.layouts = [
            { name: "numpad", data: [
                    [
                        ['7'],
                        ['8'],
                        ['9']
                    ], [
                        ['4'],
                        ['5'],
                        ['6']
                    ], [
                        ['1'],
                        ['2'],
                        ['3']
                    ], [
                        //{code:8, iconCls:'fa fa-arrow-left', grow: 2 },
                        { code: -1, iconCls: 'fa fa-times', cls: 'red' },
                        ['0'],
                        { code: 13, iconCls: 'fa fa-check', cls: 'green', grow: 2 }
                    ]
                ] },
            { name: "numpadex", data: [
                    [
                        ['7'],
                        ['8'],
                        ['9'],
                        { code: 8, iconCls: 'fa fa-arrow-left' }
                    ], [
                        ['4'],
                        ['5'],
                        ['6'],
                        ['.']
                    ], [
                        ['1'],
                        ['2'],
                        ['3'],
                        [',']
                    ], [
                        { code: -1, iconCls: 'fa fa-times', cls: 'red' },
                        ['0'],
                        { code: 13, iconCls: 'fa fa-check', cls: 'green' },
                        ['']
                    ]
                ] },
            { name: "tr_TR", data: [
                    [
                        ['@', '"'],
                        ['1', '!'],
                        ['2', '\''],
                        ['3', '#'],
                        ['4', '$'],
                        ['5', '%'],
                        ['6', '&'],
                        ['7', '/'],
                        ['8', '('],
                        ['9', ')'],
                        ['0', '='],
                        ['*', '?'],
                        ['-', '_'],
                        { code: 8, iconCls: 'fa fa-backspace', text: 'backspace', grow: 4 }
                    ], [
                        { code: 9, iconCls: 'fa fa-long-arrow-alt-right', text: 'tab', grow: 3 },
                        ['q', 'Q'],
                        ['w', 'W'],
                        ['e', 'E'],
                        ['r', 'R'],
                        ['t', 'T'],
                        ['y', 'Y'],
                        ['u', 'U'],
                        ['ı', 'I'],
                        ['o', 'O'],
                        ['p', 'P'],
                        ['ğ', 'Ğ'],
                        ['ü', 'Ü'],
                        [',', ';']
                    ], [
                        { code: 20, iconCls: 'fa fa-arrow-alt-circle-up', text: 'caps', grow: 3 },
                        ['a', 'A'],
                        ['s', 'S'],
                        ['d', 'D'],
                        ['f', 'F'],
                        ['g', 'G'],
                        ['h', 'H'],
                        ['j', 'J'],
                        ['k', 'K'],
                        ['l', 'L'],
                        ['ş', 'Ş'],
                        ['i', 'İ'],
                        { code: 13, iconCls: 'fa fa-check', text: 'enter', cls: 'green', grow: 4 }
                    ], [
                        { code: 16, iconCls: 'fa fa-arrow-up', text: 'shift', grow: 2 },
                        ['<', '>'],
                        ['z', 'Z'],
                        ['x', 'X'],
                        ['c', 'C'],
                        ['v', 'V'],
                        ['b', 'B'],
                        ['n', 'N'],
                        ['m', 'M'],
                        ['ö', 'Ö'],
                        ['ç', 'Ç'],
                        ['.', ':']
                    ], [
                        { code: 32, iconCls: ' ', text: '', grow: 1 }
                    ]
                ] }
        ];
        this.currentTarget = null;
        this.mask = null;
        this.caps = false;
        this.masked = false;
        this.shift = false;
        this.visible = false;
    }
    componentWillLoad() {
        let t = this;
        t.items = t.myElement.parentElement.querySelectorAll('input[data-kb]');
        t.items.forEach(function (item) {
            item.addEventListener('blur', () => {
                t.onBlur();
            });
            item.addEventListener('focus', (el) => {
                t.onFocus(el);
            });
        });
    }
    disconnectedCallback() {
        let t = this;
        t.items.forEach(function (item) {
            item.removeEventListener('blur');
            item.removeEventListener('focus');
        });
    }
    onBlur( /*e:Event*/) {
        this.closeAllKeyboards();
        //console.log('blur',e.target,(e.target as HTMLElement).getAttribute('data-kb-style'));
    }
    onFocus(e) {
        let t = this;
        t.closeAllKeyboards();
        //console.log('focus',e.target,(e.target as HTMLElement).getAttribute('data-kb-style'));
        t.currentTarget = e.target;
        t.myElement.shadowRoot.querySelector('.kb-' + e.target.getAttribute('data-kb-style')).classList.add("visible");
        //if((e.target as HTMLElement).getAttribute('data-mask')!==null){ 
        //  t.masked=true;
        //  t.mask=(e.target as HTMLElement).getAttribute('data-mask');
        //}else{
        //  t.masked=false;
        //  t.mask=null;
        //}
    }
    closeAllKeyboards() {
        let t = this;
        let _sr = t.myElement.shadowRoot;
        t.currentTarget = null;
        _sr.querySelector('.key-caps').classList.remove("on");
        _sr.querySelector('.key-shift').classList.remove("on");
        let items = _sr.querySelectorAll('.visible');
        items.forEach(function (item) {
            item.classList.remove("caps");
            item.classList.remove("shift");
            item.classList.remove("visible");
        });
        t.caps = false;
        t.shift = false;
    }
    _backspace() {
        let t = this;
        let _ct = t.currentTarget;
        keyEvent.simulate(0, 8, t._getModifiers(), _ct);
        _ct.value = _ct.value.slice(0, -1);
    }
    _clear() {
        this.currentTarget.value = '';
    }
    _getModifiers() {
        return ['km.keyboard', this.shift ? 'shift' : ''];
    }
    _insertCharAtCaret(c) {
        let t = this;
        let _ct = t.currentTarget;
        //console.log(_ct.value,c);
        let maxchar = false;
        if (t.currentTarget.getAttribute("maxchar")) {
            if (_ct.value.length >= t.currentTarget.getAttribute("maxchar")) {
                maxchar = true;
            }
        }
        if (t.currentTarget.getAttribute("maxlength")) {
            if (_ct.value.length >= t.currentTarget.getAttribute("maxlength")) {
                maxchar = true;
            }
        }
        if (maxchar) {
            return false;
        }
        let char = c, charCode = char.charCodeAt(0);
        keyEvent.simulate(charCode, charCode, t._getModifiers(), _ct);
        let selStart = _ct.selectionStart;
        let selEnd = _ct.selectionEnd;
        let textAreaStr = _ct.value;
        let value = textAreaStr.substring(0, selStart) + char + textAreaStr.substring(selEnd);
        _ct.value = value;
        _ct.focus();
        _ct.selectionStart = selStart + 1, _ct.selectionEnd = selStart + 1;
    }
    _isLetter(c) {
        return c.toLowerCase() != c.toUpperCase();
    }
    _shiftOn() {
        let t = this;
        let _sr = t.myElement.shadowRoot;
        t.shift = true;
        _sr.querySelector('.visible').classList.add("shift");
        _sr.querySelector('.key-shift').classList.add("on");
        t.caps = false;
        _sr.querySelector('.key-caps').classList.remove("on");
        _sr.querySelector('.visible').classList.remove("caps");
    }
    _shiftOff() {
        let t = this;
        let _sr = t.myElement.shadowRoot;
        t.shift = false;
        _sr.querySelector('.visible').classList.remove("shift");
        _sr.querySelector('.key-shift').classList.remove("on");
    }
    _toggleShift() {
        let t = this;
        let _ct = t.currentTarget;
        t.shift = !t.shift;
        if (t.shift) {
            t._shiftOn();
        }
        else {
            t._shiftOff();
        }
        keyEvent.simulate(0, 16, t._getModifiers(), _ct);
    }
    _toggleCaps() {
        let t = this;
        let _sr = t.myElement.shadowRoot;
        let _ct = t.currentTarget;
        t.caps = !t.caps;
        if (t.caps) {
            _sr.querySelector('.key-caps').classList.add("on");
            _sr.querySelector('.visible').classList.add("caps");
        }
        else {
            _sr.querySelector('.key-caps').classList.remove("on");
            _sr.querySelector('.visible').classList.remove("caps");
        }
        keyEvent.simulate(0, 20, t._getModifiers(), _ct);
    }
    _touchKey(event, obj) {
        event.preventDefault();
        event.stopPropagation();
        let t = this;
        let _ct = t.currentTarget;
        //console.log(obj,this)
        let keyData = obj.cell;
        if (obj.isSpecialKey) {
            if (keyData.code == -1) {
                t._clear();
            }
            else if (keyData.code == 8) {
                t._backspace();
            }
            else if (keyData.code == 9) {
                keyEvent.simulate(0, 9, t._getModifiers(), _ct);
            }
            else if (keyData.code == 13) {
                keyEvent.simulate(0, 13, t._getModifiers(), _ct);
                _ct.blur();
            }
            else if (keyData.code == 32) {
                t._insertCharAtCaret(' ');
            }
            else if (keyData.code == 20) {
                t._toggleCaps();
            }
            else if (keyData.code == 16) {
                t._toggleShift();
            }
        }
        else {
            if (t.masked) {
                let char = keyData[t.shift || t.caps ? 1 : 0], charCode = char.charCodeAt(0);
                keyEvent.simulate(charCode, charCode, t._getModifiers(), _ct);
            }
            else {
                t._insertCharAtCaret(keyData[t.shift || t.caps ? 1 : 0]);
            }
        }
        if (t.shift && keyData.code !== 16) {
            t._shiftOff();
        }
    }
    getTemplateKeyboardCellSpecialKeyText(cell) {
        if (typeof cell.text != 'undefined') {
            return (h("div", { class: "text" }, cell.text));
        }
    }
    getTemplateKeyboardCellSpecialKey(cell, isSpecialKey) {
        if (isSpecialKey) {
            let clsSpecialIcon = "icon " + cell.iconCls;
            return ([h("div", { class: clsSpecialIcon }),
                this.getTemplateKeyboardCellSpecialKeyText(cell)
            ]);
        }
        else {
            return ([h("div", { class: "lower" }, cell[0]),
                h("div", { class: "upper" }, cell[1])
            ]);
        }
    }
    getTemplateKeyboardCell(row) {
        let t = this;
        //<button class="btn btn--huge btn--bare btn--calendar-cell" type="button" value={this.displayedValue} hidden={!this.isValidDate} >{this.displayedValue!==''?this.displayedValue:empty}</button>
        return (row.map((cell) => {
            let isSpecialKey = typeof cell.iconCls !== 'undefined';
            let displayBoth = !isSpecialKey && !t._isLetter(cell[0]) && cell.length > 1 ? ' b' : '';
            let clsCell = "td kb-btn" + displayBoth + ' ' + (cell.cls || '') + ' ' + ((isSpecialKey && typeof cell.text != 'undefined') ? 'key-' + cell.text : '');
            let obj = { cell: cell, isSpecialKey: isSpecialKey };
            return (h("div", { class: clsCell, "on-mousedown": (event) => this._touchKey(event, obj), "on-touchstart": (event) => this._touchKey(event, obj) }, this.getTemplateKeyboardCellSpecialKey(cell, isSpecialKey)));
        }));
    }
    getTemplateKeyboardRow(data) {
        return (data.map((row) => {
            return (h("div", { class: "tr kb-row" }, this.getTemplateKeyboardCell(row)));
        }));
    }
    getTemplateKeyboardItem(layout) {
        let cname = "px-keyboard kb-" + layout.name;
        return (h("div", { class: cname }, this.getTemplateKeyboardRow(layout.data)));
    }
    getTemplateKeyboards() {
        return (this.layouts.map((layout) => {
            return this.getTemplateKeyboardItem(layout);
        }));
    }
    render() {
        return (h("div", { class: "px-keyboard-container" }, this.getTemplateKeyboards()));
    }
    static get is() { return "px-keyboard"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["../../assets/fontawesome/css/all.min.css", "./px-keyboard.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["../../assets/fontawesome/css/all.min.css", "px-keyboard.css"]
    }; }
    static get elementRef() { return "myElement"; }
}
