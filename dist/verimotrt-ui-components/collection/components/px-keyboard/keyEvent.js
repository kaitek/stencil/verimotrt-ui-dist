class keyEvent {
    constructor(data, type) {
        this.keyCode = 'keyCode' in data ? data.keyCode : 0;
        this.charCode = 'charCode' in data ? data.charCode : 0;
        let modifiers = 'modifiers' in data ? data.modifiers : [];
        this.ctrlKey = false;
        this.metaKey = false;
        this.altKey = false;
        this.shiftKey = false;
        for (let i = 0; i < modifiers.length; i++) {
            this[modifiers[i] + 'Key'] = true;
        }
        this.type = type || 'keypress';
    }
    toNative() {
        let event;
        if (typeof CustomEvent === 'function') {
            event = new CustomEvent(this.type, { detail: { fromSoftKeyboard: true } });
        }
        event.keyCode = this.keyCode;
        event.which = this.charCode || this.keyCode;
        event.shiftKey = this.shiftKey;
        event.metaKey = this.metaKey;
        event.altKey = this.altKey;
        event.ctrlKey = this.ctrlKey;
        return event;
    }
    fire(element) {
        let event = this.toNative();
        if (element.dispatchEvent) {
            element.dispatchEvent(event);
            return;
        }
        element.fireEvent('on' + this.type, event);
    }
    static simulate(charCode, keyCode, modifiers, element, repeat) {
        if (modifiers === undefined) {
            modifiers = [];
        }
        if (element === undefined) {
            element = document;
        }
        if (repeat === undefined) {
            repeat = 1;
        }
        let modifierToKeyCode = {
            'shift': 16,
            'ctrl': 17,
            'alt': 18,
            'meta': 91
        };
        // if the key is a modifier then take it out of the regular
        // keypress/keydown
        if (keyCode == 16 || keyCode == 17 || keyCode == 18 || keyCode == 91) {
            repeat = 0;
        }
        let modifiersToInclude = [];
        let keyEvents = [];
        // modifiers would go down first
        for (let i = 0; i < modifiers.length; i++) {
            modifiersToInclude.push(modifiers[i]);
            keyEvents.push(new keyEvent({
                charCode: 0,
                keyCode: modifierToKeyCode[modifiers[i]],
                modifiers: modifiersToInclude
            }, 'keydown'));
        }
        // @todo factor in duration for these
        while (repeat > 0) {
            keyEvents.push(new keyEvent({
                charCode: 0,
                keyCode: keyCode,
                modifiers: modifiersToInclude
            }, 'keydown'));
            keyEvents.push(new keyEvent({
                charCode: charCode,
                keyCode: charCode,
                modifiers: modifiersToInclude
            }, 'keypress'));
            repeat--;
        }
        keyEvents.push(new keyEvent({
            charCode: 0,
            keyCode: keyCode,
            modifiers: modifiersToInclude
        }, 'keyup'));
        // now lift up the modifier keys
        for (let i = 0; i < modifiersToInclude.length; i++) {
            let modifierKeyCode = modifierToKeyCode[modifiersToInclude[i]];
            modifiersToInclude.splice(i, 1);
            keyEvents.push(new keyEvent({
                charCode: 0,
                keyCode: modifierKeyCode,
                modifiers: modifiersToInclude
            }, 'keyup'));
        }
        for (let i = 0; i < keyEvents.length; i++) {
            // console.log('firing', keyEvents[i].type, keyEvents[i].keyCode, keyEvents[i].charCode);
            keyEvents[i].fire(element);
        }
    }
}
export { keyEvent };
