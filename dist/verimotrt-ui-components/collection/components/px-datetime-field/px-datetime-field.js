import { h } from "@stencil/core";
export class PxDatetimeField {
    constructor() {
        //validator:PxDatetimeValidate;
        this.dateIsValid = true;
        this.timeIsValid = true;
        this.fieldIsValid = true;
        this.isSelected = false;
        this.preventCellFocus = false;
        this._dropdownFocused = false;
        this.dateFormat = 'YYYY-MM-DD';
        this.timeFormat = 'HH:mm';
        this.showTimeZone = 'none';
        this.hideTime = false;
        this.hideDate = false;
        this.hideIcon = false;
        this.allowWrap = false;
        this.required = false;
        this.showButtons = false;
        this.blockFutureDates = false;
        this.blockPastDates = false;
        this.hideValidationMessage = false;
        this._validationErrorMessage = '';
        this._forcedFailedClass = false;
        this.preventApply = false;
        this._dropdownActive = false;
        this._dropdownHoverFix = false;
        this.hoist = false;
        this.timeZone = 'Europe/Istanbul';
        this.momentObj = null;
    }
    componentDidLoad() {
        //this.validator=new PxDatetimeValidate({scope:this,root:this.elfieldWrapper,moment:this.moment,timeZone:this.timeZone,momentObj:this.momentObj});
        //this.validator.on('pxMomentValid',(obj)=>{
        //  this._handleValidMoment(obj);
        //});
        //this.validator.on('pxMomentInvalid',(obj)=>{
        //  this._handleInvalidMoment(obj);
        //});
    }
    async setDate(momentObj) {
        //debugger
        if (this.momentObj === null) {
            momentObj.set({
                hours: 0,
                minutes: 0
            });
        }
        else {
            momentObj.set({
                hours: this.momentObj.hour(),
                minutes: this.momentObj.minute()
            });
        }
        this.momentObj = momentObj;
        this.elDate.momentObj = momentObj;
        this.pxMomentChanged.emit(this.momentObj);
    }
    async setTime(momentObj) {
        if (this.momentObj !== null) {
            let tmpMoment = this.momentObj.clone();
            tmpMoment.set({
                hours: momentObj.hour,
                minutes: momentObj.minute
            });
            this.momentObj = tmpMoment;
            this.elTime.momentObj = this.momentObj;
            this.pxMomentChanged.emit(this.momentObj);
        }
    }
    _fieldClicked(field) {
        this.pxDatetimeEntryIconClicked.emit({ dateOrTime: field });
    }
    /*
      * Will only show the dropdown in the date entry if `hideTime`
      * and `showTimeZone` are true
      */
    _showTimeZoneInDate(hideTime, showTimeZone) {
        if (hideTime || hideTime === true) {
            return showTimeZone;
        }
        else {
            return '';
        }
    }
    //@Listen('pxNextField')
    //_nextFieldRequired(evt) {
    //  var date = this.elDate;
    //  //if this comes from date do some logic, otherwise let it propagate
    //  if (date && evt.detail.dateOrTime === 'Date') {
    //    if (!this.hideTime) {
    //      this.elTime._focusCell(0);
    //      evt.stopPropagation();
    //      evt.stopImmediatePropagation();
    //    }
    //  }
    //}
    //@Listen('pxPreviousField')
    //_previousFieldRequired(evt) {
    //  var time = this.elTime;
    //  //if this comes from time do some logic, otherwise let it propagate
    //  if (time && evt.detail.dateOrTime === 'Time') {
    //    if (!this.hideDate) {
    //      this.elDate._focusLastCell();
    //      evt.stopPropagation();
    //      evt.stopImmediatePropagation();
    //    }
    //  }
    //}
    /*
    */
    //focus(){
    //  this._focusFirstEntry(false);
    //}
    /*
    */
    //_focusFirstEntry(forceTimeFocus) {
    //  if (!this.hideDate && !forceTimeFocus) {
    //    this.elDate._focusCell(0);
    //  } else {
    //    this.elTime._focusCell(0);
    //  }
    //}
    /*
    */
    //_focusLastEntry() {
    //  if (!this.hideTime) {
    //    this.elTime._focusLastCell();
    //  } else {
    //    this.elDate._focusLastCell();
    //  }
    //}
    /**
     * Toggles classes causing the buttons to wrap under the field
     */
    _getWrapClass(allowWrap) {
        if (allowWrap) {
            return "flex--wrap";
        }
    }
    /**
     * Adds a margin right if the buttons are showing
     */
    _getButtonMarginClass(showButtons, allowWrap) {
        var classList = "dt-field-validation-wrapper ";
        if (showButtons) {
            classList += "u-mr- ";
            if (allowWrap) {
                classList += "u-pb+";
            }
        }
        return classList;
    }
    /**
     * event that watches when the dropdown has focus
     */
    _handleDropdownFocus(e) {
        e.stopPropagation();
        this._dropdownActive = true;
    }
    _handleDropdownBlur(e) {
        e.stopPropagation();
        this._dropdownActive = false;
        this._handleHoverFix();
    }
    /**
     * when dt-field is detached set `_dropdownFocused` to false.
     * prevents the dropdown from being stuck in a focused state
     */
    detached() {
        this._dropdownFocused = false;
    }
    /**
     * This is a quick fix.
     * In order to make dt-field look like an input field we have to overwrite some css on #fieldWrapper.
     *
     * There is a hover state on #fieldWrapper and px-dropdown is a child of #fieldWrapper;
     * therefore, when dropdown is opened the hover to #fieldWrapper is extended.
     * This causes a flash of gray when the user selects a time zone with the mouse.
     * In order to prevent this flash we need to overwrite the hover state by added
     * `.dt-text-field--hover-fix` to #fieldWrapper.
     *
     * The true issue is that we can't listen to px-dropdown.opened because there is a timing issue
     * when px-dropdown.opened is set to false vs when the dropdown is not longer visible. This causes the flash.
     * We couldn't find any other events that don't have a timing issue.
     */
    _handleHoverFix() {
        this._dropdownHoverFix = true;
        setTimeout(() => {
            this._dropdownHoverFix = false;
        }, 1000);
        //this.async(function () {
        //  this.set('_dropdownHoverFix', false);
        //} 1000);
    }
    /**
     * isValid & _forcedFailedClass - determines whether to show the validation error classes
     * isSelected - sets the styles when an entryCell is selected
     * _dropdownActive - overwrites the styles when the dropdown is focused
     * _dropdownHoverFix - overwrites the styles when the user selects an item in the dropdown
     */
    _getWrapperClass(isValid, _forcedFailedClass, isSelected, _dropdownActive, _dropdownHoverFix) {
        var classList = "flex flex--middle dt-text-field ";
        if (isValid === false || _forcedFailedClass === true) {
            classList += "validation-error validation-failed ";
        }
        else if (isSelected) {
            classList += "dt-is-selected ";
        }
        else if (_dropdownActive) {
            classList += "dt-dropdown-is-active ";
        }
        else if (_dropdownHoverFix) {
            classList += "dt-text-field--hover-fix ";
        }
        return classList;
    }
    /**
     * Adds a right margin if both entries are showing
     */
    _getEntryDateClass(hideDate, hideTime, hideIcon) {
        var classList = "";
        if (hideDate === false && hideTime === false && hideIcon === true) {
            classList += "u-mr+ ";
        }
        else if (hideDate === false && hideTime === false) {
            classList += "u-mr- ";
        }
        return classList;
    }
    /**
     * Run validation on a moment object that has been set from the outside
     * This will set or unset validation errors
     */
    _validateSetMomentObj(momentObj) {
        if (momentObj === undefined) {
            return;
        }
        //var __fn_validator = this.validator._validateMomentObj.bind(this);
        //__fn_validator(momentObj);
        //this.validator._validateMomentObj(momentObj);
    }
    _getIsValid(fieldIsValid, dateIsValid, timeIsValid) {
        return fieldIsValid && dateIsValid && timeIsValid;
    }
    _handleValidationMessage(e) {
        this._validationErrorMessage = e.detail.validationErrorMessage;
    }
    /**
     * Helper function for a dom-if
     * Determines whether to display the validation error
     */
    _showValidation(isValid, hideValidationMessage) {
        var showValidation = !isValid;
        if (hideValidationMessage) {
            showValidation = false;
        }
        ;
        return showValidation;
    }
    /**
     * Handles validation coming from `_validateInput()`
     * Set `dateIsValid` and `timeIsValid` to true as needed
     * Runs validation if buttons are hidden and field is valid
     */
    //@Listen('pxMomentValid')
    //_handleValidMoment(evt) {
    //  if(evt.element && evt.element.id === 'date') {
    //    this.dateIsValid = true;
    //  }
    //  if(evt.element && evt.element.id === 'time') {
    //    this.timeIsValid = true;
    //  }
    //  if(evt.element && evt.element.nodeName === 'PX-DATETIME-FIELD') {
    //    this.dateIsValid = true;
    //    this.timeIsValid = true;
    //  }
    //  //block evt from _validationField to avoid infite loop
    //  if (!this.showButtons && evt.element.nodeName !== 'PX-DATETIME-FIELD') {
    //    var origin = "_momentChanged"
    //    this._applyChanges(origin);
    //  }
    //}
    /**
     * Sets `isValid`, `dateIsValid`, and `timeIsValid` to false as needed.
     */
    _handleInvalidMoment(evt) {
        this.isValid = false;
        if (evt.detail && evt.detail.element && evt.detail.element.id === 'date') {
            this.dateIsValid = false;
        }
        if (evt.detail && evt.detail.element && evt.detail.element.id === 'time') {
            this.timeIsValid = false;
        }
    }
    /*
    * Focuses select to the next cell when an icon is clicked
    */
    //@Listen('pxDatetimeEntryIconClicked')
    //_iconClicked(evt) {
    //  if (evt.detail.dateOrTime === "Date" && !this.preventCellFocus) {
    //    this.elDate._focusCell(0);
    //  }
    //  else if (evt.detail.dateOrTime === "Time"){
    //    this.elTime._focusCell(0);
    //  }
    //}
    /**
     * Determines whether to apply or cancel the changes
     * based on the event
     */
    //@Listen('pxDatetimeButtonClicked')
    //_dateTimeButtons(evt) {
    //  if (evt.detail.action) {
    //    this._applyChanges(null);
    //  }
    //  else {
    //    this._cancelChanges();
    //  }
    //}
    /**
     * Sets the momentObj if the field is valid, preventApply is not true,
     * and the new and old momentObjs are not the same.
     */
    //@Listen('blur')
    //_applyChanges(origin) {
    //  var dateEntry = this.elfieldWrapper.querySelector('#date');
    //  var timeEntry = this.elfieldWrapper.querySelector('#time');
    //  if (this.momentObj === undefined || (dateEntry === null && !this.hideDate) || (timeEntry === null && !this.hideTime)) { return; }
    //
    //  //var newMomentObj = this.validator._validateField(origin);
    //  //if(this.preventApply && newMomentObj && newMomentObj !== "FieldSame") {
    //  //  this.pxFieldValid.emit({"momentObj": newMomentObj});
    //  //}
    //  //else {
    //  //  if(newMomentObj === "FieldBlank"){
    //  //    let _x=null;
    //  //    for(let i=0;i<origin.path.length;i++){
    //  //      let item=origin.path[i];
    //  //      if(item.dateOrTime){
    //  //        _x=item;
    //  //        break;
    //  //      }
    //  //    }
    //  //    //var __fn_validator = this.validator._validField.bind(this);
    //  //    //__fn_validator(null);
    //  //    this.validator._validField({field:_x,momentObj:this.momentObj});
    //  //    this.momentObj=null;
    //  //    this.pxMomentChanged.emit();
    //  //  }
    //  //  else if(newMomentObj && newMomentObj !== "FieldSame") {
    //  //    this.momentObj=newMomentObj;
    //  //    this.pxMomentChanged.emit();
    //  //  }
    //  //}
    //}
    /**
     * Clear field or reset to previous moment objs
     */
    _cancelChanges() {
        var entries = this.elfieldWrapper.querySelectorAll('px-datetime-entry');
        if (this.momentObj === null) {
            entries.forEach(function (entry) {
                let tmp = entry;
                tmp.clear();
            });
        }
        else {
            entries.forEach(function (entry) { entry.setValueFromMoment(); });
        }
        //var __fn_validator = this.validator._validateField.bind(this);
        //__fn_validator(null);
        //this.validator._validateField(null);
        this.elfieldWrapper.blur();
    }
    render() {
        let _cls1 = this._getWrapClass(this.allowWrap) + ' flex';
        return (h("div", { class: _cls1 },
            h("div", { class: this._getButtonMarginClass(this.showButtons, this.allowWrap) },
                h("div", { id: "fieldWrapper", ref: (el) => this.elfieldWrapper = el, class: this._getWrapperClass(this.isValid, this._forcedFailedClass, this.isSelected, this._dropdownActive, this._dropdownHoverFix) },
                    !this.hideDate ?
                        h("px-datetime-entry", { ref: (el) => this.elDate = el, id: "date", class: this._getEntryDateClass(this.hideDate, this.hideTime, this.hideIcon), dateOrTime: "Date", "on-tap": () => this._fieldClicked("Date"), momentFormat: this.dateFormat, momentObj: this.momentObj, isSelected: this.isSelected, hideIcon: this.hideIcon, timeZone: this.timeZone, showTimeZone: this._showTimeZoneInDate(this.hideTime, this.showTimeZone), hoist: this.hoist })
                        : '',
                    !this.hideTime ?
                        h("px-datetime-entry", { ref: (el) => this.elTime = el, id: "time", dateOrTime: "Time", "on-tap": () => this._fieldClicked("Time"), momentFormat: this.timeFormat, showTimeZone: this.showTimeZone, momentObj: this.momentObj, hideIcon: this.hideIcon, timeZone: this.timeZone, isSelected: this.isSelected, hoist: this.hoist })
                        : ''),
                this._showValidation(this.isValid, this.hideValidationMessage) ?
                    h("div", { class: "form-field__help validation-error dt-validation-text" }, this._validationErrorMessage)
                    : ''),
            this.showButtons ?
                h("px-datetime-buttons", null)
                : ''));
    }
    static get is() { return "px-datetime-field"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-datetime-field.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-datetime-field.css"]
    }; }
    static get properties() { return {
        "dateIsValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "date-is-valid",
            "reflect": false,
            "defaultValue": "true"
        },
        "timeIsValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-is-valid",
            "reflect": false,
            "defaultValue": "true"
        },
        "fieldIsValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "field-is-valid",
            "reflect": false,
            "defaultValue": "true"
        },
        "isSelected": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "is-selected",
            "reflect": false,
            "defaultValue": "false"
        },
        "preventCellFocus": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "prevent-cell-focus",
            "reflect": false,
            "defaultValue": "false"
        },
        "_dropdownFocused": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_dropdown-focused",
            "reflect": false,
            "defaultValue": "false"
        },
        "dateFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "date-format",
            "reflect": false,
            "defaultValue": "'YYYY-MM-DD'"
        },
        "timeFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-format",
            "reflect": false,
            "defaultValue": "'HH:mm'"
        },
        "showTimeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'none'|'dropdown'|'extendedDropdown'|'text'|'abbreviatedText'",
                "resolved": "\"abbreviatedText\" | \"dropdown\" | \"extendedDropdown\" | \"none\" | \"text\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-time-zone",
            "reflect": false,
            "defaultValue": "'none'"
        },
        "hideTime": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-time",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideDate": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-date",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideIcon": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-icon",
            "reflect": false,
            "defaultValue": "false"
        },
        "allowWrap": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "allow-wrap",
            "reflect": false,
            "defaultValue": "false"
        },
        "required": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "required",
            "reflect": false,
            "defaultValue": "false"
        },
        "showButtons": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-buttons",
            "reflect": false,
            "defaultValue": "false"
        },
        "blockFutureDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-future-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "blockPastDates": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "block-past-dates",
            "reflect": false,
            "defaultValue": "false"
        },
        "isValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "is-valid",
            "reflect": false
        },
        "hideValidationMessage": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-validation-message",
            "reflect": false,
            "defaultValue": "false"
        },
        "_validationErrorMessage": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_validation-error-message",
            "reflect": false,
            "defaultValue": "''"
        },
        "_forcedFailedClass": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_forced-failed-class",
            "reflect": false,
            "defaultValue": "false"
        },
        "preventApply": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "prevent-apply",
            "reflect": false,
            "defaultValue": "false"
        },
        "_dropdownActive": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_dropdown-active",
            "reflect": false,
            "defaultValue": "false"
        },
        "_dropdownHoverFix": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_dropdown-hover-fix",
            "reflect": false,
            "defaultValue": "false"
        },
        "hoist": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hoist",
            "reflect": false,
            "defaultValue": "false"
        },
        "containerType": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "container-type",
            "reflect": false
        },
        "maxDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max-date",
            "reflect": false
        },
        "minDate": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min-date",
            "reflect": false
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        },
        "momentObj": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "moment-obj",
            "reflect": false,
            "defaultValue": "null"
        }
    }; }
    static get events() { return [{
            "method": "pxDatetimeEntryIconClicked",
            "name": "pxDatetimeEntryIconClicked",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxFieldValid",
            "name": "pxFieldValid",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxMomentChanged",
            "name": "pxMomentChanged",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "setDate": {
            "complexType": {
                "signature": "(momentObj: any) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setTime": {
            "complexType": {
                "signature": "(momentObj: any) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get watchers() { return [{
            "propName": "fieldIsValid",
            "methodName": "_getIsValid"
        }, {
            "propName": "dateIsValid",
            "methodName": "_getIsValid"
        }, {
            "propName": "timeIsValid",
            "methodName": "_getIsValid"
        }]; }
    static get listeners() { return [{
            "name": "pxDropdownFocused",
            "method": "_handleDropdownFocus",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDropdownBlurred",
            "method": "_handleDropdownBlur",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxValidationMessage",
            "method": "_handleValidationMessage",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxMomentInvalid",
            "method": "_handleInvalidMoment",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
