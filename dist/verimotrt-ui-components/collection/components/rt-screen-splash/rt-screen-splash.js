import { h } from "@stencil/core";
export class RtScreenSplash {
    render() {
        return (h("div", { class: "flex flex--col flex--center flex--stretch" },
            h("div", null,
                h("px-logo", { cls: "v1 m" })),
            h("div", null,
                h("px-steps", { ref: (el) => this.stepper = el, items: [
                        { "id": "db_start", "label": "DB Başlatma" },
                        { "id": "2", "label": "Select Services" },
                        { "id": "3", "label": "Billing" },
                        { "id": "4", "label": "Review" },
                        { "id": "5", "label": "Deploy" }
                    ], currentStep: 1 }))));
    }
    static get is() { return "rt-screen-splash"; }
    static get originalStyleUrls() { return {
        "$": ["rt-screen-splash.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["rt-screen-splash.css"]
    }; }
    static get elementRef() { return "myElement"; }
}
