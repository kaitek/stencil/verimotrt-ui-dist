import { h } from "@stencil/core";
export class PxModal {
    constructor() {
        this.lastFocusedElement = null;
        this.fitInto = window;
        this.hideAcceptButton = false;
        this.hideRejectButton = false;
        this.opened = false;
        this.disableCloseOnEscape = false;
        this.disableAutoFocus = false;
        this.fillContainer = false;
        this.autoFitOnAttach = true;
        this._focusableElements = [];
        /**
         * IronFitBehavior start
         */
        this.sizingTarget = this;
        this.__shouldPosition = false;
    }
    async getVisible() {
        return this.opened;
    }
    async setVisible(isVisible) {
        if (isVisible) {
            this.myElement.setAttribute('opened', 'true');
            this.elDivBox.style['display'] = 'flex';
            this.opened = true;
        }
        else if (!isVisible) {
            this.myElement.removeAttribute('opened');
            this.elDivBox.style['display'] = 'none';
            this.opened = false;
        }
    }
    keydownHandler(evt) {
        evt.preventDefault();
        if (evt.code === 'Tab' && !evt.shiftKey) {
            const lastFocusedElement = this.lastFocusedElement;
            if (lastFocusedElement) {
                const lastFocusedElementIndex = this._focusableElements.indexOf(lastFocusedElement);
                if (this._focusableElements[lastFocusedElementIndex + 1]) {
                    this._focusableElements[lastFocusedElementIndex + 1].focus();
                    this.lastFocusedElement = this._focusableElements[lastFocusedElementIndex + 1];
                }
                else {
                    this._firstFocusableElement.focus();
                    this.lastFocusedElement = this._firstFocusableElement;
                }
            }
            else {
                this._firstFocusableElement.focus();
                this.lastFocusedElement = this._firstFocusableElement;
            }
            evt.preventDefault();
        }
        if (evt.code === 'Tab' && evt.shiftKey) {
            let lastFocusedElement = this.lastFocusedElement;
            if (lastFocusedElement) {
                const lastFocusedElementIndex = this._focusableElements.indexOf(lastFocusedElement);
                if (this._focusableElements[lastFocusedElementIndex - 1]) {
                    this._focusableElements[lastFocusedElementIndex - 1].focus();
                    this.lastFocusedElement = this._focusableElements[lastFocusedElementIndex - 1];
                }
                else {
                    this._lastFocusableElement.focus();
                    this.lastFocusedElement = this._lastFocusableElement;
                }
            }
            else {
                this._lastFocusableElement.focus();
                this.lastFocusedElement = this._lastFocusableElement;
            }
            evt.preventDefault();
            if (lastFocusedElement && this._lastFocusableElement && lastFocusedElement === this._firstFocusableElement) {
                evt.preventDefault();
                this._lastFocusableElement.focus();
            }
        }
        if (evt.code === 'Enter' || evt.code === 'Space') {
            this.opened = false;
            this.myElement.removeAttribute('opened');
            if (this.lastFocusedElement === this.elRejectButton) {
                this.pxModalRejected.emit();
            }
            if (this.lastFocusedElement === this.elAcceptButton) {
                this.pxModalAccepted.emit();
            }
        }
        if (evt.code === 'Escape' && !this.disableCloseOnEscape) {
            this.opened = false;
            this.myElement.removeAttribute('opened');
            this.pxModalDismissed.emit();
        }
    }
    _handleFillContainerChanged( /*newValue*/) {
        this.sizingTarget = this.myElement;
        this.fitInto = this.sizingTarget.parentElement;
        this._refit();
        //    if(newValue === true) {
        //      this.listen(this.fitInto, 'scroll', '_refit');
        //      this.listen(this.fitInto, 'resize', '_refit');
        //    }
        //    else {
        //      this.unlisten(this.fitInto, 'scroll', '_refit');
        //      this.unlisten(this.fitInto, 'resize', '_refit');
        //    }
    }
    componentWillLoad() {
        //console.log('componentWillLoad',this.fillContainer , this.fitInto);
    }
    componentDidLoad() {
        if (this.fillContainer) {
            this.fitInto = this.myElement.parentElement;
        }
        this._localeHorizontalAlign = this.get_localeHorizontalAlign();
        this.elDivBox.style['display'] = 'none';
    }
    //_openTriggerTappedFn = () => {
    //  this.opened = true
    //}
    _handleTransitionEnd(evt) {
        if (evt.currentTarget === this.elDivModal && evt.propertyName === 'visibility') {
            if (this.opened) {
                this.refit();
                // When the modal first opens, find focusable elements and
                // focus the first one, unless the `disableAutoFocus` boolean
                // is set to `true`
                this._elementFocusedBeforeOpened = document.activeElement;
                this.findFocusableElements();
                if (this._focusableElements.length) {
                    if (this._firstFocusableElement && !this.disableAutoFocus) {
                        this._firstFocusableElement.focus();
                        this.lastFocusedElement = this._firstFocusableElement;
                    }
                }
            }
            else {
                // Set the display back to none to make the modal invisible
                this.elDivBox.style['display'] = 'none';
                // Focus the last focused element in the document unless the
                // `disableAutoFocus` boolean is set to `true`
                if (this._elementFocusedBeforeOpened && !this.disableAutoFocus) {
                    this._elementFocusedBeforeOpened.focus();
                }
                this._elementFocusedBeforeOpened = null;
                this._focusableElements = [];
            }
        }
    }
    findFocusableElements() {
        if (!this.hideRejectButton) {
            this._focusableElements = [
                ...this._focusableElements,
                this.elRejectButton
            ];
        }
        if (!this.hideAcceptButton) {
            this._focusableElements = [
                ...this._focusableElements,
                this.elAcceptButton
            ];
        }
        if (this._focusableElements.length) {
            this._firstFocusableElement = this._focusableElements[0];
            this._lastFocusableElement = this._focusableElements[this._focusableElements.length - 1];
        }
        else {
            this._firstFocusableElement = null;
            this._lastFocusableElement = null;
        }
    }
    _handleRejectTriggerTapped(e) {
        const target = e.currentTarget;
        const triggerContainer = this.elRejectTriggerContainer;
        /*
          * - First check: We should only close the modal if the user clicked on
          *   a button, not if this click event is sourced from the container.
          * - Second check: A click on a disabled input element like a <button>
          *   should not trigger our click listener. But it does in IE. So we
          *   manually check the event source, and ignore the event if the
          *   source has the `disabled` attribute. Bof.
          */
        if (target === triggerContainer && !target.hasAttribute('disabled')) {
            this.opened = false;
            this.myElement.removeAttribute('opened');
            this.pxModalRejected.emit();
        }
    }
    _handleAcceptTriggerTapped(e) {
        const target = e.currentTarget;
        const triggerContainer = this.elAcceptTriggerContainer;
        /*
          * - First check: We should only close the modal if the user clicked on
          *   a button, not if this click event is sourced from the container.
          * - Second check: A click on a disabled input element like a <button>
          *   should not trigger our click listener. But it does in IE. So we
          *   manually check the event source, and ignore the event if the
          *   source has the `disabled` attribute. Bof.
          */
        if (target === triggerContainer && !target.hasAttribute('disabled')) {
            this.opened = false;
            this.myElement.removeAttribute('opened');
            this.pxModalAccepted.emit();
        }
    }
    _refit() {
        if (this.fillContainer && this.fitInto) {
            this.refit();
        }
    }
    get_localeHorizontalAlign() {
        return this.horizontalAlign;
    }
    _discoverInfo() {
        if (this._fitInfo) {
            return;
        }
        var target = window.getComputedStyle(this.fitInto);
        var sizer = window.getComputedStyle(this.sizingTarget);
        this._fitInfo = {
            inlineStyle: {
                top: this.sizingTarget.style.top || '',
                left: this.sizingTarget.style.left || '',
                position: this.sizingTarget.style.position || ''
            },
            sizerInlineStyle: {
                maxWidth: this.sizingTarget.style.maxWidth || '',
                maxHeight: this.sizingTarget.style.maxHeight || '',
                boxSizing: this.sizingTarget.style.boxSizing || ''
            },
            positionedBy: {
                vertically: target.top !== 'auto' ?
                    'top' :
                    (target.bottom !== 'auto' ? 'bottom' : null),
                horizontally: target.left !== 'auto' ?
                    'left' :
                    (target.right !== 'auto' ? 'right' : null)
            },
            sizedBy: {
                height: sizer.maxHeight !== 'none',
                width: sizer.maxWidth !== 'none',
                minWidth: parseInt(sizer.minWidth, 10) || 0,
                minHeight: parseInt(sizer.minHeight, 10) || 0
            },
            margin: {
                top: parseInt(target.marginTop, 10) || 0,
                right: parseInt(target.marginRight, 10) || 0,
                bottom: parseInt(target.marginBottom, 10) || 0,
                left: parseInt(target.marginLeft, 10) || 0
            }
        };
    }
    refit() {
        var scrollLeft = this.sizingTarget.scrollLeft;
        var scrollTop = this.sizingTarget.scrollTop;
        this.resetFit();
        this.fit();
        this.sizingTarget.scrollLeft = scrollLeft;
        this.sizingTarget.scrollTop = scrollTop;
    }
    resetFit() {
        if (this.sizingTarget.style) {
            this.sizingTarget.style["boxSizing"] = "";
            this.sizingTarget.style["left"] = "";
            this.sizingTarget.style["maxHeight"] = "";
            this.sizingTarget.style["maxWidth"] = "";
            this.sizingTarget.style["position"] = "";
            this.sizingTarget.style["top"] = "";
        }
    }
    fit() {
        //this.position();
        this.constrain();
        this.center();
    }
    constrain() {
        if (this.__shouldPosition) {
            return;
        }
        this._discoverInfo();
        var info = this._fitInfo;
        // position at (0px, 0px) if not already positioned, so we can measure the
        // natural size.
        if (!info.positionedBy.vertically) {
            this.sizingTarget.style.position = 'fixed';
            this.sizingTarget.style.top = '0px';
        }
        if (!info.positionedBy.horizontally) {
            this.sizingTarget.style.position = 'fixed';
            this.sizingTarget.style.left = '0px';
        }
        // need border-box for margin/padding
        this.sizingTarget.style.boxSizing = 'border-box';
        // constrain the width and height if not already set
        var rect = this.sizingTarget.getBoundingClientRect();
        if (!info.sizedBy.height) {
            this.__sizeDimension(rect, info.positionedBy.vertically, 'top', 'bottom', 'Height');
        }
        if (!info.sizedBy.width) {
            this.__sizeDimension(rect, info.positionedBy.horizontally, 'left', 'right', 'Width');
        }
    }
    __getNormalizedRect(target) {
        if (target === document.documentElement || target === window) {
            return {
                top: 0,
                left: 0,
                width: window.innerWidth,
                height: window.innerHeight,
                right: window.innerWidth,
                bottom: window.innerHeight
            };
        }
        return target.getBoundingClientRect();
    }
    __sizeDimension(rect, positionedBy, start, end, extent) {
        var info = this._fitInfo;
        var fitRect = this.__getNormalizedRect(this.fitInto);
        var max = extent === 'Width' ? fitRect.width : fitRect.height;
        var flip = (positionedBy === end);
        var offset = flip ? max - rect[end] : rect[start];
        var margin = info.margin[flip ? start : end];
        //var offsetExtent = 'offset' + extent;
        var sizingOffset = 0; //this[offsetExtent] - this.sizingTarget[offsetExtent];
        this.sizingTarget.style['max' + extent] =
            (max - margin - offset - sizingOffset) + 'px';
    }
    center() {
        if (this.__shouldPosition) {
            return;
        }
        this._discoverInfo();
        var positionedBy = this._fitInfo.positionedBy;
        if (positionedBy.vertically && positionedBy.horizontally) {
            // Already positioned.
            return;
        }
        // Need position:fixed to center
        this.sizingTarget.style.position = 'fixed';
        // Take into account the offset caused by parents that create stacking
        // contexts (e.g. with transform: translate3d). Translate to 0,0 and
        // measure the bounding rect.
        if (!positionedBy.vertically) {
            this.sizingTarget.style.top = '0px';
        }
        if (!positionedBy.horizontally) {
            this.sizingTarget.style.left = '0px';
        }
        // It will take in consideration margins and transforms
        var rect = this.sizingTarget.getBoundingClientRect();
        var fitRect = this.__getNormalizedRect(this.fitInto);
        if (!positionedBy.vertically) {
            var top = fitRect.top - rect.top + (fitRect.height - rect.height) / 2;
            this.sizingTarget.style.top = top + 'px';
        }
        if (!positionedBy.horizontally) {
            var left = fitRect.left - rect.left + (fitRect.width - rect.width) / 2;
            this.sizingTarget.style.left = left + 'px';
        }
    }
    /**
     * IronFitBehavior finish
     */
    render() {
        return (h("div", { class: "modal", id: "modal", "on-transitionend": (event) => this._handleTransitionEnd(event), ref: (el) => this.elDivModal = el },
            h("div", { id: "box", class: "modal__box shadow-modal", ref: (el) => this.elDivBox = el },
                h("div", null,
                    h("div", { class: "modal__header" },
                        h("slot", { name: "header" },
                            h("span", { class: "modal__header__text epsilon weight--normal" }, this.headerText))),
                    h("div", { class: "modal__body" },
                        h("slot", { name: "body" },
                            h("span", { class: "modal__body__text" }, this.bodyText)))),
                h("div", { class: "modal__triggers" },
                    h("slot", { name: "actions" }),
                    !this.hideRejectButton
                        ? h("div", { class: "modal__reject-trigger", "on-tap": (event) => this._handleRejectTriggerTapped(event), id: "reject-trigger-container", ref: (el) => this.elRejectTriggerContainer = el },
                            h("slot", { name: "reject-trigger" },
                                h("button", { type: "button", class: "btn", id: "reject-trigger-button", ref: (el) => this.elRejectButton = el }, this.rejectText)))
                        : '',
                    !this.hideAcceptButton
                        ? h("div", { class: "modal__accept-trigger", "on-tap": (event) => this._handleAcceptTriggerTapped(event), id: "accept-trigger-container", ref: (el) => this.elAcceptTriggerContainer = el },
                            h("slot", { name: "accept-trigger" },
                                h("button", { type: "button", class: "btn btn--call-to-action", id: "accept-trigger-button", ref: (el) => this.elAcceptButton = el }, this.acceptText)))
                        : ''))));
    }
    static get is() { return "px-modal"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-modal.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-modal.css"]
    }; }
    static get properties() { return {
        "headerText": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "header-text",
            "reflect": false
        },
        "bodyText": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "body-text",
            "reflect": false
        },
        "acceptText": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "accept-text",
            "reflect": false
        },
        "rejectText": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "reject-text",
            "reflect": false
        },
        "hideAcceptButton": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-accept-button",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideRejectButton": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-reject-button",
            "reflect": false,
            "defaultValue": "false"
        },
        "disableCloseOnEscape": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-close-on-escape",
            "reflect": false,
            "defaultValue": "false"
        },
        "disableAutoFocus": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-auto-focus",
            "reflect": false,
            "defaultValue": "false"
        },
        "fillContainer": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "fill-container",
            "reflect": true,
            "defaultValue": "false"
        },
        "autoFitOnAttach": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "auto-fit-on-attach",
            "reflect": false,
            "defaultValue": "true"
        },
        "_focusableElements": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "HTMLElement[]",
                "resolved": "HTMLElement[]",
                "references": {
                    "HTMLElement": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        }
    }; }
    static get events() { return [{
            "method": "pxModalDismissed",
            "name": "pxModalDismissed",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxModalRejected",
            "name": "pxModalRejected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxModalAccepted",
            "name": "pxModalAccepted",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "getVisible": {
            "complexType": {
                "signature": "() => Promise<boolean>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<boolean>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setVisible": {
            "complexType": {
                "signature": "(isVisible: boolean) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "fillContainer",
            "methodName": "_handleFillContainerChanged"
        }]; }
    static get listeners() { return [{
            "name": "keydown",
            "method": "keydownHandler",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
