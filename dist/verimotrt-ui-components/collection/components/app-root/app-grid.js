import { h } from "@stencil/core";
//import { GridElement } from '@vaadin/vaadin-grid';
//import '@vaadin/vaadin-grid/all-imports.js';
export class AppGrid {
    constructor() {
        //vgrid:GridElement
        this.usersSmall = [
            {
                first: '1-Elizabeth',
                last: 'Wong',
                full: 'Elizabeth Wong',
                email: 'sika@iknulber.cl',
                born: '1985-04-12',
                age: 32,
                appointment: '2009-05-05 01:10'
            },
            {
                first: '2-Jeffrey',
                last: 'Hamilton',
                email: 'cofok@rac.be',
                born: '1995-04-12',
                age: 23,
                appointment: '2011-03-04 01:10'
            },
            {
                first: '3-Alma',
                last: 'Martin',
                email: 'dotta@behtam.la',
                born: '1975-04-12',
                age: 42,
                appointment: '2010-03-04 02:20'
            },
            {
                first: '4-Carl',
                last: 'Saunders',
                email: 'seh@bibapu.gy',
                born: '2002-04-12',
                age: 12,
                appointment: '2010-03-04 03:30'
            },
            {
                first: '5-Willie',
                last: 'Dennis',
                email: 'izko@dahokwej.ci',
                born: '1973-04-12',
                age: 44,
                appointment: '2010-03-04 04:40'
            },
            {
                first: '6-Angel',
                last: 'Lewis',
                email: 'ma@et.nz',
                born: '1932-04-12',
                age: 89,
                appointment: '2010-03-04 05:50'
            },
            {
                first: '7-Jessie',
                last: 'Sherman',
                email: 'hunocnas@mosuraj.gi',
                born: '1990-04-12',
                age: 27,
                appointment: '2010-03-04 06:00'
            },
            {
                first: '8-Eric',
                last: 'Brewer',
                email: 'tu@coajoul.de',
                born: '1930-04-12',
                age: 88,
                appointment: '2010-03-04 07:00'
            },
            {
                first: '9-Cory',
                last: 'Ramos',
                email: 'kilamja@oviafbek.ss',
                born: '2006-04-12',
                age: 9,
                appointment: '2010-03-04 08:00'
            },
            {
                first: '10-Bertie',
                last: 'Ross',
                email: 'ifuvu@getvi.my',
                born: '1990-04-12',
                age: 27,
                appointment: '2010-03-04 09:00'
            },
            {
                first: '11-Oscar',
                last: 'Estrada',
                email: 'minu@kerireg.gp',
                born: '2002-04-12',
                age: 15,
                appointment: '2010-03-04 10:00'
            },
            {
                first: '12-Estelle',
                last: 'Patton',
                email: 'hudliami@lijihen.fi',
                born: '2006-04-12',
                age: 11,
                appointment: '2010-03-04 11:00'
            },
            {
                first: '13-Sallie',
                last: 'George',
                email: 'wo@zof.bf',
                born: '1985-04-12',
                age: 33,
                appointment: '2010-03-04 12:00'
            },
            {
                first: '14-Harriett',
                last: 'Wheeler',
                email: 'woguw@cibevo.pt',
                born: '1968-04-12',
                age: 52,
                appointment: '2010-03-04 13:00'
            },
            {
                first: '15-Bryan',
                last: 'Houston',
                email: 'tekkubom@gaahu.ge',
                born: '1940-04-12',
                age: 77,
                appointment: '2010-03-04 14:00'
            },
            {
                first: '16-Leon',
                last: 'Craig',
                email: 'wo@gurozo.gs',
                born: '2015-04-12',
                age: 2,
                appointment: '2010-03-04 15:00'
            },
            {
                first: '17-Mable',
                last: 'Taylor',
                email: 'um@fegnocka.pg',
                born: '2006-04-12',
                age: 11,
                appointment: '2010-03-04 16:00'
            },
            {
                first: '18-Ida',
                last: 'Hansen',
                email: 'lufahu@gewlaskoc.kh',
                born: '1973-04-12',
                age: 44,
                appointment: '2010-03-04 17:00'
            },
            {
                first: '19-Adele',
                last: 'Thornton',
                email: 'gugugo@nevigi.th',
                born: '1973-04-12',
                age: 41,
                appointment: '2010-03-04 18:00'
            },
            {
                first: '20-Jerry',
                last: 'Kelley',
                email: 'solef@zoose.as',
                born: '1950-04-12',
                age: 67,
                appointment: '2010-03-04 19:00'
            },
            {
                first: '21-Clara',
                last: 'Delgado',
                email: 'fivticnuf@upkib.rw',
                born: '1951-04-12',
                age: 66,
                appointment: '2010-03-04 20:00'
            },
            {
                first: '22-Joseph',
                last: 'Stevenson',
                email: 'be@viijo.lk',
                born: '1995-04-12',
                age: 22,
                appointment: '2010-03-04 21:00'
            },
            {
                first: '23-Ellen',
                last: 'Perry',
                email: 'ce@jewo.sv',
                born: '1996-04-12',
                age: 23,
                appointment: '2010-03-04 22:00'
            },
            {
                first: '24-Ronnie',
                last: 'Cummings',
                email: 'iro@wi.vn',
                born: '2000-04-12',
                age: 17,
                appointment: '2010-03-04 23:00'
            },
            {
                first: '25-Olive',
                last: 'Santos',
                email: 'vo@nees.cn',
                born: '1918-04-12',
                age: 99,
                appointment: '2010-03-05 00:00'
            },
            {
                first: '26-Rena',
                last: 'Tucker',
                email: 'uharoc@lohpol.gl',
                born: '1915-04-12',
                age: 102,
                appointment: '2010-03-05 01:00'
            },
            {
                first: '27-Nell',
                last: 'Hicks',
                email: 'felaf@vo.sb',
                born: '1940-04-12',
                age: 76,
                appointment: '2010-03-05 02:00'
            },
            {
                first: '28-Jesus',
                last: 'Hawkins',
                email: 'wahgab@mu.mk',
                born: '1983-04-12',
                age: 34,
                appointment: '2010-03-05 03:00'
            },
            {
                first: '29-Duane',
                last: 'Harris',
                email: 'kellanjob@daava.ph',
                born: '1996-04-12',
                age: 23,
                appointment: '2010-03-05 04:00'
            },
            {
                first: '30-Jonathan',
                last: 'Holmes',
                email: 'jir@bikuf.dj',
                born: '1969-04-12',
                age: 49,
                appointment: '2010-03-05 05:00'
            },
            {
                first: '31-Christine',
                last: 'Collier',
                email: 'bocago@jerla.ba',
                born: '1970-04-12',
                age: 48,
                appointment: '2010-03-05 06:00'
            },
            {
                first: '32-Brandon',
                last: 'Thompson',
                email: 'regoh@ji.kn',
                born: '1973-04-12',
                age: 43,
                appointment: '2010-03-05 07:00'
            },
            {
                first: '33-Anne',
                last: 'Dunn',
                email: 'samhof@are.sc',
                born: '1976-04-12',
                age: 41,
                appointment: '2010-03-05 08:00'
            },
            {
                first: '34-Viola',
                last: 'Sherman',
                email: 'wep@kezori.lt',
                born: '1973-04-12',
                age: 44,
                appointment: '2010-03-05 09:00'
            },
            {
                first: '35-Kathryn',
                last: 'Harper',
                email: 'keotoci@je.mr',
                born: '1941-04-12',
                age: 77,
                appointment: '2010-03-05 10:00'
            },
            {
                first: '36-Calvin',
                last: 'Bates',
                email: 'bomeg@lip.bw',
                born: '1979-04-12',
                age: 38,
                appointment: '2010-03-05 11:00'
            },
            {
                first: '37-Maggie',
                last: 'Collins',
                email: 'zonefto@ihihij.ke',
                born: '1982-04-12',
                age: 35,
                appointment: '2010-03-05 12:00'
            },
            {
                first: '38-Zachary',
                last: 'Mitchell',
                email: 'wamez@cilvahbod.mk',
                born: '1993-04-12',
                age: 26,
                appointment: '2010-03-05 13:00'
            },
            {
                first: '39-Raymond',
                last: 'Kelley',
                email: 'wuz@napujos.tt',
                born: '1995-04-12',
                age: 21,
                appointment: '2010-03-05 14:00'
            },
            {
                first: '40-Augusta',
                last: 'Torres',
                email: 'bum@ne.lt',
                born: '1967-04-12',
                age: 54,
                appointment: '2010-03-05 15:00'
            },
            {
                first: '41-Charlie',
                last: 'Lindsey',
                email: 'ruhlu@tuobujen.ao',
                born: '2002-04-12',
                age: 15,
                appointment: '2010-03-05 16:00'
            },
            {
                first: '42-Jeremy',
                last: 'Swanson',
                email: 'ed@ted.km',
                born: '1997-04-12',
                age: 21,
                appointment: '2010-03-05 17:00'
            },
            {
                first: '43-Joel',
                last: 'Gonzalez',
                email: 'ej@pot.bz',
                born: '1999-04-12',
                age: 18,
                appointment: '2010-03-05 18:00'
            },
            {
                first: '44-Lillie',
                last: 'Hawkins',
                email: 'uguiv@mudbuve.pa',
                born: '1975-04-12',
                age: 43,
                appointment: '2010-03-05 19:00'
            },
            {
                first: '45-Joel',
                last: 'Watts',
                email: 'naafe@oli.bb',
                born: '2017-04-12',
                age: 1,
                appointment: '2010-03-05 20:00'
            },
            {
                first: '46-Isabella',
                last: 'Sandoval',
                email: 'asobu@wedef.af',
                born: '2016-04-12',
                age: 3,
                appointment: '2010-03-05 21:00'
            },
            {
                first: '47-Roy',
                last: 'Lyons',
                email: 'rasaogu@tadiri.tc',
                born: '1924-04-12',
                age: 88,
                appointment: '2010-03-05 22:00'
            }
        ];
    }
    componentDidLoad() {
        //console.log('componentDidLoad');
        //console.log(this.vgrid);
        //this.vgrid.size=20;
        //this.vgrid.items=this.users;
        //var grid:GridElement = this.elContainer.querySelector("vaadin-grid");
        //grid.items=this.users;
    }
    handleTableAction(e) {
        console.log(e);
        if (e.detail.id === 'CSV') {
            //this.exportToCsv();
        }
    }
    render() {
        let columns = [
            {
                name: '#',
                path: '__#__',
                type: 'number',
                width: '60px',
                renderer: 'px-data-grid-number-renderer',
                frozen: true,
                flexGrow: 0
            },
            {
                name: "First Name",
                path: "first",
                renderer: "px-data-grid-string-renderer",
                editable: true,
                frozen: true,
                id: "first[string]"
                //}, {
                //  name: "Last Name",
                //  path: "last",
                //  renderer: "px-data-grid-string-renderer",
                //  editable: true,
                //  frozen: false,
                //  id: "last[string]"
            }, {
                name: "Email",
                path: "email",
                frozen: false,
                id: "email[string]"
            }, {
                name: 'Age',
                path: 'age',
                renderer: 'px-data-grid-number-renderer',
                editable: true,
                width: '20px',
                type: 'number'
            }, {
                name: 'Birth Date',
                path: 'born',
                //dateFormat: {
                //  format: 'YYYY-MM-DD'
                //},
                rendererConfig: {
                    hideTime: true,
                    dateFormat: 'YYYY-MM-DD'
                },
                renderer: 'px-data-grid-date-renderer',
                editable: true,
                width: '60px',
                type: 'date'
            }
            //,{
            //  name: 'Appointment',
            //  path: 'appointment',
            //  //dateFormat: {
            //  //  format: 'DD-MM-YYYY HH:mm',
            //  //  timezone: 'UTC'
            //  //},
            //  filterByTime: true,
            //  renderer: 'px-data-grid-date-renderer',
            //  rendererConfig: {
            //    hideTime: false,
            //    dateFormat: 'YYYY-MM-DD HH:mm',
            //    timezone: 'Europe/Istanbul',
            //    datePickerDateFormat: 'YYYY-MM-DD',
            //    datePickerTimeFormat: 'HH:mm'
            //  },
            //  editable: true,
            //  type: 'date'
            //}
        ];
        //let tableActions=[{
        //  "name": "Export CSV",
        //  "icon": "px-doc:document-pdf",
        //  "id": "CSV"
        //}];
        let itemActions = [{
                "name": "Add Row",
                "icon": "px-utl:add",
                "id": "add"
            }, {
                "name": "Delete Row",
                "icon": "px-utl:delete",
                "id": "delete"
            }];
        //let highLights=[{type:"cell",color:"lightgreen",condition:(cellContent/*, column, item*/) => {
        //  return cellContent == 'Dennis';
        //}},{type:"column",color:"lightskyblue",condition:(column, item) => {
        //  return item && column.path == 'last';
        //}},{type:"row",color:"lightsalmon",condition:(cellContent, item) => {
        //  return cellContent!=='' && item && (item.last == 'Dennis' || item.first == 'Carl');
        //}}];
        let selectedItems = [];
        //console.log(this.users.splice(0,10));
        return (h("div", { id: "demo1", class: "flex flex--row flex--left flex--top", ref: (el) => this.elContainer = el },
            h("div", { style: { width: "750px", height: "400px" } },
                h("px-data-grid", { columns: columns, tableData: this.usersSmall, striped: true, sortable: true, multiSort: false, resizable: false, editable: false, hideSelectionColumn: false, selectionMode: "multi", allowSortBySelection: false, hideActionMenu: false, hideColumnFilter: false, itemActions: itemActions, size: 20, itemIdPath: "email", selectedItems: selectedItems, "on-table-action": (e) => this.handleTableAction(e) })),
            h("div", { style: { width: "750px", height: "400px" } },
                h("vaadin-grid", { "aria-label": "Dynamic Data Example", theme: "row-stripes", items: this.usersSmall, pageSize: "20" },
                    h("vaadin-grid-selection-column", { "auto-select": true }),
                    h("vaadin-grid-column", { path: "first", header: "First name" }),
                    h("vaadin-grid-column", { path: "last", header: "Last name" }),
                    h("vaadin-grid-column", { path: "email" }),
                    h("vaadin-grid-column", { path: "age", width: "80px", "text-align": "end" }),
                    h("vaadin-grid-column", { path: "born", width: "100px" })))));
        /*
        <div style={{width:"750px",height:"400px"}}>
              <vaadin-grid
              aria-label="Dynamic Data Example"
              theme="row-stripes compact"
              items={this.usersSmall}
              pageSize="20"
              >
                <vaadin-grid-selection-column auto-select></vaadin-grid-selection-column>
                <vaadin-grid-column path="first" header="First name"></vaadin-grid-column>
                <vaadin-grid-column path="last" header="Last name"></vaadin-grid-column>
                <vaadin-grid-column path="email"></vaadin-grid-column>
                <vaadin-grid-column path="age" width="80px" text-align="end"></vaadin-grid-column>
                <vaadin-grid-column path="born" width="100px" ></vaadin-grid-column>
              </vaadin-grid>
            </div>
        */
    }
    static get is() { return "app-grid"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["../px-flexbox-design/_base.flexbox.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["../px-flexbox-design/_base.flexbox.css"]
    }; }
}
