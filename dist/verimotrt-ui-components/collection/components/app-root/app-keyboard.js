import { h } from "@stencil/core";
export class AppKeyboard {
    //input1:HTMLInputElement;
    //input2:HTMLInputElement;
    render() {
        return (h("div", null,
            h("label", { class: "" }, "Input Label-1"),
            h("input", { id: "input1", placeholder: "Type something...", type: "text", class: "text-input input--regular", value: "", "data-kb": true, "data-kb-style": "numpad" }),
            h("br", null),
            h("label", { class: "" }, "Input Label-2"),
            h("input", { id: "input2", placeholder: "Type something...", type: "text", class: "text-input input--regular", value: "", "data-kb": true, "data-kb-style": "numpadex" }),
            h("br", null),
            h("label", { class: "" }, "Input Label-3"),
            h("input", { id: "input3", placeholder: "Type something...", type: "text", class: "text-input input--regular", value: "", "data-kb": true, "data-kb-style": "tr_TR", "data-mask": "hh.99.9999" }),
            h("br", null),
            h("px-datetime-picker", { "date-time": "2016-01-01T01:00:00.000Z", "date-format": "YYYY-MM-DD", "time-format": "HH:mm", "show-buttons": "false", "show-time-zone": "none" }),
            h("px-keyboard", null)));
    }
    static get is() { return "app-keyboard"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./app-keyboard.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["app-keyboard.css"]
    }; }
    static get elementRef() { return "myElement"; }
}
