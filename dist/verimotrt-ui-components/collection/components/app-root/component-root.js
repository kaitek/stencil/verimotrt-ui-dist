import { h } from "@stencil/core";
import '@polymer/iron-pages/iron-pages';
import { ThresholdConfig } from '../px-threshold-bar/px-thresholdConfig';
export class ComponentRoot {
    constructor() {
        //@State() modalOpened: boolean=false;
        //@State() errorMessage: string='';
        //@State() alertVisible: boolean=false;
        this.usersSmall = [
            {
                first: '1-Elizabeth',
                last: 'Wong',
                full: 'Elizabeth Wong',
                email: 'sika@iknulber.cl',
                born: '1985-04-12',
                age: 32,
                appointment: '2009-05-05 01:10'
            },
            {
                first: '2-Jeffrey',
                last: 'Hamilton',
                email: 'cofok@rac.be',
                born: '1995-04-12',
                age: 23,
                appointment: '2011-03-04 01:10'
            },
            {
                first: '3-Alma',
                last: 'Martin',
                email: 'dotta@behtam.la',
                born: '1975-04-12',
                age: 42,
                appointment: '2010-03-04 02:20'
            },
            {
                first: '4-Carl',
                last: 'Saunders',
                email: 'seh@bibapu.gy',
                born: '2002-04-12',
                age: 12,
                appointment: '2010-03-04 03:30'
            },
            {
                first: '5-Willie',
                last: 'Dennis',
                email: 'izko@dahokwej.ci',
                born: '1973-04-12',
                age: 44,
                appointment: '2010-03-04 04:40'
            },
            {
                first: '6-Angel',
                last: 'Lewis',
                email: 'ma@et.nz',
                born: '1932-04-12',
                age: 89,
                appointment: '2010-03-04 05:50'
            },
            {
                first: '7-Jessie',
                last: 'Sherman',
                email: 'hunocnas@mosuraj.gi',
                born: '1990-04-12',
                age: 27,
                appointment: '2010-03-04 06:00'
            },
            {
                first: '8-Eric',
                last: 'Brewer',
                email: 'tu@coajoul.de',
                born: '1930-04-12',
                age: 88,
                appointment: '2010-03-04 07:00'
            },
            {
                first: '9-Cory',
                last: 'Ramos',
                email: 'kilamja@oviafbek.ss',
                born: '2006-04-12',
                age: 9,
                appointment: '2010-03-04 08:00'
            },
            {
                first: '10-Bertie',
                last: 'Ross',
                email: 'ifuvu@getvi.my',
                born: '1990-04-12',
                age: 27,
                appointment: '2010-03-04 09:00'
            },
            {
                first: '11-Oscar',
                last: 'Estrada',
                email: 'minu@kerireg.gp',
                born: '2002-04-12',
                age: 15,
                appointment: '2010-03-04 10:00'
            },
            {
                first: '12-Estelle',
                last: 'Patton',
                email: 'hudliami@lijihen.fi',
                born: '2006-04-12',
                age: 11,
                appointment: '2010-03-04 11:00'
            },
            {
                first: '13-Sallie',
                last: 'George',
                email: 'wo@zof.bf',
                born: '1985-04-12',
                age: 33,
                appointment: '2010-03-04 12:00'
            },
            {
                first: '14-Harriett',
                last: 'Wheeler',
                email: 'woguw@cibevo.pt',
                born: '1968-04-12',
                age: 52,
                appointment: '2010-03-04 13:00'
            },
            {
                first: '15-Bryan',
                last: 'Houston',
                email: 'tekkubom@gaahu.ge',
                born: '1940-04-12',
                age: 77,
                appointment: '2010-03-04 14:00'
            },
            {
                first: '16-Leon',
                last: 'Craig',
                email: 'wo@gurozo.gs',
                born: '2015-04-12',
                age: 2,
                appointment: '2010-03-04 15:00'
            },
            {
                first: '17-Mable',
                last: 'Taylor',
                email: 'um@fegnocka.pg',
                born: '2006-04-12',
                age: 11,
                appointment: '2010-03-04 16:00'
            },
            {
                first: '18-Ida',
                last: 'Hansen',
                email: 'lufahu@gewlaskoc.kh',
                born: '1973-04-12',
                age: 44,
                appointment: '2010-03-04 17:00'
            },
            {
                first: '19-Adele',
                last: 'Thornton',
                email: 'gugugo@nevigi.th',
                born: '1973-04-12',
                age: 41,
                appointment: '2010-03-04 18:00'
            },
            {
                first: '20-Jerry',
                last: 'Kelley',
                email: 'solef@zoose.as',
                born: '1950-04-12',
                age: 67,
                appointment: '2010-03-04 19:00'
            },
            {
                first: '21-Clara',
                last: 'Delgado',
                email: 'fivticnuf@upkib.rw',
                born: '1951-04-12',
                age: 66,
                appointment: '2010-03-04 20:00'
            },
            {
                first: '22-Joseph',
                last: 'Stevenson',
                email: 'be@viijo.lk',
                born: '1995-04-12',
                age: 22,
                appointment: '2010-03-04 21:00'
            },
            {
                first: '23-Ellen',
                last: 'Perry',
                email: 'ce@jewo.sv',
                born: '1996-04-12',
                age: 23,
                appointment: '2010-03-04 22:00'
            },
            {
                first: '24-Ronnie',
                last: 'Cummings',
                email: 'iro@wi.vn',
                born: '2000-04-12',
                age: 17,
                appointment: '2010-03-04 23:00'
            },
            {
                first: '25-Olive',
                last: 'Santos',
                email: 'vo@nees.cn',
                born: '1918-04-12',
                age: 99,
                appointment: '2010-03-05 00:00'
            },
            {
                first: '26-Rena',
                last: 'Tucker',
                email: 'uharoc@lohpol.gl',
                born: '1915-04-12',
                age: 102,
                appointment: '2010-03-05 01:00'
            },
            {
                first: '27-Nell',
                last: 'Hicks',
                email: 'felaf@vo.sb',
                born: '1940-04-12',
                age: 76,
                appointment: '2010-03-05 02:00'
            },
            {
                first: '28-Jesus',
                last: 'Hawkins',
                email: 'wahgab@mu.mk',
                born: '1983-04-12',
                age: 34,
                appointment: '2010-03-05 03:00'
            },
            {
                first: '29-Duane',
                last: 'Harris',
                email: 'kellanjob@daava.ph',
                born: '1996-04-12',
                age: 23,
                appointment: '2010-03-05 04:00'
            },
            {
                first: '30-Jonathan',
                last: 'Holmes',
                email: 'jir@bikuf.dj',
                born: '1969-04-12',
                age: 49,
                appointment: '2010-03-05 05:00'
            },
            {
                first: '31-Christine',
                last: 'Collier',
                email: 'bocago@jerla.ba',
                born: '1970-04-12',
                age: 48,
                appointment: '2010-03-05 06:00'
            },
            {
                first: '32-Brandon',
                last: 'Thompson',
                email: 'regoh@ji.kn',
                born: '1973-04-12',
                age: 43,
                appointment: '2010-03-05 07:00'
            },
            {
                first: '33-Anne',
                last: 'Dunn',
                email: 'samhof@are.sc',
                born: '1976-04-12',
                age: 41,
                appointment: '2010-03-05 08:00'
            },
            {
                first: '34-Viola',
                last: 'Sherman',
                email: 'wep@kezori.lt',
                born: '1973-04-12',
                age: 44,
                appointment: '2010-03-05 09:00'
            },
            {
                first: '35-Kathryn',
                last: 'Harper',
                email: 'keotoci@je.mr',
                born: '1941-04-12',
                age: 77,
                appointment: '2010-03-05 10:00'
            },
            {
                first: '36-Calvin',
                last: 'Bates',
                email: 'bomeg@lip.bw',
                born: '1979-04-12',
                age: 38,
                appointment: '2010-03-05 11:00'
            },
            {
                first: '37-Maggie',
                last: 'Collins',
                email: 'zonefto@ihihij.ke',
                born: '1982-04-12',
                age: 35,
                appointment: '2010-03-05 12:00'
            },
            {
                first: '38-Zachary',
                last: 'Mitchell',
                email: 'wamez@cilvahbod.mk',
                born: '1993-04-12',
                age: 26,
                appointment: '2010-03-05 13:00'
            },
            {
                first: '39-Raymond',
                last: 'Kelley',
                email: 'wuz@napujos.tt',
                born: '1995-04-12',
                age: 21,
                appointment: '2010-03-05 14:00'
            },
            {
                first: '40-Augusta',
                last: 'Torres',
                email: 'bum@ne.lt',
                born: '1967-04-12',
                age: 54,
                appointment: '2010-03-05 15:00'
            },
            {
                first: '41-Charlie',
                last: 'Lindsey',
                email: 'ruhlu@tuobujen.ao',
                born: '2002-04-12',
                age: 15,
                appointment: '2010-03-05 16:00'
            },
            {
                first: '42-Jeremy',
                last: 'Swanson',
                email: 'ed@ted.km',
                born: '1997-04-12',
                age: 21,
                appointment: '2010-03-05 17:00'
            },
            {
                first: '43-Joel',
                last: 'Gonzalez',
                email: 'ej@pot.bz',
                born: '1999-04-12',
                age: 18,
                appointment: '2010-03-05 18:00'
            },
            {
                first: '44-Lillie',
                last: 'Hawkins',
                email: 'uguiv@mudbuve.pa',
                born: '1975-04-12',
                age: 43,
                appointment: '2010-03-05 19:00'
            },
            {
                first: '45-Joel',
                last: 'Watts',
                email: 'naafe@oli.bb',
                born: '2017-04-12',
                age: 1,
                appointment: '2010-03-05 20:00'
            },
            {
                first: '46-Isabella',
                last: 'Sandoval',
                email: 'asobu@wedef.af',
                born: '2016-04-12',
                age: 3,
                appointment: '2010-03-05 21:00'
            },
            {
                first: '47-Roy',
                last: 'Lyons',
                email: 'rasaogu@tadiri.tc',
                born: '1924-04-12',
                age: 88,
                appointment: '2010-03-05 22:00'
            }
        ];
    }
    alertHide() {
        //this.alertVisible=false;
        //debugger
        //this.pxAlertMessage.setVisible(false);
    }
    async setValueGauge(value) {
        this.gauge.setValue(value);
    }
    async setValueTreshold1(value) {
        this.thresholdBar1.setValue(value);
    }
    async setValueTreshold2(value) {
        this.thresholdBar2.setValue(value);
    }
    async setValuePercentege(value) {
        this.percentCircle.setValue(value);
    }
    async setValueProgress(value) {
        this.progressBar.setValue(value);
    }
    async setValueKpi(value) {
        this.kpi.value = value;
        this.kpi.statusIcon = "fa-arrow-down";
        this.kpi.statusColor = "red";
        this.kpi.statusLabel = "25%";
    }
    async setValueKpiList() {
        let kpiItems = [
            { "label": "Availability", "value": 90, "uom": "%" },
            { "label": "Reliability", "value": 62.5, "uom": "%" },
        ];
        this.kpiList.values = kpiItems;
    }
    async setIOview(group, index, state) {
        await this.ioview.setPort(group, index, state);
    }
    async alertShow() {
        //if(!this.alertVisible){
        //  this.alertVisible=!this.alertVisible;
        //}
        let _v = await this.alertMessage.getVisible();
        if (_v === false) {
            this.alertMessage.setVisible(true);
        }
    }
    async setOEE(kul, per, kal, oee) {
        this.oeeview.usability = kul;
        this.oeeview.performance = per;
        per;
        this.oeeview.quality = kal;
        this.oeeview.oee = oee;
    }
    modalHide() {
        //this.modalOpened=false;
    }
    async modalShow() {
        //if(!this.modalOpened){
        //  this.modalOpened=!this.modalOpened;
        //}
        let _v = await this.modal.getVisible();
        if (_v === false) {
            this.modal.setVisible(true);
        }
    }
    componentDidLoad() {
        ////this.modalContainer.style.minHeight='250px';
        //console.log('componentDidLoad');
        this.progressBar.setValue(40);
        this.thresholdBar1.setValue(45);
        this.thresholdBar2.setValue(0);
        this.gauge.setValue(55);
        this.percentCircle.setValue(42);
    }
    next() {
        if (this.stepper.currentStep < this.stepper.items.length - 1) {
            this.stepper.next();
        }
    }
    previous() {
        if (this.stepper.currentStep > 0) {
            this.stepper.previous();
        }
    }
    complete() {
        this.stepper.complete();
    }
    error() {
        this.stepper.error(parseInt(this.errorIndex.value) - 1);
    }
    isValidCheck() {
        var validator = this.validationEl;
        var inputValue = this.validationInput.value;
        validator.val = inputValue;
    }
    validationStateChange(event) {
        if (!event.detail.passedValidation) {
            this.validationWarning.classList.remove("hidden");
            this.validationInput.classList.add("validation-error");
            this.validationWarning.innerHTML = event.detail.failedValidationErrorMessage;
        }
        else {
            this.validationWarning.classList.add("hidden");
            this.validationInput.classList.remove("validation-error");
            this.validationWarning.innerHTML = '';
        }
    }
    handleTableSelect(e) {
        console.log(e);
        if (e.detail.id === 'CSV') {
            //this.exportToCsv();
        }
    }
    handleTableDeselect(e) {
        console.log(e);
        if (e.detail.id === 'CSV') {
            //this.exportToCsv();
        }
    }
    handleViewHeaderChanged(e) {
        console.log(e);
        this.viewHeader.buttonStateChange('next', 'disable');
    }
    render() {
        /*
         
        
        */
        let columns = [
            {
                name: '#',
                path: '__#__',
                type: 'number',
                width: '60px',
                renderer: 'px-data-grid-number-renderer',
                frozen: true,
                flexGrow: 0
            },
            {
                name: "First Name",
                path: "first",
                renderer: "px-data-grid-string-renderer",
                editable: true,
                frozen: true,
                id: "first[string]"
                //}, {
                //  name: "Last Name",
                //  path: "last",
                //  renderer: "px-data-grid-string-renderer",
                //  editable: true,
                //  frozen: false,
                //  id: "last[string]"
            }, {
                name: "Email",
                path: "email",
                frozen: false,
                id: "email[string]"
            }, {
                name: 'Age',
                path: 'age',
                renderer: 'px-data-grid-number-renderer',
                editable: true,
                width: '20px',
                type: 'number'
            }, {
                name: 'Birth Date',
                path: 'born',
                //dateFormat: {
                //  format: 'YYYY-MM-DD'
                //},
                rendererConfig: {
                    hideTime: true,
                    dateFormat: 'YYYY-MM-DD'
                },
                renderer: 'px-data-grid-date-renderer',
                editable: true,
                width: '60px',
                type: 'date'
            }
            //,{
            //  name: 'Appointment',
            //  path: 'appointment',
            //  //dateFormat: {
            //  //  format: 'DD-MM-YYYY HH:mm',
            //  //  timezone: 'UTC'
            //  //},
            //  filterByTime: true,
            //  renderer: 'px-data-grid-date-renderer',
            //  rendererConfig: {
            //    hideTime: false,
            //    dateFormat: 'YYYY-MM-DD HH:mm',
            //    timezone: 'Europe/Istanbul',
            //    datePickerDateFormat: 'YYYY-MM-DD',
            //    datePickerTimeFormat: 'HH:mm'
            //  },
            //  editable: true,
            //  type: 'date'
            //}
        ];
        //let tableActions=[{
        //  "name": "Export CSV",
        //  "icon": "px-doc:document-pdf",
        //  "id": "CSV"
        //}];
        let itemActions = [{
                "name": "Add Row",
                "icon": "px-utl:add",
                "id": "add"
            }, {
                "name": "Delete Row",
                "icon": "px-utl:delete",
                "id": "delete"
            }];
        //let highLights=[{type:"cell",color:"lightgreen",condition:(cellContent/*, column, item*/) => {
        //  return cellContent == 'Dennis';
        //}},{type:"column",color:"lightskyblue",condition:(column, item) => {
        //  return item && column.path == 'last';
        //}},{type:"row",color:"lightsalmon",condition:(cellContent, item) => {
        //  return cellContent!=='' && item && (item.last == 'Dennis' || item.first == 'Carl');
        //}}];
        let selectedItems = [];
        let dropdownitems = [
            { "key": "1", "val": "iPhone" },
            { "key": "2", "val": "Android" },
            { "key": "3", "val": "Blackberry" },
            { "key": "4", "val": "Windows Phone" },
            { "key": "5", "val": "Flip Phone", "disabled": true }
        ];
        let tabItems = [
            { id: "tab1", title: "Data Grid" },
            { id: "tab2", title: "Steps" },
            { id: "tab3", title: "View Header" },
            { id: "tab4", title: "KPI List" },
            { id: "tab5", title: "Card" },
            { id: "tab6", title: "Keyboard" },
            { id: "tab7", title: "Form Elements" },
            { id: "tab8", title: "Data Visualization" }
        ];
        let viewHeaderItems = [
            { id: "view1", title: "Bengal Tiger", subTitle: "1" },
            { id: "view2", title: "Black Spider Monkey", subTitle: "2" },
            { id: "view3", title: "Sumatran Elephant", subTitle: "3" },
            { id: "view4", title: "Ganges River Dolphin", subTitle: "4" }
        ];
        let _thresholdConfig = [
            new ThresholdConfig({ min: 0, max: 40, color: "#F34336" }),
            new ThresholdConfig({ min: 40, max: 80, color: "#FF8B3A" }),
            new ThresholdConfig({ min: 80, max: 100, color: "#7FAE1B" })
        ];
        let _thresholdConfig2 = [
            { "min": -50, "max": -25, "color": "#F34336" },
            { "min": -25, "max": -10, "color": "#FF8B3A" },
            { "min": -10, "max": 10, "color": "#7FAE1B" },
            { "min": 10, "max": 25, "color": "#FF8B3A" },
            { "min": 25, "max": 50, "color": "#F34336" }
        ];
        let kpiItems = [
            { "label": "Availability", "value": 99, "uom": "%" },
            { "label": "Reliability", "value": 92.5, "uom": "%" },
        ];
        let err = [[0, 12], [79, 100]];
        let abnormal = [[12, 32], [68, 79]];
        let anomaly = [[32, 45], [54, 68]];
        let normal = [[45, 54]];
        return (h("div", { style: { width: "100%", height: "100%" } },
            h("px-tabs", { id: "tab-set1", selected: "1", items: tabItems }),
            h("iron-pages", null,
                h("div", { id: "tab1-content" },
                    h("div", { id: "demo1_data_grid", class: "flex flex--row flex--left flex--top" },
                        h("div", { style: { width: "750px", height: "400px" } },
                            h("px-data-grid", { columns: columns, tableData: this.usersSmall, striped: true, sortable: true, multiSort: false, resizable: false, editable: false, hideSelectionColumn: false, selectionMode: "multi", allowSortBySelection: false, hideActionMenu: false, hideColumnFilter: false, itemActions: itemActions, size: 20, itemIdPath: "email", selectedItems: selectedItems, onPxTableSelect: (e) => this.handleTableSelect(e), onPxTableDeselect: (e) => this.handleTableDeselect(e) })),
                        h("div", { style: { width: "750px", height: "400px" } },
                            h("vaadin-grid", { "aria-label": "Dynamic Data Example", theme: "row-stripes", items: this.usersSmall, pageSize: "20" },
                                h("vaadin-grid-selection-column", { "auto-select": true, frozen: true }),
                                h("vaadin-grid-column", { path: "first", header: "First name" }),
                                h("vaadin-grid-column", { path: "last", header: "Last name" }),
                                h("vaadin-grid-column", { path: "email" }),
                                h("vaadin-grid-column", { path: "age", width: "80px", "text-align": "end" }),
                                h("vaadin-grid-column", { path: "born", width: "100px" }))))),
                h("div", { id: "tab2-content" },
                    h("div", { id: "demo1_steps", class: "flex flex--row flex--justify flex--top" },
                        h("div", { style: { width: "100%", height: "100%" } },
                            h("px-steps", { ref: (el) => this.stepper = el, items: [{ "id": "1", "label": "Basic Information" }, { "id": "2", "label": "Select Services" }, { "id": "3", "label": "Billing" }, { "id": "4", "label": "Review" }, { "id": "5", "label": "Deploy" }], completed: ["1", "2"], errored: ["1"], currentStep: 2 }),
                            h("br", null),
                            h("div", { style: { "margin-top": "4rem;" } },
                                h("p", null, "For demo purposes only:"),
                                h("button", { class: "btn", "on-click": () => this.previous() }, "Previous"),
                                h("button", { class: "btn", "on-click": () => this.complete() }, "Complete Current Step"),
                                h("button", { class: "btn", "on-click": () => this.next() }, "Next"),
                                h("button", { class: "btn", "on-click": () => this.error() }, "Error Step: "),
                                h("select", { id: "errorIndex", ref: (el) => this.errorIndex = el, class: "btn" },
                                    h("option", { style: { background: "var(--px-dropdown-bg-color, white)" } }, "1"),
                                    h("option", { style: { background: "var(--px-dropdown-bg-color, white)" } }, "2"),
                                    h("option", { style: { background: "var(--px-dropdown-bg-color, white)" } }, "3"),
                                    h("option", { style: { background: "var(--px-dropdown-bg-color, white)" } }, "4"),
                                    h("option", { style: { background: "var(--px-dropdown-bg-color, white)" } }, "5")))))),
                h("div", { id: "tab3-content" },
                    h("px-view-header", { ref: (el) => this.viewHeader = el, selected: "1", items: viewHeaderItems, onPxViewHeaderChanged: (e) => this.handleViewHeaderChanged(e) }),
                    h("iron-pages", null,
                        h("div", { id: "view1-content" },
                            h("p", null, "111"),
                            h("px-button", { text: "Next Enable", type: "call-to-action", size: "huge", "on-click": () => this.viewHeader.buttonStateChange('next', 'enable') })),
                        h("div", { id: "view2-content" },
                            h("p", null, "222"),
                            h("px-button", { text: "Next Enable", type: "call-to-action", size: "huge", "on-click": () => this.viewHeader.buttonStateChange('next', 'enable') })),
                        h("div", { id: "view3-content" },
                            h("p", null, "333"),
                            h("px-button", { text: "Next Enable", type: "call-to-action", size: "huge", "on-click": () => this.viewHeader.buttonStateChange('next', 'enable') })),
                        h("div", { id: "view4-content" },
                            h("p", null, "444"),
                            h("px-alert-message", { ref: (el) => this.alertMessageviewheader = el, type: "error", action: "acknowledge", messageTitle: "This message title is too long. The user will need to expand it by clicking 'Show More'.", message: "We understand that sometimes long messages are needed but it is not a best practice to have more than three lines of text in the alert message.", autoDismiss: 0, onPxAlertMessageAlertHiddenTriggered: () => this.alertHide() }),
                            h("px-button", { text: "Finish", type: "call-to-action", size: "huge", "on-click": async () => {
                                    let _v = await this.alertMessageviewheader.getVisible();
                                    if (_v === false) {
                                        this.alertMessageviewheader.setVisible(true);
                                    }
                                } })))),
                h("div", { id: "tab4-content" },
                    h("div", { id: "demo1_kpi", class: "flex flex--row flex--left flex--top" },
                        h("div", { style: { width: "500px", height: "400px" } },
                            h("px-kpi-list", { ref: (el) => this.kpiList = el, label: "Asset GE1892", values: kpiItems, statusIcon: "fa-arrow-up", statusColor: "green", statusLabel: "12%", footer: "Last 30 days" })))),
                h("div", { id: "tab5-content" },
                    h("div", { id: "demo1_card", class: "flex flex--row flex--left flex--top" },
                        h("div", { style: { width: "750px", height: "400px" } },
                            h("px-card", { headerText: "Card", icon: "fa-camera" },
                                h("px-icon", { slot: 'actions', icon: 'fa-info', onPxIconTapped: () => { this.modal.fillContainer = false; this.modalShow(); } }),
                                h("px-icon", { slot: 'actions', icon: 'fa-camera' }),
                                h("px-icon", { slot: 'actions', icon: 'fa-times', onPxIconTapped: () => { this.modal.fillContainer = true; this.modalShow(); } }),
                                h("div", { class: "demo-component-container u-p++", ref: (el) => this.modalContainer = el },
                                    h("px-modal", { ref: (el) => this.modal = el, 
                                        //opened={this.modalOpened}
                                        headerText: "Confirm delete", bodyText: "Do you want to delete this record? The record will be deleted permanently.", acceptText: "Delete Record", rejectText: "Cancel", onPxModalRejected: () => this.modalHide(), onPxModalAccepted: () => this.modalHide(), onPxModalDismissed: () => this.modalHide() }),
                                    h("px-button", { text: "Open Modal", size: "large", type: "tertiary", onPxButtonClick: () => this.modalShow() }),
                                    h("px-notification", { type: "healthy", statusIcon: "fa-filter", content: "healthy-26 Filters", actionIcon: "fa-times", opened: true }),
                                    h("px-notification", { type: "info", statusIcon: "fa-trash-alt", content: "info-Widget has been removed from your dashboard.", opened: true },
                                        h("button", { slot: 'right', class: 'whiteButton' }, "Undo")),
                                    h("px-notification", { type: "error", statusIcon: "fa-trash-alt", content: "error-Widget has been removed from your dashboard.", opened: true },
                                        h("button", { slot: 'right', class: 'whiteButton' }, "Undo")),
                                    h("px-notification", { type: "warning", statusIcon: "fa-trash-alt", content: "warning-Widget has been removed from your dashboard.", opened: true },
                                        h("button", { slot: 'right', class: 'whiteButton' }, "Undo")),
                                    h("px-notification", { type: "important", statusIcon: "fa-trash-alt", content: "important-Widget has been removed from your dashboard.", opened: true },
                                        h("button", { slot: 'right', class: 'whiteButton' }, "Undo"))))))),
                h("div", { id: "tab6-content" },
                    h("div", { id: "demo1_keyboard", class: "flex flex--row flex--left flex--top" },
                        h("div", { style: { width: "500px", height: "400px" } },
                            h("label", { class: "" }, "Input Label-1"),
                            h("input", { id: "input1", placeholder: "Type something...", type: "text", class: "text-input input--regular", value: "", "data-kb": true, "data-kb-style": "numpad" }),
                            h("br", null),
                            h("label", { class: "" }, "Input Label-2"),
                            h("input", { id: "input2", placeholder: "Type something...", type: "text", class: "text-input input--regular", value: "", "data-kb": true, "data-kb-style": "numpadex" }),
                            h("br", null),
                            h("label", { class: "" }, "Input Label-3"),
                            h("input", { id: "input3", placeholder: "Type something...", type: "text", class: "text-input input--regular", value: "", "data-kb": true, "data-kb-style": "tr_TR", "data-mask": "hh.99.9999" }),
                            h("px-keyboard", null)))),
                h("div", { id: "tab7-content" },
                    h("div", { id: "demo1_datetime", class: "flex flex--row flex--left flex--top" },
                        h("div", { style: { width: "750px", height: "400px" } },
                            h("px-calendar-picker", { 
                                // from-moment="{{props.fromMoment.value}}"
                                // to-moment="{{props.toMoment.value}}"
                                displayMode: 'day', blockFutureDates: false, blockPastDates: true, preventRangeSelection: true, hideNextButton: false, hidePreviousButton: false, shrink: false, minDate: "2020-10-09 07:36:58", maxDate: "2020-11-25 07:36:58" }),
                            h("br", null),
                            h("px-time-picker", { displayedValue: "00:00" })),
                        h("div", { style: { width: "500px", height: "400px" } },
                            h("px-datetime-picker", { "date-time": "2016-01-01T01:00:00.000Z", "date-format": "YYYY-MM-DD", "time-format": "HH:mm", "show-buttons": "false", "show-time-zone": "none" }),
                            h("br", null),
                            h("div", { style: { width: "120px" } },
                                h("px-heading", { text: "App Starter", class: "epsilon" })),
                            h("div", null,
                                h("px-dropdown", { items: dropdownitems, sortMode: "key", buttonStyle: "default", multi: true })),
                            h("px-alert-message", { ref: (el) => this.alertMessage = el, type: "error", action: "acknowledge", messageTitle: "This message title is too long. The user will need to expand it by clicking 'Show More'.", message: "We understand that sometimes long messages are needed but it is not a best practice to have more than three lines of text in the alert message.", autoDismiss: 0, onPxAlertMessageAlertHiddenTriggered: () => this.alertHide() }),
                            h("px-button", { text: "Alert", size: "large", onPxButtonClick: () => this.alertShow() }),
                            h("px-button", { text: "Profile", type: "call-to-action", size: "huge" }),
                            h("div", { style: { width: "100px" } },
                                h("px-toggle", { size: "huge" }))),
                        h("div", { style: { width: "300px" } },
                            h("px-validation", { id: "validationEl", ref: (el) => this.validationEl = el, onPxValidatonStateChange: (res) => { this.validationStateChange(res); } },
                                h("px-validator", { 
                                    //validationMethod="isNumber"
                                    multiStepValidation: ["isNumber", "isBetween"], arguments: [1, 5] })),
                            h("input", { id: "example", ref: (el) => this.validationInput = el, class: "text-input input--regular", type: "text", placeholder: "", min: 1, max: 5, step: 1, value: 1, "on-input": () => { this.isValidCheck(); } }),
                            h("br", null),
                            h("small", { id: "validationWarning", ref: (el) => this.validationWarning = el, class: "caps form-field__help validation-error" }),
                            h("br", null),
                            h("px-typeahead", { placeholder: "Enter your search query", localCandidates: ["Alabama", "Alaska", "American Samoa", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District Of Columbia", "Federated States Of Micronesia", "Florida", "Georgia", "Guam", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Marshall Islands", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Northern Mariana Islands", "Ohio", "Oklahoma", "Oregon", "Palau", "Pennsylvania", "Puerto Rico", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virgin Islands", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"] })))),
                h("div", { id: "tab8-content" },
                    h("div", { id: "demo1_datavis", class: "flex flex--row flex--left flex--top" },
                        h("div", null,
                            h("div", { style: { width: "250px" } },
                                h("px-percent-circle", { ref: (el) => this.percentCircle = el, max: 100, thickness: 30 }))),
                        h("div", null,
                            h("div", { style: { width: "250px" } },
                                h("px-gauge", { ref: (el) => this.gauge = el, min: 0, max: 100, barWidth: 0, unit: "adet", error: err, abnormal: abnormal, anomaly: anomaly, normal: normal }))),
                        h("div", { style: { marginLeft: "50px" } },
                            h("div", { style: { width: "200px", height: "50px" } },
                                h("px-progress-bar", { ref: (el) => this.progressBar = el, min: 0, max: 100 })),
                            h("div", { style: { width: "200px", height: "100px" } },
                                h("div", { style: { width: "60px" } },
                                    h("px-spinner", { size: 50 })),
                                h("div", { style: { width: "60px" } },
                                    h("px-icon", { icon: "fa-camera", size: "large", pxstyle: { color: "green" } })),
                                h("div", { style: { width: "220px", height: "70px" } },
                                    h("px-io-view", { ref: (el) => this.ioview = el, inputs: 8, outputs: 6 })),
                                h("div", { style: { width: "200px", height: "160px" } },
                                    h("px-oee-view", { ref: (el) => this.oeeview = el, usability: 95, performance: 90, quality: 80, oee: 70 }))))),
                    h("div", { id: "demo1_datavis_2", class: "flex flex--row flex--left flex--top" },
                        h("div", null,
                            h("div", { style: { width: "200px", height: "70px" } },
                                h("px-threshold-bar", { ref: (el) => this.thresholdBar2 = el, min: -50, max: 50, uom: "\u00B0", config: _thresholdConfig2 }))),
                        h("div", null,
                            h("div", { style: { width: "200px", marginLeft: "50px" } },
                                h("px-kpi", { ref: (el) => this.kpi = el, "spark-type": "line", label: "Orders", value: "30M", uom: "USD", statusIcon: "fa-arrow-up", statusColor: "green", statusLabel: "12%" })))),
                    h("div", { id: "demo1_datavis_3", class: "flex flex--row flex--left flex--top" },
                        h("div", null,
                            h("div", { style: { width: "200px", height: "70px" } },
                                h("px-threshold-bar", { ref: (el) => this.thresholdBar1 = el, min: 0, max: 100, uom: "%", config: _thresholdConfig })))),
                    h("div", { id: "demo1_datavis_4", class: "flex flex--row flex--left flex--top" },
                        h("div", null,
                            h("div", { style: { width: "200px", height: "70px" } },
                                h("px-logo", null))))))));
    }
    static get is() { return "component-root"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["../px-sample-app-design/px-sample-app.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["../px-sample-app-design/px-sample-app.css"]
    }; }
    static get methods() { return {
        "setValueGauge": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValueTreshold1": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValueTreshold2": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValuePercentege": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValueProgress": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValueKpi": {
            "complexType": {
                "signature": "(value: string) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValueKpiList": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setIOview": {
            "complexType": {
                "signature": "(group: \"input\" | \"output\", index: number, state: 0 | 1) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "alertShow": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setOEE": {
            "complexType": {
                "signature": "(kul: number, per: number, kal: number, oee: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
}
