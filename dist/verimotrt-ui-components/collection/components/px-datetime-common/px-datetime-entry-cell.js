import { h } from "@stencil/core";
export class PxDatetimeEntryCell {
    constructor() {
        this.isSelected = false;
        this._cellLength = 0;
        this.timeZone = 'Europe/Istanbul';
        this.momentObj = null;
    }
    componentWillLoad() {
        this._max = this._getMax(this.momentFormat);
        this._min = this._getMin(this.momentFormat);
        this._isNumber = this._computeIsNumber(this.momentFormat);
    }
    componentDidLoad() {
        //this._addGeneralKeyBindings();
        setTimeout(() => {
            this._updateInputValue();
        }, 10);
    }
    clear() {
        this.el.value = '';
    }
    setValueFromMoment() {
        //if moment is null (or somehow undefined) => clear
        if (!this.momentObj) {
            this.clear();
        }
        else {
            this.el.value = this.momentObj.tz(this.timeZone).format(this.momentFormat);
        }
    }
    _computeIsNumber(momentFormat) {
        return !(/^(?:a|A|MMM|MMMM|Do|ddd|dddd|Z|ZZ)$/.test(momentFormat));
    }
    _getPattern() {
        if (this._isNumber === undefined) {
            return null;
        }
        if (this._isNumber) {
            //should help with mobile keyboard (i.e get the number keyboard rather than the phone one)
            return '[0-9]*';
        }
        else {
            return null;
        }
    }
    /**
     * Set the correct type of input field based on the momentFormat
     */
    _getType() {
        if (this._isNumber === undefined) {
            return;
        }
        return this._isNumber ? 'tel' : 'text';
    }
    /**
     * Disables the input field for time zone entry cells.
     */
    _getDisabled() {
        return (this.momentFormat === 'Z' || this.momentFormat === 'ZZ');
    }
    _formatChanged() {
        if (this.momentFormat !== undefined) {
            this._updateInputValue();
            this._sizeInputs();
            if (this.momentFormat === 'A' || this.momentFormat === 'a') {
                this._cellLength = 2;
            }
            else if (this.momentFormat !== 'Z' && this.momentFormat !== 'ZZ') {
                this._cellLength = this.momentFormat.length;
            }
            else {
                //don't prevent timezone size
                this._cellLength = -1;
            }
        }
    }
    /**
     * Size the input fields based on the value or placeholder text
     */
    _sizeInputs() {
        //create a dummy canvas to measure string and make size the input correctly
        var style = window.getComputedStyle(this.el, null), fontSize = style.getPropertyValue('font-size'), fontFamily = style.getPropertyValue('font-family'), c = document.createElement('canvas'), ctx = c.getContext('2d'), length;
        if (fontSize === "0px" || fontSize === "0" || fontSize === "") {
            setTimeout(function () {
                this._sizeInputs();
            }.bind(this), 50);
            return;
        }
        ctx.font = fontSize + " " + fontFamily;
        if (this.el.value) {
            length = ctx.measureText(this.el.value.toUpperCase()).width + 1;
        }
        else {
            // the extra width is needed for the placeholder ONLY on Edge and Firefox
            length = ctx.measureText(this.el.placeholder).width + 2;
        }
        length = Math.ceil(length);
        this.el.style['width'] = length + 'px';
    }
    /**
     * Sets up regular key bindings
     */
    //_addGeneralKeyBindings(){
    //    if(this.momentFormat === 'A' || this.momentFormat === 'a'){
    //      this.addOwnKeyBinding('a p','_toggleAMPM');
    //      this.addOwnKeyBinding('down up', '_toggleAMPM');
    //      this.addOwnKeyBinding('backspace del','_preventDeleteDefault');
    //    } else if(this.momentFormat !== 'Z' && this.momentFormat !== 'ZZ') {
    //      this.addOwnKeyBinding('down up', '_wraparound');
    //    }
    //}
    /**
     * Sets up separator keybindings
     */
    //_addSeparatorKeyBinding(){
    //    var separator = (this.symbol.trim() === '') ? 'space' : this.symbol.trim();
    //    this.addOwnKeyBinding(separator,'_moveFocusForward');
    //}
    /**
     * If `momentObj` or `timeZone` update, reflect this in the cell
     */
    _updateInputValue() {
        if (this.momentObj !== undefined && this.timeZone !== undefined) {
            this.setValueFromMoment();
            this._sizeInputs();
        }
    }
    /**
     * When resources is changed, update placeholder text and resize input if need.
     */
    _onResourcesUpdated() {
        const entryInput = this.el;
        const currentPlaceholderText = entryInput.placeholder;
        const nextPlaceholderText = this._placeholderText(this.momentFormat);
        if (nextPlaceholderText !== currentPlaceholderText) {
            entryInput.placeholder = nextPlaceholderText;
            if (!this.momentObj) {
                this._sizeInputs();
            }
        }
    }
    /**
     * Fires when 'left' key is hit
     *
     * @event px-entry-cell-move
     * @param {number} dir - Values -1
     */
    //_moveFocusBack (){
    //    setTimeout(function(){
    //      this.fire('px-entry-cell-move', { 'dir' : -1 });
    //    }.bind(this),10);
    //}
    /**
     * Fires when 'right' key is hit
     *
     * @event px-entry-cell-move
     * @param {number} dir - Values 1
     */
    //_moveFocusForward (){
    //    setTimeout(function(){
    //      this.fire('px-entry-cell-move', { 'dir' : 1 });
    //    }.bind(this),10);
    //}
    /**
     * Propagate the focus event up to entry to apply inline edit styles if applicable.
     */
    //_handleFocus() {
    //  if(this.momentFormat !== 'Z' && this.momentFormat !== 'ZZ') {
    //    this.isSelected=true;
    //  }
    //  this.pxCellFocused.emit();
    //}
    /**
     * Allow the user to loop through valid cell values
     * ex If the momentFormat is MM the cell value can go from 12 to 1 by hitting the up arrow
     */
    //_wraparound(evt) {
    //    var key = evt.detail.combo;
    //        dtEntryValue = this.el.value,
    //        currentVal = dtEntryValue ? parseInt(dtEntryValue) : (this._min - 1),
    //        isUpDown = false;
    //    if(key === 'up') {
    //      currentVal++;
    //      currentVal = currentVal > this._max ? this._min : currentVal;
    //      currentVal = currentVal < this._min ? this._min : currentVal;
    //      this.el.value = currentVal;
    //      evt.preventDefault();
    //    }
    //    else if(key === 'down') {
    //      currentVal--;
    //      currentVal = currentVal > this._max ? this._max : currentVal;
    //      currentVal = currentVal < this._min ? this._max : currentVal;
    //      this.el.value = currentVal;
    //      evt.preventDefault();
    //    }
    //}
    /**
     * Checks that the value has the correct number of digits for our format
     * Autocomplete function
     */
    _checkValue() {
        //if our format requires two digits and we only have one, add a 0 in front
        if (this.el.value && this.el.value.length === 1 &&
            /^(?:MM|DD|HH|hh|kk|mm|ss|YY)$/.test(this.momentFormat)) {
            this.el.value = '0' + this.el.value;
        }
        //if the format is YYYY or Y and the input is 2 characters then convert the input to a 4 character year representation
        else if (this.el.value && this.el.value.length === 2 && this.momentFormat === 'YYYY') {
            var mo = this.moment(this.el.value, 'YY');
            this.el.value = mo.format(this.momentFormat);
        }
        //else if(this.el.value && this.momentFormat[0] === 'S' && this.el.value.length < this.momentFormat.length){
        //  var dtNumber = parseInt(this.el.value);
        //  dtNumber = dtNumber.toPrecision(this.momentFormat.length) * Math.pow(10, (this.momentFormat.length - this.el.value.length));
        //  this.el.value =  dtNumber.toString();
        //}
        this._sizeInputs();
    }
    /**
     * Fires on blur
     *
     * @event px-cell-blurred
     */
    //_handleBlur() {
    //  //var ne = Polymer.dom(evt);
    //  this.isSelected=false;
    //  this._checkValue();
    //  this.pxCellBlurred.emit();
    //}
    /**
     * Toggles AM & PM when up/down or A/P keys are pressed.
     */
    //_toggleAMPM (evt){
    //    evt.preventDefault();
    //    var ne = Polymer.dom(evt),
    //        key = evt.detail.combo;
    //
    //    if(key === 'A' || key === 'a'){
    //      this._setAMPM('AM');
    //      this._moveFocusForward();
    //
    //    } else if(key === 'P' || key === 'p'){
    //      this._setAMPM('PM');
    //      this._moveFocusForward();
    //
    //    } else if(key === 'up' || key === 'down'){
    //      if(this.el.value === 'AM' || this.el.value === 'am'){
    //        this._setAMPM('PM');
    //      } else {
    //        this._setAMPM('AM');
    //      }
    //    }
    //}
    /**
     * Sets the AM/PM value.
     */
    //_setAMPM(ampm){
    //    var mo = Px.moment.tz('01:00:00 ' + ampm, 'hh:mm:ss ' + this.momentFormat, this.timeZone);
    //    this.el.value = mo.tz(this.timeZone).format(this.momentFormat);
    //}
    /**
     * Prevent the delete button from navigating back
     */
    _preventDeleteDefault(evt) {
        evt.preventDefault();
        this.clear();
    }
    /**
     * If the momentFormat format is not 'Z', 'ZZ', 'X', 'x', 'A' or 'a' then return the momentFormat
     * Note: placeholder text supports i18n
     */
    _placeholderText(momentFormat) {
        var phText = {
            'Z': String.fromCharCode(177) + 'xx:xx',
            'ZZ': String.fromCharCode(177) + 'xxxx',
            'X': 'epoch time (s)',
            'x': 'epoch time (ms)',
            'a': 'AM',
            'A': 'AM'
        };
        let text = momentFormat;
        if (phText[momentFormat]) {
            text = phText[momentFormat];
        }
        //return this.localize(text);
        return text;
    }
    /**
     * Handles before copy event
     *
     * @event px-request-datetime-entry-copy
     * @param {string} dir - Values clipboardData
     */
    /**
     * Handles before copy event, used for IE instead of the copy event
     */
    //_beforeCopy(evt) {
    //    //IE only
    //    if(!evt.clipboardData) {
    //      this.fire('px-request-datetime-entry-copy', window.clipboardData);
    //    }
    //}
    /**
     * Handles before paste event, used for IE instead of the paste event
     */
    //_beforePaste(evt) {
    //    //IE stores the data in window.clipboardData
    //    if(!evt.clipboardData) {
    //      this.fire('px-request-datetime-entry-paste', window.clipboardData);
    //    }
    //}
    /**
     * Handles paste event
     */
    //_handlePaste(evt){
    //    //IE stores the data in window.clipboardData and will deal with it on _beforePaste
    //    if(evt.clipboardData) {
    //      this.fire('px-request-datetime-entry-paste', evt.clipboardData);
    //    }
    //    evt.preventDefault();
    //}
    /**
     * Handles copy event
     */
    //_handleCopy(evt){
    //    //IE stores the data in window.clipboardData and will deal with it on _beforePaste
    //    if(evt.clipboardData) {
    //      this.fire('px-request-datetime-entry-copy', evt.clipboardData);
    //    }
    //    evt.preventDefault();
    //}
    /**
     * Format strings and setting/modification strings are different (?!?!)
     * This method converts one to the other
     */
    _convertMomentFormat() {
        var momentTypeConversion = {
            'Y': 'y',
            'M': 'M',
            'D': 'd',
            'H': 'h',
            'h': 'h',
            'k': 'h',
            'm': 'm',
            's': 's',
            'X': 's',
            'S': 'ms',
            'x': 'ms' //milliseconds
        };
        // take the first char of the moment format and convert it to the other format
        return momentTypeConversion[this.momentFormat[0]];
    }
    /**
     * Set the max value a token can be
     */
    _getMax(momentFormat) {
        if (/^(?:M|MM|h|hh)$/.test(momentFormat)) {
            return 12;
        }
        if (/^(?:D|DD)$/.test(momentFormat)) {
            return 31;
        }
        if (/^(?:H|HH)$/.test(momentFormat)) {
            return 23;
        }
        if (/^(?:k|kk)$/.test(momentFormat)) {
            return 24;
        }
        if (/^(?:m|mm|ss|s)$/.test(momentFormat)) {
            return 59;
        }
        if (momentFormat === "S") {
            return 9;
        }
        if (momentFormat === "SS") {
            return 99;
        }
        if (momentFormat === "SSS") {
            return 999;
        }
        if (momentFormat === "YY") {
            return 99;
        }
        if (momentFormat === "YYYY") {
            return 9999;
        }
        return '';
    }
    /**
     * Set the min value a token can be
     */
    _getMin(momentFormat) {
        if (/^(?:YY|YYYY|X|x|H|HH|m|mm|s|ss|S|SS|SSS)$/.test(momentFormat)) {
            return 0;
        }
        if (/^(?:M|MM|D|DD|h|hh|k|kk)$/.test(momentFormat)) {
            return 1;
        }
        return '';
    }
    //_keyPress(evt:KeyboardEvent) {
    //
    //    if(evt.key !== 'Backspace' && evt.key !== 'Delete' && evt.key !== 'Cut' && evt.key !== 'Copy' && evt.key !== 'ArrowLeft' && evt.key !== 'ArrowRight') {
    //
    //      //AM PM, only accept A and P
    //      if((this.momentFormat === 'A' || this.momentFormat === 'a') && !/[aApP]/.test(evt.key)) {
    //        evt.preventDefault();
    //        return;
    //      }
    //
    //      //ensure for numbers we only accept digits
    //      if(this._isNumber && !/[0-9]/.test(evt.key)) {
    //        evt.preventDefault();
    //        return;
    //      }
    //
    //      //already at full length, delete current value and let this new value be applied
    //      if(this._cellLength !== -1 && this.el.value.length === this._cellLength) {
    //        this.el.value = '';
    //        this._addSeparatorKeyBinding();
    //      }
    //
    //      // if it is full, we're done with this cell, go to the next
    //      if(this.el.value.length === this._cellLength - 1) {
    //        this._moveFocusForward();
    //      }
    //    }
    //}
    render() {
        return (h("input", { ref: (el) => this.el = el, "data-order": this.order, class: "datetime-entry-input text-input--bare", 
            //on-focus={ () => this._handleFocus()}
            //on-blur={ () => this._handleBlur()}
            //on-paste={ (event: ClipboardEvent) => this._handlePaste(event)}
            //on-beforepaste={ (event: ClipboardEvent) => this._beforePaste(event)}
            //on-beforecopy={ (event: ClipboardEvent) => this._beforeCopy(event)}
            //on-copy={ (event: ClipboardEvent) => this._handleCopy(event)}
            disabled: this._getDisabled(), type: this._getType(), pattern: this._getPattern(), placeholder: this._placeholderText(this.momentFormat), 
            //on-keypress={ (event: KeyboardEvent) => this._keyPress(event)}
            max: this._max, min: this._min, step: 1, value: 0 }));
    }
    static get is() { return "px-datetime-entry-cell"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-datetime-entry-cell.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-datetime-entry-cell.css"]
    }; }
    static get properties() { return {
        "momentFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "moment-format",
            "reflect": false
        },
        "symbol": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "symbol",
            "reflect": false
        },
        "order": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "order",
            "reflect": true
        },
        "isSelected": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "is-selected",
            "reflect": false,
            "defaultValue": "false"
        },
        "_max": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "\"\" | 12 | 31 | 23 | 24 | 59 | 9 | 99 | 999 | 9999",
                "resolved": "\"\" | 12 | 23 | 24 | 31 | 59 | 9 | 99 | 999 | 9999",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_max",
            "reflect": false
        },
        "_min": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "\"\" | 0 | 1",
                "resolved": "\"\" | 0 | 1",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_min",
            "reflect": false
        },
        "_isNumber": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_is-number",
            "reflect": false
        },
        "_cellLength": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_cell-length",
            "reflect": false,
            "defaultValue": "0"
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        },
        "momentObj": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "Moment",
                "resolved": "any",
                "references": {
                    "Moment": {
                        "location": "import",
                        "path": "moment-timezone"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "moment-obj",
            "reflect": false,
            "defaultValue": "null"
        }
    }; }
    static get watchers() { return [{
            "propName": "momentFormat",
            "methodName": "_formatChanged"
        }, {
            "propName": "momentObj",
            "methodName": "_updateInputValue"
        }, {
            "propName": "timeZone",
            "methodName": "_updateInputValue"
        }]; }
}
