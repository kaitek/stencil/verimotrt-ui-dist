import { h } from "@stencil/core";
//import { PxDatetimeValidate } from './px-datetime-validate';
import moment from 'moment-timezone';
import 'moment/dist/locale/tr';
export class PxDatetimeEntry {
    constructor() {
        this.dateOrTime = 'Date';
        this.momentFormat = 'YYYY-MM-DD'; //observer: '_formatChanged'
        this.hideIcon = false;
        this.hideTimeZone = false;
        this._symbolCharArray = [];
        this.showTimeZone = 'none';
        this.hoist = false;
        this.timeZone = 'Europe/Istanbul';
        this.momentObj = null;
    }
    componentWillLoad() {
        moment.locale('tr');
        moment.tz(moment(), this.timeZone);
    }
    componentDidLoad() {
        //this.validator=new PxDatetimeValidate({scope:this,root:this.elwrapper,moment:moment(),timeZone:this.timeZone,momentObj:this.momentObj});
        if (typeof this.momentFormat === 'string') {
            setTimeout(() => {
                this._formatChanged(this.momentFormat);
            }, 10);
        }
    }
    async toggleClass(name, bool, node) {
        if (arguments.length == 1) {
            bool = !node.classList.contains(name);
        }
        if (bool) {
            node.classList.add(name);
        }
        else {
            node.classList.remove(name);
        }
    }
    async clear() {
        var cells = this.elwrapper.querySelectorAll('.cell');
        cells.forEach(function (cell) {
            let tmp = cell;
            tmp.clear();
        });
    }
    async setValueFromMoment() {
        var cells = this.elwrapper.querySelectorAll('.cell');
        cells.forEach(function (cell) {
            let tmp = cell;
            tmp.setValueFromMoment();
        });
    }
    _formatChanged(newMomentFormat) {
        if (newMomentFormat !== undefined) {
            var format = this._splitFormat(newMomentFormat);
            if (format._cellFormatArray !== this._cellFormatArray || format._symbolCharArray !== this._symbolCharArray) {
                this._cellFormatArray = format._cellFormatArray;
                this._symbolCharArray = format._symbolCharArray;
                this._cellFormatArray = this._cellFormatArray;
                this._symbolCharArray = this._symbolCharArray;
                this._sizeSymbols();
            }
        }
    }
    _sizeSymbols() {
        var style = window.getComputedStyle(this.elwrapper, null), fontSize = style.getPropertyValue('font-size'), fontFamily = style.getPropertyValue('font-family'), c = document.createElement('canvas'), ctx = c.getContext('2d'), symbols = this.elwrapper.querySelectorAll('.dt-entry-symbol'), length;
        ctx.font = fontSize + " " + fontFamily;
        symbols.forEach(function (symbol) {
            length = ctx.measureText(symbol.innerHTML.trim().replace('&nbsp;', ' ')).width;
            length = Math.ceil(length);
            symbol.style['width'] = length + 'px';
        });
    }
    _splitFormat(momentFormat) {
        var re = /[MDYAaHhkmsSZXx]+/g;
        var reSymbol = /\W+/g;
        return {
            _cellFormatArray: momentFormat.match(re),
            _symbolCharArray: momentFormat.match(reSymbol)
        };
    }
    _isSymbol(i, _symbolCharArray) {
        if (_symbolCharArray === null || typeof (_symbolCharArray[i]) === 'undefined') {
            return false;
        }
        return true;
    }
    _returnSymbol(i, _symbolCharArray) {
        if (_symbolCharArray === null || typeof (_symbolCharArray[i]) === 'undefined') {
            return '';
        }
        return _symbolCharArray[i].split(' ').join('\xa0');
    }
    _setIcon() {
        if (this.dateOrTime.toLowerCase() === 'date') {
            return 'fa-calendar-alt';
        }
        else {
            return 'fa-clock';
        }
    }
    //@Listen('pxEntryCellMove')
    //_entryCellMove(evt){
    //    var ne = Polymer.dom(evt);
    //    var entryCellOrder = ne.rootTarget.order;
    //
    //    if(entryCellOrder === this._cellFormatArray.length - 1 && evt.detail.dir === 1){
    //      this.pxNextField.emit({dateOrTime: this.dateOrTime});
    //    } else if(entryCellOrder === 0 && evt.detail.dir === -1){
    //      this.pxPreviousField.emit({dateOrTime: this.dateOrTime});
    //    } else {
    //      this._focusCell(parseInt(entryCellOrder) + evt.detail.dir);
    //    }
    //}
    //@Method()
    //async _focusCell(cellNumber) {
    //  var elem = this.elwrapper.querySelector("#cell" + cellNumber);
    //  var elemInput:HTMLInputElement=elem.shadowRoot.querySelector('.datetime-entry-input');
    //  elemInput.focus();
    //}
    //_focusLastCell() {
    //  this._focusCell(this._cellFormatArray.length - 1);
    //}
    //@Listen('pxCellBlurred')
    //_handleBlur(){
    //  //var __fn_validator = this.validator._validateInput.bind(this);
    //  //__fn_validator(this.momentFormat, this.timeZone);
    //  //this.validator._validateInput(this.momentFormat, this.timeZone);
    //}
    _handleDropdownFocus() {
        this.pxDropdownFocused.emit({});
    }
    _handleDropdownBlur() {
        this.pxDropdownBlured.emit({});
    }
    _showTimeZoneText(showTimeZone) {
        return showTimeZone === "text" || showTimeZone === "abbreviatedText";
    }
    _showTimeZoneDropdown(showTimeZone) {
        return showTimeZone === "dropdown" || showTimeZone === "extendedDropdown";
    }
    _getTimeZoneList(showTimeZone) {
        if (showTimeZone === "extendedDropdown") {
            return moment.tz.names();
        }
        else if (showTimeZone === "dropdown") {
            return [
                "Etc/GMT-14",
                "Etc/GMT-13",
                "Etc/GMT-12",
                "Etc/GMT-11",
                "Etc/GMT-10",
                "Etc/GMT-9",
                "Etc/GMT-8",
                "Etc/GMT-7",
                "Etc/GMT-6",
                "Etc/GMT-5",
                "Etc/GMT-4",
                "Etc/GMT-3",
                "Etc/GMT-2",
                "Etc/GMT-1",
                "Etc/GMT-0",
                "Etc/GMT",
                "Etc/GMT0",
                "Etc/GMT+0",
                "Etc/GMT+1",
                "Etc/GMT+2",
                "Etc/GMT+3",
                "Etc/GMT+4",
                "Etc/GMT+5",
                "Etc/GMT+6",
                "Etc/GMT+7",
                "Etc/GMT+8",
                "Etc/GMT+9",
                "Etc/GMT+10",
                "Etc/GMT+11",
                "Etc/GMT+12",
                "UCT",
                "UTC"
            ];
        }
        return [];
    }
    _updateTimeZone(e) {
        this.timeZone = e.detail.val;
    }
    _handleCopy(e) {
        e.detail.setData('Text', moment.tz(this.momentObj, this.timeZone).format(this.momentFormat));
        e.stopPropagation();
    }
    _handlePaste(e) {
        //try parsing what's been pasted...
        var data = e.detail.getData('Text'), newDate = moment(data, this.momentFormat);
        if (newDate.isValid()) {
            //if this comes from another datetime-entry make sure it is the same type
            //otherwise just allow. This is to avoid changing the date when parsing a time
            /*if(!dateOrTime || (dateOrTime === this.dateOrTime)) {
      
            }*/
            this.momentObj = newDate;
        }
        else {
            console.log("tried to paste non valid date format: " + data + ". Expecting: " + this.momentFormat);
        }
        e.stopPropagation();
    }
    _getIconClass(hideIcon) {
        return !hideIcon ? "u-mr--" : "";
    }
    _getTimeZoneText(timeZone, showTimeZone) {
        if (timeZone !== undefined && showTimeZone === "abbreviatedText") {
            let tempMomentObj = this.momentObj ? this.momentObj : moment();
            return moment.tz.zone(timeZone).abbr(tempMomentObj);
        }
        else if (showTimeZone === "text") {
            return timeZone;
        }
    }
    _getTimeZoneTextClass(showTimeZone) {
        var classList = "dt-flex--no-shrink ";
        if (showTimeZone === "text" || showTimeZone === "abbreviatedText") {
            classList += "u-ml- ";
        }
        else if (showTimeZone === "dropdown" || showTimeZone === "extendedDropdown") {
            classList += "u-ml-- ";
        }
        return classList;
    }
    //_iconClicked() {
    //  this.pxDatetimeEntryIconClicked.emit({dateOrTime: this.dateOrTime});
    //}
    getTemplateEntrySymbol(idx, _symbolCharArray) {
        if (this._isSymbol(idx, _symbolCharArray)) {
            return (h("div", { class: "dt-entry-symbol" }, this._returnSymbol(idx, _symbolCharArray)));
        }
    }
    getTamplateEntryCells() {
        if (!this._cellFormatArray) {
            return;
        }
        let index = 0;
        return (this._cellFormatArray.map((item) => {
            let idx = index++;
            let _id = "cell" + idx;
            return ([
                h("px-datetime-entry-cell", { id: _id, class: "cell", order: idx, momentObj: this.momentObj, momentFormat: item, symbol: this._returnSymbol(idx, this._symbolCharArray), timeZone: this.timeZone, isSelected: this.isSelected }),
                this.getTemplateEntrySymbol(idx, this._symbolCharArray)
            ]);
        }));
    }
    render() {
        let _cls_btn = this._getIconClass(this.hideIcon) + " btn btn--bare dt-icon-button";
        return (h("div", { id: "wrapper", class: "flex flex--middle", ref: (el) => this.elwrapper = el },
            h("div", { class: "flex flex--middle dt-flex--no-shrink" },
                h("button", { class: _cls_btn },
                    h("span", { class: "a11y" }, this.dateOrTime),
                    !this.hideIcon ?
                        h("px-icon", { id: "icon", class: "dt-icon", icon: this._setIcon() })
                        : ''),
                this.getTamplateEntryCells()),
            h("div", { class: this._getTimeZoneTextClass(this.showTimeZone) },
                this._showTimeZoneDropdown(this.showTimeZone) ?
                    h("px-dropdown", { id: "dropdown", class: "dropdown", displayValue: this.timeZone, buttonStyle: "bare", searchMode: true, disableClear: true, items: this._getTimeZoneList(this.showTimeZone), onFocus: () => this._handleDropdownFocus(), onBlur: () => this._handleDropdownBlur(), hoist: this.hoist })
                    : '',
                this._showTimeZoneText(this.showTimeZone) ?
                    h("span", { id: "timeZoneText", class: "dt-text-input--non-editable" }, this._getTimeZoneText(this.timeZone, this.showTimeZone))
                    : '')));
    }
    static get is() { return "px-datetime-entry"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-datetime-entry.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-datetime-entry.css"]
    }; }
    static get properties() { return {
        "dateOrTime": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "date-or-time",
            "reflect": false,
            "defaultValue": "'Date'"
        },
        "momentFormat": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "moment-format",
            "reflect": false,
            "defaultValue": "'YYYY-MM-DD'"
        },
        "hideIcon": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-icon",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideTimeZone": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-time-zone",
            "reflect": false,
            "defaultValue": "false"
        },
        "_cellFormatArray": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "_symbolCharArray": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "showTimeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-time-zone",
            "reflect": false,
            "defaultValue": "'none'"
        },
        "isSelected": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "is-selected",
            "reflect": false
        },
        "hoist": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hoist",
            "reflect": false,
            "defaultValue": "false"
        },
        "timeZone": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "time-zone",
            "reflect": false,
            "defaultValue": "'Europe/Istanbul'"
        },
        "momentObj": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object",
                "resolved": "object",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "null"
        }
    }; }
    static get events() { return [{
            "method": "pxDropdownFocused",
            "name": "pxDropdownFocused",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxDropdownBlured",
            "name": "pxDropdownBlured",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "toggleClass": {
            "complexType": {
                "signature": "(name: string, bool: boolean, node: Element) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "Element": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "clear": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "PxDatetimeEntryCell": {
                        "location": "import",
                        "path": "./px-datetime-entry-cell"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValueFromMoment": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "PxDatetimeEntryCell": {
                        "location": "import",
                        "path": "./px-datetime-entry-cell"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get listeners() { return [{
            "name": "pxDropdownFocused",
            "method": "_handleDropdownFocus",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDropdownBlured",
            "method": "_handleDropdownBlur",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDropdownSelectionChanged",
            "method": "_updateTimeZone",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxRequestDatetimeEntryCopy",
            "method": "_handleCopy",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxRequestDatetimeEntryPaste",
            "method": "_handlePaste",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
