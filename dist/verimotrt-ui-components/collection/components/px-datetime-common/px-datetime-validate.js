import { EventEmitter } from "events";
//import { PxDatetimeEntry } from './px-datetime-entry';
export class PxDatetimeValidate extends EventEmitter {
    constructor(config) {
        super();
        this.isValid = true;
        this.validationErrorMessage = 'invalid';
        this.language = 'en';
        this.useKeyIfMissing = true;
        this.resources = {
            'en': {
                'Future dates are not allowed': 'Future dates are not allowed',
                'Past dates are not allowed': 'Past dates are not allowed',
                'Year': 'Year',
                'Month': 'Month',
                'Day': 'Day',
                'Hour': 'Hour',
                'Minute': 'Minute',
                'Second': 'Second',
                'Millisecond': 'Millisecond',
                'is not valid': 'is not valid',
                'This hour is not within a 12 hour clock': 'This hour is not within a 12 hour clock'
            }
        };
        this.hideDate = false;
        this.hideTime = false;
        this.blockFutureDates = false;
        this.blockPastDates = false;
        this.dateFormat = '';
        this.timeFormat = '';
        this.required = false;
        this._root = config.root;
        this._moment = config.moment;
        this.timeZone = config.timeZone;
        this.hideDate = config.hideDate;
        this.hideTime = config.hideTime;
        this.momentObj = config.momentObj;
        this.blockFutureDates = config.blockFutureDates;
        this.blockPastDates = config.blockPastDates;
        this.maxDate = config.maxDate;
        this.minDate = config.minDate;
        this.dateFormat = config.dateFormat;
        this.timeFormat = config.timeFormat;
        this.required = config.required;
    }
    toggleClass(name, bool, node) {
        if (arguments.length == 1) {
            bool = !node.classList.contains(name);
        }
        if (bool) {
            node.classList.add(name);
        }
        else {
            node.classList.remove(name);
        }
    }
    /**
     * Validation function for 'px-datetime-entry'
     *
     * @event px-moment-valid
     * @param {element} this
  
      * @event px-moment-invalid
      * @param {element} this
      */
    _validateInput(format, tz) {
        var inputArr = this._root.querySelectorAll('.cell'), inputStr = this._entryInputString(inputArr);
        if (inputStr) {
            var inputMoment = this._moment.tz(inputStr, format, tz);
            if (inputMoment.isValid()) {
                this.isValid = true;
                this.toggleClass('validation-error', false, this._root);
                this.emit('pxMomentValid', { "element": this });
            }
            else {
                this.validationErrorMessage = this._determineValidationMessage(inputMoment, new Object());
                this._updateValidationEvent(this.validationErrorMessage);
                this.isValid = false;
                this.toggleClass('validation-error', true, this._root);
                this.emit('pxMomentInvalid', { "element": this });
            }
        }
    }
    /**
     * Validate a complete field
     * called in px-datetime-field and px-datetime-range-field
     *
     * @param {String} funcOrigin - The function that called validation. Used for `_handleIncompleteEntries`
     * @return validation result:
     * - MomentObj - The validated new moment obj
     * - False - If the new moment is invalid
     * - FieldSame - If the new moment is the same as the old moment
     * - FieldBlank - If the visible field(s) are blank
     */
    _validateField(funcOrigin) {
        var fieldNodes = new Object(), dateEntry = this._root.querySelector('#date'), timeEntry = this._root.querySelector('#time');
        if (dateEntry !== null) {
            fieldNodes.dateCells = dateEntry.shadowRoot.querySelectorAll('.cell');
        }
        ;
        if (timeEntry !== null) {
            fieldNodes.timeCells = timeEntry.shadowRoot.querySelectorAll('.cell');
        }
        ;
        var dateString = (dateEntry !== null) ? this._entryInputString(fieldNodes.dateCells) : '', timeString = (timeEntry !== null) ? this._entryInputString(fieldNodes.timeCells) : '';
        // If the timeString is incomplete when only the time entry is showing or
        // if the dateString is incomplete when only the date entry is showing or
        // if either dateString or timeSting are incomplete when both entries are showing
        // run `_handleIncompleteEntries()`
        if ((this.hideDate && !timeString) || (this.hideTime && !dateString) || ((!this.hideDate && !this.hideTime) && (!dateString || !timeString))) {
            var entry = this.hideDate ? timeEntry : dateEntry;
            return this._handleIncompleteEntries(funcOrigin, entry, fieldNodes);
        }
        //Create a string from all of the entries put together
        var dateTimeString = dateString.trim() + " " + timeString.trim();
        dateTimeString = dateTimeString.split('\xa0').join(' '); //added to be able to use Moment strict parsing
        var dateFormat = this.hideDate ? '' : this.dateFormat, timeFormat = this.hideTime ? '' : this.timeFormat, dateTimeFormat = dateFormat + " " + timeFormat, dateTimeMoment = this._moment.tz(dateTimeString, dateTimeFormat, this.timeZone);
        if (this.momentObj) {
            //preserve parts of the momentObj that are not displayed
            if (!/Y/.test(dateTimeFormat)) {
                dateTimeMoment.year(this.momentObj.year());
            }
            if (!/M/.test(dateTimeFormat)) {
                dateTimeMoment.month(this.momentObj.month());
            }
            if (!/D/.test(dateTimeFormat)) {
                dateTimeMoment.date(this.momentObj.date());
            }
            if (!/[Hhk]/.test(dateTimeFormat)) {
                dateTimeMoment.hour(this.momentObj.hour());
            }
            if (!/m/.test(dateTimeFormat)) {
                dateTimeMoment.minute(this.momentObj.minute());
            }
            if (!/s/.test(dateTimeFormat)) {
                dateTimeMoment.second(this.momentObj.second());
            }
            if (!/S/.test(dateTimeFormat)) {
                dateTimeMoment.millisecond(this.momentObj.millisecond());
            }
        }
        return this._validateMomentObj(dateTimeMoment);
    }
    /**
     * Validate a moment object
     * called in from _validateField and px-datetime-field
     *
     * @param {Object} momentObj - Moment object getting validated
     * @return validation result:
     * - MomentObj - The validated new moment obj
     * - False - If the new moment is invalid
     * - FieldSame - If the new moment is the same as the old moment
     */
    _validateMomentObj(validatingMomentObj) {
        //If null is passed in clear the validation errors
        if (validatingMomentObj === null) {
            this._validField(validatingMomentObj);
            return;
        }
        var invalidObj = new Object();
        invalidObj.futureSelection = this.blockFutureDates && validatingMomentObj.isAfter(this._moment.tz(this._moment(), this.timeZone));
        invalidObj.pastSelection = this.blockPastDates && validatingMomentObj.isBefore(this._moment.tz(this._moment(), this.timeZone));
        invalidObj.pastMaxSelection = this.maxDate && validatingMomentObj.isAfter(this.maxDate);
        invalidObj.beforeMinSelection = this.minDate && validatingMomentObj.isBefore(this.minDate);
        if ((validatingMomentObj.isValid() && !invalidObj.futureSelection && !invalidObj.pastSelection && !invalidObj.pastMaxSelection && !invalidObj.beforeMinSelection)) {
            this._validField(validatingMomentObj);
            //if the momentObj is null or not that same as this.momentObj return validatingMomentObj
            if (!this.momentObj || !this.momentObj.isSame(validatingMomentObj)) {
                return validatingMomentObj;
            }
            else if (this.momentObj.isSame(validatingMomentObj)) {
                return 'FieldSame';
            }
        }
        else {
            this._invalidField(validatingMomentObj, invalidObj);
            return false;
        }
    }
    /**
     * Reset field to valid
     *
     * @param {Object} submittedMoment - valid momentObj
     */
    _validField(obj) {
        console.log('_validField', obj);
        if (obj.field && obj.field.id === 'date') {
            let dateEntryEl = this._root.querySelector('#date');
            if (obj.field !== null) {
                obj.field.toggleClass('validation-error', false, dateEntryEl.shadowRoot.querySelectorAll('#wrapper')[0]);
            }
            this.emit('pxMomentValid', { "element": obj.field });
        }
        if (obj.field && obj.field.id === 'time') {
            let timeEntryEl = this._root.querySelector('#date');
            if (obj.field !== null) {
                obj.field.toggleClass('validation-error', false, timeEntryEl.shadowRoot.querySelectorAll('#wrapper')[0]);
            }
            this.emit('pxMomentValid', { "element": obj.field });
        }
        //this.fieldIsValid=true;
    }
    /**
     * Set field to invalid
     * Set validation Error Message, turn on validation error, set isValid to false, and fire event.
     *
     * @param {Object} submittedMoment - Invalid momentObj
     * @param {Object} invalidObj - Stores the different invalid
     */
    _invalidField(submittedMoment, invalidObj) {
        console.log('_invalidField', submittedMoment, invalidObj);
        this.validationErrorMessage = this._determineValidationMessage(submittedMoment, invalidObj);
        this._updateValidationEvent(this.validationErrorMessage);
        //this.fieldIsValid=false;
        //this.toggleClass('validation-error', true, this.elfieldWrapper);
        this.emit('pxMomentInvalid', { "element": this });
    }
    /**
     * loop through array of the entry cells and return a string of the value
     *
     * @param {Object} cells - Date or time cell nodes
     */
    _entryInputString(cells) {
        var outputStr = "";
        for (var i = 0; i < cells.length; i++) {
            var value = cells[i].shadowRoot.querySelector('.datetime-entry-input').value;
            if (value === '') {
                return '';
            }
            if (cells[i].momentFormat !== "Z") {
                outputStr = outputStr + value + cells[i].symbol;
            }
        }
        return outputStr;
    }
    /**
     * Returns a meaningful validation message
     *
     * @param {Object} submittedMoment - Invalid momentObj
     * @param {Object} invalidObj - Stores the different invalid
     */
    _determineValidationMessage(submittedMoment, invalidObj) {
        console.log('_determineValidationMessage', submittedMoment, invalidObj);
        return '';
        //if (!submittedMoment) { return this.localize('Incomplete'); }
        //if (invalidObj.futureSelection) { return this.localize('Future dates are not allowed'); }
        //else if (invalidObj.pastSelection) { return this.localize('Past dates are not allowed'); }
        //else if (invalidObj.pastMaxSelection) { return this.localize('Date is past max date'); }
        //else if (invalidObj.beforeMinSelection) { return this.localize('Date is before min date'); }
        //else {
        //  var invalidAt = submittedMoment.invalidAt();
        //
        //  if (invalidAt !== -1) {
        //    switch (invalidAt) {
        //      case 0:
        //        return this.localize('Year') + ' ' + submittedMoment._a[invalidAt] + ' ' + this.localize('is not valid');
        //      case 1:
        //        return this.localize('Month') + ' ' + (submittedMoment._a[invalidAt] + 1) + ' ' + this.localize('is not valid');
        //      case 2:
        //        return this.localize('Day') + ' ' + submittedMoment._a[invalidAt] + ' ' + this.localize('is not valid');
        //      case 3:
        //        return this.localize('Hour') + ' ' + submittedMoment._a[invalidAt] + ' ' + this.localize('is not valid');
        //      case 4:
        //        return this.localize('Minute') + ' ' + submittedMoment._a[invalidAt] + ' ' + this.localize('is not valid');
        //      case 5:
        //        return this.localize('Second') + ' ' + submittedMoment._a[invalidAt] + ' ' + this.localize('is not valid');
        //      case 6:
        //        return this.localize('Millisecond') + ' ' + submittedMoment._a[invalidAt] + ' ' + this.localize('is not valid');
        //    }
        //  } else if (submittedMoment.parsingFlags().bigHour === true) {
        //    return this.localize('This hour is not within a 12 hour clock');
        //  }
        //}
    }
    /**
     * Fires when 'validationErrorMessage' is changed
     *
     * @event px-validation-message
     * @param {string} validationErrorMessage - Values validationErrorMessage
     */
    _updateValidationEvent(validationErrorMessage) {
        if (validationErrorMessage !== undefined) {
            this.emit('pxValidationMessage', { 'validationErrorMessage': this.validationErrorMessage });
        }
    }
    /**
     * Sets the button state based on if validation has been passed or not. This in turn enables or disables the Submit button.
     *
     * @param {Boolean} state - Where the validation has passed or failed
     */
    _submitButtonState(state) {
        console.log('_submitButtonState', state);
        //this.isSubmitButtonValid=state;
    }
    /**
     * If required any incomplete momentObj is invalid
     * If not required determine if the field is blank
     *
     * @param {String} funcOrigin - The function that called validation
     * @param {Element} entry - px-datetime-entry date
     * @param {Object} fieldNodes - Object of date and time cell nodes
     */
    _handleIncompleteEntries(funcOrigin, entry, fieldNodes) {
        if (funcOrigin === "_momentChanged") {
            return false;
        }
        ;
        if (!this.required) {
            var dateEntryBlank = this.hideDate ? true : this._loopOverCellValues(fieldNodes.dateCells), timeEntryBlank = this.hideTime ? true : this._loopOverCellValues(fieldNodes.timeCells);
            if (dateEntryBlank && timeEntryBlank) {
                entry.toggleClass('validation-error', false, entry.parentElement);
                entry.isValid = true;
                return "FieldBlank";
            }
            this._invalidField("", new Object());
        }
        else {
            this._invalidField("", new Object());
            return false;
        }
    }
    /**
     * Checking to see if all cells are empty
     * If a cell has a value return false
     *
     * @param {Object} cells - Date or time cell nodes
     */
    _loopOverCellValues(cells) {
        var isBlank = true;
        if (cells && cells.length) {
            for (let i = 0; i < cells.length; i++) {
                var cellInput = cells[i].shadowRoot.querySelectorAll('.datetime-entry-input');
                if (cellInput[0].value !== "") {
                    isBlank = false;
                    break;
                }
            }
            ;
        }
        return isBlank;
    }
}
