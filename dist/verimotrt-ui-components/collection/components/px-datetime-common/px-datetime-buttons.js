import { h } from "@stencil/core";
export class PxDatetimeButtons {
    constructor() {
        this.isSubmitButtonValid = true;
        this.hideSubmit = false;
        this.hideCancel = false;
        this.spaceBetween = false;
        this.useKeyIfMissing = true;
    }
    _fireCancelEvent() {
        this.pxDatetimeButtonClicked.emit({ action: false });
    }
    _fireSubmitEvent() {
        this.pxDatetimeButtonClicked.emit({ action: true });
    }
    submitBtnClasses(base, submitButtonValid) {
        return (submitButtonValid) ? base : base + ' btn--disabled';
    }
    _isEqual(source, target) {
        return source === target;
    }
    render() {
        return (h("div", { class: "dt-btn-wrapper" },
            !this.hideCancel ? h("button", { type: "button", class: "btn u-mr-", "on-tap": () => this._fireCancelEvent() }, "Vazge\u00E7") : '',
            !this.hideSubmit ? h("button", { class: this.submitBtnClasses('btn btn--primary', this.isSubmitButtonValid), type: "button", "on-tap": () => this._fireSubmitEvent(), id: "submitButton", disabled: !this.isSubmitButtonValid },
                h("i", { class: this.submitIcon }),
                "Uygula") : ''));
    }
    static get is() { return "px-datetime-buttons"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-datetime-buttons.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-datetime-buttons.css"]
    }; }
    static get properties() { return {
        "isSubmitButtonValid": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "is-submit-button-valid",
            "reflect": false,
            "defaultValue": "true"
        },
        "hideSubmit": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-submit",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideCancel": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-cancel",
            "reflect": false,
            "defaultValue": "false"
        },
        "submitIcon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "submit-icon",
            "reflect": false
        },
        "spaceBetween": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "space-between",
            "reflect": true,
            "defaultValue": "false"
        },
        "useKeyIfMissing": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "use-key-if-missing",
            "reflect": false,
            "defaultValue": "true"
        }
    }; }
    static get events() { return [{
            "method": "pxDatetimeButtonClicked",
            "name": "pxDatetimeButtonClicked",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
}
