import { h } from "@stencil/core";
export class PxSteps {
    constructor() {
        this.items = [];
        this.completed = [];
        this.errored = [];
        this.currentStep = 0;
    }
    /**
     * Sets the current step to complete and advances to the next step.
     */
    async complete() {
        if (!this.items || this.items.length <= 0) {
            console.warn("Can't complete step: the steps array is empty or undefined.");
            return;
        }
        var id = this.items[this.currentStep].id;
        if (this.completed.indexOf(id) === -1) {
            this.completed = [...this.completed, id];
        }
        var erroredIndex = this.errored.indexOf(id);
        if (erroredIndex > -1) {
            this.errored.splice(erroredIndex, 1);
        }
        // Select next step, if there is one
        if (this.currentStep < this.items.length - 1) {
            this.next();
        }
    }
    /**
     * Sets the step at the passed index to error state.
     */
    async error(index) {
        if (!this.items || this.items.length <= 0) {
            console.warn("Can't complete step: the steps array is empty or undefined.");
            return;
        }
        var id = this.items[index].id;
        if (this.errored.indexOf(id) === -1) {
            this.errored = [...this.errored, id];
        }
        var completedIndex = this.completed.indexOf(id);
        if (completedIndex > -1) {
            this.completed.splice(completedIndex, 1);
        }
    }
    /**
     * Go forward one step.
     */
    async next() {
        if (!this.items || this.items.length === 0) {
            console.warn("Can't select next step: the steps array is empty or undefined.");
            return;
        }
        if (this.currentStep === this.items.length - 1) {
            console.warn("Can't select next step: you are already at the last step.");
            return;
        }
        this.currentStep++;
    }
    /**
     * Go back one step.
     */
    async previous() {
        if (!this.items || this.items.length === 0) {
            console.warn("Can't select previous step: the steps array is empty or undefined.");
            return;
        }
        if (this.currentStep === 0) {
            console.warn("Can't select previous step: you are already at the first step.");
            return;
        }
        this.currentStep--;
    }
    /**
     * Jump to an arbitrary step by index.
     */
    jumpToStep(index) {
        if (!this.items || !this.items[index]) {
            console.warn("Can't jump to step at index '" + index + "'; that step doesn't exist.");
            return;
        }
        this.currentStep = index;
    }
    /**
     * Calculates which icon to display for a given node.
     */
    _getIcon(item) {
        if (this.errored.indexOf(item.id) > -1) {
            return "fa-times-circle";
        }
        else if (this.completed.indexOf(item.id) > -1) {
            return "fa-check-circle";
        }
        return "fa-circle";
    }
    /**
     * Calculates which class to assign to a given node.
     */
    _getStepState(item, index, currentStep, completed, errored) {
        let classes = ['step-wrapper'];
        //if(this.completed.indexOf(item.id) > -1){
        //    this.currentStep++;
        //}
        if (index === currentStep) {
            classes.push('current');
        }
        if (errored.indexOf(item.id) > -1) {
            classes.push('error');
        }
        else if (completed.indexOf(item.id) > -1) {
            classes.push('complete');
        }
        return classes.join(' ');
    }
    /**
     * Fires an event when a node is tapped upon.
     */
    _handleTap(event) {
        var id = event.currentTarget.id;
        /**
         * Event fired when a step in the px-steps is tapped.
         * Event.detail will contain the id of the step, e.g.
         *
         *      { id : "3" }
         * You should intercept this event, perform any necessary
         * validation or saving on the current step in the process,
         * decide if the user is allowed to navigate to that step,
         * and then use the `jumpToStep` method to actually change steps.
         * @event px-steps-tapped
         */
        this.pxStepsTapped.emit({ "id": id });
    }
    getTemplateStepItems() {
        let _index = 0;
        return (this.items.map((item) => {
            return (h("div", { class: this._getStepState(item, _index++, this.currentStep, this.completed, this.errored) },
                h("div", { class: "step-connector" }),
                h("div", { id: item.id, class: "step-item", "on-tap": (event) => this._handleTap(event) },
                    h("div", { class: "step-icon" },
                        h("px-icon", { classPrefix: 'far', size: 'middle', icon: this._getIcon(item) })),
                    h("div", { class: "step-label" }, item.label))));
        }));
    }
    render() {
        return (h("div", { class: "container flex flex--top" }, this.getTemplateStepItems()));
    }
    static get is() { return "px-steps"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-steps.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-steps.css"]
    }; }
    static get properties() { return {
        "items": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "StepItem[]",
                "resolved": "StepItem[]",
                "references": {
                    "StepItem": {
                        "location": "import",
                        "path": "./px-stepItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "completed": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "String[]",
                "resolved": "String[]",
                "references": {
                    "String": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "errored": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "String[]",
                "resolved": "String[]",
                "references": {
                    "String": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "currentStep": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "current-step",
            "reflect": false,
            "defaultValue": "0"
        }
    }; }
    static get events() { return [{
            "method": "pxStepsTapped",
            "name": "pxStepsTapped",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "complete": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Sets the current step to complete and advances to the next step.",
                "tags": []
            }
        },
        "error": {
            "complexType": {
                "signature": "(index: any) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Sets the step at the passed index to error state.",
                "tags": []
            }
        },
        "next": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Go forward one step.",
                "tags": []
            }
        },
        "previous": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "Go back one step.",
                "tags": []
            }
        }
    }; }
}
