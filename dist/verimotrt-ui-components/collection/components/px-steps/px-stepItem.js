export class StepItem {
    constructor(obj) {
        this.id = obj.id;
        this.label = obj.label;
    }
}
