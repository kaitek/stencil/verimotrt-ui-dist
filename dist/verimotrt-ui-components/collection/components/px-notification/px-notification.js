import { h } from "@stencil/core";
export class PxNotification {
    constructor() {
        this.statusIcon = '';
        this.content = '';
        this.actionIcon = '';
        this.type = 'info';
        this.small = false;
        this.opened = false;
    }
    _hideIfEmpty(icon) {
        if (typeof icon !== "string" || icon === '')
            return 'hidden';
    }
    _handleTap() {
        this.opened = false;
        this.pxNotificationActionTapped.emit();
    }
    render() {
        let _class = "notification-left " + this._hideIfEmpty(this.statusIcon);
        return ([
            h("div", { class: _class },
                h("px-icon", { class: "notification-icon", icon: this.statusIcon })),
            h("div", { class: "notification-content" },
                h("slot", { name: "content" },
                    h("div", null, this.content))),
            h("div", { class: "notification-right", "on-tap": () => this._handleTap() },
                h("slot", { name: "right" },
                    h("div", { class: this._hideIfEmpty(this.actionIcon) },
                        h("button", { class: "btn btn--bare btn--icon" },
                            h("px-icon", { class: "notification-icon", icon: this.actionIcon })))))
        ]);
    }
    static get is() { return "px-notification"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-notification.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-notification.css"]
    }; }
    static get properties() { return {
        "statusIcon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "status-icon",
            "reflect": false,
            "defaultValue": "''"
        },
        "content": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "content",
            "reflect": false,
            "defaultValue": "''"
        },
        "actionIcon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "action-icon",
            "reflect": false,
            "defaultValue": "''"
        },
        "type": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'important'|'warning'|'error'|'info'|'healthy'|'unknown'",
                "resolved": "\"error\" | \"healthy\" | \"important\" | \"info\" | \"unknown\" | \"warning\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "type",
            "reflect": true,
            "defaultValue": "'info'"
        },
        "small": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "small",
            "reflect": true,
            "defaultValue": "false"
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": true,
            "defaultValue": "false"
        }
    }; }
    static get events() { return [{
            "method": "pxNotificationActionTapped",
            "name": "pxNotificationActionTapped",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
}
