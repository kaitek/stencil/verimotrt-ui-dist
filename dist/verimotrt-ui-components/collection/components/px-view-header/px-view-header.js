import { h } from "@stencil/core";
export class PxViewHeader {
    constructor() {
        this.isPrevVisible = false;
        this.isNextVisible = false;
        this.selected = "0";
    }
    async buttonStateChange(btn, state) {
        let _btn = this.elDivContainer.querySelectorAll('.view-' + btn)[0];
        if (state === 'enable') {
            if (btn === 'prev') {
                this.isPrevVisible = true;
            }
            else {
                this.isNextVisible = true;
            }
            _btn.classList.remove('actionable--disabled');
        }
        else {
            if (btn === 'prev') {
                this.isPrevVisible = false;
            }
            else {
                this.isNextVisible = false;
            }
            _btn.classList.add('actionable--disabled');
        }
    }
    _selectedWatcher() {
        const _selectedIndex = parseInt(this.selected);
        if (_selectedIndex === 1) {
            this.isPrevVisible = false;
        }
        else {
            if (_selectedIndex <= this.items.length) {
                this.isPrevVisible = true;
            }
        }
        if (_selectedIndex < this.items.length) {
            this.isNextVisible = true;
        }
        else {
            this.isNextVisible = false;
        }
        const clear_d = this.elPages.querySelectorAll('div.iron-selected-view');
        if (clear_d.length > 0) {
            clear_d[0].classList.remove('iron-selected');
            clear_d[0].classList.remove('iron-selected-view');
        }
        const item = this.items[_selectedIndex - 1];
        this.mainTitle = item.title;
        this.subTitle = item.subTitle;
        const list_d = this.elPages.childNodes[parseInt(this.selected) - 1];
        list_d.classList.add('iron-selected');
        list_d.classList.add('iron-selected-view');
        this.pxViewHeaderChanged.emit({
            tabID: this.selected
        });
    }
    componentWillLoad() {
        this.elPages = this.myElement.nextSibling;
        if (this.selected === "0") {
            this.selected = "1";
        }
    }
    componentDidLoad() {
        setTimeout(() => {
            this._selectedWatcher();
        }, 100);
    }
    btnBackClick(evt) {
        if (!evt.target.classList.contains('actionable--disabled')) {
            this.selected = (parseInt(this.selected) - 1).toString();
        }
    }
    btnNextClick(evt) {
        if (!evt.target.classList.contains('actionable--disabled')) {
            this.selected = (parseInt(this.selected) + 1).toString();
        }
    }
    getTemplateSubTitle() {
        return (h("div", { class: "zeta vh-subtitle truncate", title: this.subTitle }, this.subTitle));
    }
    getTemplateDeckSelector() {
        return ([
            h("div", { class: "regular vh-title truncate", title: this.mainTitle }, this.mainTitle),
            this.getTemplateSubTitle()
        ]);
    }
    render() {
        const _prevClass = this.isPrevVisible ? 'actionable view-prev' : 'actionable view-prev actionable--disabled';
        const _nextClass = this.isNextVisible ? 'actionable view-next' : 'actionable view-next actionable--disabled';
        return (h("div", { class: "vh-header", ref: (el) => this.elDivContainer = el },
            h("div", { class: _prevClass, onClick: (event) => { this.btnBackClick(event); } },
                h("px-icon", { icon: "fa-long-arrow-alt-left" }),
                "\u00A0\u00A0Back"),
            h("div", { class: "vh-header-text vh-header-text--ms-fix" }, this.getTemplateDeckSelector()),
            h("div", { class: _nextClass, onClick: (event) => { this.btnNextClick(event); } },
                "Next\u00A0\u00A0",
                h("px-icon", { icon: "fa-long-arrow-alt-right" }))));
    }
    static get is() { return "px-view-header"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-view-header.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-view-header.css"]
    }; }
    static get properties() { return {
        "selected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected",
            "reflect": false,
            "defaultValue": "\"0\""
        },
        "items": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "{[key: string]: string}[]",
                "resolved": "{ [key: string]: string; }[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        }
    }; }
    static get states() { return {
        "mainTitle": {},
        "subTitle": {},
        "isPrevVisible": {},
        "isNextVisible": {}
    }; }
    static get events() { return [{
            "method": "pxViewHeaderChanged",
            "name": "pxViewHeaderChanged",
            "bubbles": true,
            "cancelable": false,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get methods() { return {
        "buttonStateChange": {
            "complexType": {
                "signature": "(btn: \"prev\" | \"next\", state: \"enable\" | \"disable\") => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }, {
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "selected",
            "methodName": "_selectedWatcher"
        }]; }
}
