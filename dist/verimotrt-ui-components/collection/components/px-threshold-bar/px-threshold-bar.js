import { h } from "@stencil/core";
export class PxThresholdBar {
    constructor() {
        this.min = 0;
        this.max = 100;
        this.hideValue = false;
        this.hideScale = false;
        this.value = null;
        this.uom = '';
        this._scaleFactor = null;
        this._refreshStyle = false;
    }
    async getValue() {
        return this.value;
    }
    async setValue(value) {
        this.value = value;
        this._scaleDataChanged();
        const vs = this._getValueStyle();
        const ms = this._getMarkerStyle();
        const mls = this._getMarkerLineStyle();
        const span_v = this.elDivContainer.querySelectorAll('.span_value')[0];
        span_v.style['left'] = vs.left;
        span_v.innerHTML = this.value + this.uom;
        const caret = this.elDivContainer.querySelectorAll('.caret')[0];
        caret.style['left'] = ms.left;
        const marker = this.elDivContainer.querySelectorAll('.marker-line')[0];
        marker.style['left'] = mls.left;
    }
    _configDataChanged(config) {
        if (config && config.length) {
            //let configLen = config.length;
            config.sort(function (a, b) {
                return a.min - b.min;
            });
        }
    }
    _scaleDataChanged() {
        let thresholdEl = this.elDivContainer.querySelector('.container');
        if (this._checkValuesSet(false)) {
            if (thresholdEl && thresholdEl.clientWidth && thresholdEl.clientHeight) {
                let factor = (thresholdEl.clientWidth / (this.max - this.min));
                this._scaleFactor = factor;
            }
            else {
                setTimeout(function () {
                    this._scaleDataChanged();
                }.bind(this));
            }
        }
    }
    componentDidLoad() {
        //setTimeout(() => {
        //    this._scaleDataChanged();
        //}, 100);
    }
    _calculateLeftOffset() {
        const position = (Math.min(this.max, Math.max(this.min, this.value)) - this.min) * this._scaleFactor;
        return this.hideScale ? position : position + this._calculatePaddingWidth();
    }
    _calculatePaddingWidth() {
        const scaleValueEl = this.elDivContainer.querySelector('.scale__value--first');
        if (scaleValueEl) {
            const padding = window.getComputedStyle(scaleValueEl, "false")['width'];
            return Number(padding.substr(0, padding.length - 2));
        }
        return 0;
    }
    _checkValuesSet(includeValue) {
        let min = this.min !== null && this.min !== undefined && !isNaN(parseFloat(this.min.toString())), max = this.max !== null && this.max !== undefined && !isNaN(parseFloat(this.max.toString())), value = this.value !== null && this.value !== undefined && !isNaN(parseFloat(this.value.toString()));
        return includeValue ? min && max && value : min && max;
    }
    _getMarkerLineStyle() {
        if (this._checkValuesSet(true) && this._scaleFactor) {
            return { left: String(this._calculateLeftOffset()) + 'px' };
        }
        return { display: 'none;' };
    }
    _getMarkerStyle() {
        if (this._checkValuesSet(true) && this._scaleFactor) {
            return { left: String(this._calculateLeftOffset() - 4) + 'px' };
        }
        return { display: 'none;' };
    }
    _getStyles(config) {
        let width = ((config.max - config.min) * this._scaleFactor);
        return { width: width + 'px', 'background-color': config.color };
    }
    _getValueStyle() {
        if (this.elDivContainer) {
            let spanEl = this.elDivContainer.querySelector('.value > span');
            spanEl.style.display = this.hideValue ? 'none' : 'block';
            if (this._checkValuesSet(true) && this._scaleFactor) {
                const spanSize = spanEl.offsetWidth;
                if (spanSize) {
                    return { left: String(this._calculateLeftOffset() - (spanSize / 2)) + 'px' };
                }
                else {
                    setTimeout(function () {
                        this._refreshStyle = !this._refreshStyle;
                    }.bind(this), 100);
                }
            }
        }
        return { display: 'none;' };
    }
    getPreItem(i, tConfig) {
        if (i === 0) {
            return (h("div", { class: "flex flex--col flex--end", style: { "margin-right": "-1px" } },
                h("div", { class: "threshold-bar" }),
                h("div", { class: "scale__tick scale__value" }),
                h("div", { class: "scale__value--first scale__value" },
                    tConfig.min,
                    this.uom)));
        }
    }
    getTemplateConfigItems() {
        let _i = 0;
        return (this.config.map((tConfig) => {
            return ([
                this.getPreItem(_i++, tConfig),
                h("div", { class: "flex flex--col flex--end" },
                    h("div", { class: "threshold-bar", style: this._getStyles(tConfig) }),
                    h("div", { class: "scale__tick scale__value" }),
                    h("div", { class: "scale__value" },
                        tConfig.max,
                        this.uom))
            ]);
        }));
    }
    render() {
        return (h("div", { class: "flex flex--col flex--top", ref: (el) => this.elDivContainer = el },
            h("div", { class: "value-container flex flex--col" },
                h("div", { class: "value" },
                    h("span", { class: "span_value", style: this._getValueStyle() },
                        this.value,
                        this.uom)),
                h("div", { class: "marker" },
                    h("px-icon", { class: "caret", style: this._getMarkerStyle(), icon: "fa-caret-down" }))),
            h("div", { class: "container flex flex--top" },
                h("div", { class: "marker-line", style: this._getMarkerLineStyle() }),
                this.getTemplateConfigItems())));
    }
    static get is() { return "px-threshold-bar"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-threshold-bar.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-threshold-bar.css"]
    }; }
    static get properties() { return {
        "min": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min",
            "reflect": false,
            "defaultValue": "0"
        },
        "max": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max",
            "reflect": false,
            "defaultValue": "100"
        },
        "hideValue": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-value",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideScale": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-scale",
            "reflect": true,
            "defaultValue": "false"
        },
        "config": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "ThresholdConfig[]",
                "resolved": "ThresholdConfig[]",
                "references": {
                    "ThresholdConfig": {
                        "location": "import",
                        "path": "./px-thresholdConfig"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "uom": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "uom",
            "reflect": false,
            "defaultValue": "''"
        },
        "_scaleFactor": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_scale-factor",
            "reflect": false,
            "defaultValue": "null"
        }
    }; }
    static get methods() { return {
        "getValue": {
            "complexType": {
                "signature": "() => Promise<number>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<number>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValue": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "HTMLSpanElement": {
                        "location": "global"
                    },
                    "HTMLElement": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "config",
            "methodName": "_configDataChanged"
        }, {
            "propName": "max",
            "methodName": "_scaleDataChanged"
        }, {
            "propName": "min",
            "methodName": "_scaleDataChanged"
        }]; }
}
