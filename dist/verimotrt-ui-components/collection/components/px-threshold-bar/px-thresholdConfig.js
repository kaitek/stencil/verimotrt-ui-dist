export class ThresholdConfig {
    constructor(obj) {
        this.min = obj.min;
        this.max = obj.max;
        this.color = obj.color;
    }
}
