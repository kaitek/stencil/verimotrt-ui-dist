import { h } from "@stencil/core";
export class PxIcon {
    constructor() {
        this.classPrefix = 'fas';
        this.size = 'small';
        this.disabled = false;
    }
    _handleTapped(evt) {
        evt.preventDefault();
        if (!this.disabled) {
            this.pxIconTapped.emit();
        }
    }
    render() {
        let _class = this.classPrefix + ' ' + this.icon + ' ' + this.size;
        return (h("i", { class: _class, style: this.pxstyle, "on-tap": (event) => this._handleTapped(event) }));
    }
    static get is() { return "px-icon"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-icon.css", "../../assets/fontawesome/css/all.min.css"]
    }; }
    static get styleUrls() { return {
        "$": ["px-icon.css", "../../assets/fontawesome/css/all.min.css"]
    }; }
    static get properties() { return {
        "icon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "icon",
            "reflect": false
        },
        "classPrefix": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "class-prefix",
            "reflect": false,
            "defaultValue": "'fas'"
        },
        "size": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'small' | 'middle' | 'large' | 'huge'",
                "resolved": "\"huge\" | \"large\" | \"middle\" | \"small\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "size",
            "reflect": false,
            "defaultValue": "'small'"
        },
        "pxstyle": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "{ [key: string]: string; }",
                "resolved": "{ [key: string]: string; }",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
    static get events() { return [{
            "method": "pxIconTapped",
            "name": "pxIconTapped",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
}
