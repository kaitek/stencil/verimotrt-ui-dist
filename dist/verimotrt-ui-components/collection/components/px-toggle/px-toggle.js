import { h } from "@stencil/core";
export class PxToggle {
    constructor() {
        this.size = 'large';
        this.disabled = false;
        this.checked = false;
    }
    clickHandler( /*event: CustomEvent*/) {
        if (!this.disabled) {
            this.checked = !this.checked;
            this.pxToggleCheckedChanged.emit({ value: this.checked });
        }
    }
    render() {
        let _disabled_label = '';
        if (this.disabled) {
            _disabled_label = ' toggle--disabled ';
        }
        let _size_label = 'toggle__label toggle__label--' + this.size + _disabled_label;
        let _size_input = 'toggle__input toggle__input--' + this.size;
        /*return(
        <label class={_size_label}>
            <input type="checkbox" class={_size_input} disabled={this.disabled}></input>
        </label>
        );*/
        return ([
            h("input", { type: "checkbox", class: _size_input, min: 0, max: 1, step: 1, disabled: this.disabled, checked: this.checked, "aria-checked": this.checked, value: this.checked ? 1 : 0 }),
            h("label", { class: _size_label })
        ]);
    }
    static get is() { return "px-toggle"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-toggle.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-toggle.css"]
    }; }
    static get properties() { return {
        "size": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'regular' | 'small' | 'large' | 'huge'",
                "resolved": "\"huge\" | \"large\" | \"regular\" | \"small\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "size",
            "reflect": false,
            "defaultValue": "'large'"
        },
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": true,
            "defaultValue": "false"
        },
        "checked": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "checked",
            "reflect": true,
            "defaultValue": "false"
        }
    }; }
    static get events() { return [{
            "method": "pxToggleCheckedChanged",
            "name": "pxToggleCheckedChanged",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get listeners() { return [{
            "name": "click",
            "method": "clickHandler",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
