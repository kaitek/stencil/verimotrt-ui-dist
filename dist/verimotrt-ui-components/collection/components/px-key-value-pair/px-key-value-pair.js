import { h } from "@stencil/core";
export class PxKeyValuePair {
    constructor() {
        this.uom = '';
        this.size = 'alpha';
    }
    _getAdjustedSize(size) {
        return size === 'alpha' ? 'delta' :
            size === 'beta' ? 'epsilon' :
                size === 'gamma' ? 'value' :
                    size === 'delta' ? 'value' : 'zeta';
    }
    getTemplateUom() {
        let _class = this._getAdjustedSize(this.size) + ' kvp-uom--' + this.size;
        if (this.uom !== '') {
            return (h("span", { class: _class }, this.uom));
        }
    }
    render() {
        let _class = this.size + ' kvp-value--' + this.size;
        return ([
            h("div", { class: "label" }, this.label),
            h("div", { class: _class },
                h("span", null,
                    this.value,
                    " "),
                this.getTemplateUom())
        ]);
    }
    static get is() { return "px-key-value-pair"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-key-value-pair.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-key-value-pair.css"]
    }; }
    static get properties() { return {
        "label": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "label",
            "reflect": false
        },
        "value": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "value",
            "reflect": false
        },
        "uom": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "uom",
            "reflect": false,
            "defaultValue": "''"
        },
        "size": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'alpha'|'beta'|'gamma'|'delta'|'epsilon'|'regular'",
                "resolved": "\"alpha\" | \"beta\" | \"delta\" | \"epsilon\" | \"gamma\" | \"regular\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "size",
            "reflect": false,
            "defaultValue": "'alpha'"
        }
    }; }
}
