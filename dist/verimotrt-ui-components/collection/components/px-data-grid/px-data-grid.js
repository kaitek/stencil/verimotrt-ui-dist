import { h } from "@stencil/core";
import '@vaadin/vaadin-grid/all-imports.js';
export class PxDataGrid {
    constructor() {
        this._lastColumnsReceived = [];
        this._selectedActionItems = [];
        this.columns = [];
        this.expandedItems = [];
        this.itemActions = [];
        this.selectedItems = [];
        this.tableData = [];
        this._groupByColumn = false;
        this.activeItem = null;
        this.defaultColumnFlexGrow = 1;
        this.loadingSpinnerDebounce = 500;
        this.pageSize = 200;
        this.size = undefined;
        this._expandableRows = false;
        this._loading = false;
        this._spinnerHidden = true;
        this.allowSortBySelection = false;
        this.editable = false;
        this.flexToSize = false;
        this.hideActionMenu = false;
        this.hideColumnFilter = false;
        this.hideSelectionColumn = false;
        this.instantSortWhenSelection = false;
        this.multiSort = false;
        this.resizable = false;
        this.selectable = false;
        this.sortable = false;
        this.striped = false;
        this.useKeyIfMissing = true;
        this.defaultColumnWidth = '100px';
        this.selectionMode = 'none';
        this.item = undefined;
    }
    componentWillLoad() {
        if (this.columns[0].name === '#') {
            for (let i = 0; i < this.tableData.length; i++) {
                this.tableData[i]['__#__'] = (i + 1);
            }
        }
    }
    componentDidLoad() {
        this._columnSelectorLabel = this._computeColumnSelectorLabel(this.selectedItems);
        // Override selectItem method to allow easy single select handling
        this._vaadinGrid.deselectItem = (item) => this._handledeSelectItem(item);
        this._vaadinGrid.selectItem = (item) => this._handleSelectItem(item);
        const _x = this._vaadinGrid;
        let t = this;
        customElements.whenDefined('vaadin-grid').then(function () {
            const styleTag = _x.shadowRoot.querySelector('style');
            if (t.selectionMode === 'multi') {
                const selectAllCheckBox = _x.querySelector('.vaadin-grid-select-all-checkbox');
                selectAllCheckBox.addEventListener('change', t._handleSelectAllItems.bind(t));
            }
            if (t.selectionMode === 'single') {
                const selectAllCheckBox = _x.querySelector('.vaadin-grid-select-all-checkbox');
                selectAllCheckBox.setAttribute('hidden', 'true');
            }
            styleTag.innerHTML = `
      @keyframes vaadin-grid-appear {
        to {
          opacity: 1;
        }
        
        }
        
        :host {
          display: block;
          animation: 1ms vaadin-grid-appear;
          height: 400px;
          transform: translateZ(0);
        }
        
        :host([hidden]) {
          display: none !important;
        }
        
        #scroller {
          display: block;
          position: relative;
          height: 100%;
          width: 100%;
          transform: translateY(0);
        }
        
        #table {
          display: block;
          width: 100%;
          height: 100%;
          overflow: auto;
          z-index: -2;
          position: relative;
          outline: none;
        }
        
        [wheel-scrolling][edge] #table, [wheel-scrolling][ie] #table {
          z-index: auto;
        }
        
        #header {
          display: block;
          position: absolute;
          top: 0;
          width: 100%;
        }
        
        [part~="header-cell"] {
          font-weight: 500;
          text-align: left;
        }
        
        #footer {
          display: block;
          position: absolute;
          bottom: 0;
          width: 100%;
        }
        
        #items {
          display: block;
          width: 100%;
          position: relative;
          z-index: -1;
        }
        
        #items, #outersizer, #fixedsizer {
          border-top: 0 solid transparent;
          border-bottom: 0 solid transparent;
        }
        
        [part~="row"] {
          display: flex;
          width: 100%;
          box-sizing: border-box;
          margin: 0;
        }
        
        #items [part~="row"] {
          position: absolute;
        }
        
        #items [part~="row"]:empty {
          height: 1em;
        }
        
        [part~="cell"]:not([part~="details-cell"]) {
          flex-shrink: 0;
          flex-grow: 1;
          box-sizing: border-box;
          display: flex;
          width: 100%;
          position: relative;
        }
        
        [part~="details-cell"] {
          position: absolute;
          bottom: 0;
          width: 100%;
          box-sizing: border-box;
        }
        
        [part~="cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content) {
          width: 100%;
          display: inline-flex;
          justify-content: center;
          flex-direction: column;
          white-space: nowrap;
          overflow: hidden;
        }
        
        [hidden] {
          display: none !important;
        }
        
        [frozen] {
          z-index: 2;
        }
        
        #outerscroller {
          z-index: 0;
        }
        
        #scroller:not([safari]) #outerscroller {
          will-change: transform;
        }
        
        [no-scrollbars]:not([safari]):not([firefox]) #outerscroller, [no-scrollbars][safari] #table, [no-scrollbars][firefox] #table {
          overflow: hidden;
        }
        
        [no-scrollbars]:not([safari]):not([firefox]) #outerscroller {
          pointer-events: none;
        }
        
        [scrolling][safari] #outerscroller, [scrolling][firefox] #outerscroller {
          pointer-events: auto;
        }
        
        [ios] #outerscroller {
          pointer-events: auto;
                z-index: -3;
        }
        
        [ios][scrolling] #outerscroller {
          z-index: 0;
        }
        
        :host([reordering]) [part~="cell"] ::slotted(vaadin-grid-cell-content), :host([reordering]) [part~="resize-handle"], #scroller[no-content-pointer-events] [part~="cell"] ::slotted(vaadin-grid-cell-content) {
          pointer-events: none;
        }
        
        [part~="reorder-ghost"] {
          visibility: hidden;
          position: fixed;
          opacity: 0.5;
          pointer-events: none;
        }
        
        :host([reordering]) {
          -moz-user-select: none;
          -webkit-user-select: none;
          user-select: none;
        }
        
        #scroller[ie][column-reordering-allowed] [part~="header-cell"] {
          -ms-user-select: none;
        }
        
        :host([reordering]) #outerscroller {
          -webkit-overflow-scrolling: auto !important;
        }
        
        [part~="resize-handle"] {
          position: absolute;
          top: 0;
          right: 0;
          height: 100%;
          cursor: col-resize;
          z-index: 1;
        }
        
        [part~="resize-handle"]::before {
          position: absolute;
          content: "";
          height: 100%;
          width: 35px;
          transform: translateX(-50%);
        }
        
        [last-column] [part~="resize-handle"]::before, [last-frozen] [part~="resize-handle"]::before {
          width: 18px;
          transform: translateX(-100%);
        }
        
        #scroller[column-resizing] {
          -ms-user-select: none;
          -moz-user-select: none;
          -webkit-user-select: none;
          user-select: none;
        }
        
        .sizer {
          display: flex;
          position: relative;
          width: 100%;
          visibility: hidden;
        }
        
        .sizer [part~="details-cell"] {
          display: none;
        }
        
        .sizer [part~="cell"][hidden] {
          display: none;
        }
        
        .sizer [part~="cell"] {
          display: block;
          flex-shrink: 0;
          line-height: 0;
          margin-top: -1em;
          height: 0 !important;
          min-height: 0 !important;
          max-height: 0 !important;
          padding: 0 !important;
        }
        
        .sizer [part~="cell"]::before {
          content: "-";
        }
        
        .sizer [part~="cell"] ::slotted(vaadin-grid-cell-content) {
          display: none;
        }
        
        #fixedsizer {
          position: absolute;
        }
        
        :not([edge][no-scrollbars]) #fixedsizer {
          display: none;
        }
        
        [edge][no-scrollbars] {
          transform: translateZ(0);
          overflow: hidden;
        }
        
        [edge][no-scrollbars] #header, [edge][no-scrollbars] #footer {
          position: fixed;
        }
        
        [edge][no-scrollbars] #items {
          position: fixed;
          width: 100%;
          will-change: transform;
        }
        
        :host {
          --px-data-grid-padding-top: 12px;
          --px-data-grid-padding-bottom: 12px;
          --px-data-grid-padding-left: 12px;
          --px-data-grid-padding-right: 12px;
          --px-data-grid-cell-focus-outline-width: 2px;
          --px-data-grid-focused-padding-top: 10px;
          --px-data-grid-focused-padding-bottom: 10px;
          --px-data-grid-focused-padding-left: 10px;
          --px-data-grid-focused-padding-right: 10px;
          --px-data-grid-cell-focused-color: #72b9dd;
          --px-data-grid-reorder-ghost-outline-color: rgb(3, 44, 54);
          --px-data-grid-reorder-ghost-box-shadow-color: #a8a8a8;
  
          
          --px-data-grid-details-padding-left: 42px;
  
          background-color: var(--px-base-background-color, white);
          color: var(--px-base-text-color, rgb(64, 64, 64));
          border: 0;
          border-bottom: 1px solid var(--px-data-grid-border-color, rgb(126, 126, 126));
        }
        
        [part~="cell"] {
          min-height: 0;
          background: var(--px-base-background-color, white);
          padding: 0;
        }
        
        [part~="header-cell"], [part~="footer-cell"] {
          background: var(--px-data-grid-cell-details-background-color , transparent);
          color: var(--px-data-grid-header-text-color, rgb(44, 64, 76));
          text-transform: uppercase;
        }
        
        [part~="header-cell"] ::slotted(*), [part~="footer-cell"] ::slotted(*) {
          font-size: var(--px-data-grid-header-font-size, 12px);
        }
        
        [part~="resize-handle"]::before {
          width: 15px;
          transform: translateX(-10px);
        }
        
        [part~="row"]:last-child [part~="header-cell"] {
          border-bottom: 1px solid var(--px-data-grid-border-color, rgb(126, 126, 126));
        }
        
        [part~="row"]:first-child [part~="footer-cell"] {
          border-top: 1px solid var(--px-data-grid-border-color, rgb(126, 126, 126));
        }
        
        [part~="body-cell"] {
          border-bottom: 1px solid var(--px-data-grid-separator-color, rgb(209, 209, 209));
        }
        
        :host([auto-height]) tr:last-child [part~="body-cell"] {
          border-bottom: 0;
        }
        
        [part~="row"][loading] [part~="body-cell"] ::slotted(vaadin-grid-cell-content) {
          opacity: 0;
        }
        
        :host(:not([reordering])) [part~="row"][selected] [part~="cell"] {
          background: var(--px-data-grid-cell-background-color--selected, rgb(195, 195, 195));
          border-bottom: 1px solid var(--px-data-grid-cell-border-color--selected, rgba(154, 154, 154, 0.2));
        }
        
        :host([tree-grid]) [part~="row"][aria-level="1"] [part~="cell"] {
          background: var(--px-data-grid-background-color-striped--groupby, rgb(195, 195, 195));
        }
        
        :host([horizontal-offset]) [part~="cell"][last-frozen] {
          border-right: 1px solid var(--px-data-grid-separator-color, rgb(209, 209, 209));
        }
        
        [part~="cell"][last-frozen] {
          border-right: 0px solid transparent;
          transition: border-right 0.3s ease-out;
        }
        
        [part~="cell"]:focus {
          outline: none;
        }
        
        :host([navigating]) [part~="cell"]:focus {
          box-shadow: inset 0 0 0 var(--px-data-grid-cell-focus-outline-width) var(--px-data-grid-cell-focused-color, transparent);
        }
        
        :host([navigating]) [part~="cell"]:not([part~="header-cell"]):not([part~="details-cell"]):focus ::slotted(vaadin-grid-cell-content) {
          padding-top: var(--px-data-grid-focused-padding-top);
          padding-bottom: var(--px-data-grid-focused-padding-bottom);
          padding-left: var(--px-data-grid-focused-padding-left);
          padding-right: var(--px-data-grid-focused-padding-right);
          border: var(--px-data-grid-cell-focus-outline-width) solid var(--px-data-grid-cell-focused-color, transparent);
        }
        
        :host([striped]) [part~="row"][odd] {
          background: var(--px-data-grid-background-color-striped--odd, rgb(224, 224, 224));
        }
        
        :host([striped]) [part~="row"][selected] {
          background: var(--px-data-grid-background-color-striped--selected, rgb(195, 195, 195));
        }
        
        :host([striped]) [part~="row"][odd] [part~="cell"] {
          background: var(--px-data-grid-background-color-striped--odd, rgb(224, 224, 224));
        }
        
        :host([striped]) [part~="row"][selected] [part~="cell"] {
          background: var(--px-data-grid-background-color-striped--selected, rgb(195, 195, 195));
        }
        
        [part~="cell"]:not(:empty):not([details-cell]) {
          padding: 6px;
        }

        [part~="cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content) {
          padding-top: var(--px-data-grid-padding-top);
          padding-bottom: var(--px-data-grid-padding-bottom);
          padding-left: var(--px-data-grid-padding-left);
          padding-right: var(--px-data-grid-padding-right);
          white-space: normal;
        }
        
        [part~="header-cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content), [part~="footer-cell"]:not([part~="details-cell"]) ::slotted(vaadin-grid-cell-content) {
          padding-top: 0;
          padding-bottom: 0;
          height: 35px;
        }
        
        [part~="reorder-ghost"] {
          opacity: 0.65;
          outline: 1px solid var(--px-data-grid-reorder-ghost-outline-color);
          box-shadow: 0 0 0 1px var(--px-data-grid-reorder-ghost-box-shadow-color), 0 2px 8px 0 var(--px-data-grid-reorder-ghost-box-shadow-color);
          padding-top: var(--px-data-grid-padding-top) !important;
          padding-bottom: var(--px-data-grid-padding-bottom) !important;
          padding-left: var(--px-data-grid-padding-left) !important;
          padding-right: var(--px-data-grid-padding-right) !important;
        }
        
        [part~="cell"][reorder-status="dragging"] {
          background: var(--px-data-grid-dragged-column-background-color, rgb(251, 251, 251));
          --px-data-grid-cell-dragging-color: transparent;
        }
        
        [part~="cell"][reorder-status="dragging"] ::slotted(vaadin-grid-cell-content) {
          background: var(--px-data-grid-dragged-column-background-color, rgb(251, 251, 251));
        }
        
        [part~="body-cell"][aria-expanded="true"] {
          border-bottom: none;
          background-color: var(--px-data-grid-cell-details-background-color, rgb(168, 168, 168));
        }
        
        :host(:not([reordering])) [part~="row"][selected] [part~="body-cell"][aria-expanded="true"] {
          border-bottom: none;
        }
        
        [part~="details-cell"] {
          background-color: var(--px-data-grid-cell-details-background-color, rgb(168, 168, 168));
          border-bottom: 1px solid var(--px-data-grid-separator-color, rgb(209, 209, 209));
          padding-top: var(--px-data-grid-padding-top);
          padding-bottom: var(--px-data-grid-padding-bottom);
          padding-left: var(--px-data-grid-details-padding-left);
          padding-right: var(--px-data-grid-padding-right);
          white-space: normal;
        }
        
        [part~="row"] [part~="body-cell"][hidden-unless-hover] ::slotted(vaadin-grid-cell-content) {
          visibility: hidden;
        }
        
        [part~="row"]:hover [part~="body-cell"][hidden-unless-hover] ::slotted(vaadin-grid-cell-content), [part~="row"] [part~="body-cell"][hidden-unless-hover]:focus ::slotted(vaadin-grid-cell-content), [part~="row"] [part~="body-cell"][hidden-unless-hover]:active ::slotted(vaadin-grid-cell-content), .safari-hover [part~="body-cell"][hidden-unless-hover] ::slotted(vaadin-grid-cell-content) {
          visibility: visible;
        }
 
      `;
        });
        customElements.whenDefined('vaadin-checkbox').then(function () {
            const checkboxes = _x.querySelectorAll('vaadin-checkbox');
            for (let i = 0; i < checkboxes.length; i++) {
                const styleTag = checkboxes[i].shadowRoot.querySelector('style');
                styleTag.innerHTML = styleTag.innerHTML + ' \n ' +
                    '[part="checkbox"] { background-color: var(--lumo-primary-color); }' +
                    ':host(:not([checked]):not([indeterminate]):not([disabled]):hover) [part="checkbox"] { ' +
                    '  background-color: var(--lumo-primary-color); ' +
                    '}';
            }
        });
        customElements.whenDefined('vaadin-grid-sorter').then(function () {
            const checkboxes = _x.querySelectorAll('vaadin-grid-sorter');
            for (let i = 0; i < checkboxes.length; i++) {
                const styleTag = checkboxes[i].shadowRoot.querySelector('style');
                styleTag.innerHTML = styleTag.innerHTML + ' \n ' +
                    ':host(:not([direction])) [part="indicators"]::before { ' +
                    '  opacity: 0.8; ' +
                    '}';
            }
        });
        this._vaadinGrid.items = this.tableData;
    }
    disconnectedCallback() {
        const _x = this._vaadinGrid;
        if (this.selectionMode === 'multi') {
            const selectAllCheckBox = _x.querySelector('.vaadin-grid-select-all-checkbox');
            selectAllCheckBox.removeEventListener('on-checked-changed', this._handleSelectAllItems);
        }
    }
    _checkColumnResizable(gridResizable, columnResizable) {
        return gridResizable ? (columnResizable === undefined ? true : columnResizable) : false;
    }
    _computeColumnSelectorLabel(selectedItems) {
        const columnsSelected = selectedItems.length;
        const selectionText = 'Kolon Seçildi'; //columnsSelected > 1 ? 'columns selected' : 'column selected';
        return { columnsSelected } + ' ' + { selectionText };
    }
    _doInstantSortAfterSelect() {
        return this.instantSortWhenSelection
            && this.allowSortBySelection
            && Array.from(this._vaadinGrid._sorters)
                .map((s) => s.path)
                .filter(p => p === '--selection--').length > 0;
    }
    _getColumnByPath(columnPath) {
        if (!this.columns) {
            return;
        }
        return this.columns.filter((column) => column.path === columnPath)[0];
    }
    _getColumnElementById(columnId) {
        return this._getColumns()
            .filter((column) => column.mappedObject && column.mappedObject.id == columnId)[0];
    }
    _getColumnFlexGrow(column) {
        return column.flexGrow === undefined ? this.defaultColumnFlexGrow : column.flexGrow;
    }
    _getColumns() {
        return this._vaadinGrid ? Array.from(this._vaadinGrid.querySelectorAll('vaadin-grid' + (this.sortable ? '-sort' : '') + '-column')) : [];
    }
    _getColumnWidth(column) {
        return column.width ? column.width : this.defaultColumnWidth;
    }
    _getColumnsWithName(name) {
        return this._getColumns().filter((c) => c.name == name);
    }
    /**
     * Returns a value that identifies the item. Uses `itemIdPath` if available.
     * Can be customized by overriding.
     */
    getItemId(item) {
        return this.itemIdPath ? this.get(this.itemIdPath, item) : item;
    }
    _getItemIndexInArray(item, array) {
        let result = -1;
        array.forEach((i, idx) => {
            if (this._itemsEqual(i, item)) {
                result = idx;
            }
        });
        return result;
    }
    _getValue(column, item) {
        if (column && item) {
            return this.get(column.path, item);
        }
        else {
            return undefined;
        }
    }
    _gridGetItemId(item) {
        if (this._groupByColumn && item && item._groupId) {
            return item._groupId;
        }
        else {
            return this.itemIdPath ? this._vaadinGrid.get(this.itemIdPath, item) : item;
        }
    }
    _groupByColumnAllowed(hasLocalDataProvider, expandableRows) {
        return hasLocalDataProvider && !expandableRows;
    }
    _handledeSelectItem(item) {
        if (item) {
            if (this._vaadinGrid._isSelected(item)) {
                this.pxTableDeselect.emit(item);
                if (this._isMultiSelect()) {
                    const index = this._getItemIndexInArray(item, this._vaadinGrid.selectedItems);
                    if (index > -1) {
                        this._vaadinGrid.splice('selectedItems', index, 1);
                    }
                }
                else {
                    this._vaadinGrid.pop('selectedItems');
                }
            }
            //console.log('_handledeSelectItem',item,this._vaadinGrid.selectedItems);
        }
    }
    _handleSelectAllItems(e) {
        console.log('_handleSelectAllItems', e, this._vaadinGrid.selectedItems);
    }
    _handleSelectItem(item) {
        if (item) {
            if (!this._vaadinGrid._isSelected(item)) {
                this.pxTableSelect.emit(item);
                if (!this._isMultiSelect()) {
                    this._vaadinGrid.pop('selectedItems');
                }
                this._vaadinGrid.push('selectedItems', item);
            }
            //console.log('_handleSelectItem',item,this._vaadinGrid.selectedItems);
        }
    }
    _isAutoHeight(gridHeight) {
        return gridHeight === 'auto';
    }
    _isMultiSelect() {
        return this._isSelectable() && this.selectionMode == 'multi';
    }
    _isSelectable() {
        return this.selectionMode == 'single' || this.selectionMode == 'multi';
    }
    _isStriped(striped, groupByColumn) {
        return striped && !groupByColumn;
    }
    _itemsEqual(item1, item2) {
        return this.getItemId(item1) === this.getItemId(item2);
    }
    _offerActionColumn(editable, itemActions /*, editingItem*/) {
        return (editable) || (itemActions && itemActions.length > 0);
    }
    _offerEditColumn(editable) {
        return editable;
    }
    _resolveColumnPath(column) {
        //console.log('_resolveColumnPath',column);
        if (typeof column.path === 'undefined') {
            console.warn(`column.path for column ${JSON.stringify(column)} should be initialized.`);
        }
        return column.path ? column.path : '';
    }
    _selectedItemsChanged(selectedItems) {
        console.log('_selectedItemsChanged', selectedItems);
        // Cause resort if ready and asked to instantly re-sort
        if (this._vaadinGrid && this._doInstantSortAfterSelect()) {
            this._vaadinGrid.clearCache();
        }
    }
    _undefinedToFalse(value) {
        if (value === undefined) {
            return false;
        }
        else {
            return value;
        }
    }
    get(path, item) {
        return item[path] ? item[path] : undefined;
    }
    /**
     * Returns currently visible data if called with `true`.
     * Returns all cached data if called with `undefined`.
     *
     * @param {?boolean} visibleOnly
     * @return Array
     */
    getData(visibleOnly) {
        let items = [];
        if (visibleOnly) {
            items = Array.from(this._vaadinGrid.querySelectorAll('px-data-grid-cell-content-wrapper'));
            const gridRect = this._vaadinGrid.getBoundingClientRect();
            const headerRect = this._vaadinGrid
                .querySelector('px-data-grid-header-cell')
                .getBoundingClientRect();
            items = items.filter((wrapper) => {
                const wrapperRect = wrapper.getBoundingClientRect();
                return wrapperRect.top < gridRect.bottom && wrapperRect.bottom > headerRect.bottom;
            }).map((wrapper) => {
                return wrapper.item;
            }).filter((v, i, a) => {
                return a.indexOf(v) === i && v;
            });
        }
        else {
            const cachedItems = this._vaadinGrid._cache.items;
            Object.keys(cachedItems).forEach((key) => {
                items.push(cachedItems[key]);
            });
        }
        return items.map((item) => {
            const sortedObject = {};
            this.getVisibleColumns().forEach((col) => {
                sortedObject[col.path] = item[col.path];
            });
            return sortedObject;
        });
    }
    getVisibleColumns() {
        return this._getColumns().filter((col) => !col.hidden).sort((a, b) => a._order - b._order);
    }
    resolveColumnHeader(column) {
        return column.header ? column.header : column.name;
    }
    getTemplateSelectable() {
        //console.log('---getTemplateSelectable')
        if (this._isSelectable()) {
            return (h("vaadin-grid-selection-column", { "auto-select": true, frozen: true }));
        }
    }
    getTemplateColumnHeader(column) {
        //console.log('---getTemplateColumnHeader')
        return (h("vaadin-grid-sort-column", { path: this._resolveColumnPath(column), header: this.resolveColumnHeader(column) }));
    }
    getTemplateColumns() {
        //console.log('---getTemplateColumns');
        const _sortable = this.sortable;
        return (this.columns.map((column) => {
            if (_sortable) {
                return (h("vaadin-grid-sort-column", { name: column.name, path: column.path, frozen: this._undefinedToFalse(column.frozen), hidden: column.hidden, type: column.type, textAlign: column.type == 'number' ? 'end' : 'start', resizable: this._checkColumnResizable(this.resizable, column.resizable), mappedObject: column, width: this._getColumnWidth(column), flexGrow: this._getColumnFlexGrow(column), groupByColumnAllowed: this._groupByColumnAllowed(this._hasLocalDataProvider, this._expandableRows), isDataColumn: true }, this.getTemplateColumnHeader(column)));
            }
            else {
                return (h("vaadin-grid-column", { name: column.name, path: column.path, frozen: this._undefinedToFalse(column.frozen), hidden: column.hidden, type: column.type, textAlign: column.type == 'number' ? 'end' : 'start', resizable: this._checkColumnResizable(this.resizable, column.resizable), mappedObject: column, width: this._getColumnWidth(column), flexGrow: this._getColumnFlexGrow(column), groupByColumnAllowed: this._groupByColumnAllowed(this._hasLocalDataProvider, this._expandableRows), isDataColumn: true }, this.getTemplateColumnHeader(column)));
            }
        }));
    }
    getTemplateOfferEditColumn() {
        //console.log('---getTemplateOfferEditColumn',this._offerEditColumn(this.editable))
        if (this._offerEditColumn(this.editable)) {
            return;
            //return(<vaadin-grid-sort-column>
            //  <vaadin-checkbox aria-label="Select Row" checked="{{selected}}">Selected</vaadin-checkbox>
            //</vaadin-grid-sort-column>);
        }
    }
    getTemplateOfferActionColumn() {
        //console.log('---getTemplateOfferActionColumn',this._offerActionColumn(this.editable, this.itemActions))
        if (this._offerActionColumn(this.editable, this.itemActions)) {
            return;
        }
    }
    render() {
        //console.log('px-data-grid---render')
        return ([
            h("vaadin-grid", { ref: (el) => this._vaadinGrid = el, size: this.size, activeItem: this.activeItem, columnReorderingAllowed: false, expandedItems: this.expandedItems, striped: this._isStriped(this.striped, this._groupByColumn), selectedItems: this.selectedItems, multiSort: this.multiSort, itemIdPath: this.itemIdPath, pageSize: this.pageSize, autoHeight: this._isAutoHeight(this.gridHeight), loading: this._loading },
                this.getTemplateSelectable(),
                this.getTemplateColumns(),
                h("vaadin-grid-column-group", null,
                    this.getTemplateOfferEditColumn(),
                    this.getTemplateOfferActionColumn())),
            h("px-spinner", { size: 40, finished: this._spinnerHidden })
        ]);
    }
    static get is() { return "px-data-grid"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./sass/px-data-grid.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["./sass/px-data-grid.css"]
    }; }
    static get properties() { return {
        "_lastColumnsReceived": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "_selectedActionItems": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "columns": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "expandedItems": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "itemActions": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "selectedItems": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "tableData": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object[]",
                "resolved": "object[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "_groupByColumn": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_group-by-column",
            "reflect": false,
            "defaultValue": "false"
        },
        "activeItem": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "object",
                "resolved": "object",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "null"
        },
        "defaultColumnFlexGrow": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "default-column-flex-grow",
            "reflect": false,
            "defaultValue": "1"
        },
        "loadingSpinnerDebounce": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "loading-spinner-debounce",
            "reflect": false,
            "defaultValue": "500"
        },
        "pageSize": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "page-size",
            "reflect": false,
            "defaultValue": "200"
        },
        "size": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "size",
            "reflect": false,
            "defaultValue": "undefined"
        },
        "_expandableRows": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_expandable-rows",
            "reflect": false,
            "defaultValue": "false"
        },
        "_hasLocalDataProvider": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_has-local-data-provider",
            "reflect": false
        },
        "_loading": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_loading",
            "reflect": false,
            "defaultValue": "false"
        },
        "_spinnerHidden": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_spinner-hidden",
            "reflect": false,
            "defaultValue": "true"
        },
        "allowSortBySelection": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "allow-sort-by-selection",
            "reflect": false,
            "defaultValue": "false"
        },
        "editable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "editable",
            "reflect": false,
            "defaultValue": "false"
        },
        "flexToSize": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "flex-to-size",
            "reflect": true,
            "defaultValue": "false"
        },
        "hideActionMenu": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-action-menu",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideColumnFilter": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-column-filter",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideSelectionColumn": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-selection-column",
            "reflect": false,
            "defaultValue": "false"
        },
        "instantSortWhenSelection": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "instant-sort-when-selection",
            "reflect": false,
            "defaultValue": "false"
        },
        "multiSort": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "multi-sort",
            "reflect": false,
            "defaultValue": "false"
        },
        "resizable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "resizable",
            "reflect": false,
            "defaultValue": "false"
        },
        "selectable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selectable",
            "reflect": false,
            "defaultValue": "false"
        },
        "sortable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "sortable",
            "reflect": false,
            "defaultValue": "false"
        },
        "striped": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "striped",
            "reflect": false,
            "defaultValue": "false"
        },
        "useKeyIfMissing": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "use-key-if-missing",
            "reflect": false,
            "defaultValue": "true"
        },
        "_autoFilterValue": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_auto-filter-value",
            "reflect": false
        },
        "defaultColumnWidth": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "default-column-width",
            "reflect": false,
            "defaultValue": "'100px'"
        },
        "gridHeight": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "grid-height",
            "reflect": true
        },
        "itemIdPath": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "item-id-path",
            "reflect": false
        },
        "selectionMode": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'none'|'single'|'multi'",
                "resolved": "\"multi\" | \"none\" | \"single\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selection-mode",
            "reflect": false,
            "defaultValue": "'none'"
        }
    }; }
    static get events() { return [{
            "method": "pxTableSelect",
            "name": "pxTableSelect",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxTableDeselect",
            "name": "pxTableDeselect",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "selectedItems",
            "methodName": "_selectedItemsChanged"
        }]; }
}
