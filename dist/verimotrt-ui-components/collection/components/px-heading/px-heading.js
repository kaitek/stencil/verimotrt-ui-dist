import { h } from "@stencil/core";
export class PxHeading {
    constructor() {
        this.text = '';
    }
    render() {
        return (h("span", { class: this.class }, this.text));
    }
    static get is() { return "px-heading"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["../px-headings-design/_base.headings.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["../px-headings-design/_base.headings.css"]
    }; }
    static get properties() { return {
        "text": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "text",
            "reflect": false,
            "defaultValue": "''"
        },
        "class": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'' | 'heading--page' | 'heading--section' | 'heading--subsection' | 'label' | 'value' | 'alpha' | 'beta' | 'gamma' | 'delta' | 'epsilon' | 'zeta'",
                "resolved": "\"\" | \"alpha\" | \"beta\" | \"delta\" | \"epsilon\" | \"gamma\" | \"heading--page\" | \"heading--section\" | \"heading--subsection\" | \"label\" | \"value\" | \"zeta\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "class",
            "reflect": false
        }
    }; }
}
