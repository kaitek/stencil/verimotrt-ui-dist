import { h } from "@stencil/core";
export class PxOeeView {
    render() {
        return (h("div", { id: "oeeview_container", style: { width: "200px", height: "160px" } },
            h("div", { id: "row1", class: "flex flex--row flex--justify flex--top" },
                h("div", { id: "col_1_1", class: "flex flex--col flex--center flex--top" },
                    h("div", { class: "usability delta" },
                        h("span", null, "KUL")),
                    h("div", { class: "usability delta" },
                        h("span", null, this.usability))),
                h("div", { id: "col_1_2", class: "flex flex--col flex--center flex--top" },
                    h("div", { class: "performance delta" },
                        h("span", null, "PER")),
                    h("div", { class: "performance delta" },
                        h("span", null, this.performance))),
                h("div", { id: "col_1_3", class: "flex flex--col flex--center flex--top" },
                    h("div", { class: "quality delta" },
                        h("span", null, "KAL")),
                    h("div", { class: "quality delta" },
                        h("span", null, this.quality)))),
            h("div", { id: "row2", class: "flex flex--row flex--center flex--top" },
                h("div", { id: "col_2_1", class: "flex flex--col flex--center flex--top" },
                    h("div", { class: "oee delta" },
                        h("span", null, "OEE")),
                    h("div", { class: "oee delta" },
                        h("span", null, this.oee))))));
    }
    static get is() { return "px-oee-view"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-oee-view.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-oee-view.css"]
    }; }
    static get properties() { return {
        "usability": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "usability",
            "reflect": false
        },
        "performance": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "performance",
            "reflect": false
        },
        "quality": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "quality",
            "reflect": false
        },
        "oee": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "oee",
            "reflect": false
        }
    }; }
}
