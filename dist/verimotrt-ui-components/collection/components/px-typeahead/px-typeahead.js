import { h } from "@stencil/core";
export class PxTypeahead {
    constructor() {
        this.disabled = false;
        this.placeholder = 'Enter Your Search Query';
        this.localCandidates = [];
        this._suggestions = [];
        this.maxSuggestions = 5;
        this.value = '';
        this.inputValue = '';
        this._isFocused = false;
        this.required = false;
    }
    toggleClass(name, bool, node) {
        if (arguments.length == 1) {
            bool = !node.classList.contains(name);
        }
        if (bool) {
            node.classList.add(name);
        }
        else {
            node.classList.remove(name);
        }
    }
    _computeHasInputAndNotDisabled() {
        return this.inputValue.length && this.disabled === false;
    }
    _setSearchResultSize() {
        if (this.elSearchResults) {
            var searchInputRect = this.elSearchBox.getBoundingClientRect();
            this.elSearchResults.style.width = searchInputRect.width + 'px';
        }
    }
    _isSuggestionsEmpty() {
        return this._suggestions.length;
    }
    _keydown(e) {
        if (e.which === 40) { //down Arrow
            e.preventDefault();
            this._downArrowPress();
        }
        else if (e.which === 38) { //up arrow
            e.preventDefault();
            this._upArrowPress();
        }
        else if (e.which === 13) { //enter key
            this._enterPress();
        }
        else if (e.which === 8) { //enter or space keys
            this._backspacePress(e);
        }
        else if (e.which === 27) { //esc key
            this._clear();
        }
        else {
            this._checkIfSingleResult();
        }
    }
    _onhover(e) {
        var alreadySelected = this.elContainer.querySelector('.item-selected');
        if (alreadySelected) {
            alreadySelected.classList.remove('item-selected');
        }
        e.currentTarget.classList.add('item-selected');
    }
    _onfocus() {
        this._isFocused = true;
    }
    _onblur() {
        this._isFocused = false;
        this.toggleClass('validation-error', this.required && !this.value, this.elSearchBox);
    }
    _onSearchInputChange(e) {
        this.pxTypeaheadSearchInputChange.emit(e);
        this.inputValue = this.elSearchBox.value;
    }
    //Fired when an item is selected.
    //@event px-typeahead-item-selected
    _select(e) {
        var selectedItem = e.currentTarget, text = selectedItem.textContent.trim();
        this.elSearchBox.focus();
        this.inputValue = text;
        this.value = text;
        //this.async(function () {
        //  this.set('_suggestions', []);
        //}, 100);
        setTimeout(() => {
            this._suggestions = [];
        }, 5);
        this.toggleClass('validation-error', this.required && !this.value, this.elSearchBox);
        this.pxTypeaheadItemSelected.emit(this.value);
        e.stopPropagation();
    }
    _downArrowPress() {
        var alreadySelected = this.elContainer.querySelector('.item-selected'), suggestionsMenu = this.elContainer.querySelector('ul'), selectedItem, children;
        if (alreadySelected) {
            selectedItem = alreadySelected.nextElementSibling;
            if (selectedItem && selectedItem.tagName === 'LI') {
                alreadySelected.classList.remove('item-selected');
                selectedItem.classList.add('item-selected');
                suggestionsMenu.scrollTop = selectedItem.offsetTop;
            }
            else {
                return;
            }
        }
        else {
            if (suggestionsMenu) {
                children = suggestionsMenu.getElementsByTagName('li');
                selectedItem = children[0];
                if (selectedItem) {
                    selectedItem.classList.add('item-selected');
                }
            }
        }
    }
    _upArrowPress() {
        var alreadySelected = this.elContainer.querySelector('.item-selected'), suggestionsMenu = this.elContainer.querySelector('ul'), selectedItem, children;
        if (alreadySelected) {
            selectedItem = alreadySelected.previousElementSibling;
            if (selectedItem && selectedItem.tagName === 'LI') {
                alreadySelected.classList.remove('item-selected');
                selectedItem.classList.add('item-selected');
                suggestionsMenu.scrollTop = selectedItem.offsetTop;
            }
            else {
                return;
            }
        }
        else {
            if (suggestionsMenu) {
                children = suggestionsMenu.getElementsByTagName('li');
                selectedItem = children[0];
                if (selectedItem) {
                    selectedItem.classList.add('item-selected');
                }
            }
        }
    }
    _enterPress() {
        var alreadySelected, selectedItem, results;
        alreadySelected = this.elContainer.querySelector('.item-selected');
        //if we only have 1 option, select it, otherwise, do nothing.
        results = this.elContainer.querySelectorAll('.result');
        if (results.length === 1) {
            //if we only have 1 result, we should select it.
            selectedItem = results[0];
        }
        else if (results.length > 1) {
            //let make sure we have a selected item, and if we do, set it to selectedItem and remove the class.
            if (alreadySelected) {
                //and if we do, mark it.
                selectedItem = alreadySelected;
                selectedItem.classList.remove('item-selected');
            }
            else {
                //we have results, but none of them are selected.
                return false;
            }
        }
        else {
            //no results, return false
            return false;
        }
        //if we've reached this point, selectedItem should be set.
        this.value = selectedItem.textContent.trim();
        this.inputValue = selectedItem.textContent.trim();
        //this.async(function () {
        //  this._suggestions=[];
        //}, 100);
        setTimeout(() => {
            this._suggestions = [];
        }, 100);
        this.toggleClass('validation-error', false, this.elSearchBox);
        this.pxTypeaheadItemSelected.emit(this.value);
        this.elContainer.querySelector('input').focus();
    }
    _backspacePress(e) {
        this.value = '';
        /**
         * Fired when an item is deselected because the user has
         * pressed the backspace key to change or clear the search field.
         * @event px-typeahead-item-deselected
         */
        this.pxTypeaheadItemDeselected.emit();
        var val = e.target.value;
        this._search(val);
    }
    _checkIfSingleResult() {
        var results = this.elContainer.querySelectorAll('.result');
        if (this._suggestions.length === 1) {
            this.toggleClass('item-selected', true, results[0]);
        }
    }
    _localCandidatesSearch(term) {
        var matched = [];
        matched = this.localCandidates.filter(function (candidate) {
            return candidate.trim().toLowerCase().indexOf(term.trim().toLowerCase()) > -1;
        });
        return matched;
    }
    /**
     * Highlights matching letters in bold.
     */
    _boldResults(items, term) {
        if (items.length === 0) {
            return [];
        }
        var resultsList = items.map(function (fullString) {
            var termLocation = fullString.trim().toLowerCase().indexOf(term.trim().toLowerCase());
            if (termLocation > -1) {
                return {
                    prefix: fullString.trim().substr(0, termLocation),
                    highlight: fullString.trim().substr(termLocation, term.length),
                    suffix: fullString.trim().substr(termLocation + term.length)
                };
            }
        });
        resultsList = resultsList.filter(function (result) {
            return result !== "undefined";
        });
        return resultsList;
    }
    _search(term) {
        if (this._isFocused) {
            //this.debounce('search', function () {
            var matched = [];
            if (!term) {
                this._suggestions = [];
                return;
            }
            if (this.localCandidates && this.localCandidates.length) {
                matched = this._localCandidatesSearch(term);
            }
            this._suggestions = matched;
            //make sure the results are the same size as the input box.
            //this.async(function () {
            //  this._setSearchResultSize();
            //}, 5);
            setTimeout(() => {
                this._setSearchResultSize();
            }, 5);
            //}, 100);
        }
    }
    _clear() {
        this.inputValue = '';
        this.value = '';
        this._suggestions = [];
        this.elContainer.querySelector('input').focus();
        this.pxTypeaheadItemDeselected.emit();
    }
    _disabledClass(disabled) {
        return disabled ? 'disableIcon' : '';
    }
    _requiredChanged(newValue) {
        if (newValue === false) {
            this.toggleClass('validation-error', false, this.elSearchBox);
        }
    }
    getTemplateSearchIcon() {
        let cls = "typeahead__icon " + this._disabledClass(this.disabled);
        if (!this.inputValue) {
            return (h("px-icon", { id: "search__icon", class: cls, icon: "fa-search" }));
        }
    }
    getTemplateCloseIcon() {
        if (this._computeHasInputAndNotDisabled()) {
            return (h("px-icon", { id: "clear__icon", class: "typeahead__icon", icon: "fa-times", "on-tap": () => this._clear() }));
        }
    }
    getTemplateResults() {
        let results = this._boldResults(this._suggestions, this.inputValue);
        return (results.map((item) => {
            return (h("li", { "on-tap": (event) => this._select(event), "on-mouseover": (event) => this._onhover(event), "on-keydown": (event) => this._keydown(event), class: "result" },
                item.prefix,
                h("span", { class: "bold" }, item.highlight),
                item.suffix));
        }));
    }
    getTemplateSuggestionsEmpty() {
        return (h("div", { class: "dropdown__container" },
            h("ul", { id: "searchResults", class: "list-bare", ref: (el) => this.elSearchResults = el }, this.getTemplateResults())));
    }
    render() {
        return ([
            h("div", { class: "search__form", ref: (el) => this.elContainer = el },
                h("input", { class: "text-input search__box", ref: (el) => this.elSearchBox = el, disabled: this.disabled, placeholder: this.placeholder, value: this.inputValue, "on-keydown": (event) => this._keydown(event), "on-focus": () => this._onfocus(), "on-blur": () => this._onblur(), "on-input": (event) => this._onSearchInputChange(event), "data-kb": true, autocomplete: "off", required: this.required }),
                this.getTemplateSearchIcon(),
                this.getTemplateCloseIcon()),
            this.getTemplateSuggestionsEmpty()
        ]);
    }
    static get is() { return "px-typeahead"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-typeahead.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-typeahead.css"]
    }; }
    static get properties() { return {
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": false,
            "defaultValue": "false"
        },
        "placeholder": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "placeholder",
            "reflect": false,
            "defaultValue": "'Enter Your Search Query'"
        },
        "localCandidates": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "_suggestions": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "maxSuggestions": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max-suggestions",
            "reflect": false,
            "defaultValue": "5"
        },
        "value": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "value",
            "reflect": false,
            "defaultValue": "''"
        },
        "inputValue": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "input-value",
            "reflect": false,
            "defaultValue": "''"
        },
        "_isFocused": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_is-focused",
            "reflect": false,
            "defaultValue": "false"
        },
        "required": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "required",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
    static get events() { return [{
            "method": "pxTypeaheadSearchInputChange",
            "name": "pxTypeaheadSearchInputChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxTypeaheadItemSelected",
            "name": "pxTypeaheadItemSelected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxTypeaheadItemDeselected",
            "name": "pxTypeaheadItemDeselected",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "inputValue",
            "methodName": "_search"
        }, {
            "propName": "required",
            "methodName": "_requiredChanged"
        }]; }
}
