export class ListItem {
    constructor(obj) {
        this.key = obj.key;
        this.val = obj.val;
        this.disabled = obj.disabled;
        this.disableSelect = obj.disableSelect;
        this.selected = obj.selected;
        this.icon = obj.icon;
        this.color = obj.color;
    }
}
