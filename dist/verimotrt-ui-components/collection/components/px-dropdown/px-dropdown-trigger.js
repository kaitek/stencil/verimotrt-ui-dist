import { h } from "@stencil/core";
export class PxDropdownTrigger {
    constructor() {
        this._icon = 'fa-chevron-down';
        this.hideChevron = false;
        this.displayValue = 'Seçiniz';
        this.buttonStyle = 'default';
        this.opened = false;
        this.displayValueSelected = '';
        this.selectedValues = [];
    }
    _displayValueChanged() {
        this.displayValueSelected = this.displayValue;
    }
    displayValueSelectedChanged() {
        this.openWatchHandler();
    }
    _setDisabledViewableClass() {
        let cname = 'dropdown--disabled-viewable';
        if (this.disabledViewable) {
            this.toggleClass(cname, true, this.elDivTrigger);
        }
        else {
            this.toggleClass(cname, false, this.elDivTrigger);
        }
    }
    _setReadOnlyClass() {
        let cname = 'dropdown--read-only';
        if (this.readOnly) {
            this.toggleClass(cname, true, this.elDivTrigger);
        }
        else {
            this.toggleClass(cname, false, this.elDivTrigger);
        }
    }
    _setDisabledClass() {
        let cname = 'btn--disabled';
        if (this.disabled) {
            this.toggleClass(cname, true, this.elDivTrigger);
        }
        else {
            this.toggleClass(cname, false, this.elDivTrigger);
        }
    }
    openWatchHandler() {
        if (this.triggerIcon) {
            if (this._showClearButton(this.disableClear, this.buttonStyle, this.opened, this.selected, this.selectedValues)) {
                this.triggerIcon.icon = 'fa-times';
            }
            if (this._showChevron(this.disableClear, this.hideChevron, this.buttonStyle, this.opened, this.selected, this.selectedValues)) {
                this.triggerIcon.icon = this._icon;
            }
        }
    }
    componentWillLoad() {
        if (this.icon === '') {
            this.icon = this._icon;
        }
        if (this.displayValueSelected === '') {
            this.displayValueSelected = this.displayValue;
        }
    }
    componentDidLoad() {
        this._setDisabledClass();
    }
    toggleClass(name, bool, node) {
        if (arguments.length == 1) {
            bool = !node.classList.contains(name);
        }
        if (bool) {
            node.classList.add(name);
        }
        else {
            node.classList.remove(name);
        }
    }
    _handleTapped(evt) {
        evt.preventDefault();
        if (!this.disabled) {
            this.pxDropdownTriggerTapped.emit();
        }
    }
    clear() {
        if (this.triggerIcon.icon === 'fa-times') {
            this.pxDropdownClearTapped.emit();
        }
    }
    _getClass() {
        if (this.buttonStyle === 'bare')
            return 'btn--bare u-ph0';
        else if (this.buttonStyle === 'bare--primary')
            return 'btn--bare--primary u-ph0';
        else if (this.buttonStyle === 'tertiary')
            return 'btn--tertiary';
        else if (this.buttonStyle === 'icon')
            return 'btn--bare btn--icon';
        else
            return '';
    }
    _showClearButton(disableClear, buttonStyle, opened, selected, selectedValues) {
        // Always hide it if the configuration is present or the 'icon' button style is used.
        if (this.readOnly || this.disabledViewable || disableClear || buttonStyle === 'icon' || this.displayValueSelected === this.displayValue) {
            return false;
        }
        // Show it if the dropdown is opened and something is selected.
        else if (opened && (typeof selected === 'string' || typeof selected === 'number' || selectedValues.length > 0)) {
            return true;
        }
        // Otherwise hide it.
        else {
            return false;
        }
    }
    _showChevron(disableClear, hideChevron, buttonStyle, opened, selected, selectedValues) {
        // Always hide it if the configuration is present or the 'icon' button style is used.
        if (hideChevron || buttonStyle === 'icon') {
            return false;
        }
        // Show it if the clear button is disabled, or if nothing is selected, otherwise it looks too empty.
        else if (this.readOnly || this.disabledViewable || !opened || disableClear || (opened && !selected && selectedValues.length === 0) || this.displayValueSelected === this.displayValue) {
            return true;
        }
        // Otherwise hide it.
        else {
            return false;
        }
    }
    getTemplateLabel() {
        if (this.buttonStyle !== 'icon') {
            return (h("div", { id: "label", class: "dropdown-label" }, this.displayValueSelected));
        }
        else {
            return (h("px-icon", { class: "custom-icon", icon: this.icon }));
        }
    }
    getTemplateTriggerIcon() {
        if (!this.hideChevron && this.buttonStyle !== 'icon') {
            return (h("px-icon", { ref: (el) => this.triggerIcon = el, class: "dropdown-icon", icon: this.icon, "on-tap": () => this.clear() }));
        }
    }
    render() {
        let _class = "dropdown-trigger btn " + this._getClass();
        return (h("div", { class: "trigger" },
            h("div", { id: "trigger", class: _class, tabindex: "0", "on-tap": (event) => this._handleTapped(event), ref: (el) => this.elDivTrigger = el },
                this.getTemplateLabel(),
                this.getTemplateTriggerIcon())));
    }
    static get is() { return "px-dropdown-trigger"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-dropdown.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-dropdown.css"]
    }; }
    static get properties() { return {
        "hideChevron": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-chevron",
            "reflect": false,
            "defaultValue": "false"
        },
        "displayValue": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value",
            "reflect": false,
            "defaultValue": "'Se\u00E7iniz'"
        },
        "buttonStyle": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'default'|'bare'|'bare--primary'|'tertiary'|'icon'",
                "resolved": "\"bare\" | \"bare--primary\" | \"default\" | \"icon\" | \"tertiary\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "button-style",
            "reflect": false,
            "defaultValue": "'default'"
        },
        "icon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "icon",
            "reflect": false
        },
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": false
        },
        "disableClear": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-clear",
            "reflect": false
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": false,
            "defaultValue": "false"
        },
        "displayValueSelected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value-selected",
            "reflect": false,
            "defaultValue": "''"
        },
        "disabledViewable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled-viewable",
            "reflect": false
        },
        "readOnly": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "read-only",
            "reflect": false
        },
        "selected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected",
            "reflect": false
        },
        "selectedValues": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        }
    }; }
    static get events() { return [{
            "method": "pxDropdownTriggerTapped",
            "name": "pxDropdownTriggerTapped",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxDropdownClearTapped",
            "name": "pxDropdownClearTapped",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "displayValue",
            "methodName": "_displayValueChanged"
        }, {
            "propName": "displayValueSelected",
            "methodName": "displayValueSelectedChanged"
        }, {
            "propName": "disabledViewable",
            "methodName": "_setDisabledViewableClass"
        }, {
            "propName": "readOnly",
            "methodName": "_setReadOnlyClass"
        }, {
            "propName": "disabled",
            "methodName": "_setDisabledClass"
        }, {
            "propName": "opened",
            "methodName": "openWatchHandler"
        }]; }
}
