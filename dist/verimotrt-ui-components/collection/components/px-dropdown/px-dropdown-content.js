import { h } from "@stencil/core";
import '@polymer/iron-a11y-keys-behavior/iron-a11y-keys-behavior';
import '@polymer/iron-dropdown/iron-dropdown';
import '@polymer/iron-selector/iron-selector';
export class PxDropdownContent {
    constructor() {
        this.openTrigger = null;
        this.displayValueCount = 1;
        this.language = 'en';
        this._keyboardBeingUsed = false;
    }
    _setReadOnly() {
        if (this.multi && this.readOnly) {
            this.toggleClass('dropdown--read-only-multi', true, this.elDivDropdownContent);
        }
        else if (this.readOnly) {
            this.toggleClass('dropdown--read-only', true, this.elDivDropdownContent);
        }
        else {
            this.toggleClass('dropdown--read-only', false, this.elDivDropdownContent);
        }
    }
    _setDisabledViewableClass() {
        if (this.disabledViewable) {
            this.toggleClass('dropdown--disabled-viewable', true, this.elDivDropdownContent);
        }
        else {
            this.toggleClass('dropdown--disabled-viewable', false, this.elDivDropdownContent);
        }
    }
    _updateSelection() {
        if (Array.isArray(this.items) && this.items.length > 0) {
            let length, i;
            if (this.multi) {
                let selected;
                length = this.items.length;
                selected = Array.isArray(this.selectedValues) ? this.selectedValues.slice(0) : [];
                for (i = 0; i < length; i++) {
                    if (this.items[i].selected !== undefined && this.items[i].selected.toString() === 'true') {
                        if (selected && selected.indexOf(this.items[i][this.selectBy]) === -1) {
                            selected.push(this.items[i][this.selectBy]);
                        }
                    }
                }
                this.selectedValues = selected;
            }
            else {
                let selected;
                length = this.items.length;
                selected = this.selected;
                for (i = 0; i < length; i++) {
                    if (this.items[i].selected !== undefined && this.items[i].selected.toString() === 'true') {
                        if (selected !== this.items[i][this.selectBy]) {
                            this.selected = this.items[i][this.selectBy];
                            break;
                        }
                    }
                }
            }
        }
    }
    /**
     * Any time that `items` changes, this method will convert any
     * simple strings in the array to an object with key/val.
     */
    _itemsChanged(items) {
        if (items === undefined)
            return;
        var newComputedItems = [];
        if (items !== null) {
            items.forEach(function (item, idx) {
                if (typeof item === 'string') {
                    newComputedItems[idx] = { "key": idx, "val": item };
                }
                else {
                    newComputedItems[idx] = item;
                }
            }.bind(this));
        }
        this._computedItems = newComputedItems;
        if (this.clearSelectionsOnChange) {
            this.selected = null;
            this.selectedValues = [];
        }
        this._updateSelection();
        this._notifyResize();
    }
    /**
       * Bind to open trigger, open/close on trigger tap.
       */
    triggerAssignedHandler() {
        this.elDropdown.positionTarget = this.openTrigger;
        this._openTriggerChanged(this.openTrigger, null);
    }
    openChangeHandler() {
        this.openTrigger.opened = this.opened;
    }
    _handleKeyPress(event) {
        this._keyboardBeingUsed = true;
        document.addEventListener('mousemove', this._bindMouse.bind(this));
        let keyPressed = event.code, options = this.elSelector.getEffectiveChildren().filter(function (node) {
            return (node.nodeType === Node.ELEMENT_NODE && node.nodeName === 'DIV'
                && !node.hasAttribute('disabled') && !node.hasAttribute('data-disabled') && !node.classList.contains('hidden'));
        }), focused = options.indexOf(this.elDivFocusedOption), searchFocused = this.searchMode && this.elDropdown.querySelector(':focus') === this.elDropdown.querySelector('.search__box');
        let optionFocused = options[focused];
        let optionFocusedNext = options[focused + 1];
        switch (keyPressed) {
            case 'Space':
            case 'Enter':
                if (searchFocused)
                    break;
                // If closed, open it
                if (!this.opened) {
                    this.toggle();
                }
                // If opened, select currently focused item
                else if (focused !== -1) {
                    optionFocused.click();
                }
                //event.detail.keyboardEvent.preventDefault();
                event.preventDefault();
                break;
            case 'Escape':
                this.elDropdown.close();
                break;
            case 'Tab':
                // Close dropdown and (default behavior) move to next item
                if (this.opened) {
                    this.elDropdown.close();
                }
                break;
            case 'ArrowDown':
                // If closed, open it
                if (!this.opened) {
                    this.toggle();
                }
                // If something is focused, move to the next sibling
                if (focused > -1 && focused < options.length - 1) {
                    this._setFocusedOption(options[focused + 1], options[focused]);
                    this.elDropdown.querySelector('.dropdown-content').scrollTop += optionFocusedNext.offsetHeight;
                }
                // If last element is focused, do nothing
                else if (focused === options.length - 1) {
                    break;
                }
                // If nothing is focused and search-mode is true, move to the search box
                else if (focused === -1 && this.searchMode && !searchFocused) {
                    let elSelector = this.elDropdown.querySelector('.search__box');
                    elSelector.focus();
                }
                // If searchbox is focused, move to the first option
                else if (searchFocused) {
                    let elSelector = this.elDropdown.querySelector('.search__box');
                    elSelector.blur();
                    let _openTriggerHTML = this.openTrigger;
                    _openTriggerHTML.focus();
                    this._setFocusedOption(options[0], null);
                }
                // Else focus the first item in the list
                else {
                    this._setFocusedOption(options[0], null);
                }
                //event.detail.keyboardEvent.preventDefault();
                event.preventDefault();
                break;
            case 'ArrowUp':
                // If closed, open it
                if (!this.opened) {
                    this.toggle();
                }
                // If something is focused, move to the previous sibling
                if (focused > 0) {
                    this._setFocusedOption(options[focused - 1], options[focused]);
                    this.elDropdown.querySelector('.dropdown-content').scrollTop -= optionFocused.offsetHeight;
                }
                // If the first item is focused and search-mode is true, move to the search box
                else if (focused === 0 && this.searchMode && !searchFocused) {
                    let elSelector = this.elDropdown.querySelector('.search__box');
                    elSelector.focus();
                    this._setFocusedOption(null, options[0]);
                }
                // Else focus the last item in the list
                else if (focused === -1 && !searchFocused) {
                    this._setFocusedOption(options[options.length - 1], null);
                }
                event.preventDefault();
                break;
        }
    }
    /**
       * When iron-activate is fired, this method checks whether the item is disabled
       * or if selection is disabled in the dropdown overall (for use in menus).
       * If so, it cancels the event so that iron-select is not called.
       */
    /**
    * Event fired when any non-disabled element is clicked in the dropdown.
    * If disableSelect is set on the entire dropdown or individual item,
    * the event is still fired and the dropdown is closed, but the item
    * is not selected. Useful for menu dropdowns, as opposed to selection dropdowns.
    *
    * @event pxDropdownClick
    */
    _handleActivate(evt) {
        if (evt.detail.item.hasAttribute('disabled') || evt.detail.item.hasAttribute('data-disabled') || this.readOnly || this.disabledViewable) {
            evt.preventDefault();
        }
        else if (this.disableSelect || evt.detail.item.disableSelect) {
            evt.preventDefault();
            this.pxDropdownClick.emit(evt);
            this.elDropdown.close();
        }
        else {
            this.pxDropdownClick.emit(evt);
        }
    }
    /**
       * Handles the selection event from iron-selector to update
       * the label displayed inside the dropdown. Use the displayValueCount property to define how many values
       * are displayed in the trigger value of multiple select lists
       */
    _handleSelection(evt) {
        this.selectedValues = this.elSelector.selectedValues;
        if (this.multi && this.elSelector.selectedItems.length > 0 && this.selectedValues.length <= this.displayValueCount && !this.hideSelected) {
            this.displayValueSelected = this.elSelector.selectedItems.map(function (item) {
                return item.getAttribute("data-val").trim();
            }).join(', ');
        }
        else if (this.multi && this.selectedValues.length > this.displayValueCount && !this.hideSelected) {
            this.displayValueSelected = this.selectedValues.length + ' ' + 'öğe seçildi';
        }
        else if (!this.hideSelected) {
            if (this.elSelector.selectedItem) {
                let elSelected = this.elSelector;
                let elSelectedItem = elSelected.selectedItem;
                this.displayValueSelected = elSelectedItem.getAttribute("data-val");
            }
            else {
                this.displayValueSelected = this.displayValue;
            }
            this.elDropdown.close();
        }
        this.elDropdown.notifyResize();
        /**
         * Event fired when any given element is selected or deselected in the list.
         * `evt.detail` contains:
         * ```
         * { val: "text of the changed element",
         *   key: "key of the changed element",
         *   selected: true/false }
         * ```
         * @event pxDropdownSelectionChanged
         */
        this.pxDropdownSelectionChanged.emit({
            val: evt.detail.item.getAttribute("data-val"),
            key: evt.detail.item.getAttribute(this.selectBy),
            selected: true,
            selectedItems: this.elSelector.selectedItems,
            selectedValues: this.elSelector.selectedValues,
            displayValueSelected: this.displayValueSelected
        });
    }
    /**
     * Handles the de-selection event from iron-selector to update
     * the label displayed inside the dropdown.
     */
    _handleDeselection(evt) {
        this.selectedValues = this.elSelector.selectedValues;
        if (this.multi && this.elSelector.selectedItems.length > 0 && this.elSelector.selectedItems.length <= this.displayValueCount && !this.hideSelected) {
            this.displayValueSelected = this.elSelector.selectedItems.map(function (item) {
                return item.getAttribute("data-val").trim();
            }).join(', ');
        }
        else if (this.multi && this.selectedValues.length > this.displayValueCount && !this.hideSelected) {
            this.displayValueSelected = this.selectedValues.length + ' ' + 'öğe seçildi';
        }
        else {
            this.displayValueSelected = this.displayValue;
        }
        this.elDropdown.notifyResize();
        /**
         * Event fired when any given element is selected or deselected in the list.
         * `evt.detail` contains:
         * ```
         * { val: "text of the changed element",
         *   key: "key of the changed element",
         *   selected: true/false }
         * ```
         * @event pxDropdownSelectionChanged
         */
        this.pxDropdownSelectionChanged.emit({
            val: evt.detail.item.getAttribute("data-val"),
            key: evt.detail.item.getAttribute(this.selectBy),
            selected: false,
            selectedItems: this.elSelector.selectedItems,
            selectedValues: this.elSelector.selectedValues,
            displayValueSelected: this.displayValueSelected
        });
    }
    _handleCancel(e) {
        e.preventDefault();
        var dropdownPath = e.detail.path;
        if (dropdownPath) {
            if (dropdownPath.indexOf(this.openTrigger) === -1) {
                this.elDropdown.close();
            }
        }
        else {
            this.elDropdown.allowOutsideScroll = true;
        }
    }
    _handleClose(e) {
        e.preventDefault();
        this.opened = false;
    }
    _handleOpen(e) {
        e.preventDefault();
        this.opened = true;
    }
    toggleClass(name, bool, node) {
        if (arguments.length == 1) {
            bool = !node.classList.contains(name);
        }
        if (bool) {
            node.classList.add(name);
        }
        else {
            node.classList.remove(name);
        }
    }
    componentWillLoad() {
        this._computedItems = this.items;
    }
    componentDidLoad() {
        this._handleOpenTriggerTappedBound = this.toggle.bind(this);
        this._handleClearTappedBound = this._handleClearTapped.bind(this);
        this._closeBound = this.close.bind(this);
        setTimeout(() => {
            this._updateSelection();
        }, 100);
    }
    componentDidUnload() {
        //Polymer.RenderStatus.afterNextRender(this, function() {
        //  this._updateSelection();
        //}.bind(this));
    }
    toggle() {
        this.elDropdown.toggle();
    }
    close() {
        this.elDropdown.close();
    }
    open() {
        this.elDropdown.open();
    }
    _openTriggerChanged(newTrigger, oldTrigger) {
        if (oldTrigger && oldTrigger instanceof HTMLElement) {
            oldTrigger.removeEventListener('tap', this._handleOpenTriggerTappedBound);
            oldTrigger.removeEventListener('pxDropdownClearTapped', this._handleClearTappedBound);
        }
        if (newTrigger && newTrigger instanceof HTMLElement) {
            newTrigger.addEventListener('tap', this._handleOpenTriggerTappedBound, false);
            newTrigger.addEventListener('pxDropdownClearTapped', this._handleClearTappedBound, false);
        }
    }
    _handleClearTapped() {
        this.selected = null;
        this.selectedValues = [];
        this.displayValueSelected = this.displayValue;
        this.elSelector.selected = null;
        this.elSelector.selectedValues = [];
        if (this.multi) {
            this.elDropdown.querySelectorAll('input#option:checked').forEach(function (item) {
                item.checked = false;
            });
        }
        if (!this.mobile)
            this.elDropdown.close();
    }
    /**
     * Searches the DOM for the `boundTarget` element.
     */
    _getFitElement(target) {
        return document.querySelector(target);
    }
    /**
     * Determines the position target element based on `mobile`.
     */
    _getPositionElement(mobile, openTrigger) {
        return mobile ? window : openTrigger;
    }
    /**
     * Resizes the dropdown when the search term is changed.
     */
    _notifyResize() {
        this._computeFilter();
        this.elDropdown.notifyResize();
    }
    _computeFilter() {
        if (this.searchMode) {
            let term = this.elSearchHtml.value.toLowerCase(), items = this.elDropdown.querySelectorAll('.dropdown-option'), length = items.length, i;
            for (i = 0; i < length; i++) {
                this.toggleClass('hidden', items[i].getAttribute("data-val").toString().toLowerCase().indexOf(term) === -1 && items[i].getAttribute("data-key").toString().toLowerCase().indexOf(term) === -1, items[i]);
            }
        }
    }
    /**
   * Determines whether to display the clear button inside the dropdown in mobile mode.
   */
    _showMobileClearButton(mobile, selected, selectedValues) {
        if (mobile && (typeof selected === 'string' || typeof selected === 'number' || selectedValues.length > 0)) {
            return '';
        }
        else {
            return 'btn--disabled';
        }
    }
    /**
     * Initializes the sort function for the dom-repeat inside of iron-selector.
     */
    /**
     * Event handler for mouse move event. We enable mouse events when user moves the mouse.
     * Mouse events are disabled when user uses the keyboard to interact with the dropdown.
     * @private
     */
    _bindMouse() {
        this._keyboardBeingUsed = false;
        document.removeEventListener('mousemove', this._bindMouse);
    }
    /**
     * The sort function used by the dom-repeat inside of iron-selector to
     * sort the items by either 'key' or 'val' based on `sortMode`.
     */
    _computeSort(a, b) {
        var sortValue = 0;
        if (!this.sortMode) {
            return -1;
        }
        if (this.sortMode && sortValue === 0) {
            if (this.sortMode === 'key') {
                sortValue = a.key - b.key;
            }
            if (this.sortMode === 'val') {
                var nameA = a.val.toUpperCase(), nameB = b.val.toUpperCase();
                if (nameA < nameB) {
                    sortValue--;
                }
                if (nameA > nameB) {
                    sortValue++;
                }
            }
        }
        return sortValue;
    }
    _setFocusedOption(newOption, oldOption) {
        this.elDivFocusedOption = newOption;
        if (newOption) {
            this.toggleClass('focused', true, newOption);
        }
        if (oldOption) {
            this.toggleClass('focused', false, oldOption);
        }
    }
    /**
       * Event handler when the mouse hovers over a dropdown list item.
       */
    _hoverOn(event) {
        if (!this._keyboardBeingUsed && !this.mobile) {
            var currHighlightedItem = document.querySelector('.dropdown-option.focused');
            if (currHighlightedItem) {
                this.toggleClass('focused', false, currHighlightedItem);
            }
            this.toggleClass('focused', true, event.target);
        }
    }
    /**
       * Event handler when the mouse hovers out of a dropdown list item.
       */
    _hoverOff(event) {
        this.toggleClass('focused', false, event.target);
    }
    /**
    * Calculates classes for the caret based on the horizontal and vertical alignment.
    */
    _getCaretClass(showCaret, mobile, verticalAlign, horizontalAlign) {
        return (showCaret && !mobile) ? verticalAlign + ' ' + horizontalAlign : '';
    }
    /**
    * Calculates class for the dropdown items based on whether truncation is disabled.
    */
    _getTruncateClass(disableTruncate) {
        return disableTruncate ? '' : 'truncate';
    }
    /**
    * Calculates offset for the dropdown based on whether caret is shown.
    */
    _getOffset(showCaret, triggerHeight, mobile) {
        return mobile ? 0 : showCaret ? triggerHeight + 5 : triggerHeight;
    }
    _getVertical(verticalAlign, mobile) {
        return mobile ? 'middle' : verticalAlign;
    }
    _getHorizontal(horizontalAlign, mobile) {
        return mobile ? 'left' : horizontalAlign;
    }
    _showCheckmark(multi, mobile) {
        return multi && !mobile;
    }
    _showButtons(mobile, hideMobileButtons) {
        return mobile && !hideMobileButtons;
    }
    getTemplateSearchBox() {
        if (this.searchMode) {
            return (h("div", { class: "u-p- fixed" },
                h("div", { class: "search__form" },
                    h("input", { ref: (el) => this.elSearchHtml = el, class: "text-input search__box", disabled: this.disabled, placeholder: '', value: this.searchTerm, "on-input": () => this._notifyResize(), "on-change": () => this._computeFilter(), "data-kb": true, autocomplete: "off" }),
                    h("px-icon", { class: "search__icon", icon: "fa-search" }))));
        }
    }
    getTemplateDropdownItems() {
        return (this._computedItems.map((item) => {
            let iconStyle = {
                color: item.color,
                stroke: item.color
            };
            let spanclass = 'dropdown-option__item ' + this._getTruncateClass(this.disableTruncate);
            return (h("div", { class: "dropdown-option", key: item.key, "data-key": item.key, "data-val": item.val, "data-disabled": item.disabled, "disable-select": item.disableSelect, "on-mouseover": (event) => this._hoverOn(event), "on-mouseout": (event) => this._hoverOff(event), title: item.val },
                this._showCheckmark(this.multi, this.mobile) ? h("px-icon", { icon: "fa-check", class: "select-icon u-pr--" }) : '',
                item.icon ? h("px-icon", { icon: item.icon, class: "item-icon u-mr-", pxstyle: iconStyle }) : '',
                h("span", { class: spanclass }, item.val)));
        }));
    }
    getTamplateMobileButtons() {
        if (this._showButtons(this.mobile, this.hideMobileButtons)) {
            return (h("div", { class: "flex flex--middle flex--right u-m- fixed" },
                !this.disableClear
                    ? h("button", { class: "btn " + this._showMobileClearButton(this.mobile, this.selected, this.selectedValues), "on-tap": () => this._handleClearTapped() }, "Temizle")
                    : '',
                this.multi
                    ? h("button", { class: "btn btn--primary u-ml-", "on-tap": "toggle" }, "Uygula")
                    : ''));
        }
    }
    render() {
        return ([
            h("iron-dropdown", { id: "dropdown", ref: (el) => this.elDropdown = el, class: 'dropdown ' + this._getCaretClass(this.showCaret, this.mobile, this.verticalAlign, this.horizontalAlign), "auto-fit-on-attach": true, "fit-into": this._getFitElement(this.boundTarget), "position-target": this._getPositionElement(this.mobile, this.openTrigger), "dynamic-align": !this.disableDynamicAlign, "vertical-offset": this._getOffset(this.showCaret, this.triggerHeight, this.mobile), "vertical-align": this._getVertical(this.verticalAlign, this.mobile), "horizontal-align": this._getHorizontal(this.horizontalAlign, this.mobile), opened: this.opened, hover: this.hover, "no-cancel-on-outside-click": this.preventCloseOnOutsideClick, "allow-outside-scroll": this.allowOutsideScroll, disabled: this.disabled, "with-backdrop": this.mobile },
                h("div", { slot: "dropdown-content", class: "dropdown-content shadow-temporary", ref: (el) => this.elDivDropdownContent = el },
                    h("slot", { name: "header" }),
                    this.getTemplateSearchBox(),
                    h("iron-selector", { id: "selector", ref: (el) => this.elSelector = el, class: "selector", multi: this.multi, selected: this.selected, "selected-values": this.selectedValues, "selected-items": this.selectedItems, "attr-for-selected": this.selectBy }, this.getTemplateDropdownItems()),
                    this.getTamplateMobileButtons(),
                    h("slot", { name: "footer" })))
        ]);
    }
    static get is() { return "px-dropdown-content"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-dropdown.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-dropdown.css"]
    }; }
    static get properties() { return {
        "keyEvent": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "KeyboardEvent",
                "resolved": "KeyboardEvent",
                "references": {
                    "KeyboardEvent": {
                        "location": "global"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "openTrigger": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "PxDropdownTrigger",
                "resolved": "PxDropdownTrigger",
                "references": {
                    "PxDropdownTrigger": {
                        "location": "import",
                        "path": "./px-dropdown-trigger"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "null"
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": false
        },
        "hover": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hover",
            "reflect": false
        },
        "boundTarget": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "bound-target",
            "reflect": false
        },
        "preventCloseOnOutsideClick": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "prevent-close-on-outside-click",
            "reflect": false
        },
        "disableClear": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-clear",
            "reflect": false
        },
        "disableSelect": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-select",
            "reflect": false
        },
        "readOnly": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "read-only",
            "reflect": false
        },
        "disabledViewable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled-viewable",
            "reflect": false
        },
        "disableTruncate": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-truncate",
            "reflect": false
        },
        "displayValueSelected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value-selected",
            "reflect": false
        },
        "items": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "ListItem[]",
                "resolved": "ListItem[]",
                "references": {
                    "ListItem": {
                        "location": "import",
                        "path": "./px-listItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": "An array that contains the list of items which show up in the dropdown.\r\nEach item can either be a simple string, or an object consisting of:\r\n* 'key' - a unique identifier (number or string)\r\n* 'val' - the actual text that is displayed\r\n* 'disabled' - whether the item should be disabled completely - can't be selected nor used as a menu option (optional)\r\n* 'disableSelect` - whether the item should be disabled for selection, but can still be used as a menu option (optional)\r\n* 'selected' - whether the item should be selected at instantiation (optional)\r\n* 'icon' - an icon name from the px-icon-set to display next to the item (optional)\r\n* 'color' - the color to use for the icon - if not specified, the default text colors will be used (optional)\r\n\r\nNote: if you specify more than one item as `selected`, but `multi` is not enabled,\r\nonly the *first* selected item will be chosen. See also `clearSelectionsOnChange`."
            }
        },
        "multi": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "multi",
            "reflect": false
        },
        "selectBy": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "select-by",
            "reflect": false
        },
        "selected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected",
            "reflect": false
        },
        "selectedValues": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "selectedItems": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "ListItem[]",
                "resolved": "ListItem[]",
                "references": {
                    "ListItem": {
                        "location": "import",
                        "path": "./px-listItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "allowOutsideScroll": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "allow-outside-scroll",
            "reflect": false
        },
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": false
        },
        "searchMode": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "search-mode",
            "reflect": false
        },
        "searchTerm": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "search-term",
            "reflect": false
        },
        "sortMode": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "sort-mode",
            "reflect": false
        },
        "triggerHeight": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "trigger-height",
            "reflect": false
        },
        "displayValueCount": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value-count",
            "reflect": false,
            "defaultValue": "1"
        },
        "showCaret": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-caret",
            "reflect": false
        },
        "disableDynamicAlign": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-dynamic-align",
            "reflect": false
        },
        "clearSelectionsOnChange": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "clear-selections-on-change",
            "reflect": false
        },
        "hideSelected": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-selected",
            "reflect": false
        },
        "displayValue": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value",
            "reflect": false
        },
        "verticalAlign": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "vertical-align",
            "reflect": false
        },
        "horizontalAlign": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "horizontal-align",
            "reflect": false
        },
        "mobile": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "mobile",
            "reflect": true
        },
        "mobileAt": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "mobile-at",
            "reflect": false
        },
        "hideMobileButtons": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-mobile-buttons",
            "reflect": false
        },
        "language": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "language",
            "reflect": false,
            "defaultValue": "'en'"
        },
        "useKeyIfMissing": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "use-key-if-missing",
            "reflect": false
        },
        "_keyboardBeingUsed": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_keyboard-being-used",
            "reflect": false,
            "defaultValue": "false"
        },
        "_computedItems": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "ListItem[]",
                "resolved": "ListItem[]",
                "references": {
                    "ListItem": {
                        "location": "import",
                        "path": "./px-listItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        }
    }; }
    static get events() { return [{
            "method": "pxDropdownClick",
            "name": "pxDropdownClick",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }, {
            "method": "pxDropdownSelectionChanged",
            "name": "pxDropdownSelectionChanged",
            "bubbles": true,
            "cancelable": true,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get watchers() { return [{
            "propName": "readOnly",
            "methodName": "_setReadOnly"
        }, {
            "propName": "disabledViewable",
            "methodName": "_setDisabledViewableClass"
        }, {
            "propName": "selectBy",
            "methodName": "_updateSelection"
        }, {
            "propName": "items",
            "methodName": "_itemsChanged"
        }, {
            "propName": "openTrigger",
            "methodName": "triggerAssignedHandler"
        }, {
            "propName": "opened",
            "methodName": "openChangeHandler"
        }, {
            "propName": "keyEvent",
            "methodName": "_handleKeyPress"
        }]; }
    static get listeners() { return [{
            "name": "iron-activate",
            "method": "_handleActivate",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "iron-select",
            "method": "_handleSelection",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "iron-deselect",
            "method": "_handleDeselection",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "iron-overlay-canceled",
            "method": "_handleCancel",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "iron-overlay-closed",
            "method": "_handleClose",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "iron-overlay-opened",
            "method": "_handleOpen",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
