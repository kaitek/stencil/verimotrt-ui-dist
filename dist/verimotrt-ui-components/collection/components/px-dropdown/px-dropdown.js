import { h } from "@stencil/core";
import "@polymer/iron-media-query/iron-media-query";
export class PxDropdown {
    constructor() {
        this.opened = false;
        this.hideChevron = false;
        this.preventCloseOnOutsideClick = false;
        this.displayValue = 'Seçiniz';
        this.displayValueCount = 1;
        this.disableClear = false;
        this.readOnly = false;
        this.disableSelect = false;
        this.disableTruncate = false;
        this.multi = false;
        this.selectBy = 'data-key';
        this.selected = null;
        this.allowOutsideScroll = false;
        this.buttonStyle = 'default';
        this.disabled = false;
        this.disabledViewable = false;
        this.searchMode = false;
        this.searchTerm = '';
        this.hideSelected = false;
        this.icon = '';
        this.clearSelectionsOnChange = false;
        this.triggerHeight = 30;
        this.showCaret = false;
        this.disableDynamicAlign = false;
        this.verticalAlign = 'top';
        this.horizontalAlign = 'left';
        this.mobile = false;
        this.mobileAt = 400;
        this.hideMobileButtons = false;
        this.useKeyIfMissing = false;
        this._keyboardBeingUsed = false;
        this.hoist = false;
    }
    _resetMinWidthListener() {
        if (this.elTriggerHTML) {
            this.elContentHTML.style.minWidth = window.getComputedStyle(this.elTriggerHTML).width;
        }
    }
    _pxDropdownSelectionChangedListener(event) {
        this.trigger.selectedValues = event.detail.selectedValues;
        this.trigger.selected = event.detail.key;
        this.trigger.displayValueSelected = event.detail.displayValueSelected;
    }
    pxDropdownClearTappedListener() {
        this.trigger.selectedValues = [];
        this.trigger.selected = null;
        this.trigger.displayValueSelected = this.trigger.displayValue;
    }
    keydownHandler(event) {
        this.content.keyEvent = event;
    }
    _resetMinWidthWatcher() {
        this.elContentHTML.style.minWidth = window.getComputedStyle(this.elTriggerHTML).width;
    }
    _resetMinWidthWatcher2() {
        this.elContentHTML.style.minWidth = window.getComputedStyle(this.elTriggerHTML).width;
    }
    componentWillLoad() {
        //console.log('pxd-hoist:',this.hoist);
    }
    componentDidLoad() {
        this.elTriggerHTML = this.trigger;
        this.content.openTrigger = this.trigger;
        this.elContentHTML.style.minWidth = window.getComputedStyle(this.elTriggerHTML).width;
    }
    componentDidUnload() {
        //this.$.triggerSlot.removeEventListener('slotchange', this._initOpenTrigger);
    }
    /**
     * Toggles visibility of the dropdown: opens dropdown if it is closed, and
     * closes dropdown if it is open.
     */
    toggle() {
        this.content.toggle();
        this.elContentHTML.style.minWidth = window.getComputedStyle(this.elTriggerHTML).width;
    }
    /**
     * Finds the element in the the trigger slot. If the developer passed an
     * element into the `slot="trigger"` slot, returns that element. Otherwise
     * returns the default trigger in the Shadow DOM. Works for Polymer 1.x and
     * Polymer 2.x, which have different APIs for accomplishing this.
     *
     * @return {HTMLElement|null} - Reference to the trigger element, or null if none found
     */
    _getSlottedOpenTrigger() {
        var elems;
        //if (Polymer.Element) {
        //  var nodes = Polymer.FlattenedNodesObserver.getFlattenedNodes(this.$.triggerSlot);
        //  elems = nodes.filter(function(n) {
        //    return n.nodeType === Node.ELEMENT_NODE;
        //  });
        //} else {
        //  elems = this.getContentChildren('#triggerSlot');
        //}
        if (elems.length > 1) {
            console.warn('Only expected 1 element as a child of the "trigger" slot.');
        }
        var [trigger] = elems;
        return (trigger instanceof HTMLElement) ? trigger : null;
    }
    /**
     * Resizes the dropdown when the search term is changed.
     */
    _notifyResize() {
        // Debounce to try to stop notify resize from spamming with events, which
        // slows things down in Polymer 2.x + IE
        //this.debounce('on-notify-resize', function() {
        //  this.content._notifyResize();
        //}, 1);
    }
    render() {
        let _queryStyle = "(max-width: " + this.mobileAt + "px)";
        let _eventNames = ["pxDropdownClick", "pxDropdownSelectionChanged"];
        return ([
            h("iron-media-query", { query: _queryStyle, "query-matches": this.mobile }),
            h("div", { id: "target", ref: (el) => this.elTarget = el },
                h("slot", { name: "trigger" },
                    h("px-dropdown-trigger", { id: "trigger", ref: (el) => this.trigger = el, opened: this.opened, 
                        // use-key-if-missing="[[useKeyIfMissing]]"
                        disabled: this.disabled, disabledViewable: this.disabledViewable, readOnly: this.readOnly, hideChevron: this.hideChevron, displayValue: this.displayValue, disableClear: this.disableClear, selected: this.selected, selectedValues: this.selectedValues, buttonStyle: this.buttonStyle, icon: this.icon, displayValueSelected: this._displayValueSelected }))),
            h("px-overlay-content", { hoist: this.hoist, containerType: this.containerType, eventNames: _eventNames },
                h("px-dropdown-content", { id: "content", ref: (el) => { this.content = el; this.elContentHTML = el; }, items: this.items, multi: this.multi, opened: this.opened, displayValue: this.displayValue, displayValueCount: this.displayValueCount, useKeyIfMissing: this.useKeyIfMissing, disabled: this.disabled, disabledViewable: this.disabledViewable, boundTarget: this.boundTarget, preventCloseOnOutsideClick: this.preventCloseOnOutsideClick, disableSelect: this.disableSelect, readOnly: this.readOnly, selectBy: this.selectBy, selected: this.selected, selectedValues: this.selectedValues, selectedItems: this.selectedItems, triggerHeight: this.triggerHeight, showCaret: this.showCaret, disableDynamicAlign: this.disableDynamicAlign, verticalAlign: this.verticalAlign, horizontalAlign: this.horizontalAlign, mobile: this.mobile, mobileAt: this.mobileAt, hideMobileButtons: this.hideMobileButtons, searchMode: this.searchMode, searchTerm: this.searchTerm, sortMode: this.sortMode, hideSelected: this.hideSelected, allowOutsideScroll: this.allowOutsideScroll, displayValueSelected: this._displayValueSelected, disableTruncate: this.disableTruncate }))
        ]);
    }
    static get is() { return "px-dropdown"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-dropdown.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-dropdown.css"]
    }; }
    static get properties() { return {
        "containerType": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "container-type",
            "reflect": false
        },
        "opened": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "opened",
            "reflect": false,
            "defaultValue": "false"
        },
        "hideChevron": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-chevron",
            "reflect": false,
            "defaultValue": "false"
        },
        "boundTarget": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "bound-target",
            "reflect": false
        },
        "preventCloseOnOutsideClick": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "prevent-close-on-outside-click",
            "reflect": false,
            "defaultValue": "false"
        },
        "displayValue": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value",
            "reflect": false,
            "defaultValue": "'Se\u00E7iniz'"
        },
        "displayValueCount": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "display-value-count",
            "reflect": false,
            "defaultValue": "1"
        },
        "disableClear": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-clear",
            "reflect": false,
            "defaultValue": "false"
        },
        "readOnly": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "read-only",
            "reflect": false,
            "defaultValue": "false"
        },
        "disableSelect": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-select",
            "reflect": false,
            "defaultValue": "false"
        },
        "disableTruncate": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-truncate",
            "reflect": false,
            "defaultValue": "false"
        },
        "_displayValueSelected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_display-value-selected",
            "reflect": false
        },
        "items": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "ListItem[]",
                "resolved": "ListItem[]",
                "references": {
                    "ListItem": {
                        "location": "import",
                        "path": "./px-listItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "multi": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "multi",
            "reflect": false,
            "defaultValue": "false"
        },
        "selectBy": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "select-by",
            "reflect": false,
            "defaultValue": "'data-key'"
        },
        "selected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected",
            "reflect": false,
            "defaultValue": "null"
        },
        "selectedValues": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "selectedItems": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "ListItem[]",
                "resolved": "ListItem[]",
                "references": {
                    "ListItem": {
                        "location": "import",
                        "path": "./px-listItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        },
        "allowOutsideScroll": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "allow-outside-scroll",
            "reflect": false,
            "defaultValue": "false"
        },
        "buttonStyle": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "'default'|'bare'|'bare--primary'|'tertiary'|'icon'",
                "resolved": "\"bare\" | \"bare--primary\" | \"default\" | \"icon\" | \"tertiary\"",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "button-style",
            "reflect": false,
            "defaultValue": "'default'"
        },
        "disabled": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled",
            "reflect": false,
            "defaultValue": "false"
        },
        "disabledViewable": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disabled-viewable",
            "reflect": false,
            "defaultValue": "false"
        },
        "searchMode": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "search-mode",
            "reflect": false,
            "defaultValue": "false"
        },
        "searchTerm": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "search-term",
            "reflect": false,
            "defaultValue": "''"
        },
        "sortMode": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "sort-mode",
            "reflect": false
        },
        "hideSelected": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-selected",
            "reflect": false,
            "defaultValue": "false"
        },
        "icon": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "icon",
            "reflect": false,
            "defaultValue": "''"
        },
        "clearSelectionsOnChange": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "clear-selections-on-change",
            "reflect": false,
            "defaultValue": "false"
        },
        "triggerHeight": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "trigger-height",
            "reflect": false,
            "defaultValue": "30"
        },
        "showCaret": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "show-caret",
            "reflect": false,
            "defaultValue": "false"
        },
        "disableDynamicAlign": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "disable-dynamic-align",
            "reflect": false,
            "defaultValue": "false"
        },
        "verticalAlign": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "vertical-align",
            "reflect": false,
            "defaultValue": "'top'"
        },
        "horizontalAlign": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "horizontal-align",
            "reflect": false,
            "defaultValue": "'left'"
        },
        "mobile": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "mobile",
            "reflect": false,
            "defaultValue": "false"
        },
        "mobileAt": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "mobile-at",
            "reflect": false,
            "defaultValue": "400"
        },
        "hideMobileButtons": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hide-mobile-buttons",
            "reflect": false,
            "defaultValue": "false"
        },
        "useKeyIfMissing": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "use-key-if-missing",
            "reflect": false,
            "defaultValue": "false"
        },
        "_keyboardBeingUsed": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_keyboard-being-used",
            "reflect": false,
            "defaultValue": "false"
        },
        "hoist": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "hoist",
            "reflect": false,
            "defaultValue": "false"
        }
    }; }
    static get watchers() { return [{
            "propName": "displayValue",
            "methodName": "_resetMinWidthWatcher"
        }, {
            "propName": "displayValueCount",
            "methodName": "_resetMinWidthWatcher2"
        }]; }
    static get listeners() { return [{
            "name": "iron-resize",
            "method": "_resetMinWidthListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDropdownSelectionChanged",
            "method": "_pxDropdownSelectionChangedListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "pxDropdownClearTapped",
            "method": "pxDropdownClearTappedListener",
            "target": undefined,
            "capture": false,
            "passive": false
        }, {
            "name": "keydown",
            "method": "keydownHandler",
            "target": undefined,
            "capture": false,
            "passive": false
        }]; }
}
