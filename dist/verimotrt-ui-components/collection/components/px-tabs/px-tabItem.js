export class TabItem {
    constructor(obj) {
        this.id = obj.id;
        this.title = obj.title;
    }
}
