import { h } from "@stencil/core";
export class PxTab {
    componentDidLoad() {
        this.myElement.setAttribute('role', 'tab');
    }
    render() {
        return (h("div", { class: "tab-title" },
            h("slot", null)));
    }
    static get is() { return "px-tab"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-tab.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-tab.css"]
    }; }
    static get elementRef() { return "myElement"; }
}
