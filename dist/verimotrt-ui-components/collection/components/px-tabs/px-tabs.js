import { h } from "@stencil/core";
export class PxTabs {
    constructor() {
        this.selected = "0";
    }
    _selectedWatcher() {
        //temizle
        const clear_t = this.elDivContainer.querySelectorAll('px-tab.iron-selected');
        if (clear_t.length > 0) {
            clear_t[0].classList.remove('iron-selected');
        }
        const clear_d = this.elPages.querySelectorAll('div.iron-selected-tab');
        if (clear_d.length > 0) {
            clear_d[0].classList.remove('iron-selected');
            clear_d[0].classList.remove('iron-selected-tab');
        }
        //seçili olarak işaretle
        const list_t = this.elDivContainer.querySelectorAll('px-tab');
        list_t[parseInt(this.selected) - 1].classList.add('iron-selected');
        list_t[parseInt(this.selected) - 1].classList.add('iron-selected-tab');
        const list_d = this.elPages.childNodes[parseInt(this.selected) - 1];
        list_d.classList.add('iron-selected');
        list_d.classList.add('iron-selected-tab');
        this.pxTabChanged.emit({
            tabID: this.selected
        });
    }
    componentDidLoad() {
        this.elPages = this.myElement.nextSibling;
        this.myElement.setAttribute('role', 'tablist');
        setTimeout(() => {
            this._selectedWatcher();
        }, 100);
    }
    _click(key) {
        this.selected = key.toString();
    }
    getTemplateTabItems() {
        let counter = 1;
        return (this.items.map((item) => {
            let _i = counter++;
            return (h("px-tab", { id: item.id, "data-key": _i, "on-click": () => this._click(_i) }, item.title));
        }));
    }
    render() {
        return (h("div", { id: "container", class: "tabs-container", tabindex: "0", ref: (el) => this.elDivContainer = el },
            h("div", { class: "tabs-container__nav flex" }, this.getTemplateTabItems())));
    }
    static get is() { return "px-tabs"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-tabs.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-tabs.css"]
    }; }
    static get properties() { return {
        "selected": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "selected",
            "reflect": false,
            "defaultValue": "\"0\""
        },
        "items": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "TabItem[]",
                "resolved": "TabItem[]",
                "references": {
                    "TabItem": {
                        "location": "import",
                        "path": "./px-tabItem"
                    }
                }
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            }
        }
    }; }
    static get events() { return [{
            "method": "pxTabChanged",
            "name": "pxTabChanged",
            "bubbles": true,
            "cancelable": false,
            "composed": true,
            "docs": {
                "tags": [],
                "text": ""
            },
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            }
        }]; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "selected",
            "methodName": "_selectedWatcher"
        }]; }
}
