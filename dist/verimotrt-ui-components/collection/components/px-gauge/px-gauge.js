import { h } from "@stencil/core";
import * as d3 from "d3";
export class PxGauge {
    constructor() {
        this.value = 0;
        this.min = 0;
        this.max = 100;
        this.error = [];
        this.anomaly = [];
        this.abnormal = [];
        this.normal = [];
        this._states = ['error', 'abnormal', 'anomaly', 'normal'];
        this.unit = '';
        this._valueOfProcess = 0;
        this.barWidth = 0;
        this._actualBarWidth = 0;
        this._calculatedValue = 0;
        this.marginTop = 0;
        this.marginBottom = 0;
        this.marginRight = 0;
        this.marginLeft = 0;
        this.preventResize = false;
        this._targetPerc = 0;
    }
    async getValue() {
        return this.value;
    }
    async setValue(value) {
        this.value = value;
        this._calculatedValue = this._computeValue();
    }
    /**
     * Change position of needle and update gauge bars follow _calculatedValue
     * @private
     * @method _valueChanged
     */
    _valueChanged() {
        this._chart && this._moveTo(this._calculatedValue);
    }
    /**
     * Re-render gauge bars with new bar width
     * @private
     * @method _barWidthChanged
     */
    _barWidthChanged() {
        this._drawChart();
        this._chart && this._moveTo(this._calculatedValue);
    }
    componentWillLoad() {
    }
    componentDidLoad() {
        this.svg = d3.select(this.elSVGchartSVG);
        this._chart = d3.select(this.elSVGchartG);
        if (!this.preventResize) {
            var wrapperRect = this.elDivchart.getBoundingClientRect();
            if (!wrapperRect.width) {
                // If no height, toss this back and wait for the next animation frame
                // to try again.
                window.requestAnimationFrame(function () {
                    setTimeout(() => {
                        this.componentDidLoad();
                    }, 1);
                }.bind(this));
                return;
            }
            this.width = Math.max(wrapperRect.width, 0);
            this.height = this.width / 2;
            this.elSVGchartSVG.setAttribute("width", this.width.toString());
            this.elSVGchartSVG.setAttribute("height", this.height.toString());
            this.elSVGchartSVG.style.width = this.width + 'px';
            this.elSVGchartSVG.style.height = this.height + 'px';
        }
        setTimeout(() => {
            this._actualBarWidth = this._computeActualBarWidth();
            this._drawChart();
        }, 100);
    }
    /**
     * Calculate percentage of input value base on min and max value
     * @private
     * @method _computeValue
     * @return {Number} - The number will be used to calculate needle position
     */
    _computeValue() {
        if (this.value) {
            return ((this.value - this.min) / (this.max - this.min));
        }
    }
    _computeActualBarWidth() {
        return parseInt((this.barWidth || (50 * this.width / 300)));
    }
    /**
     * Select elements and convert them to D3 element object
     * @private
     * @method _d3Select
     * @param {String} selector - a valid CSS selector
     * @return {Object} - D3 element object
     */
    _d3Select(selector) {
        return d3.select(this.myElement.shadowRoot.querySelector(selector));
    }
    /**
     * Convert from percentage to degree
     * @private
     * @method _percToDeg
     * @param {Number} perc - percentage
     * @return {Number} - The number which is converted from percentage to degree
     */
    _percToDeg(perc) {
        return perc * 360;
    }
    /**
     * Convert from percentage to radian
     * @private
     * @method _percToRad
     * @param {Number} perc - percentage
     * @return {Number} - The number which is converted from percentage to radian
     */
    _percToRad(perc) {
        return this._degToRad(this._percToDeg(perc));
    }
    /**
     * Convert from degree to radian
     * @private
     * @method _degToRad
     * @param {Number} deg - degree
     * @return {Number} - The number which is converted from degree to radian
     */
    _degToRad(deg) {
        return deg * Math.PI / 180;
    }
    /**
     * @method _drawChart
     * draws _chart. Debounced.
     */
    _drawChart() {
        if (this.svg) {
            // multiple calls to _drawChart need to be debounced
            //this.debounce('_drawChartDebounced', function() {
            this._drawChartDebounced();
            this._moveTo(this._calculatedValue);
            //} 310);
        }
    }
    /**
     * Calculate element attributes and append require elements to main element
     * @private
     * @method _renderChart
     */
    _drawChartDebounced() {
        this.svg.attr("width", this.width + this.marginLeft + this.marginRight);
        this.svg.attr("height", this.height + this.marginTop + this.marginBottom);
        this._chart.attr('transform', "translate(" + ((this.width + this.marginLeft) / 2) + ", " + ((this.height + this.marginTop)) + ")");
        this._calculateGaugeBarsArc();
        this._adjustFontSize();
        this._generateNeedlePath();
    }
    /**
     * Calculate gauge bars radius and size, then append them to main element
     * @private
     * @method _calculateGaugeBarsArc
     */
    _calculateGaugeBarsArc() {
        var radius = this.width / 2, chartInset = 10;
        this._filledBarArc = d3.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - this._actualBarWidth);
        this._emptyBarArc = d3.arc().outerRadius(radius - chartInset).innerRadius(radius - chartInset - this._actualBarWidth);
    }
    /**
     * Generate needle paths
     * @private
     * @method _generateNeedlePath
     */
    _generateNeedlePath() {
        this._chart.select('.needle')
            .attr('d', "M 0.114 -" + (this.width / 60) + " c 0 0 0.298 1.273 " + ((this.width / 2) - 10) + " " + (this.width / 90) + " c 1.071 0.06 1.091 1.233 -0.035 " + (this.width / 80) + " C " + ((this.width / 2) - 10) + " 1.876 1.683 " + (this.width / 60) + " 0.114 " + (this.width / 60) + " Z");
    }
    /**
     * Adjust font-size of number and unit label base on bar width
     * @private
     * @method _adjustFontSize
     */
    _adjustFontSize() {
        this._d3Select('.text-value').style('font-size', this._actualBarWidth + 'px');
        this._d3Select('.text-unit').style('font-size', (this._actualBarWidth * 0.8333) + 'px');
    }
    /**
     * Calculate start and end radian of gauge bars.
     * @private
     * @method _repaintGauge
     */
    _repaintGauge() {
        var nextStart = .75;
        var arcStartRad = this._percToRad(nextStart);
        var arcEndRad = arcStartRad + this._percToRad(this._currentPerc / 2);
        this._filledBarArc.startAngle(arcStartRad).endAngle(arcEndRad);
        nextStart += this._currentPerc / 2;
        arcStartRad = this._percToRad(nextStart);
        arcEndRad = arcStartRad + this._percToRad((1 - this._currentPerc) / 2);
        this._emptyBarArc.startAngle(arcStartRad).endAngle(arcEndRad);
        this._chart.select(".chart-filled").attr('d', this._filledBarArc);
        this._chart.select(".chart-empty").attr('d', this._emptyBarArc);
        this._changeFilledGaugeBarState();
    }
    /**
     * Check which state can be applied to filled gauge bar by current value
     * @private
     * @method _changeFilledGaugeBarColor
     */
    _changeFilledGaugeBarState() {
        this._chart && this._states && this._states.forEach((function (state) {
            var isMatched = false;
            var ranges = this[state];
            if (ranges) {
                for (var i = 0; i < ranges.length; i++) {
                    var range = ranges[i];
                    if (!isMatched) {
                        isMatched = (this.value > range[0] && this.value <= range[1]);
                    }
                }
                this._chart.select(".chart-filled").classed(state, isMatched);
            }
        }).bind(this));
    }
    /**
     * Change position of needle base on _calculatedValue
     * @private
     * @method _moveTo
     */
    _moveTo(targetPerc) {
        if (this._filledBarArc && this._emptyBarArc) {
            targetPerc = targetPerc || 0;
            if (targetPerc > 1)
                return;
            var scale = d3.scaleLinear().range([0, 1]).domain([0, 1]);
            var oldPerc = this._targetPerc;
            this._chart.transition()
                .delay(100)
                .ease(d3.easeCubic)
                .duration(800)
                .select('.needle')
                .tween('progress', (function () {
                return (function (percentOfPercent) {
                    this._currentPerc = oldPerc + percentOfPercent * (targetPerc - oldPerc);
                    this._valueOfProcess = (percentOfPercent == 1 ? this.value : parseInt(this.min + (this._currentPerc * (this.max - this.min)))) || 0;
                    this._repaintGauge();
                    return this._d3Select('.needle').attr('transform', "rotate(" + (-180 + (scale(this._currentPerc) * 180)) + ")");
                }).bind(this);
            }).bind(this))
                .on("end", function () {
                this._targetPerc = targetPerc;
            }.bind(this));
        }
    }
    render() {
        return ([
            h("div", { id: "chart", ref: (el) => this.elDivchart = el },
                h("svg", { xmlns: "http://www.w3.org/2000/svg", id: "chartSVG", ref: (el) => this.elSVGchartSVG = el },
                    h("g", { id: "chartG", ref: (el) => this.elSVGchartG = el },
                        h("path", { class: "arc chart-filled" }),
                        h("path", { class: "arc chart-empty" }),
                        h("path", { class: "needle", transform: "rotate(-180)" })))),
            h("div", { class: "text-wrapper text--center" },
                h("span", { class: "text-value" }, this._valueOfProcess),
                h("span", { class: "text-unit" },
                    " ",
                    this.unit))
        ]);
    }
    static get is() { return "px-gauge"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["./px-gauge.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["px-gauge.css"]
    }; }
    static get properties() { return {
        "min": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "min",
            "reflect": false,
            "defaultValue": "0"
        },
        "max": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "max",
            "reflect": false,
            "defaultValue": "100"
        },
        "error": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "number[][]",
                "resolved": "number[][]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "anomaly": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "number[][]",
                "resolved": "number[][]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "abnormal": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "number[][]",
                "resolved": "number[][]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "normal": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "number[][]",
                "resolved": "number[][]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "[]"
        },
        "_states": {
            "type": "unknown",
            "mutable": false,
            "complexType": {
                "original": "string[]",
                "resolved": "string[]",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "defaultValue": "['error', 'abnormal', 'anomaly', 'normal']"
        },
        "unit": {
            "type": "string",
            "mutable": false,
            "complexType": {
                "original": "string",
                "resolved": "string",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "unit",
            "reflect": false,
            "defaultValue": "''"
        },
        "_valueOfProcess": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_value-of-process",
            "reflect": false,
            "defaultValue": "0"
        },
        "barWidth": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "bar-width",
            "reflect": false,
            "defaultValue": "0"
        },
        "_actualBarWidth": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_actual-bar-width",
            "reflect": false,
            "defaultValue": "0"
        },
        "_calculatedValue": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_calculated-value",
            "reflect": false,
            "defaultValue": "0"
        },
        "marginTop": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "margin-top",
            "reflect": false,
            "defaultValue": "0"
        },
        "marginBottom": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "margin-bottom",
            "reflect": false,
            "defaultValue": "0"
        },
        "marginRight": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "margin-right",
            "reflect": false,
            "defaultValue": "0"
        },
        "marginLeft": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "margin-left",
            "reflect": false,
            "defaultValue": "0"
        },
        "preventResize": {
            "type": "boolean",
            "mutable": false,
            "complexType": {
                "original": "boolean",
                "resolved": "boolean",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "prevent-resize",
            "reflect": false,
            "defaultValue": "false"
        },
        "_filledBarArc": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_filled-bar-arc",
            "reflect": false
        },
        "_emptyBarArc": {
            "type": "any",
            "mutable": false,
            "complexType": {
                "original": "any",
                "resolved": "any",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_empty-bar-arc",
            "reflect": false
        },
        "_targetPerc": {
            "type": "number",
            "mutable": false,
            "complexType": {
                "original": "number",
                "resolved": "number",
                "references": {}
            },
            "required": false,
            "optional": false,
            "docs": {
                "tags": [],
                "text": ""
            },
            "attribute": "_target-perc",
            "reflect": false,
            "defaultValue": "0"
        }
    }; }
    static get methods() { return {
        "getValue": {
            "complexType": {
                "signature": "() => Promise<number>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<number>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "setValue": {
            "complexType": {
                "signature": "(value: number) => Promise<void>",
                "parameters": [{
                        "tags": [],
                        "text": ""
                    }],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
    static get watchers() { return [{
            "propName": "_calculatedValue",
            "methodName": "_valueChanged"
        }, {
            "propName": "barWidth",
            "methodName": "_barWidthChanged"
        }]; }
}
