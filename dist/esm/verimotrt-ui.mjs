import { p as patchBrowser, b as bootstrapLazy } from './index-97793c32.js';
import { g as globalScripts } from './app-globals-6a31cfce.js';

patchBrowser().then(options => {
  globalScripts();
  return bootstrapLazy([["app-root_2",[[1,"app-root",{"splashDestroy":[64],"splashShow":[64]}],[1,"screen-splash",{"destroy":[64]}]]]], options);
});
