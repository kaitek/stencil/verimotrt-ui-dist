import { h } from "@stencil/core";
export class ScreenSplash {
    async destroy() {
        this.myElement.remove();
    }
    render() {
        return (h("div", { class: "container" },
            h("rt-screen-splash", null)));
    }
    static get is() { return "screen-splash"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["screen-splash.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["screen-splash.css"]
    }; }
    static get methods() { return {
        "destroy": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
}
