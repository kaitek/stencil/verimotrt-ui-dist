import { configureStore } from "../../store/index";
export class AppRoot {
    componentWillLoad() {
        this.store.setStore(configureStore({}));
    }
    componentDidLoad() {
        window['root'] = this;
        this.splashShow();
    }
    async splashDestroy() {
        this.screenSplash.destroy();
    }
    async splashShow() {
        this.screenSplash = document.createElement('screen-splash');
        document.body.append(this.screenSplash);
    }
    render() {
        return null;
    }
    static get is() { return "app-root"; }
    static get encapsulation() { return "shadow"; }
    static get originalStyleUrls() { return {
        "$": ["app-root.scss"]
    }; }
    static get styleUrls() { return {
        "$": ["app-root.css"]
    }; }
    static get contextProps() { return [{
            "name": "store",
            "context": "store"
        }]; }
    static get methods() { return {
        "splashDestroy": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        },
        "splashShow": {
            "complexType": {
                "signature": "() => Promise<void>",
                "parameters": [],
                "references": {
                    "Promise": {
                        "location": "global"
                    },
                    "ScreenSplash": {
                        "location": "import",
                        "path": "../screen-splash/screen-splash"
                    }
                },
                "return": "Promise<void>"
            },
            "docs": {
                "text": "",
                "tags": []
            }
        }
    }; }
    static get elementRef() { return "myElement"; }
}
