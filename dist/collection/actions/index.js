export var TypeKeys;
(function (TypeKeys) {
    // Won't match anything
    TypeKeys["NULL"] = "NULL";
    TypeKeys["ERROR"] = "ERROR";
    TypeKeys["APP_SET_NAME"] = "APP_SET_NAME";
})(TypeKeys || (TypeKeys = {}));
;
