import { TypeKeys } from '../actions/index';
export const appSetName = (name) => async (dispatch, _getState) => {
    return dispatch({
        type: TypeKeys.APP_SET_NAME,
        name: name
    });
};
