import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import ReduxThunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from '../reducers/index';
const composeEnhancers = composeWithDevTools({ trace: true, traceLimit: 25 });
// const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const configureStore = (preloadedState) => createStore(rootReducer, preloadedState, composeEnhancers(applyMiddleware(ReduxThunk, logger)));
export { configureStore };
