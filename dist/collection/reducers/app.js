import { TypeKeys } from '../actions/index';
const getInitialState = () => {
    return {
        name: ''
    };
};
const app = (state = getInitialState(), action) => {
    console.log('reducer:', state, action);
    switch (action.type) {
        case TypeKeys.APP_SET_NAME: {
            return Object.assign(Object.assign({}, state), { name: action.name });
        }
    }
    return state;
};
export default app;
